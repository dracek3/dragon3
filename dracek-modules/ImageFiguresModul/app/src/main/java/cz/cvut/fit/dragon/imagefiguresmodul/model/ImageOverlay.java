package cz.cvut.fit.dragon.imagefiguresmodul.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ImageOverlay implements Parcelable {
    boolean horizontal;
    boolean vertical;
    boolean diagonalMain;
    boolean diagonalSecond;

    int degree;
    int lineSize;

    public final static int DEGREE_0 = 0;
    public final static int DEGREE_90 = 90;
    public final static int DEGREE_180 = 180;
    public final static int DEGREE_270 = 270;

    public ImageOverlay(){

    }

    protected ImageOverlay(Parcel in) {
        horizontal = in.readByte() != 0;
        vertical = in.readByte() != 0;
        diagonalMain = in.readByte() != 0;
        diagonalSecond = in.readByte() != 0;
        degree = in.readInt();
        lineSize = in.readInt();
    }

    public void setDiagonalMain(boolean diagonalMain) {
        this.diagonalMain = diagonalMain;
    }

    public void setDiagonalSecond(boolean diagonalSecond) {
        this.diagonalSecond = diagonalSecond;
    }

    public void setHorizontal(boolean horizontal) {
        this.horizontal = horizontal;
    }

    public void setVertical(boolean vertical) {
        this.vertical = vertical;
    }

    public void setLineSize(int lineSize) {
        this.lineSize = lineSize;
    }

    public void setDegree(int degree) {
        this.degree = degree;
    }

    public boolean isDiagonalMain() {
        return diagonalMain;
    }

    public boolean isDiagonalSecond() {
        return diagonalSecond;
    }

    public boolean isHorizontal() {
        return horizontal;
    }

    public boolean isVertical() {
        return vertical;
    }

    public int getLineSize() {
        return lineSize;
    }

    public int getDegree() {
        return degree;
    }

    public static final Creator<ImageOverlay> CREATOR = new Creator<ImageOverlay>() {
        @Override
        public ImageOverlay createFromParcel(Parcel in) {
            return new ImageOverlay(in);
        }

        @Override
        public ImageOverlay[] newArray(int size) {
            return new ImageOverlay[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (horizontal ? 1 : 0));
        dest.writeByte((byte) (vertical ? 1 : 0));
        dest.writeByte((byte) (diagonalMain ? 1 : 0));
        dest.writeByte((byte) (diagonalSecond ? 1 : 0));
        dest.writeInt(degree);
        dest.writeInt(lineSize);
    }
}
