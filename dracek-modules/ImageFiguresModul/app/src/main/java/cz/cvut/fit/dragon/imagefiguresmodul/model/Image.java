package cz.cvut.fit.dragon.imagefiguresmodul.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Image implements Parcelable {

    int uniqueId;
    boolean special;

    public Image(){

    }

    public Image(int uniqueId){
        this.uniqueId = uniqueId;
    }


    protected Image(Parcel in) {
        uniqueId = in.readInt();
        special = in.readByte() != 0;
    }

    public static final Creator<Image> CREATOR = new Creator<Image>() {
        @Override
        public Image createFromParcel(Parcel in) {
            return new Image(in);
        }

        @Override
        public Image[] newArray(int size) {
            return new Image[size];
        }
    };

    public boolean isSpecial() {
        return special;
    }

    public void setSpecial(boolean special) {
        this.special = special;
    }

    public int getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(uniqueId);
        dest.writeByte((byte) (special ? 1 : 0));
    }
}
