package cz.cvut.fit.dragon.imagefiguresmodul.editor;

import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import cz.cvut.fit.dragon.imagefiguresmodul.R;
import cz.cvut.fit.dragon.imagefiguresmodul.model.Image;
import cz.cvut.fit.dragon.imagefiguresmodul.model.ImageHolder;
import cz.cvut.fit.dragon.imagefiguresmodul.model.SingleExercise;
import cz.cvut.fit.dragon.corelibrary.util.IO;
import cz.cvut.fit.dragon.corelibrary.util.InfoDialog;
import cz.cvut.fit.dragon.corelibrary.util.Size;
import cz.cvut.fit.dragon.corelibrary.util.Utilities;

public class ImageManager implements ImageEditDialog.OnResultListener {

    public static final int EDITOR = 11;
    public static final int GAME = 12;
    public final static int DIALOG_CHANGE_ONLY= 13;
    public final static int DIALOG_FULL = 14;
    public final static int AREA_TOP = 15;
    public final static int AREA_BOTTOM = 16;

    Rect mAreaTop,mAreaBottom;

    protected int mType;
    protected Context mContext;
    protected FragmentManager mFragmentManager;
    protected int mImagePadding;

    List<ImageHolder> mImageHolderTopList;
    List<ImageHolder> mImageHolderBottomList;

    View mSingleEditorView;
    SingleExercise mSingleExercise;

    public ImageManager(Context context, FragmentManager fragmentManager, View singleEditorView,
                        SingleExercise singleExercise, int type){
        this.mContext = context;
        this.mFragmentManager = fragmentManager;
        this.mSingleEditorView = singleEditorView;
        this.mSingleExercise = singleExercise;
        this.mType = type;

        mImagePadding = context.getResources().getDimensionPixelSize(R.dimen.view_images_horizontal_space);

        initImages();
    }
    protected void initImages(){
        mImageHolderTopList = new ArrayList<>();
        mImageHolderBottomList = new ArrayList<>();

        if(mSingleExercise.getImageListTop().size() == 0 || mSingleExercise.getImageListBottom().size() == 0){

            addTopImage();
            addTopImage();

            addBottomImage();
            mSingleExercise.getImageListBottom().get(0).setSpecial(true);
            addBottomImage();
            addBottomImage();
        }
        else {
            for(int i=0;i<mSingleExercise.getImageListTop().size();i++){
                ImageHolder image = new ImageHolder(mContext,mSingleExercise.getImageListTop().get(i),true,mSingleExercise.getUniqueId(),AREA_TOP);
                mImageHolderTopList.add(image);
            }
            for(int i=0;i<mSingleExercise.getImageListBottom().size();i++){
                ImageHolder image = new ImageHolder(mContext,mSingleExercise.getImageListBottom().get(i),true,mSingleExercise.getUniqueId(),AREA_BOTTOM);
                mImageHolderBottomList.add(image);
            }

        }
    }
    public void resizeImages(Rect areaTop,Rect areaBottom){

        this.mAreaTop = areaTop;
        this.mAreaBottom = areaBottom;

        resize(areaTop,mImageHolderTopList);
        resize(areaBottom,mImageHolderBottomList);
    }
    public void resize(Rect area,List<ImageHolder> images){

        int maxWidthImage = (area.width() - (mImagePadding * (images.size() -1))) / images.size();
        int maxHeightImage = area.height();

        int imageSize;
        if(maxWidthImage > maxHeightImage)imageSize = maxHeightImage;
        else imageSize = maxWidthImage;

        int imagesWidth = (images.size() * imageSize) + ((images.size() -1) * mImagePadding);
        int imagesHeight = imageSize;

        int offsetX = (area.width()  - imagesWidth) / 2;
        int offsetY = (area.height() - imagesHeight) / 2;

        for(int i=0;i<images.size();i++){
            int left = offsetX + (i*imageSize) + (i*mImagePadding);

            Rect r = new Rect(area.left + left, area.top + offsetY,area.left + left+imageSize,area.top + offsetY+imageSize);
            images.get(i).setRect(r);
        }
    }
    public void draw(Canvas canvas){

        if(mImageHolderBottomList.get(0).isRectSet() && mType == EDITOR)unionTopImages();

        for(int i=0;i<mImageHolderTopList.size();i++) {
            mImageHolderTopList.get(i).draw(canvas,mSingleExercise.getImageOverlay());
        }
        for(int i=0;i<mImageHolderBottomList.size();i++) {
            mImageHolderBottomList.get(i).draw(canvas, mSingleExercise.getImageOverlay());
        }
    }
    public int getMaxId(){
        int maxUniqueId = 0;
        for(int i=0;i<mImageHolderTopList.size();i++){
            if(mImageHolderTopList.get(i).getImage().getUniqueId() > maxUniqueId)
                maxUniqueId = mImageHolderTopList.get(i).getImage().getUniqueId();
        }
        for(int i=0;i<mImageHolderBottomList.size();i++){
            if(mImageHolderBottomList.get(i).getImage().getUniqueId() > maxUniqueId)
                maxUniqueId = mImageHolderBottomList.get(i).getImage().getUniqueId();
        }
        maxUniqueId++;

        return maxUniqueId;
    }
    public void addTopImage(){
        int maxUniqueId = getMaxId();

        Image image = new Image(maxUniqueId);
        mSingleExercise.getImageListTop().add(image);
        ImageHolder imageHolder = new ImageHolder(mContext,image,false,mSingleExercise.getUniqueId(),AREA_TOP);
        mImageHolderTopList.add(imageHolder);
    }
    public void addBottomImage(){
        int maxUniqueId = getMaxId();

        Image image = new Image(maxUniqueId);
        mSingleExercise.getImageListBottom().add(image);
        ImageHolder imageHolder = new ImageHolder(mContext,image,false,mSingleExercise.getUniqueId(),AREA_BOTTOM);
        mImageHolderBottomList.add(imageHolder);
    }
    public void touchTopDown(int x,int y){

        for (int i=0;i<mImageHolderTopList.size();i++){
            if(mImageHolderTopList.get(i).getRect().contains(x,y)){
                showImageEditDialog(i,AREA_TOP);
            }
        }
    }
    public void touchBottomDown(int x,int y){

        for (int i=0;i<mImageHolderBottomList.size();i++){
            if(mImageHolderBottomList.get(i).getRect().contains(x, y)){
                if(i != 0)showImageEditDialog(i,AREA_BOTTOM);

            }
        }
    }
    public void unionTopImages(){

        int width = mImageHolderBottomList.get(0).getRect().width();
        int height = mImageHolderBottomList.get(0).getRect().height();

        Bitmap bitmap = Bitmap.createBitmap(width,height, Bitmap.Config.ARGB_8888);
        bitmap.setHasAlpha(true);
        for (int x = 0; x < bitmap.getWidth(); x++) {
            for (int y = 0; y < bitmap.getHeight(); y++) {
                bitmap.setPixel(x,y, Color.TRANSPARENT);
            }
        }

        Canvas canvas = new Canvas(bitmap);

        Paint p = new Paint();
        p.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));

        int degree = 0;
        boolean blob = false;
        for (int i=0;i<mImageHolderTopList.size();i++){

            if(mImageHolderTopList.get(i).isSet()) {
                blob = true;
                Matrix m = new Matrix();
                m.postRotate(degree);

                File f = IO.getImageFile(mImageHolderTopList.get(i).singleExerciseUniqueId, mImageHolderTopList.get(i).getImage().getUniqueId());
                Bitmap tmpx = Utilities.loadBitmapFromFile(f);
                Bitmap tmpxx =  Bitmap.createScaledBitmap(tmpx, width, height, true);
                Bitmap r =  Bitmap.createBitmap(tmpxx, 0, 0,width,height, m, true);
                Bitmap tmp = r.copy(r.getConfig(),true);

                tmp.setHasAlpha(true);
                for (int x = 0; x < tmp.getWidth(); x++) {
                    for (int y = 0; y < tmp.getHeight(); y++) {

                        int packedValue = tmp.getPixel(x,y);

                        int A = packedValue >> 24;
                        int R = (packedValue & 0x00ffffff) >> 16;
                        int G = (packedValue & 0x0000ffff) >> 8;
                        int B = (packedValue & 0x000000ff);

                        if (R >= 240 && G >= 240 && B >= 240 && A != 0){

                            tmp.setPixel(x, y, Color.argb(0,255,255,255));
                        }
                    }
                }
                canvas.drawBitmap(tmp, 0, 0, p);
                degree = (degree + mSingleExercise.getImageOverlay().getDegree() % 360);
            }
        }
        if(blob) {
            try {
                File file = IO.getImageFile(mSingleExercise.getUniqueId(), mImageHolderBottomList.get(0).getImage().getUniqueId());
                Utilities.saveBitmapToFile(file,bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
            mImageHolderBottomList.get(0).loadImage();
        }
    }

    public void showImageEditDialog(int imageId,int area){
        Bundle dataBundle = new Bundle();
        ImageEditDialog imageEditDialog = new ImageEditDialog();

        if(area == AREA_TOP){
            if(mImageHolderTopList.size() == 1)dataBundle.putInt(ImageEditDialog.DIALOG_TYPE,DIALOG_CHANGE_ONLY);
              else dataBundle.putInt(ImageEditDialog.DIALOG_TYPE,DIALOG_FULL);
        }
        else if(area == AREA_BOTTOM){
            if(mImageHolderBottomList.size() == 2)dataBundle.putInt(ImageEditDialog.DIALOG_TYPE,DIALOG_CHANGE_ONLY);
              else dataBundle.putInt(ImageEditDialog.DIALOG_TYPE,DIALOG_FULL);
        }
        dataBundle.putInt(ImageEditDialog.IMAGE_ID, imageId);
        dataBundle.putInt(ImageEditDialog.AREA, area);

        imageEditDialog.setArguments(dataBundle);
        imageEditDialog.setOnResultListener(this);
        imageEditDialog.show(mFragmentManager,"imageEditDialog");
    }

    public boolean imagesValid(){

        int none = 0;
        for (int i=0;i<mImageHolderTopList.size();i++)
            if(!mImageHolderTopList.get(i).isSet())none++;

        for (int i=0;i<mImageHolderBottomList.size();i++)
            if(!mImageHolderBottomList.get(i).isSet())none++;

        if(none > 0){
            showInfoMessage(mContext.getString(R.string.all_images_required));
            return false;
        }
        return true;
    }

    public final static int RESULT_MISTAKE =8;
    public final static int RESULT_SUCCESS = 9;
    public final static int RESULT_NONE = 11;

    public int touchUp(int x,int y, int lastX,int lastY){

        int result = RESULT_NONE;
        for (int i=0;i<mImageHolderBottomList.size();i++) {
            if (mImageHolderBottomList.get(i).getRect().contains(x, y) &&
                    mImageHolderBottomList.get(i).getRect().contains(lastX,lastY)) {

                if(mImageHolderBottomList.get(i).getImage().isSpecial())
                    result = RESULT_SUCCESS;
                else
                    result = RESULT_MISTAKE;
            }
        }
        return result;
    }

    @Override
    public void onDelete(int imageId,int area) {

        List<ImageHolder> imageHolderList;
        if(area == AREA_TOP)imageHolderList = mImageHolderTopList;
        else imageHolderList = mImageHolderBottomList;

        if(imageHolderList.get(imageId).isSet()){
            String imageFileName = mContext.getString(R.string.image_file_name_prefix) +"_" + mSingleExercise.getUniqueId() +"_" +
                    imageHolderList.get(imageId).getImage().getUniqueId();
            File f = new File(mContext.getFilesDir(),imageFileName);
            if (f.exists()) f.delete();
        }
        imageHolderList.remove(imageId);

        if(area == AREA_TOP)mSingleExercise.getImageListTop().remove(imageId);
        else mSingleExercise.getImageListBottom().remove(imageId);

        resizeImages(mAreaTop,mAreaBottom);
        mSingleEditorView.invalidate();
    }

    private void loadImage(String imageFilePath,int area,int imagePosition) {

        List<ImageHolder> imageHolderList;
        if (area == AREA_TOP) imageHolderList = mImageHolderTopList;
        else imageHolderList = mImageHolderBottomList;

        File savedImageFile = IO.getImageFile(mSingleExercise.getUniqueId(), imageHolderList.get(imagePosition).getImage().getUniqueId());

        File loadedImageFile = new File(imageFilePath);
        Size imageSize = Utilities.getImageSize(loadedImageFile);

        if (imageSize.getWidth() == 0 || imageSize.getHeight() == 0) {
            showInfoMessage(mContext.getString(R.string.not_zero_image_size));
        } else if (imageSize.getWidth() != imageSize.getHeight()) {
            showInfoMessage(mContext.getString(R.string.square_image_required));
        } else {
            Size scaledSize = Utilities.scaleSize(imageSize, new Size(IO.MAX_IMAGE_SIZE, IO.MAX_IMAGE_SIZE));

            Bitmap scaledBitmap = Utilities.loadBitmapFromFile(loadedImageFile, scaledSize.getWidth(), scaledSize.getHeight());

            try {
                Utilities.saveBitmapToFile(savedImageFile, scaledBitmap);
                imageHolderList.get(imagePosition).loadImage();

            } catch (IOException e) {
                showInfoMessage(mContext.getString(R.string.load_image_failed));
            }
        }
    }

    @Override
    public void onLoaded(int imagePosition,int area,String imageFilePath) {
        if(imageFilePath != null) {
            loadImage(imageFilePath,area, imagePosition);
        }
        else showInfoMessage(mContext.getString(R.string.load_image_failed));
    }

    private void showInfoMessage(String message){
        InfoDialog infoDialog = new InfoDialog();
        Bundle data = new Bundle();
        data.putString(InfoDialog.MESSAGE,message);
        infoDialog.setArguments(data);
        infoDialog.show(mFragmentManager, "infoDialog");
    }
}
