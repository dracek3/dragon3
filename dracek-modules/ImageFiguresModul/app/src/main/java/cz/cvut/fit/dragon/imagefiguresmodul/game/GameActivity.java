package cz.cvut.fit.dragon.imagefiguresmodul.game;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;

import cz.cvut.fit.dragon.imagefiguresmodul.R;
import cz.cvut.fit.dragon.imagefiguresmodul.model.BaseActivity;
import cz.cvut.fit.dragon.corelibrary.model.ResultWriter;
import cz.cvut.fit.dragon.corelibrary.util.ConfirmDialog;

public class GameActivity extends BaseActivity implements View.OnClickListener, GameView.NoticeEventListener{

    GameView mGameView;
    LinearLayout mGameViewLinearLayout;
    ImageButton mCloseGameButton;
    TextView mExerciseNamesTextView;

    ExerciseManager mExerciseManager ;

    long mStartTime;
    long mPauseStartTime;
    long mPauseTime;

    protected void init() {
        setContentView(R.layout.activity_game);
        mCloseGameButton = (ImageButton)findViewById(R.id.close_game_button);
        mExerciseNamesTextView = (TextView)findViewById(R.id.exercise_names_text_view);

        mExerciseManager = new ExerciseManager(mExercise);
        mGameView = new GameView(GameActivity.this,getFragmentManager(),mExerciseManager,mExerciseNamesTextView);
        mGameView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mGameViewLinearLayout = (LinearLayout)findViewById(R.id.game_view_linear_layout);
        mGameViewLinearLayout.addView(mGameView);

        mCloseGameButton.setOnClickListener(this);
        mGameView.setNoticeEventListener(this);

        initTime();
    }
    protected void initTime(){
        mStartTime = SystemClock.elapsedRealtime();
        mPauseTime = 0;
    }
    @Override
    protected void noExerciseProvided() {
        showMessage(getString(R.string.load_exercise_failed), true);
    }

    @Override
    public void onClick(View v) {
        if(v == mCloseGameButton){
            closeGame();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPauseTime += SystemClock.elapsedRealtime() - mPauseStartTime;
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPauseStartTime = SystemClock.elapsedRealtime();
    }

    private void closeGame(){
        ConfirmDialog confirmDialog = new ConfirmDialog();
        Bundle data = new Bundle();
        data.putString(MESSAGE, getString(R.string.game_close_confirm));
        confirmDialog.setArguments(data);
        confirmDialog.setNoticeDialogListener(new ConfirmDialog.NoticeDialogListener() {
            @Override
            public void onDialogPositive(DialogFragment dialog) {
                finish();
            }

            @Override
            public void onDialogNegative(DialogFragment dialog) {

            }
        });
        confirmDialog.show(getFragmentManager(), "confirmDialogGame");
    }

    @Override
    public void onExerciseDone() {
        int timeElapsedSeconds = (int)((SystemClock.elapsedRealtime() - mStartTime) / 1000);

        String xmlResults = "";
        try {
            ResultWriter resultWriter = new ResultWriter(timeElapsedSeconds,mExerciseManager.getResultsPercentage(), getString(R.string.result_time_unit),getString(R.string.result_score_unit));
            xmlResults = resultWriter.getXmlResultString();

        } catch (IOException e) {
            e.printStackTrace();
        }

        Intent resultIntent = new Intent();
        resultIntent.putExtra(RESULTS, xmlResults);
        setResult(RESULT_OK, resultIntent);
        finish();
    }
}
