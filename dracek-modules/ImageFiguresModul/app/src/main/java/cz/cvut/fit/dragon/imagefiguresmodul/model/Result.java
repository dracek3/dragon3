package cz.cvut.fit.dragon.imagefiguresmodul.model;


public class Result {
    int wrongMoves;
    int uniqueWrongMoves;

    public Result() {
        wrongMoves = 0;
    }

    public void addWrongMove() {
        this.wrongMoves++;
    }

    public int getPercentage(){
        float percentage = 1-((float)wrongMoves/uniqueWrongMoves);
        if (percentage < 0) return 0;
        else return (int)(percentage * 100);
    }
    public void setUniqueWrongMoves(int uniqueWrongMoves) {
        this.uniqueWrongMoves = uniqueWrongMoves;
    }
}
