package cz.cvut.fit.dragon.imagefiguresmodul.model;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cz.cvut.fit.dragon.imagefiguresmodul.R;
import cz.cvut.fit.dragon.corelibrary.util.IO;

public class SingleExercise implements Parcelable {

    int uniqueId;
    String name;

    ImageOverlay imageOverlay;
    List<Image> imageListTop;
    List<Image> imageListBottom;

    public SingleExercise() {
        imageListTop = new ArrayList<>();
        imageListBottom = new ArrayList<>();
    }


    protected SingleExercise(Parcel in) {
        uniqueId = in.readInt();
        name = in.readString();
        imageOverlay = in.readParcelable(ImageOverlay.class.getClassLoader());
        imageListTop = in.createTypedArrayList(Image.CREATOR);
        imageListBottom = in.createTypedArrayList(Image.CREATOR);
    }

    public static final Creator<SingleExercise> CREATOR = new Creator<SingleExercise>() {
        @Override
        public SingleExercise createFromParcel(Parcel in) {
            return new SingleExercise(in);
        }

        @Override
        public SingleExercise[] newArray(int size) {
            return new SingleExercise[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SingleExercise that = (SingleExercise) o;

        return uniqueId == that.uniqueId;
    }

    @Override
    public int hashCode() {
        return uniqueId;
    }

    public int getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Image> getImageListBottom() {
        return imageListBottom;
    }

    public void setImageListBottom(List<Image> imageListBottom) {
        this.imageListBottom = imageListBottom;
    }

    public List<Image> getImageListTop() {
        return imageListTop;
    }

    public void setImageListTop(List<Image> imageListTop) {
        this.imageListTop = imageListTop;
    }

    public void destroyImages() {
        File thumbnail = IO.getThumbnailFile(uniqueId);
        thumbnail.delete();

        for(Image image : imageListTop) {
            File imageFile = IO.getImageFile(uniqueId,image.getUniqueId());
            imageFile.delete();
        }
        for(Image image : imageListBottom) {
            File imageFile = IO.getImageFile(uniqueId,image.getUniqueId());
            imageFile.delete();
        }
    }
    public ArrayList<String> getAllImageFilePaths(){
        ArrayList<String> imageFilePaths = new ArrayList<>();

        for(Image image : imageListTop) {
            imageFilePaths.add(IO.getImageFile(uniqueId, image.getUniqueId()).getPath());
        }
        for(Image image : imageListBottom) {
            imageFilePaths.add(IO.getImageFile(uniqueId, image.getUniqueId()).getPath());
        }
        return imageFilePaths;
    }

    public ImageOverlay getImageOverlay() {
        return imageOverlay;
    }

    public void setImageOverlay(ImageOverlay imageOverlay) {
        this.imageOverlay = imageOverlay;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(uniqueId);
        dest.writeString(name);
        dest.writeParcelable(imageOverlay, flags);
        dest.writeTypedList(imageListTop);
        dest.writeTypedList(imageListBottom);
    }
}
