package cz.cvut.fit.dragon.imagefiguresmodul.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.view.View;

import cz.cvut.fit.dragon.imagefiguresmodul.R;

public abstract class BaseView extends View {

    protected Context mContext;

    protected Rect mAreaTop,mAreaBottom;

    protected int mPaddingLeft,mPaddingRight,mPaddingTop,mPaddingBottom;
    protected int mImagesVerticalSpace;
    protected int mBinSizeDifference;

    public BaseView(Context context){
        super(context);
        this.mContext = context;
        loadDimens();
    }

    protected void loadDimens(){
        mPaddingLeft = getResources().getDimensionPixelSize(R.dimen.view_left_padding);
        mPaddingRight = getResources().getDimensionPixelSize(R.dimen.view_right_padding);
        mPaddingBottom = getResources().getDimensionPixelSize(R.dimen.view_bottom_padding);
        mPaddingTop = getResources().getDimensionPixelSize(R.dimen.view_top_padding);
        mBinSizeDifference = getResources().getDimensionPixelSize(R.dimen.view_bin_size_difference);
    }
}
