package cz.cvut.fit.dragon.imagefiguresmodul.model;

import android.graphics.Bitmap;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.cvut.fit.dragon.imagefiguresmodul.editor.ImageManager;
import cz.cvut.fit.dragon.corelibrary.util.IO;
import cz.cvut.fit.dragon.corelibrary.util.LoadExerciseException;
import cz.cvut.fit.dragon.corelibrary.util.Utilities;
import cz.cvut.fit.dragon.corelibrary.util.XmlTag;


public class ExerciseReader {

    Exercise mExercise;
    String mXml;
    ArrayList<String> mImageFilePaths;

    public ExerciseReader(String xml, ArrayList<String> imageFilePaths){
        this.mXml = xml;
        this.mImageFilePaths = imageFilePaths;

        mExercise = new Exercise();
    }

    public Exercise load() throws XmlPullParserException, IOException, LoadExerciseException {

        XmlPullParserFactory xmlFactoryObject;
        xmlFactoryObject = XmlPullParserFactory.newInstance();
        XmlPullParser xmlPullParser = xmlFactoryObject.newPullParser();
        xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        xmlPullParser.setInput(new ByteArrayInputStream(mXml.getBytes("UTF-8")), "UTF-8");
        parseXML(xmlPullParser);
        copyImages();

        return mExercise;
    }

    private void copyImages() throws IOException, LoadExerciseException{
        for(String imageFilePath : mImageFilePaths){
            File imageFile = new File(imageFilePath);
            if(!imageFile.exists())throw new LoadExerciseException();
            Utilities.copyImageToDir(imageFile.getPath(), IO.IMAGES_DIR);
        }
    }

    private void parseXML(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException, LoadExerciseException {

        int eventType = xmlPullParser.getEventType();
        String text = "";
        int area = 0;

        SingleExercise singleExercise = null;
        ImageOverlay imageOverlay = null;
        List<Image> imageTopList = null;
        List<Image> imageBottomList = null;

        Image image = null;

        while (eventType != XmlPullParser.END_DOCUMENT){
            String tagName = xmlPullParser.getName();

            if(eventType == XmlPullParser.START_TAG){
                if(tagName.equals(XmlTag.SINGLE_EXERCISE))
                    singleExercise = new SingleExercise();
                else if(tagName.equals(XmlTag.IMAGE_OVERLAY))
                    imageOverlay = new ImageOverlay();
                else if(tagName.equals(XmlTag.IMAGES_TOP)) {
                    imageTopList = new ArrayList<>();
                    area = ImageManager.AREA_TOP;
                }
                else if(tagName.equals(XmlTag.IMAGES_BOTTOM)){
                    imageBottomList = new ArrayList<>();
                    area = ImageManager.AREA_BOTTOM;
                }
                else if(tagName.equals(XmlTag.IMAGE))
                    image = new Image();
            }
            else if(eventType == XmlPullParser.TEXT){
                text = xmlPullParser.getText();
            }
            else if(eventType == XmlPullParser.END_TAG) {
                if (tagName.equals(XmlTag.ID))
                    singleExercise.setUniqueId(Integer.valueOf(text));
                else if (tagName.equals(XmlTag.NAME))
                    singleExercise.setName(text);
                else if (tagName.equals(XmlTag.OVERLAY_HORIZONTAL))
                    imageOverlay.setHorizontal(Boolean.valueOf(text));
                else if (tagName.equals(XmlTag.OVERLAY_VERTICAL))
                    imageOverlay.setVertical(Boolean.valueOf(text));
                else if (tagName.equals(XmlTag.OVERLAY_MAIN_DIAGONAL))
                    imageOverlay.setDiagonalMain(Boolean.valueOf(text));
                else if (tagName.equals(XmlTag.OVERLAY_SECOND_DIAGONAL))
                    imageOverlay.setDiagonalSecond(Boolean.valueOf(text));
                else if (tagName.equals(XmlTag.OVERLAY_DEGREE))
                    imageOverlay.setDegree(Integer.valueOf(text));
                else if (tagName.equals(XmlTag.OVERLAY_LINE_SIZE))
                    imageOverlay.setLineSize(Integer.valueOf(text));

                else if (tagName.equals(XmlTag.IMAGE_ID)) {
                    image.setUniqueId(Integer.valueOf(text));
                }
                else if(tagName.equals(XmlTag.SPECIAL)){
                    image.setSpecial(Boolean.valueOf(text));
                }
                else if (tagName.equals(XmlTag.IMAGE)) {
                    if (area == ImageManager.AREA_TOP)
                       imageTopList.add(image);
                    else if (area == ImageManager.AREA_BOTTOM)
                        imageBottomList.add(image);
                    else throw new LoadExerciseException();
                }
                else if(tagName.equals(XmlTag.IMAGES_TOP))
                    singleExercise.setImageListTop(imageTopList);

                else if(tagName.equals(XmlTag.IMAGES_BOTTOM))
                    singleExercise.setImageListBottom(imageBottomList);

                else if(tagName.equals(XmlTag.IMAGE_OVERLAY))
                    singleExercise.setImageOverlay(imageOverlay);

                else if(tagName.equals(XmlTag.SINGLE_EXERCISE)) {
                    Bitmap thumb = Utilities.loadBitmapFromFile(IO.getThumbnailFile(singleExercise.getUniqueId()));
                    mExercise.addSingleExercise(singleExercise, thumb);
                }
                text = "";
            }
            eventType = xmlPullParser.next();
        }
    }
}
