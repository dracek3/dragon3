package cz.cvut.fit.dragon.imagefiguresmodul.model;

import android.util.Xml;

import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import cz.cvut.fit.dragon.corelibrary.util.IO;
import cz.cvut.fit.dragon.corelibrary.util.XmlTag;


public class ExerciseWriter {

    Exercise mExercise;

    public ExerciseWriter(Exercise exercise){
        this.mExercise = exercise;
    }

    public String getExerciseOutputXmlString() throws IOException{
        XmlSerializer xmlSerializer = Xml.newSerializer();
        ByteArrayOutputStream output =  new ByteArrayOutputStream();
        xmlSerializer.setOutput(output, IO.XML_ENCODING);
        writeXml(xmlSerializer, mExercise);
        return output.toString(IO.XML_ENCODING);
    }
    private void writeXml(XmlSerializer xmlSerializer,Exercise exercise) throws IOException {
        xmlSerializer.startDocument(IO.XML_ENCODING, false);

        xmlSerializer.startTag("", XmlTag.EXERCISE);
        for(int i=0;i<exercise.getSingleExerciseList().size();i++){
            SingleExercise singleExercise = exercise.getSingleExerciseList().get(i);

            xmlSerializer.startTag("",XmlTag.SINGLE_EXERCISE);

            xmlSerializer.startTag("",XmlTag.ID);
            xmlSerializer.text(String.valueOf(singleExercise.getUniqueId()));
            xmlSerializer.endTag("", XmlTag.ID);

            xmlSerializer.startTag("", XmlTag.NAME);
            xmlSerializer.text(singleExercise.getName());
            xmlSerializer.endTag("", XmlTag.NAME);

            saveToXmlImageOverlay(xmlSerializer, singleExercise.getImageOverlay());

            xmlSerializer.startTag("", XmlTag.IMAGES_TOP);
            for(int j=0;j<singleExercise.getImageListTop().size();j++){
                xmlSerializer.startTag("",XmlTag.IMAGE);

                xmlSerializer.startTag("",XmlTag.IMAGE_ID);
                xmlSerializer.text(String.valueOf(singleExercise.getImageListTop().get(j).getUniqueId()));
                xmlSerializer.endTag("", XmlTag.IMAGE_ID);

                xmlSerializer.startTag("", XmlTag.SPECIAL);
                xmlSerializer.text(String.valueOf(singleExercise.getImageListTop().get(j).isSpecial()));
                xmlSerializer.endTag("", XmlTag.SPECIAL);

                xmlSerializer.endTag("",XmlTag.IMAGE);
            }
            xmlSerializer.endTag("", XmlTag.IMAGES_TOP);

            xmlSerializer.startTag("",XmlTag.IMAGES_BOTTOM);
            for(int j=0;j<singleExercise.getImageListBottom().size();j++){
                xmlSerializer.startTag("",XmlTag.IMAGE);

                xmlSerializer.startTag("", XmlTag.IMAGE_ID);
                xmlSerializer.text(String.valueOf(singleExercise.getImageListBottom().get(j).getUniqueId()));
                xmlSerializer.endTag("", XmlTag.IMAGE_ID);

                xmlSerializer.startTag("", XmlTag.SPECIAL);
                xmlSerializer.text(String.valueOf(singleExercise.getImageListBottom().get(j).isSpecial()));
                xmlSerializer.endTag("",XmlTag.SPECIAL);

                xmlSerializer.endTag("",XmlTag.IMAGE);
            }
            xmlSerializer.endTag("", XmlTag.IMAGES_BOTTOM);

            xmlSerializer.endTag("", XmlTag.SINGLE_EXERCISE);
        }
        xmlSerializer.endTag("", XmlTag.EXERCISE);
        xmlSerializer.flush();
    }
    void saveToXmlImageOverlay(XmlSerializer xmlSerializer,ImageOverlay imageOverlay) throws IOException{

        xmlSerializer.startTag("",XmlTag.IMAGE_OVERLAY);

        xmlSerializer.startTag("", XmlTag.OVERLAY_HORIZONTAL);
        if(imageOverlay.isHorizontal())xmlSerializer.text("true");
        else xmlSerializer.text("false");
        xmlSerializer.endTag("", XmlTag.OVERLAY_HORIZONTAL);

        xmlSerializer.startTag("", XmlTag.OVERLAY_VERTICAL);
        if(imageOverlay.isVertical())xmlSerializer.text("true");
        else xmlSerializer.text("false");
        xmlSerializer.endTag("", XmlTag.OVERLAY_VERTICAL);

        xmlSerializer.startTag("",XmlTag.OVERLAY_MAIN_DIAGONAL);
        if(imageOverlay.isDiagonalMain())xmlSerializer.text("true");
        else xmlSerializer.text("false");
        xmlSerializer.endTag("", XmlTag.OVERLAY_MAIN_DIAGONAL);

        xmlSerializer.startTag("",XmlTag.OVERLAY_SECOND_DIAGONAL);
        if(imageOverlay.isDiagonalSecond())xmlSerializer.text("true");
        else xmlSerializer.text("false");
        xmlSerializer.endTag("", XmlTag.OVERLAY_SECOND_DIAGONAL);

        xmlSerializer.startTag("", XmlTag.OVERLAY_DEGREE);
        xmlSerializer.text(String.valueOf(imageOverlay.getDegree()));
        xmlSerializer.endTag("", XmlTag.OVERLAY_DEGREE);

        xmlSerializer.startTag("", XmlTag.OVERLAY_LINE_SIZE);
        xmlSerializer.text(String.valueOf(imageOverlay.getLineSize()));
        xmlSerializer.endTag("", XmlTag.OVERLAY_LINE_SIZE);

        xmlSerializer.endTag("",XmlTag.IMAGE_OVERLAY);
    }
}
