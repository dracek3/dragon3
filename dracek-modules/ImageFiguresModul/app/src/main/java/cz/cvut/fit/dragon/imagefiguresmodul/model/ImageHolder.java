package cz.cvut.fit.dragon.imagefiguresmodul.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;

import cz.cvut.fit.dragon.imagefiguresmodul.R;
import cz.cvut.fit.dragon.imagefiguresmodul.editor.ImageManager;
import cz.cvut.fit.dragon.corelibrary.util.IO;
import cz.cvut.fit.dragon.corelibrary.util.Utilities;

public class ImageHolder {
    Image image;
    Rect rect;
    Bitmap bitmap;
    boolean set;
    boolean rectSet;
    boolean removed;
    Context context;
    int area;
    public int singleExerciseUniqueId;
    Paint paint;
    Paint linesPaint;

    public ImageHolder(Context context,Image image,boolean imageExist,int singleExerciseUniqueId,int area){
        this.image = image;
        this.context = context;
        set = imageExist;
        removed = false;
        rectSet = false;
        this.singleExerciseUniqueId = singleExerciseUniqueId;
        this.area = area;
        paint = new Paint();
        linesPaint = new Paint();
        paint.setColor(ContextCompat.getColor(context, R.color.colorBackgroundDark));
        linesPaint.setColor(ContextCompat.getColor(context, R.color.linesOverlayColor));
        linesPaint.setStyle(Paint.Style.STROKE);
    }

    public void draw(Canvas canvas,ImageOverlay imageOverlay){

        if(set && rect != null && !removed) {
            canvas.drawBitmap(bitmap, rect.left, rect.top, null);
        }
        else if(rect != null && !removed)
            canvas.drawRect(rect, paint);

        if(area == ImageManager.AREA_BOTTOM) {
            drawLines(canvas, imageOverlay);
        }
    }
    public void drawLines(Canvas canvas,ImageOverlay imageOverlay){
        linesPaint.setStrokeWidth(imageOverlay.getLineSize());
        for (int point = 0; point < rect.width(); point += (4 + imageOverlay.getLineSize())) {

            if (imageOverlay.isHorizontal())
                canvas.drawLine(rect.left, rect.top + point, rect.right, rect.top + point, linesPaint);
            if (imageOverlay.isVertical())
                canvas.drawLine(rect.left + point, rect.top, rect.left + point, rect.bottom, linesPaint);
            if (imageOverlay.isDiagonalMain()) {
                canvas.drawLine(rect.left + point, rect.top, rect.right, rect.bottom - point, linesPaint);
                canvas.drawLine(rect.left, rect.top + point, rect.right - point, rect.bottom, linesPaint);
            }
            if (imageOverlay.isDiagonalSecond()) {
                canvas.drawLine(rect.right - point, rect.top, rect.left, rect.bottom - point, linesPaint);
                canvas.drawLine(rect.right, rect.top + point, rect.left + point, rect.bottom, linesPaint);
            }
        }
    }

    public void setRect(Rect rect){
        this.rect = new Rect(rect);
        rectSet = true;

        if(set){
            loadImage();
        }
    }

    public Rect getRect() {
        return rect;
    }

    public void loadImage(){
        Bitmap tmp = Utilities.loadBitmapFromFile(IO.getImageFile(singleExerciseUniqueId, image.getUniqueId()), rect.width(), rect.height());
        bitmap = Bitmap.createScaledBitmap(tmp, rect.width(), rect.height(), true);
        set = true;
    }

    public boolean isSet(){
        return set;
    }

    public boolean isRectSet() {
        return rectSet;
    }

    public Image getImage() {
        return image;
    }
}
