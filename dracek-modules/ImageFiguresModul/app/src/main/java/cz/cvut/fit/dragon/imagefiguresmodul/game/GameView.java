package cz.cvut.fit.dragon.imagefiguresmodul.game;

import android.app.FragmentManager;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.TextView;

import cz.cvut.fit.dragon.imagefiguresmodul.R;
import cz.cvut.fit.dragon.imagefiguresmodul.editor.ImageManager;
import cz.cvut.fit.dragon.imagefiguresmodul.model.BaseView;
import cz.cvut.fit.dragon.imagefiguresmodul.model.Result;
import cz.cvut.fit.dragon.imagefiguresmodul.model.SingleExercise;


public class GameView extends BaseView {

    protected Context mContext;
    protected FragmentManager mFragmentManager;

    ImageManager mImageManager;
    private ExerciseManager mExerciseManager;
    private SingleExercise mCurrentSingleExercise;
    TextView mCurrentSingleExerciseNameTextView;

    private int mLastX;
    private int mLastY;

    public GameView(Context context, FragmentManager fragmentManager, ExerciseManager exerciseManager, TextView currentSingleExerciseNameTextView) {
        super(context);
        this.mContext = context;
        this.mFragmentManager = fragmentManager;
        this.mExerciseManager = exerciseManager;
        this.mCurrentSingleExerciseNameTextView = currentSingleExerciseNameTextView;

        loadDimens();
        mCurrentSingleExercise = mExerciseManager.getCurrentSingleExercise();
        setSingleExercise();
    }

    private void setSingleExercise(){

        mImageManager = new ImageManager(mContext,mFragmentManager,this, mCurrentSingleExercise,ImageManager.GAME);
        if(mAreaTop != null && mAreaBottom != null) mImageManager.resizeImages(mAreaTop,mAreaBottom);

        mCurrentSingleExerciseNameTextView.setText(mCurrentSingleExercise.getName());
    }

    private void resizeView(int width,int height) {

        mImagesVerticalSpace = getResources().getDimensionPixelSize(R.dimen.view_images_vertical_space);

        int areaHeight = (height - mPaddingTop - mPaddingBottom - mImagesVerticalSpace) /2;

        mAreaTop = new Rect(mPaddingLeft,mPaddingTop,width- mPaddingRight ,mPaddingTop + areaHeight - mBinSizeDifference);
        mAreaBottom = new Rect(mPaddingLeft,mAreaTop.bottom+mImagesVerticalSpace,width-mPaddingRight,height-mPaddingBottom);

        mImageManager.resizeImages(mAreaTop, mAreaBottom);
        invalidate();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        resizeView(w, h);
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(mImageManager != null)
            mImageManager.draw(canvas);
    }

    void touchDown(int x,int y){
        if (mAreaBottom.contains(x, y)){
            mLastX = x;
            mLastY = y;
        }
    }

    void touchUp(int x,int y){
        int result = mImageManager.touchUp(x,y,mLastX,mLastY);
        if(result == ImageManager.RESULT_SUCCESS){
            singleExerciseFinished();
        }
        else if(result == ImageManager.RESULT_MISTAKE){
            mExerciseManager.getCurrentResult().addWrongMove();
        }
    }
    private void singleExerciseFinished(){

        if(mExerciseManager.nextSingleExercise()){
            mCurrentSingleExercise = mExerciseManager.getCurrentSingleExercise();
            setSingleExercise();
        }
        else {
            if(noticeEventListener != null)
                noticeEventListener.onExerciseDone();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        final int x = (int) event.getX();
        final int y = (int) event.getY();
        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            touchDown(x,y);
            invalidate();
        }
        else if(event.getAction() == MotionEvent.ACTION_UP){
            touchUp(x,y);
            invalidate();
        }
        return true;
    }

    NoticeEventListener noticeEventListener;

    public interface NoticeEventListener{
        void onExerciseDone();
    }
    public void setNoticeEventListener(NoticeEventListener noticeEventListener){
        this.noticeEventListener = noticeEventListener;

    }
}
