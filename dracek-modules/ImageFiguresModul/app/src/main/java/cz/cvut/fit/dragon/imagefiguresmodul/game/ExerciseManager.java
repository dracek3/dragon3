package cz.cvut.fit.dragon.imagefiguresmodul.game;

import java.util.Collections;

import cz.cvut.fit.dragon.imagefiguresmodul.model.Exercise;
import cz.cvut.fit.dragon.imagefiguresmodul.model.Result;
import cz.cvut.fit.dragon.imagefiguresmodul.model.SingleExercise;

public class ExerciseManager {

    int currentSingleExercise;
    Exercise exercise;
    Result [] results;

    public ExerciseManager(Exercise exercise) {
        this.exercise = exercise;
        currentSingleExercise = 0;
        results = new Result[exercise.getSingleExerciseSize()];

        for(int i=0;i<exercise.getSingleExerciseSize();i++){
            Collections.shuffle(exercise.getSingleExercise(i).getImageListTop());
            Collections.shuffle(exercise.getSingleExercise(i).getImageListBottom());
        }

        results = new Result[exercise.getSingleExerciseSize()];
        for (int i = 0; i < results.length; i++){
            results[i] = new Result();
            results[i].setUniqueWrongMoves(exercise.getSingleExercise(i).getImageListBottom().size()-1);
        }
    }

    public boolean nextSingleExercise(){
        if(currentSingleExercise >= exercise.getSingleExerciseSize()-1)
            return false;
        else {
            currentSingleExercise++;
            return true;
        }
    }
    public SingleExercise getCurrentSingleExercise(){
        return exercise.getSingleExercise(currentSingleExercise);
    }
    public Result getCurrentResult(){
        return results[currentSingleExercise];
    }
    public int getResultsPercentage(){
        int percentageSum = 0;
        for (int i=0;i<results.length;i++){
            percentageSum += results[i].getPercentage();
        }
        return percentageSum / results.length;
    }
}
