package cz.cvut.fit.dragon.imagefiguresmodul.editor;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import cz.cvut.fit.dragon.imagefiguresmodul.R;
import cz.cvut.fit.dragon.imagefiguresmodul.model.ImageOverlay;

public class ImageOverLayDialog extends DialogFragment implements View.OnClickListener,CompoundButton.OnCheckedChangeListener {

    public static String IMAGE_OVERLAY = "IMAGE_OVERLAY";

    CheckBox verticalCheckBox,horizontalCheckBox;
    CheckBox diagonalMainCheckBox,diagonalSecondCheckBox;

    EditText lineSizeInput;
    Button saveButton,lineSizePlusButton,lineSizeMinusButton;
    private Spinner mRotationSpinner;
    ArrayAdapter<CharSequence> transformTypesAdapter;


    ImageOverlay imageOverlay;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getDialog().setTitle(getString(R.string.image_overlay_dialog_title));
        getDialog().setCanceledOnTouchOutside(false);
        setCancelable(false);

        transformTypesAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.rotation_types, android.R.layout.simple_spinner_item);
        transformTypesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        View view = inflater.inflate(R.layout.image_overlay_dialog,container);

        initViews(view);
        initImageOverLay();

        return view;
    }
    private void initViews(View view){
        verticalCheckBox = (CheckBox)view.findViewById(R.id.vertical_check_box);
        horizontalCheckBox = (CheckBox)view.findViewById(R.id.horizontal_check_box);
        diagonalMainCheckBox = (CheckBox)view.findViewById(R.id.diagonal_main_check_box);
        diagonalSecondCheckBox = (CheckBox)view.findViewById(R.id.diagonal_second_check_box);

        lineSizeInput = (EditText)view.findViewById(R.id.line_overlay_size_input);
        lineSizePlusButton = (Button)view.findViewById(R.id.line_overlay_size_plus_button);
        lineSizeMinusButton = (Button)view.findViewById(R.id.line_overlay_size_minus_button);
        saveButton = (Button)view.findViewById(R.id.save_image_overlay_button);

        mRotationSpinner = (Spinner)view.findViewById(R.id.rotation_spinner);
        mRotationSpinner.setAdapter(transformTypesAdapter);


        lineSizePlusButton.setOnClickListener(this);
        lineSizeMinusButton.setOnClickListener(this);
        saveButton.setOnClickListener(this);

        verticalCheckBox.setOnCheckedChangeListener(this);
        horizontalCheckBox.setOnCheckedChangeListener(this);
        diagonalMainCheckBox.setOnCheckedChangeListener(this);
        diagonalSecondCheckBox.setOnCheckedChangeListener(this);

    }
    private void initImageOverLay(){
        imageOverlay = getArguments().getParcelable(IMAGE_OVERLAY);

        if(imageOverlay.isVertical())verticalCheckBox.setChecked(true);
        if(imageOverlay.isHorizontal())horizontalCheckBox.setChecked(true);
        if(imageOverlay.isDiagonalMain())diagonalMainCheckBox.setChecked(true);
        if(imageOverlay.isDiagonalSecond())diagonalSecondCheckBox.setChecked(true);

        mRotationSpinner.setSelection(getTransformTypePosition());


        lineSizeInput.setText(String.valueOf(imageOverlay.getLineSize()));
    }

    @Override
    public void onClick(View v) {
        if(v == lineSizePlusButton){
            if(imageOverlay.getLineSize() < 10) {
                imageOverlay.setLineSize(imageOverlay.getLineSize() + 1);
            }
            lineSizeInput.setText(String.valueOf(imageOverlay.getLineSize()));
        }
        else if(v == lineSizeMinusButton){
            if(imageOverlay.getLineSize() > 1) {
                imageOverlay.setLineSize(imageOverlay.getLineSize() - 1);
            }
            lineSizeInput.setText(String.valueOf(imageOverlay.getLineSize()));
        }
        else if (v == saveButton){
            imageOverlay.setDegree(parseTransformType());
            if(onResultListener != null) onResultListener.onSave(imageOverlay);
            dismiss();
        }
    }

    private int parseTransformType(){
        String selectedItem = mRotationSpinner.getSelectedItem().toString();
        if(selectedItem.equals(getString(R.string.rotation_type_0))) return ImageOverlay.DEGREE_0;
        else if(selectedItem.equals(getString(R.string.rotation_type_90)))return ImageOverlay.DEGREE_90;
        else if(selectedItem.equals(getString(R.string.rotation_type_180))) return ImageOverlay.DEGREE_180;
        else return ImageOverlay.DEGREE_270;
    }
    private int getTransformTypePosition(){
        if(imageOverlay.getDegree() == ImageOverlay.DEGREE_0)
            return transformTypesAdapter.getPosition(getString(R.string.rotation_type_0));
        else if(imageOverlay.getDegree() == ImageOverlay.DEGREE_90)
            return transformTypesAdapter.getPosition(getString(R.string.rotation_type_90));
        else if(imageOverlay.getDegree() == ImageOverlay.DEGREE_180)
            return transformTypesAdapter.getPosition(getString(R.string.rotation_type_180));
        else return transformTypesAdapter.getPosition(getString(R.string.rotation_type_270));
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(buttonView == verticalCheckBox)imageOverlay.setVertical(isChecked);
        else if(buttonView == horizontalCheckBox)imageOverlay.setHorizontal(isChecked);
        else if(buttonView == diagonalMainCheckBox)imageOverlay.setDiagonalMain(isChecked);
        else if(buttonView == diagonalSecondCheckBox)imageOverlay.setDiagonalSecond(isChecked);
    }

    OnResultListener onResultListener;
    public interface OnResultListener {
        void onSave(ImageOverlay imageOverlay);
    }
    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }
}
