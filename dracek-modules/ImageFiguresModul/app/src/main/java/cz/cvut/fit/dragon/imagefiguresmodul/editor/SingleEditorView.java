package cz.cvut.fit.dragon.imagefiguresmodul.editor;

import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;

import cz.cvut.fit.dragon.imagefiguresmodul.R;
import cz.cvut.fit.dragon.imagefiguresmodul.model.BaseView;
import cz.cvut.fit.dragon.imagefiguresmodul.model.SingleExercise;

public class SingleEditorView extends BaseView {

    int mImageNumberChangeSize;
    int mImageNumberChangePaddingHorizontal;

    Rect mAreaPlusButtonTop;
    Rect mAreaPlusButtonBottom;

    ImageManager mImageManager;

    Bitmap mPlusBitmap;

    SingleExercise mSingleExercise;

    FragmentManager mFragmentManager;

    public SingleEditorView(Context context, FragmentManager fragmentManager, SingleExercise singleExercise) {
        super(context);
        this.mFragmentManager = fragmentManager;
        this.mSingleExercise = singleExercise;

        mImagesVerticalSpace = getResources().getDimensionPixelSize(R.dimen.view_images_vertical_space_editor);
        mImageNumberChangePaddingHorizontal = getResources().getDimensionPixelSize(R.dimen.single_editor_image_number_change_horizontal_padding);

        mImageManager = new ImageManager(context,fragmentManager,this,mSingleExercise,ImageManager.EDITOR);
        mPlusBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.plus);
    }

    private void resizeView(int width,int height) {

        int areaHeight = (height - mPaddingTop - mPaddingBottom - mImagesVerticalSpace) /2;

        mImageNumberChangeSize = areaHeight / 2;

        int areaRight = width - mPaddingRight - mImageNumberChangeSize - mImageNumberChangePaddingHorizontal;

        mAreaTop = new Rect(mPaddingLeft,mPaddingTop,areaRight ,mPaddingTop + areaHeight);
        mAreaBottom = new Rect(mPaddingLeft,mAreaTop.bottom+mImagesVerticalSpace,areaRight,height-mPaddingBottom);

        int imageNumberChangeSizeOffsetTopY = (mAreaTop.height() - mImageNumberChangeSize) / 2;
        int imageNumberChangeSizeOffsetBottomY = (mAreaBottom.height() - mImageNumberChangeSize ) / 2;

        mAreaPlusButtonTop = new Rect(mAreaTop.right + mImageNumberChangePaddingHorizontal,mAreaTop.top + imageNumberChangeSizeOffsetTopY,
                width-mPaddingRight,mAreaTop.top + imageNumberChangeSizeOffsetTopY + mImageNumberChangeSize);

        mAreaPlusButtonBottom = new Rect(mAreaBottom.right + mImageNumberChangePaddingHorizontal,mAreaBottom.top + imageNumberChangeSizeOffsetBottomY,
                width-mPaddingRight,mAreaBottom.top + imageNumberChangeSizeOffsetBottomY + mImageNumberChangeSize);

        mImageManager.resizeImages(mAreaTop,mAreaBottom);

        mPlusBitmap = Bitmap.createScaledBitmap(mPlusBitmap, mAreaPlusButtonTop.width(), mAreaPlusButtonTop.height(), true);
    }

    boolean isSingleExerciseValid()
    {
        return mImageManager.imagesValid();
    }

    protected Bitmap getWholeScreen(){
        Bitmap bitmap = Bitmap.createBitmap(getWidth(),getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);

        mImageManager.draw(canvas);

        return bitmap;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        resizeView(w, h);
        invalidate();
    }
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (mAreaPlusButtonTop != null && mAreaPlusButtonBottom != null) {
            canvas.drawBitmap(mPlusBitmap, mAreaPlusButtonTop.left, mAreaPlusButtonTop.top, null);
            canvas.drawBitmap(mPlusBitmap, mAreaPlusButtonBottom.left, mAreaPlusButtonBottom.top, null);

        }
        if(mImageManager != null){
            mImageManager.draw(canvas);
        }
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if(event.getAction() == MotionEvent.ACTION_DOWN) {

            final int x = (int) event.getX();
            final int y = (int) event.getY();

            if (mAreaPlusButtonTop.contains(x, y)) {
                mImageManager.addTopImage();
                mImageManager.resizeImages(mAreaTop,mAreaBottom);
            }
            else if (mAreaPlusButtonBottom.contains(x, y)) {
                mImageManager.addBottomImage();
                mImageManager.resizeImages(mAreaTop,mAreaBottom);
            }
            else if (mAreaTop.contains(x, y)){
                mImageManager.touchTopDown(x,y);
            }
            else if (mAreaBottom.contains(x, y)){
                mImageManager.touchBottomDown(x,y);
            }
            invalidate();
        }
        return true;
    }
}
