package cz.cvut.fit.dragon.imagerotatemodul.game;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cz.cvut.fit.dragon.imagerotatemodul.R;
import cz.cvut.fit.dragon.imagerotatemodul.model.SingleExercise;

import static android.media.CamcorderProfile.get;

/**
 * This method is responsible for drawing images and managing user input
 */
public class ImageManager {

    final static int NO_MOVE = -1;
    final static int RESULT_MISTAKE =8;
    final static int RESULT_SUCCESS = 9;
    final static int RESULT_NONE = 10;

    protected Context mContext;
    protected int mImagePadding;
    SingleExercise mSingleExercise;
    List<ImageHolder> mImageHolderList;
    View view;
    int mSelectedImageId;

    public ImageManager(Context context,View view,SingleExercise mSingleExercise){
        this.mContext = context;
        this.view = view;
        this.mSingleExercise = mSingleExercise;

        mImagePadding = context.getResources().getDimensionPixelSize(R.dimen.view_images_horizontal_space);

        initImages();
        mSelectedImageId = NO_MOVE;
    }

    /**
     * This method initializes images from exercise
     */
    protected void initImages(){
        mImageHolderList = new ArrayList<>();

        int rotatedId = new Random(System.nanoTime()).nextInt(mSingleExercise.getNumberOfImages());
        for(int i=0;i<mSingleExercise.getNumberOfImages();i++){
            ImageHolder imageHolder;

            if(i == rotatedId)
                 imageHolder = new ImageHolder(mSingleExercise.getUniqueId(),true,mSingleExercise.getTransformType());
            else
                imageHolder = new ImageHolder(mSingleExercise.getUniqueId(),false,mSingleExercise.getTransformType());

            mImageHolderList.add(imageHolder);
        }

    }
    public void resizeImages(Rect area){

        int maxWidthImage = (area.width() - (mImagePadding * (mImageHolderList.size() -1))) / mImageHolderList.size();
        int maxHeightImage = area.height();

        int imageSize;
        if(maxWidthImage > maxHeightImage)imageSize = maxHeightImage;
        else imageSize = maxWidthImage;

        int imagesWidth = (mImageHolderList.size() * imageSize) + (mImageHolderList.size() * mImagePadding);
        int imagesHeight = imageSize;

        int offsetX = (area.width()  - imagesWidth) / 2;
        int offsetY = (area.height() - imagesHeight) / 2;

        for(int i=0;i< mImageHolderList.size();i++){
            int left = offsetX + (i*imageSize) + (i*mImagePadding);

            Rect rect = new Rect(area.left + left,area.top + offsetY,area.left + left+imageSize,area.top + offsetY + imageSize);
            mImageHolderList.get(i).setRect(rect);
        }
    }
    public void draw(Canvas canvas){
        Paint p = new Paint();p.setColor(Color.LTGRAY);
        for(int i=0;i< mImageHolderList.size();i++) {
            mImageHolderList.get(i).draw(canvas);
        }
        if(mSelectedImageId != NO_MOVE)
            mImageHolderList.get(mSelectedImageId).draw(canvas);
    }

    public void touchDown(int x,int y){
        for (int i=0;i< mImageHolderList.size();i++){
            if(mImageHolderList.get(i).getRect().contains(x,y)){
                mSelectedImageId = i;
            }
        }
    }
    public void touchMove(int offsetX,int offsetY){
        if(mSelectedImageId != NO_MOVE)
             mImageHolderList.get(mSelectedImageId).offSetRectMoving(offsetX, offsetY);
    }

    public int touchUp(Rect binArea) {
        int result = RESULT_NONE;

        if (mSelectedImageId != NO_MOVE) {
            ImageHolder ih = mImageHolderList.get(mSelectedImageId);
            if (binArea.contains(ih.getRect().centerX(), ih.getRect().centerY()) && mImageHolderList.get(mSelectedImageId).isSpecial()) {
                mSelectedImageId = NO_MOVE;
                result= RESULT_SUCCESS;
            } else if (binArea.contains(ih.getRect().centerX(), ih.getRect().centerY()) && !mImageHolderList.get(mSelectedImageId).isSpecial()) {
                ih.putBack();
                result= RESULT_MISTAKE;
            } else {
                ih.putBack();
                mSelectedImageId = NO_MOVE;
                result= RESULT_NONE;
            }
        }
        mSelectedImageId = NO_MOVE;
        return result;
    }
}
