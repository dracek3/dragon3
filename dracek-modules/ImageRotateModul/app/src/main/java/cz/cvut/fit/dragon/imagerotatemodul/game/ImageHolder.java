package cz.cvut.fit.dragon.imagerotatemodul.game;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;

import cz.cvut.fit.dragon.imagerotatemodul.model.SingleExercise;
import cz.cvut.fit.dragon.corelibrary.util.IO;
import cz.cvut.fit.dragon.corelibrary.util.Utilities;

public class ImageHolder {

    Rect rect;
    Rect defaultRect;
    private int singleExerciseUniqueId;
    Bitmap bitmap;

    //image file is in folder
    boolean set;
    boolean special;
    int transformType;

    public ImageHolder(int singleExerciseUniqueId, boolean special, int transformType){
        set = true;
        this.singleExerciseUniqueId = singleExerciseUniqueId;
        this.special = special;
        this.transformType = transformType;
    }

    public void draw(Canvas canvas){
        Paint p = new Paint();p.setColor(Color.DKGRAY);

        if(set && rect != null) {

            Matrix matrix = new Matrix();

            if(transformType == SingleExercise.TYPE_ROTATION90 && special)
                matrix.postRotate(90);
            else if(transformType == SingleExercise.TYPE_ROTATION180 && special)
                matrix.postRotate(180);
            else if(transformType == SingleExercise.TYPE_ROTATION270 && special)
                matrix.postRotate(270);
            else if(transformType == SingleExercise.TYPE_HORIZONTAL&& special )
                matrix.preScale(-1.0f, 1.0f);
            else if(transformType == SingleExercise.TYPE_VERTICAL && special)
                matrix.preScale(1.0f, -1.0f);

            Bitmap transformedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            canvas.drawBitmap(transformedBitmap, rect.left, rect.top, null);
        }
        else if(rect != null)
            canvas.drawRect(rect, p);
    }

    /**
     * This method loads new image from game folder and scale the image so it fits
     */
    private void loadImage(){
        Bitmap tmp = Utilities.loadBitmapFromFile(IO.getImageFile(singleExerciseUniqueId), rect.width(), rect.height());
        bitmap = Bitmap.createScaledBitmap(tmp, rect.width(), rect.height(), true);
    }

    public Rect getRect(){
        return rect;
    }

    public void putBack(){
        this.rect = new Rect(defaultRect);
    }

    public void setRect(Rect rect){
        this.rect = new Rect(rect);
        this.defaultRect = new Rect(rect);

        if(set){
            loadImage();
        }
    }
    public void offSetRectMoving(int dx,int dy){
        rect.offset(dx,dy);
    }

    public boolean isSpecial() {
        return special;
    }
}
