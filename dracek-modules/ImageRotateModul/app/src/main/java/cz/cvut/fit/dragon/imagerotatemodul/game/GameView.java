package cz.cvut.fit.dragon.imagerotatemodul.game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import cz.cvut.fit.dragon.corelibrary.util.Utilities;
import cz.cvut.fit.dragon.imagerotatemodul.R;
import cz.cvut.fit.dragon.imagerotatemodul.model.SingleExercise;

/**
 * This class manages user interaction and resize images
 */
public class GameView extends View {

    private Context mContext;
    private Rect mAreaTop,mAreaBottom,mAreaBin,mAreaBinInsides, mAreaWhole;
    private int mPaddingLeft,mPaddingRight,mPaddingTop,mPaddingBottom;
    private int mImagesVerticalSpace;
    private int mBinSizeDifference;

    private Bitmap mBinBackBitmap;
    private Bitmap mBinFrontBitmap;
    private Bitmap mBackgroundBitmap;
    private ImageManager mImageManager;
    private ExerciseManager mExerciseManager;
    private SingleExercise mCurrentSingleExercise;
    private TextView mCurrentSingleExerciseNameTextView;

    private int mLastX;
    private int mLastY;
    private int firstPressedId;
    private final static int FINGER_NOT_PRESSED = -13;

    public GameView(Context context,ExerciseManager exerciseManager,TextView currentSingleExerciseNameTextView) {
        super(context);
        this.mContext = context;
        this.mExerciseManager = exerciseManager;
        this.mCurrentSingleExerciseNameTextView = currentSingleExerciseNameTextView;

        loadDimens();

        mCurrentSingleExercise = mExerciseManager.getCurrentSingleExercise();
        firstPressedId = FINGER_NOT_PRESSED;
        setSingleExercise();
    }

    /**
     * This method loads dimensions from resources
     */
    private void loadDimens(){
        mPaddingLeft = (int)getResources().getDimension(R.dimen.view_left_padding);
        mPaddingRight = (int)getResources().getDimension(R.dimen.view_right_padding);
        mPaddingBottom = (int)getResources().getDimension(R.dimen.view_bottom_padding);
        mPaddingTop = (int)getResources().getDimension(R.dimen.view_top_padding);
        mBinSizeDifference = (int)getResources().getDimension(R.dimen.view_bin_size_difference);
        mImagesVerticalSpace = getResources().getDimensionPixelSize(R.dimen.view_images_vertical_space);
    }

    /**
     * This method sets the game current single exercise
     */
    private void setSingleExercise(){
        mImageManager = new ImageManager(mContext,this,mCurrentSingleExercise);
        if(mAreaTop != null)mImageManager.resizeImages(mAreaTop);
        mCurrentSingleExerciseNameTextView.setText(mCurrentSingleExercise.getName());
    }

    /**
     * Calculates the rectangles where images and bin are showed
     * @param width Width of the screen
     * @param height Height of the screen
     */
    private void resizeView(int width,int height) {
        int areaHeight = (height - mPaddingTop - mPaddingBottom - mImagesVerticalSpace) /4;

        mAreaTop = new Rect(mPaddingLeft,mPaddingTop,width- mPaddingRight ,mPaddingTop + areaHeight - mBinSizeDifference);
        mAreaBottom = new Rect(mPaddingLeft,mAreaTop.bottom+mImagesVerticalSpace,width-mPaddingRight,height-mPaddingBottom);
        mAreaWhole = new Rect(0,0,width, height);

        if(mBinBackBitmap == null)mBinBackBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.avatar_kosb);
        if(mBinFrontBitmap == null)mBinFrontBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.kos_front);
        resizeBin();

        if(mBackgroundBitmap == null) mBackgroundBitmap = Utilities.loadBitmapFromResource(getResources(), R.drawable.background, mAreaWhole.width(), mAreaWhole.height());


        mImageManager.resizeImages(mAreaTop);
        invalidate();
    }

    /**
     * Calculates the rectangle where bin is showed
     */
    private void resizeBin(){
        int binMaxWidth = mAreaBottom.width();
        int binMaxHeight = mAreaBottom.height();

        int binHeight = mBinBackBitmap.getHeight();
        int binWidth = mBinBackBitmap.getWidth();
        double hToW = 1.0*binHeight/binWidth;

        int binMaxSize;
        if(1.0*binMaxHeight/binHeight > 1.0*binMaxWidth/binWidth){
            binMaxHeight = (int)(binMaxWidth*hToW);
        }
        else{
            binMaxWidth = (int)(binMaxHeight/hToW);
        }

        int binOffsetX = (mAreaBottom.width() - binMaxWidth)/2;
        int binOffsetY = 0;//(mAreaBottom.height() - binMaxSize)/2;

        mAreaBin = new Rect(mAreaBottom.left + binOffsetX,
                mAreaBottom.top + binOffsetY,
                mAreaBottom.left + binOffsetX + binMaxWidth,
                mAreaBottom.top + binOffsetY + binMaxHeight);

        mAreaBinInsides = new Rect(
                (int)(mAreaBin.left + mAreaBin.width()*504.0/1435),
                (int)(mAreaBin.top + mAreaBin.height()*432.0/886),
                (int)(mAreaBin.left + mAreaBin.width()*905.0/1435),
                (int)(mAreaBin.top + mAreaBin.height()*855.0/886));

        mBinBackBitmap = Bitmap.createScaledBitmap(mBinBackBitmap, mAreaBin.width(), mAreaBin.height(), true);
        mBinFrontBitmap = Bitmap.createScaledBitmap(mBinFrontBitmap, mAreaBin.width(), mAreaBin.height(), true);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        resizeView(w, h);
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(mAreaWhole != null){
            canvas.drawBitmap(mBackgroundBitmap,null,mAreaWhole, null);
        }

        if(mAreaBin != null){
            canvas.drawBitmap(mBinBackBitmap, mAreaBin.left, mAreaBin.top, null);
        }

        if(mImageManager != null)
            mImageManager.draw(canvas);

        if(mAreaBin != null){
            canvas.drawBitmap(mBinFrontBitmap,null, mAreaBinInsides, null);
        }
    }

    private void touchDown(int x,int y){
        if (mAreaTop.contains(x, y)){
            mImageManager.touchDown(x,y);
            mLastX = x;
            mLastY = y;
        }
    }
    private void touchMove(int x,int y){
        int dx = x - mLastX;
        int dy = y - mLastY;
        mImageManager.touchMove(dx,dy);
        mLastX = x;
        mLastY = y;
    }

    /**
     * Evaluate whether game is finished or next single exercise should be set
     */
    private void touchUp(){

        int result = mImageManager.touchUp(mAreaBinInsides);
        if(result == ImageManager.RESULT_SUCCESS){
            Utilities.showOKCheck(mContext);
            singleExerciseFinished();
        }
        else if(result == ImageManager.RESULT_MISTAKE){
            Utilities.showXCheck(mContext);
            mExerciseManager.getCurrentResult().addWrongMove();
        }
    }
    private void singleExerciseFinished(){
        if(mExerciseManager.nextSingleExercise()){
            mCurrentSingleExercise = mExerciseManager.getCurrentSingleExercise();
            setSingleExercise();
        }
        else {
            if(noticeEventListener != null)
                noticeEventListener.onExerciseDone();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        final int x = (int) event.getX();
        final int y = (int) event.getY();

        int action = event.getActionMasked();
        int index = event.getActionIndex();
        int id = event.getPointerId(index);

        if(action == MotionEvent.ACTION_DOWN) {

            if(firstPressedId == FINGER_NOT_PRESSED){
                touchDown(x,y);
                invalidate();
                firstPressedId = id;
            }
        }
        else if(action == MotionEvent.ACTION_MOVE){
            if(id == firstPressedId) {
                touchMove(x, y);
                invalidate();
            }
        }
        else if(action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_POINTER_UP){
            if(id == firstPressedId){
                touchUp();
                invalidate();
                firstPressedId = FINGER_NOT_PRESSED;
            }
        }
        return true;
    }

    NoticeEventListener noticeEventListener;
    public interface NoticeEventListener{
        void onExerciseDone();
    }
    public void setNoticeEventListener(NoticeEventListener noticeEventListener){
        this.noticeEventListener = noticeEventListener;

    }
}
