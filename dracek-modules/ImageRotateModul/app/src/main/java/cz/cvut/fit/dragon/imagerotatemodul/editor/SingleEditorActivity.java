package cz.cvut.fit.dragon.imagerotatemodul.editor;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import java.io.File;
import java.io.IOException;

import cz.cvut.fit.dragon.imagerotatemodul.R;
import cz.cvut.fit.dragon.imagerotatemodul.model.SingleExercise;
import cz.cvut.fit.dragon.corelibrary.util.IO;
import cz.cvut.fit.dragon.corelibrary.util.InfoDialog;
import cz.cvut.fit.dragon.corelibrary.util.Size;
import cz.cvut.fit.dragon.corelibrary.util.Utilities;

/**
 * This class manages creating and editing single exercise
 */
public class SingleEditorActivity extends Activity implements View.OnClickListener{

    private static final int SELECT_PHOTO = 100;
    private static final int MAX_IMAGES = 15;
    private static final int MIN_IMAGES = 3;

    public final static int CREATE = 105;
    public final static int EDIT = 106;
    int mType;

    private Spinner mRotationSpinner;
    private Button mLoadButton, mSaveButton, mCancelButton;
    private ImageView mImageThumbnailView;
    private EditText mNumberOfImagesInput, mNameInput;

    private SingleExercise mSingleExercise;
    ArrayAdapter<CharSequence> transformTypesAdapter;
    private boolean mImageLoaded;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_editor);

        transformTypesAdapter = ArrayAdapter.createFromResource(SingleEditorActivity.this,
        R.array.rotation_types, android.R.layout.simple_spinner_item);
        transformTypesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        init();
        initSingleExercise(getIntent());
    }

    private void init(){
        mRotationSpinner = (Spinner)findViewById(R.id.rotation_spinner);
        mSaveButton = (Button) findViewById(R.id.save_image_button);
        mLoadButton = (Button) findViewById(R.id.load_image_button);
        mCancelButton = (Button) findViewById(R.id.cancel_image_button);
        mImageThumbnailView = (ImageView) findViewById(R.id.image_thumb_nail);
        mNumberOfImagesInput = (EditText)findViewById(R.id.number_of_images_input);
        mNameInput = (EditText)findViewById(R.id.single_exercise_name_input);

        mRotationSpinner.setAdapter(transformTypesAdapter);

        mLoadButton.setOnClickListener(this);
        mSaveButton.setOnClickListener(this);
        mCancelButton.setOnClickListener(this);
    }

    /**
     * This class set up the editor. If intent has single exercise method set the view the way is
     * represents the single exercise.
     * @param intent Intent that contains single exercise. If no single exercise is provided it creates new single exercise.
     */
    private void initSingleExercise(Intent intent){
        if(intent.hasExtra(EditorActivity.SINGLE_EXERCISE)){
            mSingleExercise = getIntent().getParcelableExtra(EditorActivity.SINGLE_EXERCISE);

            Bitmap sample = Utilities.loadBitmapFromFile(IO.getImageFile(mSingleExercise.getUniqueId()));
            mImageThumbnailView.setImageBitmap(sample);
            mImageLoaded = true;

            mRotationSpinner.setSelection(getTransformTypePosition());
            mNumberOfImagesInput.setText(String.valueOf(mSingleExercise.getNumberOfImages()));
            mNameInput.setText(mSingleExercise.getName());

            mCancelButton.setVisibility(View.GONE);
            mSaveButton.setText(getString(R.string.save_single_exercise));
            mType = EDIT;
        }
        else if(intent.hasExtra(EditorActivity.SINGLE_EXERCISE_UNIQUE_ID)) {
            int uniqueId = getIntent().getIntExtra(EditorActivity.SINGLE_EXERCISE_UNIQUE_ID, 0);

            mSingleExercise = new SingleExercise();
            mSingleExercise.setUniqueId(uniqueId);
            mImageLoaded = false;

            mSaveButton.setText(getString(R.string.create_single_exercise));
            mType = CREATE;
        }
    }

    /**
     * Checks if single exercise is valid. If not dialog with error message is shown
     * @return true if single exercise is valid. Otherwise returns false.
     */
    private boolean isSingleExerciseValid(){
        boolean valid = true;
        String msg = "";

        if(!mImageLoaded){
            msg += getString(R.string.image_required) + "\n";
            valid = false;
        }

        if(mNumberOfImagesInput.getText().toString().equals("")) {
            msg += getString(R.string.number_of_images_input_is_empty);
            valid = false;
        }
        else {

            int value = Integer.parseInt(mNumberOfImagesInput.getText().toString());
            if (value < MIN_IMAGES) {
                msg += getString(R.string.number_of_image_greater) + " " + MIN_IMAGES;
                valid = false;
            } else if (value > MAX_IMAGES) {
                msg += getString(R.string.number_of_image_less) + " " + MAX_IMAGES;
                valid = false;
            }
        }
        if(!valid)showInfoMessage(msg);

        return valid;
    }

    /**
     * Parse transform type from user input
     * @return transform type
     */
    private int parseTransformType(){

        String selectedItem = mRotationSpinner.getSelectedItem().toString();
        if(selectedItem.equals(getString(R.string.transform_type_90))) return SingleExercise.TYPE_ROTATION90;
        else if(selectedItem.equals(getString(R.string.transform_type_180))) return SingleExercise.TYPE_ROTATION180;
        else if(selectedItem.equals(getString(R.string.transform_type_270))) return SingleExercise.TYPE_ROTATION270;
        else if(selectedItem.equals(getString(R.string.transform_type_horizontal))) return SingleExercise.TYPE_HORIZONTAL;
        else return SingleExercise.TYPE_VERTICAL;
    }

    /**
     * Returns position of transform type
     * @return Id representing position of transform type
     */
    private int getTransformTypePosition(){
        if(mSingleExercise.getTransformType() == SingleExercise.TYPE_ROTATION90)
            return transformTypesAdapter.getPosition(getString(R.string.transform_type_90));
        else if(mSingleExercise.getTransformType() == SingleExercise.TYPE_ROTATION180)
            return transformTypesAdapter.getPosition(getString(R.string.transform_type_180));
        else if(mSingleExercise.getTransformType() == SingleExercise.TYPE_ROTATION270)
            return transformTypesAdapter.getPosition(getString(R.string.transform_type_270));
        else if(mSingleExercise.getTransformType() == SingleExercise.TYPE_HORIZONTAL)
            return transformTypesAdapter.getPosition(getString(R.string.transform_type_horizontal));
        else return transformTypesAdapter.getPosition(getString(R.string.transform_type_vertical));
    }


    @Override
    public void onClick(View v) {
        if(v == mLoadButton){
            Intent photoPickIntent = new Intent(Intent.ACTION_PICK);
            photoPickIntent.setType("image/*");
            startActivityForResult(photoPickIntent, SELECT_PHOTO);
        }
        else if(v == mSaveButton) {
            if(isSingleExerciseValid()){
                save();
            }
        }
        else if(v == mCancelButton){
            setResult(RESULT_CANCELED);
            finish();
        }
    }

    /**
     * This method load image from file and saves the image and thumb to game folder.
     * @param imageFilePath Path of the image
     */
    private void loadImage(String imageFilePath){
        File savedImageFile = IO.getImageFile(mSingleExercise.getUniqueId());
        File savedThumbnail = IO.getThumbnailFile(mSingleExercise.getUniqueId());

        File loadedImageFile = new File(imageFilePath);
        Size imageSize = Utilities.getImageSize(loadedImageFile);

        if(imageSize.getWidth() ==  0 || imageSize.getHeight() == 0){
            showInfoMessage(getString(R.string.not_zero_image_size));
        }
        else if (imageSize.getWidth() != imageSize.getHeight()){
            showInfoMessage(getString(R.string.square_image_required));
        }
        else {
            Size scaledSize = Utilities.scaleSize(imageSize, new Size(IO.MAX_IMAGE_SIZE, IO.MAX_IMAGE_SIZE));
            Size scaledThumbSize = Utilities.scaleSize(imageSize, new Size(IO.MAX_THUMB_SIZE,IO.MAX_THUMB_SIZE));

            Bitmap scaledBitmap = Utilities.loadBitmapFromFile(loadedImageFile, scaledSize.getWidth(), scaledSize.getHeight());
            Bitmap scaledThumbBitmap = Utilities.loadBitmapFromFile(loadedImageFile, scaledThumbSize.getWidth(), scaledThumbSize.getHeight());
            Bitmap scaleSampleBitmap = Utilities.loadBitmapFromFile(loadedImageFile,getResources().getDimensionPixelSize(R.dimen.sample_view_size),
                    getResources().getDimensionPixelSize(R.dimen.sample_view_size));
            mImageThumbnailView.setImageBitmap(scaleSampleBitmap);

            try {
                Utilities.saveBitmapToFile(savedImageFile,scaledBitmap);
                Utilities.saveBitmapToFile(savedThumbnail, scaledThumbBitmap);
                mImageLoaded = true;

            } catch (IOException e) {
                showInfoMessage(getString(R.string.load_image_failed));
            }
        }
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == SELECT_PHOTO && resultCode == Activity.RESULT_OK) {
            String path = Utilities.getPath(SingleEditorActivity.this, data.getData());
            if(path != null){
                loadImage(path);
            }
            else showInfoMessage(getString(R.string.load_image_failed));
        }
    }

    /**
     * This method shows simple dialog with message
     * @param message
     */
    private void showInfoMessage(String message){
        InfoDialog infoDialog = new InfoDialog();
        Bundle data = new Bundle();
        data.putString(InfoDialog.MESSAGE,message);
        infoDialog.setArguments(data);
        infoDialog.show(getFragmentManager(), "infoDialog");
    }

    /**
     * Save single exercise and returns the single exercise to editor
     */
    private void save(){
        mSingleExercise.setName(mNameInput.getText().toString());
        mSingleExercise.setTransformType(parseTransformType());
        mSingleExercise.setNumberOfImages(Integer.valueOf(mNumberOfImagesInput.getText().toString()));

        Intent resultIntent = new Intent();
        resultIntent.putExtra(EditorActivity.SINGLE_EXERCISE, mSingleExercise);
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    @Override
    public void onBackPressed() {
        if(mType == EDIT) {
            if(isSingleExerciseValid())
                save();
        }
        else if(mType == CREATE){
            setResult(RESULT_CANCELED);
            finish();
        }
    }
}
