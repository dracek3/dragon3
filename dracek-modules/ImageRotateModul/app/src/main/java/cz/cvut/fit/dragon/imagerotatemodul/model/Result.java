package cz.cvut.fit.dragon.imagerotatemodul.model;


/**
 * Represent results of single exercise
 */
public class Result {
    int wrongMoves;
    int uniqueWrongMoves;

    public Result() {
        wrongMoves = 0;
    }

    public void addWrongMove() {
        this.wrongMoves++;
    }

    /**
     * Calculates percentage score of single exercise
     * @return Percentage score
     */
    public int getPercentage(){
        float percentage = 1-((float)wrongMoves/uniqueWrongMoves);
        if (percentage < 0) return 0;
        else return (int)(percentage * 100);
    }

    /**
     * Sets unique moves
     * @param uniqueWrongMoves All unique moves which user can make
     */
    public void setUniqueWrongMoves(int uniqueWrongMoves) {
        this.uniqueWrongMoves = uniqueWrongMoves;
    }
}
