package cz.cvut.fit.dragon.imagerotatemodul.model;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

import cz.cvut.fit.dragon.imagerotatemodul.R;
import cz.cvut.fit.dragon.corelibrary.util.IO;
import cz.cvut.fit.dragon.corelibrary.util.InfoDialog;
import cz.cvut.fit.dragon.corelibrary.util.LoadExerciseException;

public abstract  class BaseActivity extends Activity {

    public final static String EXERCISE_FILE = "EXERCISE_FILE";
    public final static String IMAGES = "IMAGES";
    public final static String RESULTS = "RESULTS";
    public final static String MESSAGE = "MESSAGE";
    public final static String SINGLE_EXERCISE = "SINGLE_EXERCISE";

    protected Exercise mExercise;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IO.readResources(BaseActivity.this);
        initExercise();
    }

    /**
     * This method initialize new exercise
     */
    private void initExercise(){
        if(getIntent().hasExtra(EXERCISE_FILE) && getIntent().hasExtra(IMAGES)) {
            ExerciseReader e = new ExerciseReader(getIntent().getStringExtra(EXERCISE_FILE),getIntent().getStringArrayListExtra(IMAGES));
            try {
                mExercise = e.load();
                init();
            } catch (XmlPullParserException | IOException | LoadExerciseException x) {
                showMessage(getString(R.string.load_exercise_failed), true);
            }
        }
        else noExerciseProvided();
    }

    /**
     * This method is called when the core module does not provide exercise
     */
    protected abstract void noExerciseProvided();

    /**
     * This method is called when exercise was successfully parsed
     */
    protected abstract void init();

    /**
     * This method shows simple message
     * @param message Message
     * @param finish If true module will close after user clicks on button to close message
     */
    protected void showMessage(String message, final boolean finish){
        InfoDialog infoDialog = InfoDialog.createNewDialog(message);
        infoDialog.setNoticeDialogListener(new InfoDialog.NoticeDialogListener() {
            @Override
            public void onDialogPositiveClick(DialogFragment dialog) {
               if(finish) finish();
            }
        });
        infoDialog.show(getFragmentManager(), "infoDialog");
    }
}
