package cz.cvut.fit.dragon.imagerotatemodul.model;

import android.util.Xml;

import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import cz.cvut.fit.dragon.corelibrary.util.IO;
import cz.cvut.fit.dragon.corelibrary.util.XmlTag;

/**
 * This class is used to write Exercise to xml
 */
public class ExerciseWriter {

    Exercise mExercise;

    public ExerciseWriter(Exercise exercise){
        this.mExercise = exercise;
    }

    /**
     * This method writes exercise to xml string
     * @return Xml string with exercise
     * @throws IOException
     */
    public String getExerciseOutputXmlString() throws IOException{
        XmlSerializer xmlSerializer = Xml.newSerializer();
        ByteArrayOutputStream output =  new ByteArrayOutputStream();
        xmlSerializer.setOutput(output, IO.XML_ENCODING);
        writeXml(xmlSerializer,mExercise);
        return output.toString(IO.XML_ENCODING);
    }

    private void writeXml(XmlSerializer xmlSerializer,Exercise exercise) throws IOException {
        xmlSerializer.startDocument(IO.XML_ENCODING, false);

        xmlSerializer.startTag("", XmlTag.EXERCISE);
        for(int i=0;i<exercise.getSingleExerciseList().size();i++){
            SingleExercise singleExercise = exercise.getSingleExerciseList().get(i);

            xmlSerializer.startTag("",XmlTag.SINGLE_EXERCISE);

            xmlSerializer.startTag("", XmlTag.ID);
            xmlSerializer.text(String.valueOf(singleExercise.getUniqueId()));
            xmlSerializer.endTag("", XmlTag.ID);

            xmlSerializer.startTag("", XmlTag.NAME);
            xmlSerializer.text(singleExercise.getName());
            xmlSerializer.endTag("", XmlTag.NAME);

            xmlSerializer.startTag("", XmlTag.TRANSFORM_TYPE);
            xmlSerializer.text(String.valueOf(singleExercise.getTransformType()));
            xmlSerializer.endTag("",XmlTag.TRANSFORM_TYPE);

            xmlSerializer.startTag("",XmlTag.NUMBER_OF_IMAGES);
            xmlSerializer.text(String.valueOf(singleExercise.getNumberOfImages()));
            xmlSerializer.endTag("",XmlTag.NUMBER_OF_IMAGES);

            xmlSerializer.endTag("",XmlTag.SINGLE_EXERCISE);
        }
        xmlSerializer.endTag("", XmlTag.EXERCISE);
        xmlSerializer.flush();
    }
}
