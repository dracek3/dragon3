package cz.cvut.fit.dragon.imagerotatemodul.model;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents Exercise
 */
public class Exercise {
    List<SingleExercise> mSingleExerciseList;
    List<Bitmap> mThumbnailList;

    public Exercise(){
        mSingleExerciseList = new ArrayList<>();
        mThumbnailList = new ArrayList<>();
    }

    /**
     * Adds new single exercise
     * @param singleExercise New single exercise
     * @param thumbnail Thumbnail that represent single exercise
     */
    public void addSingleExercise(SingleExercise singleExercise,Bitmap thumbnail){

        int index = mSingleExerciseList.indexOf(singleExercise);
        if(index < 0) {
            mSingleExerciseList.add(singleExercise);
            mThumbnailList.add(thumbnail);
        }
        else {
            mSingleExerciseList.set(index,singleExercise);
            mThumbnailList.set(index,thumbnail);
        }

    }

    public void removeSingleExercise(int index){
        mSingleExerciseList.get(index).destroyImages();
        mThumbnailList.remove(index);
        mSingleExerciseList.remove(index);

    }
    public SingleExercise getSingleExercise(int index){
        return mSingleExerciseList.get(index);
    }

    public Bitmap getSingleExerciseThumbnail(int index){
        return mThumbnailList.get(index);
    }

    public List<SingleExercise> getSingleExerciseList() {
        return mSingleExerciseList;
    }
    public int getSingleExerciseSize(){
        return mSingleExerciseList.size();
    }

    /**
     * Calculates maximum id that is used for single exercise
     * @return max unique id
     */
    public int getMaxUniqueId(){
        int max = 0;
        for (int i=0;i<mSingleExerciseList.size();i++){
            if(mSingleExerciseList.get(i).getUniqueId() > max)max = mSingleExerciseList.get(i).getUniqueId();
        }
        return max+1;
    }
}
