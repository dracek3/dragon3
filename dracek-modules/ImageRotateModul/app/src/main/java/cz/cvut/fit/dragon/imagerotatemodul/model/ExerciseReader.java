package cz.cvut.fit.dragon.imagerotatemodul.model;

import android.graphics.Bitmap;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import cz.cvut.fit.dragon.corelibrary.util.IO;
import cz.cvut.fit.dragon.corelibrary.util.LoadExerciseException;
import cz.cvut.fit.dragon.corelibrary.util.Utilities;
import cz.cvut.fit.dragon.corelibrary.util.XmlTag;

/**
 * This class is used to parse xml to Exercise
 */
public class ExerciseReader {

    Exercise mExercise;
    String mXml;
    ArrayList<String> mImageFilePaths;

    public ExerciseReader(String xml, ArrayList<String> imageFilePaths){
        this.mXml = xml;
        this.mImageFilePaths = imageFilePaths;

        mExercise = new Exercise();
    }

    /**
     * This method creates new exercise from xml string
     * @return New exercise
     * @throws XmlPullParserException
     * @throws IOException
     * @throws LoadExerciseException
     */
    public Exercise load() throws XmlPullParserException, IOException, LoadExerciseException {

        XmlPullParserFactory xmlFactoryObject;
        xmlFactoryObject = XmlPullParserFactory.newInstance();
        XmlPullParser xmlPullParser = xmlFactoryObject.newPullParser();
        xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        xmlPullParser.setInput(new ByteArrayInputStream(mXml.getBytes(IO.XML_ENCODING)), IO.XML_ENCODING);
        parseXML(xmlPullParser);
        copyImages();

        return mExercise;
    }

    private void copyImages() throws IOException, LoadExerciseException{
        for(String imageFilePath : mImageFilePaths){
            File imageFile = new File(imageFilePath);
            if(!imageFile.exists())throw new LoadExerciseException();
            Utilities.copyImageToDir(imageFile.getPath(),IO.IMAGES_DIR);
        }
    }

    private void parseXML(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException, LoadExerciseException {

        int eventType = xmlPullParser.getEventType();
        String text = "";

        SingleExercise singleExercise = null;

        while (eventType != XmlPullParser.END_DOCUMENT){
            String tagName = xmlPullParser.getName();

            if(eventType == XmlPullParser.START_TAG){
                if(tagName.equals(XmlTag.SINGLE_EXERCISE))
                    singleExercise = new SingleExercise();
            }
            else if(eventType == XmlPullParser.TEXT){
                text = xmlPullParser.getText();
            }
            else if(eventType == XmlPullParser.END_TAG){
                if(tagName.equals(XmlTag.ID)) {
                    if (singleExercise != null) singleExercise.setUniqueId(Integer.valueOf(text));
                    else throw new LoadExerciseException();
                }
                else if(tagName.equals(XmlTag.NAME)) {
                    if (singleExercise != null) singleExercise.setName(text);
                    else throw new LoadExerciseException();
                }
                else if(tagName.equals(XmlTag.TRANSFORM_TYPE)) {
                    if (singleExercise != null)
                        singleExercise.setTransformType(Integer.valueOf(text));
                    else throw new LoadExerciseException();
                }
                else if(tagName.equals(XmlTag.NUMBER_OF_IMAGES)) {
                    if (singleExercise != null)
                        singleExercise.setNumberOfImages(Integer.valueOf(text));
                    else throw new LoadExerciseException();
                }
                else if(tagName.equals(XmlTag.SINGLE_EXERCISE)) {
                    if(singleExercise != null) {
                        Bitmap thumb = Utilities.loadBitmapFromFile(IO.getThumbnailFile(singleExercise.getUniqueId()));
                        mExercise.addSingleExercise(singleExercise, thumb);
                    }
                    else throw new LoadExerciseException();
                }
                text = "";
            }
            eventType = xmlPullParser.next();
        }
    }
}
