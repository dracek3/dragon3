package cz.cvut.fit.dragon.imagerotatemodul.game;

import cz.cvut.fit.dragon.imagerotatemodul.model.Exercise;
import cz.cvut.fit.dragon.imagerotatemodul.model.Result;
import cz.cvut.fit.dragon.imagerotatemodul.model.SingleExercise;

/**
 * This class is manages the single exercises in game
 */
public class ExerciseManager {

    int currentSingleExercise;
    Exercise exercise;
    Result [] results;

    public ExerciseManager(Exercise exercise) {
        this.exercise = exercise;
        currentSingleExercise = 0;
        results = new Result[exercise.getSingleExerciseSize()];

        results = new Result[exercise.getSingleExerciseSize()];
        for (int i = 0; i < results.length; i++){
            results[i] = new Result();
            results[i].setUniqueWrongMoves(exercise.getSingleExercise(i).getNumberOfImages() - 1);
        }
    }

    /**
     * This method sets next single exercise in game
     * @return true, if all single exercise are done, false otherwise
     */
    public boolean nextSingleExercise(){
        if(currentSingleExercise >= exercise.getSingleExerciseSize()-1)
            return false;
        else {
            currentSingleExercise++;
            return true;
        }
    }

    /**
     * This method returns current single exercise
     * @return current single exercise
     */
    public SingleExercise getCurrentSingleExercise(){
        return exercise.getSingleExercise(currentSingleExercise);
    }

    /**
     * This method returns results of current single exercise
     * @return results of current single exercise
     */
    public Result getCurrentResult(){
        return results[currentSingleExercise];
    }

    /**
     * This method calculates the score of all single exercises. The
     * score is counted using the arithmetic mean.
     * @return percentage score of all single exercise
     */
    public int getResultsPercentage(){
        int percentageSum = 0;
        for (int i=0;i<results.length;i++){
            percentageSum += results[i].getPercentage();
        }
        return percentageSum / results.length;
    }
}
