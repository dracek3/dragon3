package cz.cvut.fit.dragon.imagerotatemodul.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;
import java.util.ArrayList;

import cz.cvut.fit.dragon.corelibrary.util.IO;

/**
 * Represents single exercise, single exercise is part of exercise
 */
public class SingleExercise implements Parcelable {

    public static final int TYPE_ROTATION90 = 0;
    public static final int TYPE_ROTATION180 = 1;
    public static final int TYPE_ROTATION270 = 2;
    public static final int TYPE_HORIZONTAL = 3;
    public static final int TYPE_VERTICAL = 4;

    private int uniqueId;
    private String name;
    private int transformType;
    private int numberOfImages;

    public SingleExercise(){
        //empty
    }

    protected SingleExercise(Parcel in) {
        uniqueId = in.readInt();
        name = in.readString();
        transformType = in.readInt();
        numberOfImages = in.readInt();
    }

    /**
     * This method deletes all images associated with this single exercise
     */
    public void destroyImages(){
        File thumbnail = IO.getThumbnailFile(uniqueId);
        File image = IO.getImageFile(uniqueId);
        thumbnail.delete();
        image.delete();
    }

    public static final Creator<SingleExercise> CREATOR = new Creator<SingleExercise>() {
        @Override
        public SingleExercise createFromParcel(Parcel in) {
            return new SingleExercise(in);
        }

        @Override
        public SingleExercise[] newArray(int size) {
            return new SingleExercise[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfImages() {
        return numberOfImages;
    }

    public void setNumberOfImages(int numberOfImages) {
        this.numberOfImages = numberOfImages;
    }

    public int getTransformType() {
        return transformType;
    }

    public void setTransformType(int transformType) {
        this.transformType = transformType;
    }

    public int getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    /**
     * This method
     * @return File paths of all images associated with this single exercise
     */
    public ArrayList<String> getAllImageFilePaths(){
        ArrayList<String> imageFilePaths = new ArrayList<>();
        imageFilePaths.add(IO.getImageFile(uniqueId).getPath());
        imageFilePaths.add(IO.getThumbnailFile(uniqueId).getPath());
        return imageFilePaths;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SingleExercise that = (SingleExercise) o;

        return uniqueId == that.uniqueId;
    }

    @Override
    public int hashCode() {
        return uniqueId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(uniqueId);
        dest.writeString(name);
        dest.writeInt(transformType);
        dest.writeInt(numberOfImages);
    }
}
