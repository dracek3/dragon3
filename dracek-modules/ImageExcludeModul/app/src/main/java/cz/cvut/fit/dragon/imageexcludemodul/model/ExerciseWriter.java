package cz.cvut.fit.dragon.imageexcludemodul.model;

import android.util.Xml;

import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import cz.cvut.fit.dragon.corelibrary.util.IO;
import cz.cvut.fit.dragon.corelibrary.util.XmlTag;


public class ExerciseWriter {

    Exercise mExercise;

    public ExerciseWriter(Exercise exercise){
        this.mExercise = exercise;
    }

    public String getExerciseOutputXmlString() throws IOException{
        XmlSerializer xmlSerializer = Xml.newSerializer();
        ByteArrayOutputStream output =  new ByteArrayOutputStream();
        xmlSerializer.setOutput(output, IO.XML_ENCODING);
        writeXml(xmlSerializer,mExercise);
        return output.toString(IO.XML_ENCODING);
    }
    private void writeXml(XmlSerializer xmlSerializer,Exercise exercise) throws IOException {
        xmlSerializer.startDocument(IO.XML_ENCODING, false);

        xmlSerializer.startTag("", XmlTag.EXERCISE);
        for(int i=0;i<exercise.getSingleExerciseList().size();i++){
            SingleExercise singleExercise = exercise.getSingleExerciseList().get(i);

            xmlSerializer.startTag("",XmlTag.SINGLE_EXERCISE);

            xmlSerializer.startTag("",XmlTag.ID);
            xmlSerializer.text(String.valueOf(singleExercise.getUniqueId()));
            xmlSerializer.endTag("", XmlTag.ID);

            xmlSerializer.startTag("", XmlTag.NAME);
            xmlSerializer.text(singleExercise.getName());
            xmlSerializer.endTag("", XmlTag.NAME);

            xmlSerializer.startTag("", XmlTag.TASK);
            xmlSerializer.text(singleExercise.getTask());
            xmlSerializer.endTag("", XmlTag.TASK);

            xmlSerializer.startTag("",XmlTag.IMAGES);
            for(int j=0;j<singleExercise.getImageList().size();j++){
                xmlSerializer.startTag("",XmlTag.IMAGE);

                xmlSerializer.startTag("",XmlTag.IMAGE_ID);
                xmlSerializer.text(String.valueOf(singleExercise.getImageList().get(j).getUniqueId()));
                xmlSerializer.endTag("", XmlTag.IMAGE_ID);

                xmlSerializer.startTag("",XmlTag.SPECIAL);
                xmlSerializer.text(String.valueOf(singleExercise.getImageList().get(j).isSpecial()));
                xmlSerializer.endTag("", XmlTag.SPECIAL);

                xmlSerializer.endTag("",XmlTag.IMAGE);
            }
            xmlSerializer.endTag("", XmlTag.IMAGES);

            xmlSerializer.endTag("",XmlTag.SINGLE_EXERCISE);
        }
        xmlSerializer.endTag("", XmlTag.EXERCISE);
        xmlSerializer.flush();
    }
}
