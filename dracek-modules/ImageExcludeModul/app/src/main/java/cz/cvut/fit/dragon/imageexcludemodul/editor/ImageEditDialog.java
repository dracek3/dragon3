package cz.cvut.fit.dragon.imageexcludemodul.editor;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import cz.cvut.fit.dragon.imageexcludemodul.R;
import cz.cvut.fit.dragon.corelibrary.util.InfoDialog;
import cz.cvut.fit.dragon.corelibrary.util.Utilities;

/**
 * This class shows dialog that is used for loading and removing image in single exercise editor
 */
public class ImageEditDialog extends DialogFragment implements View.OnClickListener {

    public static final int SELECT_PHOTO = 100;
    public static final String DIALOG_TYPE = "DIALOG_TYPE";
    public static final String IMAGE_ID = "IMAGE_ID";
    public static final String IMAGE_SELECTED = "IMAGE_SELECTED";
    public static final String IMAGE_SPECIAL = "IMAGE_SPECIAL";

    Button deleteImageButton,loadImageButton,closeButton;
    Switch specialImageSwitch;

    int imageId;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getDialog().setTitle(getString(R.string.image_edit_dialog_title));
        getDialog().setCanceledOnTouchOutside(false);
        setCancelable(false);

        View view = inflater.inflate(R.layout.edit_image_dialog,container);

        loadImageButton = (Button)view.findViewById(R.id.load_image_dialog);
        deleteImageButton = (Button)view.findViewById(R.id.delete_image_dialog);
        closeButton = (Button)view.findViewById(R.id.close_image_dialog);
        specialImageSwitch = (Switch)view.findViewById(R.id.special_image_switch);

        if(getArguments().getInt(DIALOG_TYPE) == ImageManager.DIALOG_CHANGE_ONLY){
            deleteImageButton.setEnabled(false);
        }

        imageId = getArguments().getInt(IMAGE_ID);


        specialImageSwitch.setEnabled(getArguments().getBoolean(IMAGE_SELECTED));
        specialImageSwitch.setChecked(getArguments().getBoolean(IMAGE_SPECIAL));

        loadImageButton.setOnClickListener(this);
        deleteImageButton.setOnClickListener(this);
        closeButton.setOnClickListener(this);
        specialImageSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                closeButton.setText(getString(R.string.image_edit_dialog_save));
                if(onResultListener != null)
                    onResultListener.onSpecialChanged(imageId,isChecked);
            }
        });

        return view;
    }

    @Override
    public void onClick(View v) {
        if(v == loadImageButton){
            Intent photoPickIntent = new Intent(Intent.ACTION_PICK);
            photoPickIntent.setType("image/*");
            startActivityForResult(photoPickIntent, SELECT_PHOTO);
        }
        else if(v == deleteImageButton){
            if(onResultListener!=null)
                onResultListener.onDelete(imageId);

            dismiss();
        }
        else if(v == closeButton) dismiss();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == SELECT_PHOTO && resultCode == Activity.RESULT_OK) {

            String path = Utilities.getPath(getActivity(),data.getData());
            if(onResultListener!=null)
                onResultListener.onLoaded(imageId,path);
        }
       dismiss();
    }


    OnResultListener onResultListener;
    public interface OnResultListener {
        void onDelete(int imageId);
        void onLoaded(int imageId, String filePath);
        void onSpecialChanged(int imageId, boolean isSpecial);
    }
    public void setOnResultListener(OnResultListener onResultListener) {
         this.onResultListener = onResultListener;
    }
}
