package cz.cvut.fit.dragon.imageexcludemodul.model;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

public class Exercise {
    List<SingleExercise> mSingleExerciseList;
    List<Bitmap> mThumbnailList;

    public Exercise(){
        mSingleExerciseList = new ArrayList<>();
        mThumbnailList = new ArrayList<>();
    }

    public void addSingleExercise(SingleExercise singleExercise,Bitmap thumbNail){

        int index = mSingleExerciseList.indexOf(singleExercise);
        if(index < 0) {
            mSingleExerciseList.add(singleExercise);
            mThumbnailList.add(thumbNail);
        }
        else {
            mSingleExerciseList.set(index,singleExercise);
            mThumbnailList.set(index,thumbNail);
        }

    }
    public void removeSingleExercise(int index){
        mSingleExerciseList.get(index).destroyImages();
        mThumbnailList.remove(index);
        mSingleExerciseList.remove(index);

    }
    public SingleExercise getSingleExercise(int index){
        return mSingleExerciseList.get(index);
    }

    public Bitmap getSingleExerciseThumbnail(int index){
        return mThumbnailList.get(index);
    }

    public List<SingleExercise> getSingleExerciseList() {
        return mSingleExerciseList;
    }
    public int getSingleExerciseSize(){
        return mSingleExerciseList.size();
    }

    public int getMaxUniqueId(){
        int max = 0;
        for (int i=0;i<mSingleExerciseList.size();i++){
            if(mSingleExerciseList.get(i).getUniqueId() > max)max = mSingleExerciseList.get(i).getUniqueId();
        }
        return max+1;
    }
}

