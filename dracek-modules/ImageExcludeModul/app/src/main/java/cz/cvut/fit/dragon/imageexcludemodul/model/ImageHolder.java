package cz.cvut.fit.dragon.imageexcludemodul.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;

import cz.cvut.fit.dragon.imageexcludemodul.R;
import cz.cvut.fit.dragon.imageexcludemodul.editor.ImageManager;
import cz.cvut.fit.dragon.corelibrary.util.IO;
import cz.cvut.fit.dragon.corelibrary.util.Utilities;

public class ImageHolder  {

    Image image;
    Rect rect;
    Rect defaultRect;
    Bitmap bitmap;
    boolean set;
    boolean removed;
    Context context;
    int singleExerciseUniqueId;
    int type;
    Paint paint;
    Paint paintImage;

    public ImageHolder(Context context,Image image,boolean imageExist,int singleExerciseUniqueId,int type){
        this.image = image;
        this.context = context;
        this.type = type;
        set = imageExist;
        removed = false;
        this.singleExerciseUniqueId = singleExerciseUniqueId;
        paint = new Paint();
        paint.setColor(ContextCompat.getColor(context,R.color.colorBackgroundDark));
        paintImage = new Paint();
        paintImage.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER));
    }

    public void draw(Canvas canvas){

        if(set && rect != null && !removed) {
            canvas.drawBitmap(bitmap, rect.left, rect.top, null);
            if(image.isSpecial() && type == ImageManager.EDITOR) {

                Bitmap selected = BitmapFactory.decodeResource(context.getResources(), R.drawable.special);
                Bitmap selectedScaled = Bitmap.createScaledBitmap(selected, rect.width(), rect.height(), true);
                selectedScaled.setHasAlpha(true);

                canvas.drawBitmap(selectedScaled, rect.left, rect.top, paintImage);
            }
        }
        else if(rect != null && !removed)
            canvas.drawRect(rect, paint);
    }

    public void setRect(Rect rect){
        this.rect = new Rect(rect);
        this.defaultRect = new Rect(rect);

        if(set){
            loadImage();
        }
    }

    public Rect getRect() {
        return rect;
    }

    public void loadImage(){
        Bitmap tmp = Utilities.loadBitmapFromFile(IO.getImageFile(singleExerciseUniqueId,image.getUniqueId()), rect.width(), rect.height());
        bitmap = Bitmap.createScaledBitmap(tmp, rect.width(), rect.height(), true);
        set = true;
    }
    public void putBack(){
        this.rect = new Rect(defaultRect);
    }

    public void offSetRectMoving(int dx,int dy){
        rect.offset(dx,dy);
    }

    public boolean isSet(){
        return set;
    }

    public Image getImage() {
        return image;
    }

    public boolean isRemoved() {
        return removed;
    }

    public void setRemoved(boolean removed) {
        this.removed = removed;
    }
}
