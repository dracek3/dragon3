package cz.cvut.fit.dragon.imageexcludemodul.editor;

import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;

import cz.cvut.fit.dragon.imageexcludemodul.R;
import cz.cvut.fit.dragon.imageexcludemodul.model.BaseView;
import cz.cvut.fit.dragon.imageexcludemodul.model.SingleExercise;

public class SingleEditorView extends BaseView {

    int mImageNumberChangeSize;
    int mImageNumberChangePaddingHorizontal;

    Rect mAreaPlusButton;

    ImageManager mImageManager;

    Bitmap mPlusButtonBitmap;

    SingleExercise mSingleExercise;

    FragmentManager mFragmentManager;

    public SingleEditorView(Context context,FragmentManager fragmentManager,SingleExercise singleExercise) {
        super(context);
        this.mFragmentManager = fragmentManager;
        this.mSingleExercise = singleExercise;

       mImagesVerticalSpace = getResources().getDimensionPixelSize(R.dimen.view_images_vertical_space_editor);
        mImageNumberChangePaddingHorizontal = getResources().getDimensionPixelSize(R.dimen.single_editor_image_number_change_horizontal_padding);

       mImageManager = new ImageManager(context,fragmentManager,this,mSingleExercise,ImageManager.EDITOR);
        mPlusButtonBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.plus);

    }

    private void resizeView(int width,int height) {

        int areaHeight = (height - mPaddingTop - mPaddingBottom - mImagesVerticalSpace) /2;

        mImageNumberChangeSize = areaHeight / 2;

        int areaRight = width - mPaddingRight - mImageNumberChangeSize - mImageNumberChangePaddingHorizontal;

        mAreaTop = new Rect(mPaddingLeft,mPaddingTop,areaRight ,mPaddingTop + areaHeight);
        mAreaBottom = new Rect(mPaddingLeft,mAreaTop.bottom+mImagesVerticalSpace,areaRight,height-mPaddingBottom);

        int imageNumberChangeSizeOffsetY = (mAreaTop.height() - mImageNumberChangeSize) / 2;

        mAreaPlusButton = new Rect(mAreaTop.right + mImageNumberChangePaddingHorizontal,mAreaTop.top + imageNumberChangeSizeOffsetY,
                width-mPaddingRight,mAreaTop.top + imageNumberChangeSizeOffsetY + mImageNumberChangeSize);

        resizeBin();
        mImageManager.resizeImages(mAreaTop);

        mPlusButtonBitmap = Bitmap.createScaledBitmap(mPlusButtonBitmap, mAreaPlusButton.width(), mAreaPlusButton.height(), true);
    }

    boolean isSingleExerciseValid()
    {
        return mImageManager.imagesValid();
    }

    protected Bitmap getWholeScreen(){
        Bitmap bitmap = Bitmap.createBitmap(getWidth(),getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawAll(canvas,false);
        return bitmap;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        resizeView(w, h);
        invalidate();
    }
    protected void drawAll(Canvas canvas,boolean drawPlusButton){
        if(mAreaPlusButton != null && drawPlusButton){
            canvas.drawBitmap(mPlusButtonBitmap, mAreaPlusButton.left, mAreaPlusButton.top, null);
        }
        if(mAreaBin != null){
            canvas.drawBitmap(mBinBitmap, mAreaBin.left, mAreaBin.top, null);
        }
        if(mImageManager != null){
            mImageManager.draw(canvas);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawAll(canvas,true);
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        final int x = (int) event.getX();
        final int y = (int) event.getY();

        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            touchDown(x,y);
            invalidate();
        }
        return true;
    }

    private void touchDown(int x, int y){
        if (mAreaPlusButton.contains(x, y)) {
            mImageManager.addImage();
            mImageManager.resizeImages(mAreaTop);
        }
        else if (mAreaTop.contains(x, y)){
            mImageManager.touchDown(x,y);
            mImageManager.resizeImages(mAreaTop);
        }
    }
}
