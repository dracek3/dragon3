package cz.cvut.fit.dragon.imageexcludemodul.editor;

import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.cvut.fit.dragon.imageexcludemodul.R;
import cz.cvut.fit.dragon.imageexcludemodul.model.Image;
import cz.cvut.fit.dragon.imageexcludemodul.model.ImageHolder;
import cz.cvut.fit.dragon.imageexcludemodul.model.SingleExercise;
import cz.cvut.fit.dragon.corelibrary.util.IO;
import cz.cvut.fit.dragon.corelibrary.util.InfoDialog;
import cz.cvut.fit.dragon.corelibrary.util.Size;
import cz.cvut.fit.dragon.corelibrary.util.Utilities;

/**
 * This method is responsible for drawing images and managing user input
 */
public class ImageManager implements ImageEditDialog.OnResultListener {

    public final static int NO_MOVE = -1;
    public final static int RESULT_MISTAKE =8;
    public final static int RESULT_SUCCESS = 9;
    public final static int RESULT_NONE = 10;
    public static final int EDITOR = 11;
    public static final int GAME = 12;
    public final static int DIALOG_CHANGE_ONLY= 14;
    public final static int DIALOG_FULL = 15;
    public final static int MAX_IMAGES = 12;

    private Rect mArea;
    protected int mType;
    protected Context mContext;
    protected FragmentManager mFragmentManager;
    protected int mImagePadding;

    List<ImageHolder> mImageHolderList;

    View mSingleEditorView;
    SingleExercise mSingleExercise;
    int mSelectedImageId;


    public ImageManager(Context context,FragmentManager fragmentManager,View singleEditorView,
                        SingleExercise singleExercise,int type){
        this.mContext = context;
        this.mFragmentManager = fragmentManager;
        this.mSingleEditorView = singleEditorView;
        this.mSingleExercise = singleExercise;
        this.mType = type;

        mImagePadding = context.getResources().getDimensionPixelSize(R.dimen.view_images_horizontal_space);
        initImages();
    }
    /**
     * This method initializes images from exercise
     */
    protected void initImages(){
        mImageHolderList = new ArrayList<>();

        if(mSingleExercise.getImageList().size() == 0){
            addImage();
            addImage();
            addImage();
        }
        else {
            for(int i=0;i<mSingleExercise.getImageList().size();i++){
                ImageHolder image = new ImageHolder(mContext,mSingleExercise.getImageList().get(i),true,mSingleExercise.getUniqueId(),mType);
                mImageHolderList.add(image);
            }
        }
    }
    public void resizeImages(Rect area){

        mArea = area;
        int maxWidthImage = (area.width() - (mImagePadding * (mImageHolderList.size() -1))) / mImageHolderList.size();
        int maxHeightImage = area.height();

        int imageSize;
        if(maxWidthImage > maxHeightImage)imageSize = maxHeightImage;
        else imageSize = maxWidthImage;

        int imagesWidth = (mImageHolderList.size() * imageSize) + ((mImageHolderList.size() -1) * mImagePadding);
        int imagesHeight = imageSize;

        int offsetX = (area.width()  - imagesWidth) / 2;
        int offsetY = (area.height() - imagesHeight) / 2;

        for(int i=0;i<mImageHolderList.size();i++){
            int left = offsetX + (i*imageSize) + (i*mImagePadding);
            Rect rect = new Rect(area.left + left, area.top + offsetY,area.left + left+imageSize,area.top + offsetY+imageSize);
            mImageHolderList.get(i).setRect(rect);
        }
        mSingleEditorView.invalidate();
    }
    public void draw(Canvas canvas){

        for(int i=0;i<mImageHolderList.size();i++) {
            mImageHolderList.get(i).draw(canvas);
        }
    }
    public void addImage(){
        if(mImageHolderList.size() < MAX_IMAGES) {
            int maxUniqueId = 0;
            for (int i = 0; i < mImageHolderList.size(); i++) {
                if (mImageHolderList.get(i).getImage().getUniqueId() > maxUniqueId)
                    maxUniqueId = mImageHolderList.get(i).getImage().getUniqueId();
            }
            maxUniqueId++;

            Image image = new Image(maxUniqueId);
            ImageHolder imageHolder = new ImageHolder(mContext, image, false, mSingleExercise.getUniqueId(), mType);
            mSingleExercise.getImageList().add(image);
            mImageHolderList.add(imageHolder);
        }
        else Toast.makeText(mContext,mContext.getString(R.string.images_max) + " " + MAX_IMAGES,Toast.LENGTH_LONG).show();
    }
    public void touchDown(int x,int y){

        for (int i=0;i<mImageHolderList.size();i++){
            if(mImageHolderList.get(i).getRect().contains(x,y)){
                showImageEditDialog(i);
            }
        }
    }

    public void touchDownMove(int x,int y){
        for (int i=0;i<mImageHolderList.size();i++){
            if(mImageHolderList.get(i).getRect().contains(x,y)){
                mSelectedImageId = i;
            }
        }
    }
    public void touchMove(int offsetX,int offsetY){
        if(mSelectedImageId != NO_MOVE)
            mImageHolderList.get(mSelectedImageId).offSetRectMoving(offsetX, offsetY);
    }
    public int touchUp(Rect binArea) {

        int result = RESULT_NONE;

        if (mSelectedImageId != NO_MOVE) {
            if (binArea.contains(mImageHolderList.get(mSelectedImageId).getRect()) && mImageHolderList.get(mSelectedImageId).getImage().isSpecial()) {
                mImageHolderList.get(mSelectedImageId).setRemoved(true);
                boolean res = allSpecialRemoved();
                if(res) result = RESULT_SUCCESS;
            }
            else if (binArea.contains(mImageHolderList.get(mSelectedImageId).getRect()) && !mImageHolderList.get(mSelectedImageId).getImage().isSpecial()) {
                mImageHolderList.get(mSelectedImageId).putBack();
                result = RESULT_MISTAKE;
            } else {
                mImageHolderList.get(mSelectedImageId).putBack();
                result = RESULT_NONE;
            }
        }
        mSelectedImageId = NO_MOVE;
        return result;
    }

    /**
     * Checks if all special images were removed
     * @return True if user moved all special images to bin. False otherwise.
     */
    private boolean allSpecialRemoved(){
       boolean allRemoved = true;
       for(int i=0;i<mImageHolderList.size();i++){
           if(!mImageHolderList.get(i).isRemoved() && mImageHolderList.get(i).getImage().isSpecial()){
               allRemoved = false;
           }
       }
        return allRemoved;
    }

    /**
     * This method shows dialog that lets user choose operation with image
     * @param imageId Image that will be edited
     */
    public void showImageEditDialog(int imageId){
        if(mFragmentManager != null) {
            Bundle dataBundle = new Bundle();

            ImageEditDialog imageEditDialog = new ImageEditDialog();

            if (mImageHolderList.size() == 1) dataBundle.putInt(ImageEditDialog.DIALOG_TYPE, DIALOG_CHANGE_ONLY);
            else dataBundle.putInt(ImageEditDialog.DIALOG_TYPE, DIALOG_FULL);

            dataBundle.putInt(ImageEditDialog.IMAGE_ID, imageId);
                    dataBundle.putBoolean(ImageEditDialog.IMAGE_SELECTED, mImageHolderList.get(imageId).isSet());
            dataBundle.putBoolean(ImageEditDialog.IMAGE_SPECIAL, mImageHolderList.get(imageId).getImage().isSpecial());

            imageEditDialog.setArguments(dataBundle);
            imageEditDialog.setOnResultListener(this);
            imageEditDialog.show(mFragmentManager, "imageEditDialog");
        }
    }
    /**
     * Checks if single exercise is valid. If not dialog with error message is shown
     * @return true if single exercise is valid. Otherwise returns false.
     */
    public boolean imagesValid(){
        int normalValid = 0;
        int specialValid = 0;

        int nonValid = 0;

        for(int i=0;i<mImageHolderList.size();i++){
            if(mImageHolderList.get(i).isSet()){
                if(mImageHolderList.get(i).getImage().isSpecial())specialValid++;
                else normalValid++;
            }
            else nonValid++;
        }
        if(nonValid == 0) {
            if (specialValid == 0 || normalValid == 0) {
                showInfoMessage(mContext.getString(R.string.one_special_one_normal_requiered));
                return false;
            }
            else {
                List<Image> images = new ArrayList<>();
                for(int i=0;i<mImageHolderList.size();i++){
                    images.add(mImageHolderList.get(i).getImage());
                }
                mSingleExercise.setImageList(images);
                return true;
            }
        }
        else {
            showInfoMessage(mContext.getString(R.string.all_images_required));
            return false;
        }
    }

    @Override
    public void onDelete(int position) {
        if(mImageHolderList.get(position).isSet()){
            IO.getImageFile(mSingleExercise.getUniqueId(),mImageHolderList.get(position).getImage().getUniqueId()).delete();
        }
        mImageHolderList.remove(position);
        mSingleExercise.getImageList().remove(position);
        if(mArea != null)resizeImages(mArea);
    }

    /**
     * This method loads new image from game folder and scale the image so it fits
     */
    private void loadImage(String imageFilePath,int imagePosition){
        File savedImageFile = IO.getImageFile(mSingleExercise.getUniqueId(), mSingleExercise.getImageList().get(imagePosition).getUniqueId());

        File loadedImageFile = new File(imageFilePath);
        Size imageSize = Utilities.getImageSize(loadedImageFile);

        if(imageSize.getWidth() ==  0 || imageSize.getHeight() == 0){
            showInfoMessage(mContext.getString(R.string.not_zero_image_size));
        }
        else if (imageSize.getWidth() != imageSize.getHeight()){
            showInfoMessage(mContext.getString(R.string.square_image_required));
        }
        else {
            Size scaledSize = Utilities.scaleSize(imageSize, new Size(IO.MAX_IMAGE_SIZE, IO.MAX_IMAGE_SIZE));
            Bitmap scaledBitmap = Utilities.loadBitmapFromFile(loadedImageFile, scaledSize.getWidth(), scaledSize.getHeight());

            try {
                Utilities.saveBitmapToFile(savedImageFile,scaledBitmap);
                mImageHolderList.get(imagePosition).getImage().setSpecial(false);
                mImageHolderList.get(imagePosition).loadImage();

            } catch (IOException e) {
                showInfoMessage(mContext.getString(R.string.load_image_failed));
            }
        }
    }

    @Override
    public void onLoaded(int imagePosition,String imageFilePath) {
        if(imageFilePath != null) {
            loadImage(imageFilePath, imagePosition);
        }
        else showInfoMessage(mContext.getString(R.string.load_image_failed));

    }

    @Override
    public void onSpecialChanged(int imageNumber, boolean isSpecial) {
        mImageHolderList.get(imageNumber).getImage().setSpecial(isSpecial);
        mSingleEditorView.invalidate();
    }

    /**
     * This method shows simple dialog with message
     * @param message
     */
    private void showInfoMessage(String message){
        InfoDialog infoDialog = new InfoDialog();
        Bundle data = new Bundle();
        data.putString(InfoDialog.MESSAGE,message);
        infoDialog.setArguments(data);
        infoDialog.show(mFragmentManager, "infoDialog");
    }
}
