package cz.cvut.fit.dragon.imageexcludemodul.game;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import cz.cvut.fit.dragon.corelibrary.model.Time;
import cz.cvut.fit.dragon.imageexcludemodul.R;
import cz.cvut.fit.dragon.imageexcludemodul.model.BaseActivity;
import cz.cvut.fit.dragon.corelibrary.model.ResultWriter;
import cz.cvut.fit.dragon.corelibrary.util.ConfirmDialog;

public class GameActivity extends BaseActivity implements View.OnClickListener,GameView.NoticeEventListener {

    GameView mGameView;
    LinearLayout mGameViewLinearLayout;
    ImageButton mCloseGameButton;
    TextView mExerciseNamesTextView,mExerciseTaskTextView;

    Time mTime;

    private ExerciseManager mExerciseManager;

    protected void init() {
        Log.v("info", "in init");
        String s  = this.getPackageName();
        setContentView(R.layout.activity_game);
        mCloseGameButton = (ImageButton)findViewById(R.id.close_game_button);
        mExerciseNamesTextView = (TextView)findViewById(R.id.exercise_names_text_view);
        mExerciseTaskTextView = (TextView)findViewById(R.id.exercise_task_text_view);

        mExerciseManager = new ExerciseManager(mExercise);

        mGameView = new GameView(GameActivity.this,mExerciseManager,mExerciseNamesTextView,mExerciseTaskTextView);
        mGameView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mGameViewLinearLayout = (LinearLayout)findViewById(R.id.game_view_linear_layout);
        mGameViewLinearLayout.addView(mGameView);

        mCloseGameButton.setOnClickListener(this);
        mGameView.setNoticeEventListener(this);

        mTime = new Time();
        mTime.initTime();
    }

    protected void noExerciseProvided() {
        showMessage(getString(R.string.load_exercise_failed), true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTime.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mTime.pause();
    }


    @Override
    public void onBackPressed() {
        closeGame();
    }

    @Override
    public void onClick(View v) {
        if(v == mCloseGameButton){
            closeGame();
        }
    }

    private void closeGame(){
        ConfirmDialog confirmDialog = new ConfirmDialog();
        Bundle data = new Bundle();
        data.putString(MESSAGE, getString(R.string.game_close_confirm));
        confirmDialog.setArguments(data);
        confirmDialog.setNoticeDialogListener(new ConfirmDialog.NoticeDialogListener() {
            @Override
            public void onDialogPositive(DialogFragment dialog) {
                finish();
            }

            @Override
            public void onDialogNegative(DialogFragment dialog) {

            }
        });
        confirmDialog.show(getFragmentManager(), "confirmDialogGame");
    }

    @Override
    public void onExerciseDone() {
        String xmlResults = "";
        try {
            ResultWriter resultWriter = new ResultWriter(mTime.getTimeSec(),mExerciseManager.getResultsPercentage(), getString(R.string.result_time_unit),getString(R.string.result_score_unit));
            xmlResults = resultWriter.getXmlResultString();

        } catch (IOException e) {
            removeAllImages();
            setResult(RESULT_CANCELED);
            finish();
        }

        removeAllImages();
        Intent resultIntent = new Intent();
        resultIntent.putExtra(RESULTS, xmlResults);
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    private void removeAllImages(){
        ArrayList<String> imageFilePaths = new ArrayList<>();

        for(int i=0;i<mExercise.getSingleExerciseSize();i++){
            imageFilePaths.addAll(mExercise.getSingleExercise(i).getAllImageFilePaths());
        }

        for(int i=0;i<imageFilePaths.size();i++){
            File file = new File(imageFilePaths.get(i));
            file.delete();
        }
    }
}
