package cz.cvut.fit.dragon.imageexcludemodul.game;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.widget.TextView;

import cz.cvut.fit.dragon.imageexcludemodul.R;
import cz.cvut.fit.dragon.imageexcludemodul.editor.ImageManager;
import cz.cvut.fit.dragon.imageexcludemodul.model.BaseView;
import cz.cvut.fit.dragon.imageexcludemodul.model.Result;
import cz.cvut.fit.dragon.imageexcludemodul.model.SingleExercise;

public class GameView extends BaseView {

    protected Context mContext;
    ExerciseManager mExerciseManager;
    ImageManager mImageManager;
    private SingleExercise mCurrentSingleExercise;
    TextView mCurrentSingleExerciseNameTextView;
    TextView mCurrentSingleExerciseTaskTextView;

    private int mLastX;
    private int mLastY;
    private int firstPressedId;
    private final static int FINGER_NOT_PRESSED = -13;

    public GameView(Context context,ExerciseManager exerciseManager,TextView currentSingleExerciseNameTextView, TextView currentSingleExerciseTaskTextView) {
        super(context);
        this.mContext = context;
        this.mExerciseManager = exerciseManager;
        this.mCurrentSingleExerciseNameTextView = currentSingleExerciseNameTextView;
        this.mCurrentSingleExerciseTaskTextView = currentSingleExerciseTaskTextView;

        mImagesVerticalSpace = getResources().getDimensionPixelSize(R.dimen.view_images_vertical_space);
        mBinBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.trash_can);
        mCurrentSingleExercise = mExerciseManager.getCurrentSingleExercise();
        firstPressedId = FINGER_NOT_PRESSED;

        setSingleExercise();
    }

    private void setSingleExercise(){
        mImageManager = new ImageManager(mContext,null,this, mCurrentSingleExercise,ImageManager.GAME);
        if(mAreaTop != null) mImageManager.resizeImages(mAreaTop);

        mCurrentSingleExerciseNameTextView.setText(mCurrentSingleExercise.getName());
        mCurrentSingleExerciseTaskTextView.setText(mCurrentSingleExercise.getTask());
    }

    private void resizeView(int width,int height) {

        int areaHeight = (height - mPaddingTop - mPaddingBottom - mImagesVerticalSpace) /2;

        mAreaTop = new Rect(mPaddingLeft,mPaddingTop,width- mPaddingRight ,mPaddingTop + areaHeight - mBinSizeDifference);
        mAreaBottom = new Rect(mPaddingLeft,mAreaTop.bottom+mImagesVerticalSpace,width-mPaddingRight,height-mPaddingBottom);

        resizeBin();
        mImageManager.resizeImages(mAreaTop);
        invalidate();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        resizeView(w, h);
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(mImageManager != null)
            mImageManager.draw(canvas);

        if(mAreaBin != null){
            canvas.drawBitmap(mBinBitmap, mAreaBin.left, mAreaBin.top, null);
        }
    }

    void touchDown(int x,int y){
        if (mAreaTop.contains(x, y)){
            mImageManager.touchDownMove(x, y);
            mLastX = x;
            mLastY = y;
        }
    }
    void touchMove(int x,int y){
        int dx = x - mLastX;
        int dy = y - mLastY;
        mImageManager.touchMove(dx,dy);
        mLastX = x;
        mLastY = y;
    }
    void touchUp(){

        int result = mImageManager.touchUp(mAreaBin);
        if(result == ImageManager.RESULT_SUCCESS){
            singleExerciseFinished();
        }
        else if(result == ImageManager.RESULT_MISTAKE){
            mExerciseManager.getCurrentResult().addWrongMove();
        }
    }
    void singleExerciseFinished(){
        if(mExerciseManager.nextSingleExercise()){
            mCurrentSingleExercise = mExerciseManager.getCurrentSingleExercise();
            setSingleExercise();
        }
        else {
            if(noticeEventListener != null)
                noticeEventListener.onExerciseDone();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        final int x = (int) event.getX();
        final int y = (int) event.getY();

        int action = event.getActionMasked();
        int index = event.getActionIndex();
        int id = event.getPointerId(index);

        if(action == MotionEvent.ACTION_DOWN) {
            if(firstPressedId == FINGER_NOT_PRESSED){
                touchDown(x,y);
                invalidate();
                firstPressedId = id;
            }
        }
        else if(action == MotionEvent.ACTION_MOVE){
            if(id == firstPressedId) {
                touchMove(x, y);
                invalidate();
            }
        }
        else if(action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_POINTER_UP){
            if(id == firstPressedId){
                touchUp();
                invalidate();
                firstPressedId = FINGER_NOT_PRESSED;
            }
        }
        return true;
    }

    NoticeEventListener noticeEventListener;

    public interface NoticeEventListener{
        void onExerciseDone();
    }
    public void setNoticeEventListener(NoticeEventListener noticeEventListener){
        this.noticeEventListener = noticeEventListener;

    }
}
