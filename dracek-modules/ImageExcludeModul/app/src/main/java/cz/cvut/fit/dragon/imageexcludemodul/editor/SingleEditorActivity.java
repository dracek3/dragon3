package cz.cvut.fit.dragon.imageexcludemodul.editor;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import java.io.IOException;

import cz.cvut.fit.dragon.imageexcludemodul.R;
import cz.cvut.fit.dragon.imageexcludemodul.model.SingleExercise;
import cz.cvut.fit.dragon.corelibrary.util.IO;
import cz.cvut.fit.dragon.corelibrary.util.InfoDialog;
import cz.cvut.fit.dragon.corelibrary.util.Size;
import cz.cvut.fit.dragon.corelibrary.util.Utilities;

public class SingleEditorActivity extends Activity implements View.OnClickListener {

    private Button mSaveButton, mCancelButton;

    EditText mSingleExerciseNameInput,mSingleExerciseTaskInput;
    LinearLayout mSingleExerciseLinearLayout;

    public final static int CREATE = 105;
    public final static int EDIT = 106;
    int mType;

    private SingleEditorView mSingleEditorView;
    private SingleExercise mSingleExercise;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_editor);

        init();
        initSingleExercise(getIntent());

    }
    private void init(){
        mSaveButton = (Button)findViewById(R.id.save_single_exercise_button);
        mCancelButton = (Button)findViewById(R.id.exit_without_save_editor_button);
        mSingleExerciseNameInput = (EditText)findViewById(R.id.single_exercise_name_input);
        mSingleExerciseTaskInput = (EditText)findViewById(R.id.single_exercise_task_input);
        mSingleExerciseLinearLayout = (LinearLayout)findViewById(R.id.single_exercise_linear_layout);

        mSaveButton.setOnClickListener(this);
        mCancelButton.setOnClickListener(this);
    }
    private void initSingleExercise(Intent intent){
        if(intent.hasExtra(EditorActivity.SINGLE_EXERCISE)){
            mSingleExercise = getIntent().getParcelableExtra(EditorActivity.SINGLE_EXERCISE);

            mSingleExerciseNameInput.setText(mSingleExercise.getName());
            mSingleExerciseTaskInput.setText(mSingleExercise.getTask());

            mCancelButton.setVisibility(View.GONE);
            mSaveButton.setText(getString(R.string.save_single_exercise));
            mType= EDIT;

        }
        else if(intent.hasExtra(EditorActivity.SINGLE_EXERCISE_UNIQUE_ID)) {
           int uniqueId = getIntent().getIntExtra(EditorActivity.SINGLE_EXERCISE_UNIQUE_ID, 0);

            mSingleExercise = new SingleExercise();
            mSingleExercise.setUniqueId(uniqueId);

            mSaveButton.setText(getString(R.string.create_single_exercise));
            mType=CREATE;

        }
        mSingleEditorView = new SingleEditorView(SingleEditorActivity.this,getFragmentManager(),mSingleExercise);
        mSingleExerciseLinearLayout.addView(mSingleEditorView);
    }
    @Override
    public void onClick(View v) {
        if(v == mSaveButton){
            if(mSingleEditorView.isSingleExerciseValid()){
                save();
            }
        }
        else if(v == mCancelButton){
            setResult(RESULT_CANCELED);
            finish();
        }
    }
    private void showInfoMessage(String message){
        InfoDialog infoDialog = new InfoDialog();
        Bundle data = new Bundle();
        data.putString(InfoDialog.MESSAGE,message);
        infoDialog.setArguments(data);
        infoDialog.show(getFragmentManager(), "infoDialog");
    }

    private void save(){
        Bitmap thumbnail = mSingleEditorView.getWholeScreen();
        Size scaledSize =  Utilities.scaleSize(new Size(thumbnail.getWidth(), thumbnail.getHeight()), new Size(IO.MAX_THUMB_SIZE, IO.MAX_THUMB_SIZE));
        Bitmap scaledThumbBitmap = Bitmap.createScaledBitmap(thumbnail,scaledSize.getWidth(),scaledSize.getHeight(),true);

        try {
            Utilities.saveBitmapToFile(IO.getThumbnailFile(mSingleExercise.getUniqueId()),scaledThumbBitmap);
            mSingleExercise.setName(mSingleExerciseNameInput.getText().toString());
            mSingleExercise.setTask(mSingleExerciseTaskInput.getText().toString());

            Intent resultIntent = new Intent();
            resultIntent.putExtra(EditorActivity.SINGLE_EXERCISE, mSingleExercise);
            setResult(Activity.RESULT_OK, resultIntent);
            finish();
        } catch (IOException e) {
            showInfoMessage(getString(R.string.save_exercise_error));
        }
    }

    @Override
    public void onBackPressed() {
        if(mType == EDIT) {
            if(mSingleEditorView.isSingleExerciseValid())
                save();
        }
        else if(mType == CREATE){
            setResult(RESULT_CANCELED);
            finish();
        }
    }
}
