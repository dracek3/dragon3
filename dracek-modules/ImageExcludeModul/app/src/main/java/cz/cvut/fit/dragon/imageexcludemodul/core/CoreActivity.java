package cz.cvut.fit.dragon.imageexcludemodul.core;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import cz.cvut.fit.dragon.corelibrary.util.InfoDialog;
import cz.cvut.fit.dragon.corelibrary.util.Utilities;
import cz.cvut.fit.dragon.imageexcludemodul.R;
import cz.cvut.fit.dragon.imageexcludemodul.editor.EditorActivity;
import cz.cvut.fit.dragon.imageexcludemodul.game.GameActivity;


public class CoreActivity extends Activity implements View.OnClickListener {

    Button runTest,run;
    Button edit;
    Button create;

    String xml;
    ArrayList<String>  paths;

    final static int EDITOR = 100;
    final static int GAME = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_core);

        runTest = (Button)findViewById(R.id.run_test);
        run = (Button)findViewById(R.id.run);
        edit = (Button)findViewById(R.id.edit);
        create = (Button)findViewById(R.id.create);

        runTest.setOnClickListener(this);
        run.setOnClickListener(this);
        edit.setOnClickListener(this);
        create.setOnClickListener(this);

        edit.setEnabled(false);
        run.setEnabled(false);
    }

    @Override
    public void onClick(View v) {

        if(v == create){
            Intent myIntent = new Intent(CoreActivity.this, EditorActivity.class);
            CoreActivity.this.startActivityForResult(myIntent, EDITOR);
        }
        else if(v == edit){
            Intent myIntent = new Intent(CoreActivity.this, EditorActivity.class);
            myIntent.putExtra("EXERCISE_FILE", xml);
            myIntent.putExtra("IMAGES", paths);
            CoreActivity.this.startActivityForResult(myIntent, EDITOR);
        }
        else if(v == run){
            Intent myIntent = new Intent(CoreActivity.this, GameActivity.class);
            myIntent.putExtra("EXERCISE_FILE", xml);
            myIntent.putExtra("IMAGES", paths);
            CoreActivity.this.startActivityForResult(myIntent, GAME);
        }
        else if(v == runTest) {

            ArrayList<String> as = new ArrayList<>();
            try {
                InputStream bitmap1 = getAssets().open("x1_1.png");
                InputStream bitmap2 = getAssets().open("x1_2.png");
                InputStream bitmap3 = getAssets().open("x1_3.png");
                InputStream bitmap4 = getAssets().open("x1_4.png");
                InputStream bitmap5 = getAssets().open("x1_5.png");
                InputStream bitmap6 = getAssets().open("x1_6.png");

                InputStream bitmap12 = getAssets().open("x2_1.jpg");
                InputStream bitmap22 = getAssets().open("x2_2.jpg");
                InputStream bitmap32 = getAssets().open("x2_3.jpg");
                InputStream bitmap42 = getAssets().open("x2_4.jpg");
                InputStream bitmap52 = getAssets().open("x2_5.jpg");
                InputStream bitmap62 = getAssets().open("x2_6.jpg");

                InputStream bitmap13 = getAssets().open("x3_1.jpg");
                InputStream bitmap23 = getAssets().open("x3_2.jpg");
                InputStream bitmap33 = getAssets().open("x3_3.jpg");
                InputStream bitmap43 = getAssets().open("x3_4.jpg");
                InputStream bitmap53 = getAssets().open("x3_5.jpg");

                Utilities.saveBitmapToFile(new File(getFilesDir(), "image1_1"), BitmapFactory.decodeStream(bitmap1));
                Utilities.saveBitmapToFile(new File(getFilesDir(), "image1_2"), BitmapFactory.decodeStream(bitmap2));
                Utilities.saveBitmapToFile(new File(getFilesDir(), "image1_3"), BitmapFactory.decodeStream(bitmap3));
                Utilities.saveBitmapToFile(new File(getFilesDir(), "image1_4"), BitmapFactory.decodeStream(bitmap4));
                Utilities.saveBitmapToFile(new File(getFilesDir(), "image1_5"), BitmapFactory.decodeStream(bitmap5));
                Utilities.saveBitmapToFile(new File(getFilesDir(), "image1_6"), BitmapFactory.decodeStream(bitmap6));

                Utilities.saveBitmapToFile(new File(getFilesDir(), "image2_1"), BitmapFactory.decodeStream(bitmap12));
                Utilities.saveBitmapToFile(new File(getFilesDir(), "image2_2"), BitmapFactory.decodeStream(bitmap22));
                Utilities.saveBitmapToFile(new File(getFilesDir(), "image2_3"), BitmapFactory.decodeStream(bitmap32));
                Utilities.saveBitmapToFile(new File(getFilesDir(), "image2_4"), BitmapFactory.decodeStream(bitmap42));
                Utilities.saveBitmapToFile(new File(getFilesDir(), "image2_5"), BitmapFactory.decodeStream(bitmap52));
                Utilities.saveBitmapToFile(new File(getFilesDir(), "image2_6"), BitmapFactory.decodeStream(bitmap62));

                Utilities.saveBitmapToFile(new File(getFilesDir(), "image3_1"), BitmapFactory.decodeStream(bitmap13));
                Utilities.saveBitmapToFile(new File(getFilesDir(), "image3_2"), BitmapFactory.decodeStream(bitmap23));
                Utilities.saveBitmapToFile(new File(getFilesDir(), "image3_3"), BitmapFactory.decodeStream(bitmap33));
                Utilities.saveBitmapToFile(new File(getFilesDir(), "image3_4"), BitmapFactory.decodeStream(bitmap43));
                Utilities.saveBitmapToFile(new File(getFilesDir(), "image3_5"), BitmapFactory.decodeStream(bitmap53));

                as.add(new File(getFilesDir(), "image1_1").getPath());
                as.add(new File(getFilesDir(), "image1_2").getPath());
                as.add(new File(getFilesDir(), "image1_3").getPath());
                as.add(new File(getFilesDir(), "image1_4").getPath());
                as.add(new File(getFilesDir(), "image1_5").getPath());
                as.add(new File(getFilesDir(), "image1_6").getPath());

                as.add(new File(getFilesDir(), "image2_1").getPath());
                as.add(new File(getFilesDir(), "image2_2").getPath());
                as.add(new File(getFilesDir(), "image2_3").getPath());
                as.add(new File(getFilesDir(), "image2_4").getPath());
                as.add(new File(getFilesDir(), "image2_5").getPath());
                as.add(new File(getFilesDir(), "image2_6").getPath());

                as.add(new File(getFilesDir(), "image3_1").getPath());
                as.add(new File(getFilesDir(), "image3_2").getPath());
                as.add(new File(getFilesDir(), "image3_3").getPath());
                as.add(new File(getFilesDir(), "image3_4").getPath());
                as.add(new File(getFilesDir(), "image3_5").getPath());

            } catch (IOException e) {
                Log.e("e",e.getMessage());
            }

            String XML = "";
            try {
                StringBuilder sb = new StringBuilder();
                InputStream is = getAssets().open("res.xml");

                BufferedReader in = new BufferedReader(new InputStreamReader(is, "UTF-8"));

                String s;
                while ((s = in.readLine()) != null) {
                    sb.append(s);
                }
                XML = sb.toString();

            } catch (IOException e) {
                e.printStackTrace();
            }

            Log.e("S:Xml:",XML);
            for(String s : as){
                Log.e("S:Image file:",s);
            }

            Intent intent = new Intent(CoreActivity.this, GameActivity.class);
            intent.putExtra("EXERCISE_FILE", XML);
            intent.putExtra("IMAGES", as);
            CoreActivity.this.startActivityForResult(intent, GAME);

            edit.setEnabled(false);
            run.setEnabled(false);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == EDITOR && resultCode == RESULT_OK){
            edit.setEnabled(true);
            run.setEnabled(true);

            xml = data.getStringExtra("EXERCISE_FILE");
            paths = data.getStringArrayListExtra("IMAGES");

            Log.e("Xml:", xml);
            for(String s : paths){
                Log.e("Image file:",s);
            }
        }
        else if(requestCode == GAME && resultCode == RESULT_OK){
            Log.e("Results", data.getStringExtra("RESULTS"));
            //showInfoMessage(data.getStringExtra("RESULTS"));
	        Utilities.showSmiley(Utilities.getResult(data.getStringExtra("RESULTS")), this);
        }

        if(requestCode == GAME) {
            edit.setEnabled(false);
            run.setEnabled(false);
        }
    }
    /**
     * This method shows simple dialog with message
     * @param message
     */
    private void showInfoMessage(String message){
        InfoDialog infoDialog = new InfoDialog();
        Bundle data = new Bundle();
        data.putString(InfoDialog.MESSAGE,message);
        infoDialog.setArguments(data);
        infoDialog.show(getFragmentManager(), "infoDialog");
    }


}