package cz.cvut.fit.dragon.imageexcludemodul.game;

import java.util.Collections;
import java.util.Random;

import cz.cvut.fit.dragon.imageexcludemodul.model.Exercise;
import cz.cvut.fit.dragon.imageexcludemodul.model.Result;
import cz.cvut.fit.dragon.imageexcludemodul.model.SingleExercise;

public class ExerciseManager {

    int currentSingleExercise;
    Exercise exercise;
    Result [] results;

    public ExerciseManager(Exercise exercise) {
        this.exercise = exercise;
        currentSingleExercise = 0;

        for(int i=0;i<exercise.getSingleExerciseSize();i++){
            Collections.shuffle(exercise.getSingleExercise(i).getImageList(),new Random(System.currentTimeMillis()));
        }
        results = new Result[exercise.getSingleExerciseSize()];

        results = new Result[exercise.getSingleExerciseSize()];
        for (int i = 0; i < results.length; i++){

            int special = 0;
            for(int x=0;x<exercise.getSingleExercise(i).getImageList().size();x++){
                if(exercise.getSingleExercise(i).getImageList().get(x).isSpecial())
                    special++;
            }
            results[i] = new Result();
            results[i].setUniqueWrongMoves(exercise.getSingleExercise(i).getImageList().size() - special);
        }
    }

    public boolean nextSingleExercise(){
        if(currentSingleExercise >= exercise.getSingleExerciseSize()-1)
            return false;
        else {
            currentSingleExercise++;
            return true;
        }
    }
    public SingleExercise getCurrentSingleExercise(){
        return exercise.getSingleExercise(currentSingleExercise);
    }
    public Result getCurrentResult(){
        return results[currentSingleExercise];
    }

    public int getResultsPercentage(){
        int percentageSum = 0;
        for (int i=0;i<results.length;i++){
            percentageSum += results[i].getPercentage();
        }
        return percentageSum / results.length;
    }
}
