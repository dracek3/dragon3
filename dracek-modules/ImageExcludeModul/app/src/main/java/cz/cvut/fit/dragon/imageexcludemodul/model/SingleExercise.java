package cz.cvut.fit.dragon.imageexcludemodul.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cz.cvut.fit.dragon.corelibrary.util.IO;

public class SingleExercise implements Parcelable {

    int uniqueId;
    String name;

    String task;
    List<Image> imageList;

    public SingleExercise(){
        imageList = new ArrayList<>();
    }

    protected SingleExercise(Parcel in) {
        uniqueId = in.readInt();
        name = in.readString();
        task = in.readString();
        imageList = in.createTypedArrayList(Image.CREATOR);
    }

    public void destroyImages(){
        File thumbnail = IO.getThumbnailFile(uniqueId);
        thumbnail.delete();
        for(Image image : imageList) {
            File imageFile = IO.getImageFile(uniqueId,image.getUniqueId());
            imageFile.delete();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SingleExercise that = (SingleExercise) o;

        return uniqueId == that.uniqueId;

    }

    @Override
    public int hashCode() {
        return uniqueId;
    }

    public static final Creator<SingleExercise> CREATOR = new Creator<SingleExercise>() {
        @Override
        public SingleExercise createFromParcel(Parcel in) {
            return new SingleExercise(in);
        }

        @Override
        public SingleExercise[] newArray(int size) {
            return new SingleExercise[size];
        }
    };

    public int getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public List<Image> getImageList() {
        return imageList;
    }

    public void setImageList(List<Image> imageList) {
        this.imageList = imageList;
    }

    public ArrayList<String> getAllImageFilePaths(){
        ArrayList<String> imageFilePaths = new ArrayList<>();

        for(Image image : imageList) {
            imageFilePaths.add(IO.getImageFile(uniqueId,image.getUniqueId()).getPath());
        }
        imageFilePaths.add(IO.getThumbnailFile(uniqueId).getPath());

        return imageFilePaths;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(uniqueId);
        dest.writeString(name);
        dest.writeString(task);
        dest.writeTypedList(imageList);
    }
}
