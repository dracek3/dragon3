package cz.cvut.fit.dragon.imageexcludemodul.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.view.View;

import cz.cvut.fit.dragon.imageexcludemodul.R;

public abstract class BaseView extends View {

    protected Context mContext;

    protected Rect mAreaTop,mAreaBottom,mAreaBin;

    protected int mPaddingLeft,mPaddingRight,mPaddingTop,mPaddingBottom;
    protected int mImagesVerticalSpace;
    protected int mBinSizeDifference;

    protected Bitmap mBinBitmap;
    protected Bitmap mBinBitmapOrig;

    public BaseView(Context context){
        super(context);
        this.mContext = context;
        loadDimens();

        mBinBitmapOrig = BitmapFactory.decodeResource(getResources(), R.drawable.trash_can);
        mBinBitmap = mBinBitmapOrig;
    }

    protected void loadDimens(){
        mPaddingLeft = getResources().getDimensionPixelSize(R.dimen.view_left_padding);
        mPaddingRight = getResources().getDimensionPixelSize(R.dimen.view_right_padding);
        mPaddingBottom = getResources().getDimensionPixelSize(R.dimen.view_bottom_padding);
        mPaddingTop = getResources().getDimensionPixelSize(R.dimen.view_top_padding);
        mBinSizeDifference = getResources().getDimensionPixelSize(R.dimen.view_bin_size_difference);
    }

    protected void resizeBin(){
        int binMaxWidth = mAreaBottom.width();
        int binMaxHeight = mAreaBottom.height();

        int binMaxSize;
        if(binMaxWidth > binMaxHeight)binMaxSize = binMaxHeight;
        else binMaxSize = binMaxWidth;

        int binOffsetX = (mAreaBottom.width() - binMaxSize) / 2;
        int binOffsetY = (mAreaBottom.height() - binMaxSize) / 2;

        mAreaBin = new Rect(mAreaBottom.left + binOffsetX,
                mAreaBottom.top + binOffsetY,
                mAreaBottom.left + binOffsetX + binMaxSize,
                mAreaBottom.top + binOffsetY + binMaxSize);

        mBinBitmap = Bitmap.createScaledBitmap(mBinBitmapOrig,mAreaBin.width(),mAreaBin.height(),true);
    }
}
