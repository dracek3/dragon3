package cz.cvut.fit.dragon.imageordermodul.model;

import android.util.Xml;

import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import cz.cvut.fit.dragon.corelibrary.util.XmlTag;


/**
 * Created by bury on 4/14/2016.
 */
public class ExerciseWriter {

    Exercise mExercise;

    public ExerciseWriter(Exercise exercise){
        this.mExercise = exercise;
    }

    public String getExerciseOutputXmlString() throws IOException{
        XmlSerializer xmlSerializer = Xml.newSerializer();
        ByteArrayOutputStream output =  new ByteArrayOutputStream();
        xmlSerializer.setOutput(output, "UTF-8");
        writeXml(xmlSerializer,mExercise);
        return output.toString("UTF-8");
    }
    private void writeXml(XmlSerializer xmlSerializer,Exercise exercise) throws IOException {
        xmlSerializer.startDocument("UTF-8", true);

        xmlSerializer.startTag("", XmlTag.EXERCISE);
        for(int i=0;i<exercise.getSingleExerciseList().size();i++){
            SingleExercise singleExercise = exercise.getSingleExerciseList().get(i);

            xmlSerializer.startTag("",XmlTag.SINGLE_EXERCISE);

            xmlSerializer.startTag("",XmlTag.ID);
            xmlSerializer.text(String.valueOf(singleExercise.getUniqueId()));
            xmlSerializer.endTag("",XmlTag.ID);

            xmlSerializer.startTag("", XmlTag.NAME);
            xmlSerializer.text(singleExercise.getName());
            xmlSerializer.endTag("", XmlTag.NAME);

            xmlSerializer.startTag("",XmlTag.SPLITS_X);
            xmlSerializer.text(String.valueOf(singleExercise.getSplitsX()));
            xmlSerializer.endTag("",XmlTag.SPLITS_X);

            xmlSerializer.startTag("",XmlTag.SPLITS_Y);
            xmlSerializer.text(String.valueOf(singleExercise.getSplitsY()));
            xmlSerializer.endTag("",XmlTag.SPLITS_Y);

            xmlSerializer.endTag("",XmlTag.SINGLE_EXERCISE);
        }
        xmlSerializer.endTag("", XmlTag.EXERCISE);
        xmlSerializer.flush();
    }
}
