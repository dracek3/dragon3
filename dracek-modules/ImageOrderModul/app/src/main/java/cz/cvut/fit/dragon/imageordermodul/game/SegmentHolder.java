package cz.cvut.fit.dragon.imageordermodul.game;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;


public class SegmentHolder {

    Segment segment;
    Rect origRect;
    Rect rect;

    public SegmentHolder(){
    }

    public SegmentHolder(Segment segment) {
        this.segment = segment;
    }

    public void setRect(Rect rect){
        this.origRect = rect;
        this.rect = new Rect(rect);
    }
    public void setBitmap(Bitmap bitmap){
        segment.setBitmap(bitmap);
    }

    public void setSegment(Segment segment) {
        this.segment = segment;
    }

    public Segment getSegment() {
        return segment;
    }

    public boolean isImageSet(){
        return (segment != null);
    }

    public void draw(Canvas canvas){
        if(segment != null && segment.getBitmap() != null)canvas.drawBitmap(segment.getBitmap(), rect.left, rect.top, null);
    }
    public void offsetRect(int dx,int dy){
        rect.offset(dx,dy);
    }
    public void back(){
        this.rect = new Rect(origRect);
    }
}
