package cz.cvut.fit.dragon.imageordermodul.game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cz.cvut.fit.dragon.imageordermodul.R;
import cz.cvut.fit.dragon.imageordermodul.model.SingleExercise;
import cz.cvut.fit.dragon.corelibrary.util.IO;
import cz.cvut.fit.dragon.corelibrary.util.Size;
import cz.cvut.fit.dragon.corelibrary.util.Utilities;


public class SegmentManager {

    final static int RESULT_SUCCESS = 9;
    final static int RESULT_NONE = 10;

    Context mContext;
    SingleExercise mSingleExercise;

    SegmentHolder[][] mSegmentsOne;
    SegmentHolder[][] mSegmentsTwo;
    SegmentHolder selectedSegmentHolder;

    Rect mAreaOne;
    Rect mAreaTwo;

    Paint linesPaint;

    public SegmentManager(Context context, SingleExercise singleExercise){
        this.mSingleExercise = singleExercise;
        this.mContext = context;

        linesPaint = new Paint();
        int lineColor = ContextCompat.getColor(context, R.color.splitLinesColor);
        linesPaint.setColor(lineColor);

        mSegmentsOne = new SegmentHolder[mSingleExercise.getSplitsX()][mSingleExercise.getSplitsY()];
        mSegmentsTwo = new SegmentHolder[mSingleExercise.getSplitsX()][mSingleExercise.getSplitsY()];

        for(int i=0;i<mSegmentsOne.length;i++){
            for (int j=0;j<mSegmentsOne[i].length;j++){
                mSegmentsOne[i][j] = new SegmentHolder(new Segment(i,j));
                mSegmentsTwo[i][j] = new SegmentHolder();
            }
        }
        shuffleSegments();
    }
    public void resize(Rect maxAreaOne,Rect maxAreaTwo){

        File imageFile = IO.getImageFile(mSingleExercise.getUniqueId());

        Size imageSize = Utilities.getImageSize(imageFile);

        mAreaOne = fitRectangle(maxAreaOne, imageSize);
        mAreaTwo = fitRectangle(maxAreaTwo, imageSize);

        Bitmap bitmap = Utilities.loadBitmapFromFile(imageFile);
        Bitmap scaled = Bitmap.createScaledBitmap(bitmap, mAreaOne.width(), mAreaOne.height(), true);

        resizeSegments(scaled, mSegmentsOne, mAreaOne);
        resizeSegments(scaled, mSegmentsTwo, mAreaTwo);

    }
    public void resizeSegments(Bitmap bitmap,SegmentHolder[][] segmentHolders,Rect area){

        SegmentHolder[][] segments = segmentHolders;

        for (int x = 0; x < segments.length; x++) {
            for (int y = 0; y < segments[x].length; y++) {

                int imageSegmentWidth = bitmap.getWidth() / mSingleExercise.getSplitsX();
                int imageSegmentHeight = bitmap.getHeight() / mSingleExercise.getSplitsY();

                Rect rect = new Rect(area.left + x * imageSegmentWidth,area.top + y * imageSegmentHeight,
                        area.left + (x* imageSegmentWidth) + imageSegmentWidth,area.top + (y * imageSegmentHeight + imageSegmentHeight));
                segments[x][y].setRect(rect);

                if(segments[x][y].isImageSet()){

                    int imageX = segments[x][y].segment.correctX;
                    int imageY = segments[x][y].segment.correctY;

                    Bitmap segmentBitmap = Bitmap.createBitmap(bitmap, imageX * imageSegmentWidth, imageY * imageSegmentHeight, imageSegmentWidth, imageSegmentHeight);
                    segments[x][y].setBitmap(segmentBitmap);
                }
            }
        }
    }

    Rect fitRectangle(Rect outside, Size imageSize) {
        int rectWidth = outside.right - outside.left;
        int rectHeight = outside.bottom - outside.top;
        int scaledWidth, scaledHeight;


        float widthScale = 0, heightScale = 0;
        if (imageSize.getWidth() != 0)
            widthScale = (float) rectWidth / imageSize.getWidth();
        if (imageSize.getHeight() != 0)
            heightScale = (float) rectHeight / imageSize.getHeight();

        float scale = Math.min(widthScale, heightScale);

        scaledWidth = (int) (imageSize.getWidth() * scale);
        scaledHeight = (int) (imageSize.getHeight() * scale);

        scaledWidth -= scaledWidth % mSingleExercise.getSplitsX(); // every segment has same size
        scaledHeight -= scaledHeight % mSingleExercise.getSplitsY();

        int offsetX = (rectWidth - scaledWidth) /2;
        int offsetY = (rectHeight - scaledHeight) / 2;

        return new Rect(outside.left+offsetX, outside.top+offsetY,
                outside.left+scaledWidth+offsetX, outside.top+scaledHeight+offsetY);
    }
    public void draw(Canvas canvas){

        Paint x = new Paint();
        x.setColor(Color.DKGRAY);
        if(mAreaOne != null)
            canvas.drawRect(mAreaOne,x);
        if(mAreaTwo != null)
            canvas.drawRect(mAreaTwo, x);

       for (int i = 0; i < mSegmentsOne.length; i++) {
            for (int j = 0; j < mSegmentsOne[i].length; j++) {
                if (mSegmentsOne[i][j].isImageSet()) {
                    mSegmentsOne[i][j].draw(canvas);
                }
                if(mSegmentsTwo[i][j].isImageSet())
                    mSegmentsTwo[i][j].draw(canvas);
            }
        }
        drawLines(canvas);
        if(selectedSegmentHolder != null) selectedSegmentHolder.draw(canvas);
    }
    public void drawLines(Canvas canvas){
        int segmentWidth = mAreaOne.width() / mSingleExercise.getSplitsX();
        int segmentHeight = mAreaTwo.height() / mSingleExercise.getSplitsY();

        for (int i=0;i<mSingleExercise.getSplitsX()+1;i++){
            int x = segmentWidth*(i);
            canvas.drawRect(mAreaOne.left+x-1,mAreaOne.top,mAreaOne.left+x+1,mAreaOne.bottom,linesPaint);
            canvas.drawRect(mAreaTwo.left+x-1,mAreaTwo.top,mAreaTwo.left+x+1,mAreaTwo.bottom,linesPaint);

        }
        for (int j=0;j<mSingleExercise.getSplitsY()+1;j++){
            int y = segmentHeight*(j);
            canvas.drawRect(mAreaOne.left,mAreaOne.top + y-1,mAreaOne.right,mAreaTwo.top + y+1,linesPaint);
            canvas.drawRect(mAreaTwo.left,mAreaTwo.top + y-1,mAreaTwo.right,mAreaTwo.top + y+1,linesPaint);
        }
    }

    public void touchDown(int x,int y) {
        for (int i = 0; i < mSegmentsOne.length; i++) {
            for (int j = 0; j < mSegmentsOne[i].length; j++) {

                if (mSegmentsOne[i][j].rect.contains(x, y) && mSegmentsOne[i][j].isImageSet()) {
                    selectedSegmentHolder = mSegmentsOne[i][j];
                }
                if(mSegmentsTwo[i][j].rect.contains(x,y) && mSegmentsTwo[i][j].isImageSet()) {
                    selectedSegmentHolder = mSegmentsTwo[i][j];
                }
            }
        }
    }
    public void touchMove(int offsetX,int offsetY){
        if(selectedSegmentHolder != null)
            selectedSegmentHolder.offsetRect(offsetX,offsetY);
    }

    public int touchUp(){
        if(selectedSegmentHolder == null)return RESULT_NONE;
        for (int i = 0; i < mSegmentsOne.length; i++) {
            for (int j = 0; j < mSegmentsOne[i].length; j++) {

                float overlapOne = overlap(mSegmentsOne[i][j].origRect, selectedSegmentHolder.rect);

                if (overlapOne > 0.75f) {
                    Segment segment = mSegmentsOne[i][j].getSegment();
                    mSegmentsOne[i][j].setSegment(selectedSegmentHolder.getSegment());
                    selectedSegmentHolder.setSegment(segment);
                }

                float overlapTwo = overlap(mSegmentsTwo[i][j].origRect, selectedSegmentHolder.rect);

                if (overlapTwo > 0.75f) {
                    Segment segment = mSegmentsTwo[i][j].getSegment();
                    mSegmentsTwo[i][j].setSegment(selectedSegmentHolder.getSegment());
                    selectedSegmentHolder.setSegment(segment);
                }
            }
        }

        selectedSegmentHolder.back();
        selectedSegmentHolder = null;

        if(checkDone()){
            return RESULT_SUCCESS;
        }
        else return RESULT_NONE;
    }
    boolean checkDone(){

        boolean done = true;
        for (int i = 0; i < mSegmentsTwo.length; i++) {
            for (int j = 0; j < mSegmentsTwo[i].length; j++) {
                if(!mSegmentsTwo[i][j].isImageSet() || mSegmentsTwo[i][j].segment.getCorrectX() != i || mSegmentsTwo[i][j].segment.getCorrectY() != j)
                    done = false;
            }
        }
        return done;
    }

    private float overlap(Rect r, Rect movingSegmentRect) {

        Rect intersectRect = new Rect();
        boolean intersect = intersectRect.setIntersect(r,movingSegmentRect);

        if(intersect){
            int intersectRectSize = intersectRect.width() * intersectRect.height();
            int rSize = r.width() * r.height();

            return intersectRectSize / (float)rSize;
        }
        else return 0;
    }

    public void shuffleSegments(){
        List<SegmentHolder> allSegments = new ArrayList();

        for(int i=0;i<mSegmentsOne.length;i++){
            for (int j=0;j<mSegmentsOne[i].length;j++)
                allSegments.add(mSegmentsOne[i][j]);
        }
        Collections.shuffle(allSegments);

        int index = 0;
        for(int i=0;i<mSegmentsOne.length;i++){
            for (int j=0;j<mSegmentsOne[i].length;j++){
                mSegmentsOne[i][j] = allSegments.get(index);
                index++;
            }
        }
    }
}
