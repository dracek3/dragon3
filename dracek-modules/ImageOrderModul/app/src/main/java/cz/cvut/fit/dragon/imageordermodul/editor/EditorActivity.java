package cz.cvut.fit.dragon.imageordermodul.editor;

import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import cz.cvut.fit.dragon.imageordermodul.R;
import cz.cvut.fit.dragon.imageordermodul.model.BaseActivity;
import cz.cvut.fit.dragon.imageordermodul.model.Exercise;
import cz.cvut.fit.dragon.imageordermodul.model.ExerciseWriter;
import cz.cvut.fit.dragon.imageordermodul.model.SingleExercise;
import cz.cvut.fit.dragon.corelibrary.util.ConfirmDialog;
import cz.cvut.fit.dragon.corelibrary.util.IO;
import cz.cvut.fit.dragon.corelibrary.util.Utilities;


public class EditorActivity extends BaseActivity implements View.OnClickListener {

    public final static int CREATE_SINGLE_EXERCISE = 105;
    public final static int EDIT_SINGLE_EXERCISE = 106;
    public final static String SINGLE_EXERCISE_UNIQUE_ID = "SINGLE_EXERCISE_UNIQUE_ID";

    private ListView mListView;
    private Button mAddSingleExerciseButton;
    private Button mSaveExerciseButton,mCancelExerciseButton;
    ExerciseListAdapter exerciseListAdapter;

    protected void init(){
        setContentView(R.layout.activity_editor);

        mAddSingleExerciseButton = (Button)findViewById(R.id.add_single_exercise_button);
        mSaveExerciseButton = (Button)findViewById(R.id.save_exercise_button);
        mCancelExerciseButton = (Button)findViewById(R.id.cancel_exercise_button);
        mListView = (ListView)findViewById(R.id.images_list_view);

        mAddSingleExerciseButton.setOnClickListener(this);
        mSaveExerciseButton.setOnClickListener(this);
        mCancelExerciseButton.setOnClickListener(this);

        exerciseListAdapter = new ExerciseListAdapter(EditorActivity.this,mExercise);
        mListView.setAdapter(exerciseListAdapter);
    }
    protected void noExerciseProvided() {
        mExercise = new Exercise();
        init();
    }
    private void saveExercise(){
        try {
            ExerciseWriter exerciseWriter = new ExerciseWriter(mExercise);
            String xmlOutput = exerciseWriter.getExerciseOutputXmlString();

            ArrayList<String> imageFilePaths = new ArrayList<>();

            for(int i=0;i<mExercise.getSingleExerciseSize();i++){
                imageFilePaths.addAll(mExercise.getSingleExercise(i).getAllImageFilePaths());
            }

            for(int i=0;i<imageFilePaths.size();i++){
                File file = new File(imageFilePaths.get(i));
                file.setReadable(true,false);
            }

            Intent resultIntent = new Intent();
            resultIntent.putExtra(EXERCISE_FILE, xmlOutput);
            resultIntent.putStringArrayListExtra(IMAGES, imageFilePaths);
            setResult(RESULT_OK, resultIntent);
            finish();
        }
        catch (IOException e) {
            showMessage(getString(R.string.save_exercise_error), false);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent returnedIntent) {
        super.onActivityResult(requestCode, resultCode, returnedIntent);

        if ((requestCode == CREATE_SINGLE_EXERCISE || requestCode == EDIT_SINGLE_EXERCISE) && resultCode == RESULT_OK) {

            SingleExercise singleExercise = returnedIntent.getParcelableExtra(SINGLE_EXERCISE);
            Bitmap thumb = Utilities.loadBitmapFromFile(IO.getThumbnailFile(singleExercise.getUniqueId()));
            mExercise.addSingleExercise(singleExercise,thumb);
            exerciseListAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v) {

        if(v == mAddSingleExerciseButton){
            Intent intent = new Intent(EditorActivity.this, SingleEditorActivity.class);
            intent.putExtra(SINGLE_EXERCISE_UNIQUE_ID,mExercise.getMaxUniqueId());
            EditorActivity.this.startActivityForResult(intent, CREATE_SINGLE_EXERCISE);
        }
        else if(v == mSaveExerciseButton){
            if (mExercise.getSingleExerciseSize() >= 1) {
                saveExercise();
            }
            else showMessage(getString(R.string.one_single_exercise_required), false);
        }
        else if(v == mCancelExerciseButton){
            ConfirmDialog confirmDialog = new ConfirmDialog();
            Bundle data = new Bundle();
            data.putString(ConfirmDialog.MESSAGE, getString(R.string.exit_editor_confirm));
            confirmDialog.setArguments(data);
            confirmDialog.setNoticeDialogListener(new ConfirmDialog.NoticeDialogListener() {
                @Override
                public void onDialogPositive(DialogFragment dialog) {
                    setResult(RESULT_CANCELED);
                    finish();
                }

                @Override
                public void onDialogNegative(DialogFragment dialog) {

                }
            });
            confirmDialog.show(getFragmentManager(),"confirmDialog");
        }
    }
}
