package cz.cvut.fit.dragon.imageordermodul.editor;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.ImageView;

import cz.cvut.fit.dragon.imageordermodul.R;

public class SampleView extends ImageView {

    Context context;
    Paint linesPaint;
    int splitsX,splitsY;

    public SampleView(Context context){
        super(context);
        init(context);
    }

    public SampleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SampleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context){
        this.context = context;
        setSplits(1, 1);
        linesPaint = new Paint();
        int lineColor = ContextCompat.getColor(context, R.color.splitLinesColor);
        linesPaint.setColor(lineColor);
    }

    public void setSplits(int splitsX,int splitsY){
        this.splitsX = splitsX;
        this.splitsY = splitsY;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int offsetX = 0;
        int offsetY = 0;
        int scaledWidth = 0, scaledHeight=0;

        if(getDrawable() != null) {
            int xx = getDrawable().getIntrinsicWidth();
            int yy = getDrawable().getIntrinsicHeight();

            int rectWidth = getWidth();
            int rectHeight = getHeight();

            float widthScale = 0, heightScale = 0;
            if (xx != 0)
                widthScale = (float) rectWidth / xx;
            if (yy != 0)
                heightScale = (float) rectHeight / yy;

            float scale = Math.min(widthScale, heightScale);

            scaledWidth = (int) (xx * scale);
            scaledHeight = (int) (yy * scale);

            offsetX = (rectWidth - scaledWidth) / 2;
            offsetY = (rectHeight - scaledHeight) / 2;


            float segmentWidth = (float) scaledWidth / splitsX;
            float segmentHeight = (float) scaledHeight / splitsY;


            for (int i = 0; i < splitsX - 1; i++) {
                int x = (int) (segmentWidth * (i + 1));
                canvas.drawRect(offsetX + x - 1, offsetY, offsetX + x + 1, getHeight() - offsetY, linesPaint);
            }
            for (int j = 0; j < splitsY - 1; j++) {
                int y = (int) (segmentHeight * (j + 1));
                canvas.drawRect(offsetX, offsetY + y - 1, getWidth() - offsetX, offsetY + y + 1, linesPaint);
            }
        }
    }
}
