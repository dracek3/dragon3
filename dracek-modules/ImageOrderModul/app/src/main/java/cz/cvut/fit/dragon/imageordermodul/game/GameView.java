package cz.cvut.fit.dragon.imageordermodul.game;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import cz.cvut.fit.dragon.imageordermodul.R;
import cz.cvut.fit.dragon.imageordermodul.model.Result;
import cz.cvut.fit.dragon.imageordermodul.model.SingleExercise;

public class GameView extends View{

    protected Context mContext;
    protected Rect mAreaOne,mAreaTwo;
    protected int mPaddingLeft,mPaddingRight,mPaddingTop,mPaddingBottom;
    protected int mImageHorizontalSpace;

    SegmentManager mSegmentManager;
    ExerciseManager mExerciseManager;
    SingleExercise mCurrentSingleExercise;
    TextView mCurrentSingleExerciseNameTextView;

    int mLastX;
    int mLastY;
    int firstPressedId;
    final static int FINGER_NOT_PRESSED = -13;
    Paint mAreaPaint;

    public GameView(Context context,ExerciseManager exerciseManager,TextView currentSingleExerciseNameTextView) {
        super(context);
        this.mContext = context;
        this.mExerciseManager = exerciseManager;
        this.mCurrentSingleExerciseNameTextView = currentSingleExerciseNameTextView;

        loadDimens();

        mCurrentSingleExercise = mExerciseManager.getCurrentSingleExercise();
        firstPressedId = FINGER_NOT_PRESSED;
        mAreaPaint = new Paint();
        mAreaPaint.setColor(Color.DKGRAY);
        setSingleExercise();
    }

    protected void loadDimens(){
        mPaddingLeft = (int)getResources().getDimension(R.dimen.view_left_padding);
        mPaddingRight = (int)getResources().getDimension(R.dimen.view_right_padding);
        mPaddingBottom = (int)getResources().getDimension(R.dimen.view_bottom_padding);
        mPaddingTop = (int)getResources().getDimension(R.dimen.view_top_padding);
        mImageHorizontalSpace = (int)getResources().getDimension(R.dimen.view_images_horizontal_space);

    }

    private void setSingleExercise() {

        mSegmentManager = new SegmentManager(mContext,mCurrentSingleExercise);
        if(mAreaTwo != null && mAreaOne != null)
            mSegmentManager.resize(mAreaOne,mAreaTwo);

        mCurrentSingleExerciseNameTextView.setText(mCurrentSingleExercise.getName());
    }

    private void resizeView(int width,int height){
        int areaWidth = (width - mPaddingLeft - mImageHorizontalSpace - mPaddingRight) / 2;

        mAreaOne = new Rect(mPaddingLeft,mPaddingTop,mPaddingLeft+areaWidth,height-mPaddingBottom);
        mAreaTwo = new Rect(mAreaOne.right + mImageHorizontalSpace,mPaddingTop,
                mAreaOne.right + mImageHorizontalSpace + areaWidth,height-mPaddingBottom);

        mSegmentManager.resize(mAreaOne, mAreaTwo);

        invalidate();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        resizeView(w, h);
        invalidate();
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(mSegmentManager != null) {
            mSegmentManager.draw(canvas);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        final int x = (int) event.getX();
        final int y = (int) event.getY();

        int action = event.getActionMasked();
        int index = event.getActionIndex();
        int id = event.getPointerId(index);

        if(event.getAction() == MotionEvent.ACTION_DOWN) {
            if(firstPressedId == FINGER_NOT_PRESSED){
                touchDown(x,y);
                invalidate();
                firstPressedId = id;
            }
        }
        else if(event.getAction() == MotionEvent.ACTION_MOVE){
            if(id == firstPressedId) {
                touchMove(x, y);
                invalidate();
            }
        }
        else if(action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_POINTER_UP){
            if(id == firstPressedId){
                touchUp();
                invalidate();
                firstPressedId = FINGER_NOT_PRESSED;
            }
        }
        return true;
    }

    void touchDown(int x,int y){
        if (mAreaOne.contains(x, y) || mAreaTwo.contains(x,y)){
            mSegmentManager.touchDown(x, y);
            mLastX = x;
            mLastY = y;
        }
    }
    void touchMove(int x,int y){
        int dx = x - mLastX;
        int dy = y - mLastY;
        mSegmentManager.touchMove(dx,dy);
        mLastX = x;
        mLastY = y;
    }

    void touchUp(){

        mExerciseManager.getCurrentResult().addMove();

        int result = mSegmentManager.touchUp();
        if(result == SegmentManager.RESULT_SUCCESS){
            singleExerciseFinished();
        }
    }

    void singleExerciseFinished(){

        if(mExerciseManager.nextSingleExercise()){
            mCurrentSingleExercise = mExerciseManager.getCurrentSingleExercise();
            setSingleExercise();
        }
        else {
            if(noticeEventListener != null)
                noticeEventListener.onExerciseDone();
        }
    }

    NoticeEventListener noticeEventListener;
    public interface NoticeEventListener{
        void onExerciseDone();
    }
    public void setNoticeEventListener(NoticeEventListener noticeEventListener){
        this.noticeEventListener = noticeEventListener;

    }
}
