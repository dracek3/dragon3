package cz.cvut.fit.dragon.imageordermodul.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;
import java.util.ArrayList;

import cz.cvut.fit.dragon.corelibrary.util.IO;

public class SingleExercise implements Parcelable {

    int uniqueId;
    String name;
    int splitsX;
    int splitsY;

    public SingleExercise(){

    }

    protected SingleExercise(Parcel in) {
        uniqueId = in.readInt();
        name = in.readString();
        splitsX = in.readInt();
        splitsY = in.readInt();
    }

    public void destroyImages(){
        File thumbnail = IO.getThumbnailFile(uniqueId);
        File image = IO.getImageFile(uniqueId);
        thumbnail.delete();
        image.delete();
    }

    public static final Creator<SingleExercise> CREATOR = new Creator<SingleExercise>() {
        @Override
        public SingleExercise createFromParcel(Parcel in) {
            return new SingleExercise(in);
        }

        @Override
        public SingleExercise[] newArray(int size) {
            return new SingleExercise[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSplitsX() {
        return splitsX;
    }

    public void setSplitsX(int splitsX) {
        this.splitsX = splitsX;
    }

    public int getSplitsY() {
        return splitsY;
    }

    public void setSplitsY(int splitsY) {
        this.splitsY = splitsY;
    }

    public int getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    public void increaseSplitsX(){
        splitsX++;
    }
    public void decreaseSplitsX(){
        splitsX--;
    }
    public void increaseSplitsY(){
        splitsY++;
    }
    public void decreaseSplitsY(){
        splitsY--;
    }

    public ArrayList<String> getAllImageFilePaths(){
        ArrayList<String> imageFilePaths = new ArrayList<>();
        imageFilePaths.add(IO.getImageFile(uniqueId).getPath());
        imageFilePaths.add(IO.getThumbnailFile(uniqueId).getPath());
        return imageFilePaths;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SingleExercise that = (SingleExercise) o;

        return uniqueId == that.uniqueId;

    }

    @Override
    public int hashCode() {
        return uniqueId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(uniqueId);
        dest.writeString(name);
        dest.writeInt(splitsX);
        dest.writeInt(splitsY);
    }
}
