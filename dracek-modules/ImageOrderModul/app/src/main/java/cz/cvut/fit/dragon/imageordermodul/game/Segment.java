package cz.cvut.fit.dragon.imageordermodul.game;

import android.graphics.Bitmap;

public class Segment {
    Bitmap bitmap;
    int correctX;
    int correctY;

    public Segment(int correctX, int correctY) {
        this.correctX = correctX;
        this.correctY = correctY;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public int getCorrectX() {
        return correctX;
    }

    public int getCorrectY() {
        return correctY;
    }
}
