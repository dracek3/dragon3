package cz.cvut.fit.dragon.imageordermodul.model;

public class Result {
    int moves;
    int minMoves;

    public Result() {
        moves = 0;
    }

    public void addMove() {
        this.moves++;
    }

    public int getPercentage(){

        float percentage = 1/((float)moves/minMoves);
        if (percentage < 0) return 0;
        else return (int)(percentage * 100);
    }

    public void setMinMoves(int minMoves) {
        this.minMoves = minMoves;
    }
}
