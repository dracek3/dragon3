package cz.cvut.fit.dragon.imageordermodul.editor;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;

import cz.cvut.fit.dragon.imageordermodul.R;
import cz.cvut.fit.dragon.imageordermodul.model.SingleExercise;
import cz.cvut.fit.dragon.corelibrary.util.IO;
import cz.cvut.fit.dragon.corelibrary.util.InfoDialog;
import cz.cvut.fit.dragon.corelibrary.util.Size;
import cz.cvut.fit.dragon.corelibrary.util.Utilities;


public class SingleEditorActivity extends Activity implements View.OnClickListener {

    private static final int SELECT_PHOTO = 100;
    private final static int MAX_SPLITS = 12;
    private final static int MIN_SPLITS = 2;

    public final static int CREATE = 105;
    public final static int EDIT = 106;
    int mType;

    private Button mLoadButton, mSaveButton, mCancelButton;
    private SampleView mImageSampleView;
    private EditText mNameInput;

    Button mSplitsXIncrease,mSplitsXDecrease;
    Button mSplitsYIncrease,mSplitsYDecrease;
    EditText mSplitsXText,mSplitsYText;

    private SingleExercise mSingleExercise;
    private boolean mImageLoaded;
    Bitmap mImageSample;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_editor);

        init();
        initSingleExercise(getIntent());
    }
    private void init(){
        mSaveButton = (Button) findViewById(R.id.save_image_button);
        mLoadButton = (Button) findViewById(R.id.load_image_button);
        mCancelButton = (Button) findViewById(R.id.cancel_image_button);
        mImageSampleView = (SampleView) findViewById(R.id.image_thumb_nail);
        mNameInput = (EditText)findViewById(R.id.single_exercise_name_input);

        mSplitsXDecrease = (Button)findViewById(R.id.splitsX_decrease_button);
        mSplitsXText = (EditText)findViewById(R.id.splitsX_text);
        mSplitsXIncrease = (Button)findViewById(R.id.splitX_increase_button);

        mSplitsYDecrease = (Button)findViewById(R.id.splitsY_decrease_button);
        mSplitsYText = (EditText)findViewById(R.id.splitsY_text);
        mSplitsYIncrease = (Button)findViewById(R.id.splitY_increase_button);

        mLoadButton.setOnClickListener(this);
        mSaveButton.setOnClickListener(this);
        mCancelButton.setOnClickListener(this);

        mSplitsXDecrease.setOnClickListener(this);
        mSplitsXIncrease.setOnClickListener(this);
        mSplitsYDecrease.setOnClickListener(this);
        mSplitsYIncrease.setOnClickListener(this);
    }
    private void initSingleExercise(Intent intent){
        if(intent.hasExtra(EditorActivity.SINGLE_EXERCISE)){
            mSingleExercise = getIntent().getParcelableExtra(EditorActivity.SINGLE_EXERCISE);

            mImageSample = Utilities.loadBitmapFromFile(IO.getImageFile(mSingleExercise.getUniqueId()));
            mImageSampleView.setImageBitmap(mImageSample);
            mImageLoaded = true;


            mNameInput.setText(mSingleExercise.getName());
            mSplitsXText.setText(String.valueOf(mSingleExercise.getSplitsX()));
            mSplitsYText.setText(String.valueOf(mSingleExercise.getSplitsY()));

            mImageSampleView.setImageBitmap(mImageSample);

            mCancelButton.setVisibility(View.GONE);
            mSaveButton.setText(getString(R.string.save_single_exercise));
            mType = EDIT;
        }
        else if(intent.hasExtra(EditorActivity.SINGLE_EXERCISE_UNIQUE_ID)) {
            int mUniqueId = getIntent().getIntExtra(EditorActivity.SINGLE_EXERCISE_UNIQUE_ID, 0);

            mSingleExercise = new SingleExercise();
            mSingleExercise.setUniqueId(mUniqueId);

            mSingleExercise.setSplitsX(Integer.valueOf(getString(R.string.defaultSplitsX)));
            mSingleExercise.setSplitsY(Integer.valueOf(getString(R.string.defaultSplitsY)));

            mImageLoaded = false;
            mSaveButton.setText(getString(R.string.create_single_exercise));
            mType = CREATE;
        }
        mImageSampleView.setSplits(mSingleExercise.getSplitsX(), mSingleExercise.getSplitsY());

    }

    private void showInfoMessage(String message){
        InfoDialog infoDialog = new InfoDialog();
        Bundle data = new Bundle();
        data.putString(InfoDialog.MESSAGE,message);
        infoDialog.setArguments(data);
        infoDialog.show(getFragmentManager(), "infoDialog");
    }

    private void loadImage(String imageFilePath) {
        File savedImageFile = IO.getImageFile(mSingleExercise.getUniqueId());
        File savedThumbnail = IO.getThumbnailFile(mSingleExercise.getUniqueId());

        File loadedImageFile = new File(imageFilePath);
        Size imageSize = Utilities.getImageSize(loadedImageFile);

        if(imageSize.getWidth() ==  0 || imageSize.getHeight() == 0) {
            showInfoMessage(getString(R.string.not_zero_image_size));
        }
        else {
            Size scaledSize = Utilities.scaleSize(imageSize, new Size(IO.MAX_IMAGE_SIZE, IO.MAX_IMAGE_SIZE));
            Size scaledThumbSize = Utilities.scaleSize(imageSize, new Size(IO.MAX_THUMB_SIZE,IO.MAX_THUMB_SIZE));

            Bitmap scaledBitmap = Utilities.loadBitmapFromFile(loadedImageFile, scaledSize.getWidth(), scaledSize.getHeight());
            Bitmap scaledThumbBitmap = Utilities.loadBitmapFromFile(loadedImageFile, scaledThumbSize.getWidth(), scaledThumbSize.getHeight());

            mImageSample = scaledBitmap;
            mImageSampleView.setImageBitmap(mImageSample);

            try {
                Utilities.saveBitmapToFile(savedImageFile,scaledBitmap);
                Utilities.saveBitmapToFile(savedThumbnail, scaledThumbBitmap);
                mImageLoaded = true;

            } catch (IOException e) {
                showInfoMessage(getString(R.string.load_image_failed));
            }

        }
    }

        @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == SELECT_PHOTO && resultCode == Activity.RESULT_OK) {
            String path = Utilities.getPath(SingleEditorActivity.this, data.getData());
            if(path != null){
                loadImage(path);
            }
            else showInfoMessage(getString(R.string.load_image_failed));

        }
        isSingleExerciseValid();
    }

    private boolean isSingleExerciseValid() {
        boolean valid = true;
        String msg = "*";

        if (!mImageLoaded) {
            msg += getString(R.string.image_required);
            valid = false;
        }

        if (!valid) showInfoMessage(msg);

        return valid;
    }

    @Override
    public void onClick(View v) {
        if(v == mLoadButton){
            Intent photoPickIntent = new Intent(Intent.ACTION_PICK);
            photoPickIntent.setType("image/*");
            startActivityForResult(photoPickIntent, SELECT_PHOTO);
        }
        else if(v == mSaveButton){
            if(isSingleExerciseValid()){
                save();
            }
        }
        else if(v == mCancelButton){
            setResult(RESULT_CANCELED);
            finish();
        }
        else if(v == mSplitsXDecrease){
            if(mSingleExercise.getSplitsX() > MIN_SPLITS)mSingleExercise.decreaseSplitsX();
            mSplitsXText.setText(String.valueOf(mSingleExercise.getSplitsX()));
            mImageSampleView.setSplits(mSingleExercise.getSplitsX(), mSingleExercise.getSplitsY());
        }
        else if(v == mSplitsXIncrease) {
            if (mSingleExercise.getSplitsX() < MAX_SPLITS) mSingleExercise.increaseSplitsX();
            mSplitsXText.setText(String.valueOf(mSingleExercise.getSplitsX()));
            mImageSampleView.setSplits(mSingleExercise.getSplitsX(), mSingleExercise.getSplitsY());
        }
            else if(v == mSplitsYDecrease){
            if(mSingleExercise.getSplitsY() > MIN_SPLITS)mSingleExercise.decreaseSplitsY();
            mSplitsYText.setText(String.valueOf(mSingleExercise.getSplitsY()));
            mImageSampleView.setSplits(mSingleExercise.getSplitsX(), mSingleExercise.getSplitsY());
        }
        else if(v == mSplitsYIncrease){
            if(mSingleExercise.getSplitsY() < MAX_SPLITS)mSingleExercise.increaseSplitsY();
            mSplitsYText.setText(String.valueOf(mSingleExercise.getSplitsY()));
            mImageSampleView.setSplits(mSingleExercise.getSplitsX(),mSingleExercise.getSplitsY());
        }
    }
    public void save(){
        mSingleExercise.setName(mNameInput.getText().toString());

        Intent resultIntent = new Intent();
        resultIntent.putExtra(EditorActivity.SINGLE_EXERCISE, mSingleExercise);
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }
    @Override
    public void onBackPressed() {
        if(mType == EDIT) {
            if(isSingleExerciseValid())
                save();
        }
        else if(mType == CREATE){
            setResult(RESULT_CANCELED);
            finish();
        }
    }
}
