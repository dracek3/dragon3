package cz.cvut.fit.dragon.imagepointmodul.model;

import android.util.Xml;

import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import cz.cvut.fit.dragon.corelibrary.util.IO;
import cz.cvut.fit.dragon.corelibrary.util.XmlTag;


public class ExerciseWriter {

    Exercise mExercise;

    public ExerciseWriter(Exercise exercise){
        this.mExercise = exercise;
    }

    public String getExerciseOutputXmlString() throws IOException{
        XmlSerializer xmlSerializer = Xml.newSerializer();
        ByteArrayOutputStream output =  new ByteArrayOutputStream();
        xmlSerializer.setOutput(output, IO.XML_ENCODING);
        writeXml(xmlSerializer, mExercise);
        return output.toString(IO.XML_ENCODING);
    }
    private void writeXml(XmlSerializer xmlSerializer,Exercise exercise) throws IOException {
        xmlSerializer.startDocument(IO.XML_ENCODING, false);

        xmlSerializer.startTag("", XmlTag.EXERCISE);
        for(int i=0;i<exercise.getSingleExerciseList().size();i++){
            SingleExercise singleExercise = exercise.getSingleExerciseList().get(i);

            xmlSerializer.startTag("",XmlTag.SINGLE_EXERCISE);

            xmlSerializer.startTag("",XmlTag.ID);
            xmlSerializer.text(String.valueOf(singleExercise.getUniqueId()));
            xmlSerializer.endTag("", XmlTag.ID);

            xmlSerializer.startTag("", XmlTag.NAME);
            xmlSerializer.text(singleExercise.getName());
            xmlSerializer.endTag("", XmlTag.NAME);

            xmlSerializer.startTag("", XmlTag.AREAS);
            for(int j=0;j<singleExercise.getAreaList().size();j++){
                writeArea(singleExercise.getAreaList().get(j),xmlSerializer);
            }
            xmlSerializer.endTag("", XmlTag.AREAS);

            xmlSerializer.endTag("",XmlTag.SINGLE_EXERCISE);
        }
        xmlSerializer.endTag("", XmlTag.EXERCISE);
        xmlSerializer.flush();
    }
    private void writeArea(Area area,XmlSerializer xmlSerializer) throws IOException {

        xmlSerializer.startTag("",XmlTag.AREA);

        xmlSerializer.startTag("", XmlTag.AREA_ID);
        xmlSerializer.text(String.valueOf(area.getUniqueId()));
        xmlSerializer.endTag("", XmlTag.AREA_ID);

        xmlSerializer.startTag("", XmlTag.AREA_NAME);
        xmlSerializer.text(area.getName());
        xmlSerializer.endTag("", XmlTag.AREA_NAME);


        xmlSerializer.startTag("", XmlTag.AREA_REPRESENTATIVE_X);
        xmlSerializer.text(String.valueOf(area.getxRepresentive()));
        xmlSerializer.endTag("", XmlTag.AREA_REPRESENTATIVE_X);

        xmlSerializer.startTag("", XmlTag.AREA_REPRESENTATIVE_Y);
        xmlSerializer.text(String.valueOf(area.getyRepresentive()));
        xmlSerializer.endTag("", XmlTag.AREA_REPRESENTATIVE_Y);

        xmlSerializer.endTag("",XmlTag.AREA);
    }
}
