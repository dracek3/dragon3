package cz.cvut.fit.dragon.imagepointmodul.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;

import cz.cvut.fit.dragon.corelibrary.util.Size;


public class SingleExerciseView extends View {

    SmartImage mSmartImage;
    Bitmap mScaledBitmap;
    Rect mRect;

    public SingleExerciseView(Context context) {
        super(context);
    }


    public void setImage(SmartImage smartImage){
        this.mSmartImage = smartImage;
        refresh();
    }

    public Size getImageSize(){
        return new Size(mScaledBitmap.getWidth(),mScaledBitmap.getHeight());
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if(mScaledBitmap != null){
            canvas.drawBitmap(mScaledBitmap,mRect.left,mRect.top,null);
        }
    }
    public void refresh() {
        if(getWidth() != 0 && getHeight() != 0 && mSmartImage != null) {
            mScaledBitmap = mSmartImage.getScaledImage(getWidth(), getHeight());

            int offsetX = (getWidth() - mScaledBitmap.getWidth()) / 2;
            int offsetY = (getHeight() - mScaledBitmap.getHeight()) / 2;

            mRect = new Rect(offsetX, offsetY, offsetX + getWidth(), offsetY + getHeight());
            invalidate();
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        refresh();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        final int x = (int)event.getX();
        final int y = (int)event.getY();
        if(event.getAction() == MotionEvent.ACTION_DOWN){
            if(mRect != null && mRect.contains(x,y)){
                int xx = x - mRect.left;
                int yy = y - mRect.top;

                if(onClickImageListener != null)
                    onClickImageListener.onClickedImage(xx,yy);
            }
        }
        return true;
    }

    OnClickImageListener onClickImageListener;
    public interface OnClickImageListener {
        void  onClickedImage(int x, int y);
    }
    public void setOnClickImageListener(OnClickImageListener onClickImageListener)
    {
        this.onClickImageListener = onClickImageListener;
    }
}
