package cz.cvut.fit.dragon.imagepointmodul.model;

import android.graphics.Bitmap;
import android.graphics.Color;

public class SmartImage {

    Bitmap mBitmap;
    Pixel [][] mPixels;

    public SmartImage(Bitmap bitmap) {
        this.mBitmap = bitmap;

        initPixels();
    }
    public void initPixels(){
        mPixels = new Pixel[mBitmap.getWidth()][mBitmap.getHeight()];
        for(int i=0;i<mPixels.length;i++){
            for(int j=0;j<mPixels[i].length;j++){
                Pixel pixel;

                int clearPixel = clearPixel(mBitmap.getPixel(i,j));
                mBitmap.setPixel(i, j, clearPixel);

                if(clearPixel == Color.argb(255,0,0,0)){
                    pixel = new Pixel(clearPixel,true);
                }
                else {
                    pixel = new Pixel(clearPixel, false);
                }
                mPixels[i][j] = pixel;
            }
        }
    }

    public int clearPixel(int color){
        int A = color >> 24;
        int R = (color & 0x00ffffff) >> 16;
        int G = (color & 0x0000ffff) >> 8;
        int B = (color & 0x000000ff);

        int rColor;

        if ((R < 240 && G < 240 && B < 240)) {
            rColor = Color.argb(255, 0, 0, 0);
        }
        else  rColor = Color.argb(255, 255, 255, 255);

        return rColor;
    }

    public void setPixel(int x,int y,int color){
        mBitmap.setPixel(x, y, color);
        mPixels[x][y].setColor(color);
    }
    public Bitmap getScaledImage(int width,int height){

        if(width == 0 || height == 0)return null;

            int imageWidth, imageHeight;
            float widthScale = 0, heightScale = 0;
            if (mBitmap.getWidth() != 0)
                widthScale = (float) width / mBitmap.getWidth();
            if (mBitmap.getHeight() != 0)
                heightScale = (float) height / mBitmap.getHeight();

            float scale = Math.min(widthScale, heightScale);

            imageWidth = (int) (mBitmap.getWidth() * scale);
            imageHeight = (int) (mBitmap.getHeight() * scale);

            Bitmap scaledBitmap = Bitmap.createScaledBitmap(mBitmap, imageWidth, imageHeight,false);
            return scaledBitmap;
    }
    public void setPixelArea(int x,int y,int areaKey){
        if(areaKey == Pixel.AREA_KEY_NOT_SET)
            setPixel(x,y,Color.WHITE);
        mPixels[x][y].setAreaKey(areaKey);
    }
    public int getPixelArea(int x,int y){
        return mPixels[x][y].getAreaKey();
    }

    public Bitmap getBitmap() {
        return mBitmap;
    }

    public boolean isPixelBorder(int x,int y){
        return mPixels[x][y].isBorder();
    }

    public int getWidth(){
        return mBitmap.getWidth();
    }
    public int getHeight(){
        return mBitmap.getHeight();
    }
}
