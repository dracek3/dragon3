package cz.cvut.fit.dragon.imagepointmodul.model;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

import cz.cvut.fit.dragon.imagepointmodul.R;
import cz.cvut.fit.dragon.corelibrary.util.IO;
import cz.cvut.fit.dragon.corelibrary.util.InfoDialog;
import cz.cvut.fit.dragon.corelibrary.util.LoadExerciseException;

public abstract  class BaseActivity extends Activity {

    public final static String EXERCISE_FILE = "EXERCISE_FILE";
    public final static String IMAGES = "IMAGES";
    public final static String RESULTS = "RESULTS";
    public final static String MESSAGE = "MESSAGE";
    public final static String SINGLE_EXERCISE = "SINGLE_EXERCISE";

    protected Exercise mExercise;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IO.readResources(BaseActivity.this);
        initExercise();
    }
    private void initExercise(){
        if(getIntent().hasExtra(EXERCISE_FILE) && getIntent().hasExtra(IMAGES)) {
            ExerciseReader e = new ExerciseReader(getIntent().getStringExtra(EXERCISE_FILE), getIntent().getStringArrayListExtra(IMAGES));
            try {
                mExercise = e.load();
                init();
            } catch (XmlPullParserException | IOException | LoadExerciseException x) {
                showMessage(getString(R.string.load_exercise_failed), true);
            }
        }
        else noExerciseProvided();
    }

    protected abstract void noExerciseProvided();
    protected abstract void init();

    protected void showMessage(String message, final boolean finish){
        InfoDialog infoDialog = new InfoDialog();
        Bundle data = new Bundle();
        data.putString(MESSAGE, message);
        infoDialog.setArguments(data);
        infoDialog.setNoticeDialogListener(new InfoDialog.NoticeDialogListener() {
            @Override
            public void onDialogPositiveClick(DialogFragment dialog) {
                if(finish) finish();
            }
        });
        infoDialog.show(getFragmentManager(), "infoDialog");
    }
}
