package cz.cvut.fit.dragon.imagepointmodul.model;

public class Result {
    int areasCount;
    int areasClicks;

    public Result(){
        areasClicks = 0;
    }

    public void addAreaClick(){
        areasClicks++;
    }

    public void setAreasCount(int areasCount){
        this.areasCount = areasCount;
    }

    public int getPercentage(){
        float percentage = (float)areasCount/areasClicks;

        return (int)(percentage * 100);
    }
}
