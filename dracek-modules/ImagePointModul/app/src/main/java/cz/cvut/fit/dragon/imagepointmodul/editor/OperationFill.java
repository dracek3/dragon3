package cz.cvut.fit.dragon.imagepointmodul.editor;

import android.graphics.Color;
import android.graphics.Point;
import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import cz.cvut.fit.dragon.imagepointmodul.model.Area;
import cz.cvut.fit.dragon.imagepointmodul.model.SmartImage;

public class OperationFill extends AsyncTask<String, Void, String> {

    public AsyncResponse mDelegate = null;
    SmartImage mSmartImage;
    List<Area> mAreaList;
    Queue<Point> mQueue;
    int mColor;
    HashMap<Area,Integer> mAreaColorList;
    boolean [][] mPainted;
    private int mType;

    public final static int ID_ONLY = 11;
    public final static int ID_COLOR = 12;


    public OperationFill(SmartImage smartImage, Area area,int color,int type){
        this.mSmartImage = smartImage;
        this.mAreaList = new ArrayList<>();
        this.mColor = color;
        this.mType = type;
        mAreaList.add(area);
        mQueue = new LinkedList<>();
        mAreaColorList = new HashMap<>();
        mAreaColorList.put(area,color);
        mPainted = new boolean[mSmartImage.getWidth()][mSmartImage.getHeight()];
    }

    public OperationFill(SmartImage smartImage, List<Area> areaList,HashMap<Area,Integer> areaColorList,int type){
        this.mSmartImage = smartImage;
        this.mAreaList = areaList;
        this.mAreaColorList = areaColorList;
        this.mType = type;
        mQueue = new LinkedList<>();
        mPainted = new boolean[mSmartImage.getWidth()][mSmartImage.getHeight()];
    }

    @Override
    protected String doInBackground(String... params) {

        for(int i=0;i<mAreaList.size();i++){
            if(mType == ID_COLOR)mColor = mAreaColorList.get(mAreaList.get(i));
            fillArea(mAreaList.get(i));
        }
        return "Executed";
    }
    private void fillArea(Area area){

        Point first = new Point(area.getxRepresentive(),area.getyRepresentive());
        mQueue.add(first);

        setPixel(first, area);

        while (!mQueue.isEmpty()) {
            Point x = mQueue.poll();
            set(new Point(x.x + 1, x.y), area);
            set(new Point(x.x-1,x.y),area);
            set(new Point(x.x,x.y+1),area);
            set(new Point(x.x,x.y-1),area);
        }
    }
    private void set(Point x,Area area){
        if(x.x >= mSmartImage.getWidth() || x.x < 0 || x.y >= mSmartImage.getHeight() || x.y < 0)return;

        if (!mSmartImage.isPixelBorder(x.x, x.y) && !mPainted[x.x][x.y]) {
            mQueue.add(new Point(x.x, x.y));
            setPixel(x,area);
            mPainted[x.x][x.y] = true;
        }
    }
    private void setPixel(Point x,Area area){
        mSmartImage.setPixelArea(x.x,x.y,area.getUniqueId());
        if(mType == ID_COLOR)mSmartImage.setPixel(x.x,x.y, mColor);
    }
    @Override
    protected void onPostExecute(String result) {
        mDelegate.processFinish(result);
    }
}
