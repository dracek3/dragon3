package cz.cvut.fit.dragon.imagepointmodul.editor;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import cz.cvut.fit.dragon.imagepointmodul.R;
import cz.cvut.fit.dragon.imagepointmodul.model.Exercise;


public class ExerciseListAdapter extends BaseAdapter {

    Exercise mExercise;
    private Context mContext;

    public ExerciseListAdapter(Context context, Exercise mExercise){
        this.mContext = context;
        this.mExercise = mExercise;
    }

    @Override
    public int getCount() {
        return mExercise.getSingleExerciseList().size();
    }

    @Override
    public Object getItem(int position) {
        return mExercise.getSingleExerciseList().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if(convertView == null){

            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.exercise_list_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.editSingleExerciseButton.setTag(position);
        viewHolder.removeSingleExerciseButton.setTag(position);
        viewHolder.nameTextView.setText(mExercise.getSingleExercise(position).getName());
        viewHolder.imageView.setImageBitmap(mExercise.getSingleExerciseThumbnail(position));

        return convertView;
    }

    class ViewHolder implements View.OnClickListener {
        public TextView nameTextView;
        public ImageView imageView;
        public ImageButton editSingleExerciseButton;
        public ImageButton removeSingleExerciseButton;

        public ViewHolder(View view) {
            nameTextView = (TextView) view.findViewById(R.id.list_view_text);
            imageView = (ImageView) view.findViewById(R.id.list_view_image);
            editSingleExerciseButton = (ImageButton) view.findViewById(R.id.edit_single_exercise_button);
            removeSingleExerciseButton = (ImageButton) view.findViewById(R.id.delete_single_exercise_button);

            editSingleExerciseButton.setOnClickListener(this);
            removeSingleExerciseButton.setOnClickListener(this);
         }

        @Override
        public void onClick(View v) {
            if(v == editSingleExerciseButton) {
                int position = (int) v.getTag();
                Intent intent = new Intent(mContext, SingleEditorActivity.class);
                intent.putExtra(EditorActivity.SINGLE_EXERCISE, mExercise.getSingleExerciseList().get(position));

                ((Activity) mContext).startActivityForResult(intent, EditorActivity.EDIT_SINGLE_EXERCISE);
            }
            else if(v == removeSingleExerciseButton){
                int position = (int) v.getTag();

                mExercise.removeSingleExercise(position);
                ExerciseListAdapter.this.notifyDataSetChanged();
            }
        }
    }
}
