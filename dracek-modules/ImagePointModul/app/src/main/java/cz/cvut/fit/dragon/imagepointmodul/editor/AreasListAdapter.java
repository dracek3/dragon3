package cz.cvut.fit.dragon.imagepointmodul.editor;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import cz.cvut.fit.dragon.imagepointmodul.R;

public class AreasListAdapter extends ArrayAdapter<String>{

    private final Activity mContext;
    private final String[] mNames;
    private final int [] mColors;

    public AreasListAdapter(Activity context, String[] names,int [] colors) {
        super(context, R.layout.editor_list_item, names);
        this.mContext = context;
        this.mNames = names;
        this.mColors = colors;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.editor_list_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.nameTextView.setText(mNames[position]);
        viewHolder.imageView.setBackgroundColor(mColors[position]);

        return convertView;
    }

    class ViewHolder {
        public TextView nameTextView;
        public ImageView imageView;

        public ViewHolder(View view) {
            nameTextView = (TextView) view.findViewById(R.id.list_view_text);
            imageView = (ImageView) view.findViewById(R.id.list_view_image);
        }
    }
}