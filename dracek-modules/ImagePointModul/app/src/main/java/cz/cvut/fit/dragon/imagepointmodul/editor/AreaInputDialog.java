package cz.cvut.fit.dragon.imagepointmodul.editor;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cz.cvut.fit.dragon.imagepointmodul.R;
import cz.cvut.fit.dragon.imagepointmodul.model.Area;

public class AreaInputDialog extends DialogFragment implements View.OnClickListener{

    public static final int DIALOG_NEW = 100;
    public static final int DIALOG_EDIT = 101;

    public static final String AREA = "AREA";
    public static final String TYPE = "TYPE";

    Button saveAreaButton,removeAreaButton,cancelAreaButton;
    EditText areaNameInput;

    Area mArea;
    int mDialogType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mArea = getArguments().getParcelable(AREA);
        mDialogType = getArguments().getInt(TYPE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.area_input_dialog,container);

        getDialog().setTitle(getString(R.string.area_input_dialog));
        getDialog().setCanceledOnTouchOutside(false);
        setCancelable(false);

        saveAreaButton = (Button)view.findViewById(R.id.save_area_button);
        removeAreaButton = (Button)view.findViewById(R.id.remove_area_button);
        cancelAreaButton = (Button)view.findViewById(R.id.cancel_area_button);
        areaNameInput = (EditText)view.findViewById(R.id.area_name_input);

        saveAreaButton.setOnClickListener(this);
        removeAreaButton.setOnClickListener(this);
        cancelAreaButton.setOnClickListener(this);

        if(mDialogType == DIALOG_NEW)
            removeAreaButton.setEnabled(false);

        setArea();

        return view;
    }
    void setArea(){
        areaNameInput.setText(mArea.getName());
    }

    @Override
    public void onClick(View v) {
        if(v == saveAreaButton){
            if(areaNameInput.getText().toString().equals(""))
                Toast.makeText(getActivity(),getString(R.string.area_name_non_empty),Toast.LENGTH_LONG).show();
            else {
                mArea.setName(areaNameInput.getText().toString());
                if (onResultListener != null)
                    onResultListener.onSaved(mArea);
            }
        }
        else if(v == removeAreaButton){
            if(onResultListener != null)
                onResultListener.onRemoved(mArea);
        }
        else if(v == cancelAreaButton){
            dismiss();
        }
    }

    OnResultListener onResultListener;
    public interface OnResultListener {
        void onSaved(Area area);
        void onRemoved(Area area);
    }
    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

}
