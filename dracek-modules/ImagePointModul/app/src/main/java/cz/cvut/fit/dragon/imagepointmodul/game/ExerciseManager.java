package cz.cvut.fit.dragon.imagepointmodul.game;

import cz.cvut.fit.dragon.imagepointmodul.model.Exercise;
import cz.cvut.fit.dragon.imagepointmodul.model.Result;
import cz.cvut.fit.dragon.imagepointmodul.model.SingleExercise;

public class ExerciseManager {

    int currentSingleExercise;
    Exercise exercise;
    Result [] results;

    public ExerciseManager(Exercise exercise) {
        this.exercise = exercise;
        currentSingleExercise = 0;
        results = new Result[exercise.getSingleExerciseSize()];

        results = new Result[exercise.getSingleExerciseSize()];
        for (int i = 0; i < results.length; i++){
            results[i] = new Result();
            results[i].setAreasCount(exercise.getSingleExercise(i).getAreaList().size());
        }
    }

    public boolean nextSingleExercise(){
        if(currentSingleExercise >= exercise.getSingleExerciseSize()-1)
            return false;
        else {
            currentSingleExercise++;
            return true;
        }
    }


    public SingleExercise getCurrentSingleExercise(){
        return exercise.getSingleExercise(currentSingleExercise);
    }

    /**
     * This method returns results of current single exercise
     * @return results of current single exercise
     */
    public Result getCurrentResult(){
        return results[currentSingleExercise];
    }


    public int getResultsPercentage(){
        int percentageSum = 0;
        for (int i=0;i<results.length;i++){
            percentageSum += results[i].getPercentage();
        }
        return percentageSum / results.length;
    }
}
