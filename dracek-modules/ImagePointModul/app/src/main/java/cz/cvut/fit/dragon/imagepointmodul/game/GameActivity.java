package cz.cvut.fit.dragon.imagepointmodul.game;

import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import cz.cvut.fit.dragon.corelibrary.model.Time;
import cz.cvut.fit.dragon.imagepointmodul.R;
import cz.cvut.fit.dragon.imagepointmodul.editor.AsyncResponse;
import cz.cvut.fit.dragon.imagepointmodul.editor.OperationFill;
import cz.cvut.fit.dragon.imagepointmodul.model.SingleExerciseView;
import cz.cvut.fit.dragon.imagepointmodul.model.BaseActivity;
import cz.cvut.fit.dragon.corelibrary.model.ResultWriter;
import cz.cvut.fit.dragon.imagepointmodul.model.SingleExercise;
import cz.cvut.fit.dragon.imagepointmodul.model.SmartImage;
import cz.cvut.fit.dragon.corelibrary.util.ConfirmDialog;
import cz.cvut.fit.dragon.corelibrary.util.IO;
import cz.cvut.fit.dragon.corelibrary.util.Utilities;


public class GameActivity extends BaseActivity implements View.OnClickListener,AsyncResponse, SingleExerciseView.OnClickImageListener {

    OperationFill mOperationFill;
    LinearLayout mGameViewLinearLayout;
    ImageButton mCloseGameButton;
    TextView mExerciseNameTextView;
    ListView mTaskListView;
    SingleExerciseView mGameView;

    SingleExercise mCurrentSingleExercise;
    SmartImage mSmartImage;
    ExerciseManager mExerciseManager;
    Time mTime;
    int mAreaDone;

    protected void init() {
        setContentView(R.layout.activity_game);

        mCloseGameButton = (ImageButton)findViewById(R.id.close_game_button);
        mExerciseNameTextView = (TextView)findViewById(R.id.exercise_names_text_view);
        mGameViewLinearLayout = (LinearLayout)findViewById(R.id.game_view_linear_layout);
        mTaskListView = (ListView)findViewById(R.id.task_list_view_game);
        mGameView = new SingleExerciseView(GameActivity.this);
        mGameViewLinearLayout.addView(mGameView);
        mCloseGameButton.setOnClickListener(this);

        mExerciseManager = new ExerciseManager(mExercise);
        mCurrentSingleExercise = mExerciseManager.getCurrentSingleExercise();

        mTime = new Time();
        mTime.initTime();
        setSingleExercise();
        mAreaDone = 0;

        mGameView.setOnClickImageListener(this);
    }

    protected void noExerciseProvided() {
        showMessage(getString(R.string.load_exercise_failed), true);
    }

    private void setSingleExercise(){
        mSmartImage = new SmartImage(loadBitmapMutable(mCurrentSingleExercise));

        mOperationFill = new OperationFill(mSmartImage,mCurrentSingleExercise.getAreaList(),null,OperationFill.ID_ONLY);
        mOperationFill.mDelegate = this;
        mOperationFill.execute();

        mAreaDone = 0;
        mExerciseNameTextView.setText(mCurrentSingleExercise.getName());
        mGameView.setImage(mSmartImage);
        refreshListView();
        mGameView.refresh();
    }

    private Bitmap loadBitmapMutable(SingleExercise singleExercise){
        Bitmap image = Utilities.loadBitmapFromFile(IO.getImageFile(singleExercise.getUniqueId()));
        return image.copy(image.getConfig(), true);
    }

    private void refreshListView(){
        SingleExercise s = mCurrentSingleExercise;

        String [] strings = new String[s.getAreaList().size()];
        for (int i=0;i<strings.length;i++){
            strings[i] = s.getAreaList().get(i).getName();
        }
        GameListAdapter adapter;
        adapter = new GameListAdapter(GameActivity.this,strings,mAreaDone);
        mTaskListView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTime.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mTime.pause();
    }


    @Override
    public void onBackPressed() {
        closeGame();
    }

    @Override
    public void onClick(View v) {
        if(v == mCloseGameButton){
            closeGame();
        }
    }

    private void closeGame(){
        ConfirmDialog confirmDialog = new ConfirmDialog();
        Bundle data = new Bundle();
        data.putString(MESSAGE, getString(R.string.game_close_confirm));
        confirmDialog.setArguments(data);
        confirmDialog.setNoticeDialogListener(new ConfirmDialog.NoticeDialogListener() {
            @Override
            public void onDialogPositive(DialogFragment dialog) {
                finish();
            }

            @Override
            public void onDialogNegative(DialogFragment dialog) {

            }
        });
        confirmDialog.show(getFragmentManager(), "confirmDialogGame");
    }

    public void exerciseDone() {
        String xmlResults = "";
        try {
            ResultWriter resultWriter = new ResultWriter(mTime.getTimeSec(),mExerciseManager.getResultsPercentage(), getString(R.string.result_time_unit),getString(R.string.result_score_unit));
            xmlResults = resultWriter.getXmlResultString();

        } catch (IOException e) {
            removeAllImages();
            setResult(RESULT_CANCELED);
            finish();
        }

        removeAllImages();
        Intent resultIntent = new Intent();
        resultIntent.putExtra(RESULTS, xmlResults);
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    @Override
    public void processFinish(String output) {
        mGameView.refresh();
    }

    @Override
    public void onClickedImage(int x, int y) {
        float scaleX = (float) mSmartImage.getBitmap().getWidth() / mGameView.getImageSize().getWidth();//0.5
        float scaleY = (float) mSmartImage.getBitmap().getHeight() / mGameView.getImageSize().getHeight();

        int clickToOrigX = (int) (x * scaleX);
        int clickToOrigY = (int) (y * scaleY);

        if(mSmartImage.getBitmap().getWidth() <= clickToOrigX || mSmartImage.getBitmap().getHeight() <= clickToOrigY)
            return;

        mExerciseManager.getCurrentResult().addAreaClick();
        if(mSmartImage.getPixelArea(clickToOrigX,clickToOrigY) == mCurrentSingleExercise.getAreaList().get(mAreaDone).getUniqueId()) {
            mAreaDone++;
            if (mAreaDone >= mCurrentSingleExercise.getAreaList().size()) {
                if (mExerciseManager.nextSingleExercise()) {
                    mCurrentSingleExercise = mExerciseManager.getCurrentSingleExercise();
                    setSingleExercise();
                } else
                    exerciseDone();
            }
            refreshListView();
        }
    }
    private void removeAllImages(){
        ArrayList<String> imageFilePaths = new ArrayList<>();

        for(int i=0;i<mExercise.getSingleExerciseSize();i++){
            imageFilePaths.addAll(mExercise.getSingleExercise(i).getAllImageFilePaths());
        }

        for(int i=0;i<imageFilePaths.size();i++){
            File file = new File(imageFilePaths.get(i));
            file.delete();
        }
    }
}
