package cz.cvut.fit.dragon.imagepointmodul.game;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import cz.cvut.fit.dragon.imagepointmodul.R;

public class GameListAdapter extends ArrayAdapter<String> {

    private final Activity mContext;
    private final String[] mItems;
    int mItemsDone;

    public GameListAdapter(Activity context,
                           String[] items,int done) {
        super(context, R.layout.game_list_item, items);
        this.mContext = context;
        this.mItems = items;
        this.mItemsDone = done;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.game_list_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if(position < mItemsDone)
            viewHolder.nameTextView.setPaintFlags(viewHolder.nameTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        viewHolder.nameTextView.setText(mItems[position]);

        return convertView;
    }

    class ViewHolder {
        public TextView nameTextView;

        public ViewHolder(View view) {
            nameTextView = (TextView) view.findViewById(R.id.game_list_view_text);
        }
    }
}