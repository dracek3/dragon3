package cz.cvut.fit.dragon.imagepointmodul.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by bury on 4/3/2016.
 */
public class Area implements Parcelable{

    int uniqueId;
    String name;
    int xRepresentive;
    int yRepresentive;

    public Area(int uniqueId){
        this.uniqueId = uniqueId;
    }

    public Area(){

    }

    protected Area(Parcel in) {
        uniqueId = in.readInt();
        name = in.readString();
        xRepresentive = in.readInt();
        yRepresentive = in.readInt();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Area area = (Area) o;

        return uniqueId == area.uniqueId;

    }

    @Override
    public int hashCode() {
        return uniqueId;
    }

    public static final Creator<Area> CREATOR = new Creator<Area>() {
        @Override
        public Area createFromParcel(Parcel in) {
            return new Area(in);
        }

        @Override
        public Area[] newArray(int size) {
            return new Area[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    public int getxRepresentive() {
        return xRepresentive;
    }

    public void setxRepresentive(int xRepresentive) {
        this.xRepresentive = xRepresentive;
    }

    public int getyRepresentive() {
        return yRepresentive;
    }

    public void setyRepresentive(int yRepresentive) {
        this.yRepresentive = yRepresentive;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(uniqueId);
        dest.writeString(name);
        dest.writeInt(xRepresentive);
        dest.writeInt(yRepresentive);
    }
}
