package cz.cvut.fit.dragon.imagepointmodul.model;

public class Pixel {

    public static int AREA_KEY_NOT_SET = -1;

    int color;
    boolean border;
    int areaKey;

    public Pixel(int color, boolean border){
        this.color = color;
        this.border = border;
        areaKey = AREA_KEY_NOT_SET;
    }

    public int getAreaKey() {
        return areaKey;
    }

    public void setAreaKey(int areaKey) {
        this.areaKey = areaKey;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public boolean isBorder() {
        return border;
    }

    public void setBorder(boolean border) {
        this.border = border;
    }
}
