package cz.cvut.fit.dragon.imagepointmodul.editor;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import cz.cvut.fit.dragon.imagepointmodul.R;
import cz.cvut.fit.dragon.imagepointmodul.model.SingleExerciseView;
import cz.cvut.fit.dragon.imagepointmodul.model.SingleExerciseView.OnClickImageListener;
import cz.cvut.fit.dragon.imagepointmodul.model.Area;
import cz.cvut.fit.dragon.imagepointmodul.model.Pixel;
import cz.cvut.fit.dragon.imagepointmodul.model.SingleExercise;
import cz.cvut.fit.dragon.imagepointmodul.model.SmartImage;
import cz.cvut.fit.dragon.corelibrary.util.ConfirmDialog;
import cz.cvut.fit.dragon.corelibrary.util.IO;
import cz.cvut.fit.dragon.corelibrary.util.InfoDialog;
import cz.cvut.fit.dragon.corelibrary.util.Size;
import cz.cvut.fit.dragon.corelibrary.util.Utilities;

public class SingleEditorActivity extends Activity implements OnClickListener,OnClickImageListener,AreaInputDialog.OnResultListener{

    private static final int SELECT_PHOTO = 100;

    public final static int CREATE = 105;
    public final static int EDIT = 106;
    int mType;

    Button mLoadButton;
    Button mSaveButton,mCancelButton;
    EditText mSingleExerciseNameInput;
    LinearLayout mSingleExerciseLinearLayout;

    ListView mSingleExerciseListView;

    AreaInputDialog mAreaInputDialog;

    OperationFill operationFill;

    private SingleExerciseView mSingleEditorImageView;
    private SingleExercise mSingleExercise;

    SmartImage mSmartImage;

    int [] mColorList;
    int colorsF;
    HashMap<Area,Integer> mAreaColors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_editor);

        mAreaColors = new HashMap<>();
        colorsF = 0;
        init();
        initColors();
        setUpSingleExercise(getIntent());
    }
    private void init(){
        mLoadButton = (Button)findViewById(R.id.single_exercise_load_image_button);
        mSaveButton = (Button)findViewById(R.id.save_single_exercise_button);
        mCancelButton = (Button)findViewById(R.id.exit_without_save_editor_button);
        mSingleExerciseNameInput = (EditText)findViewById(R.id.single_exercise_name_input);
        mSingleExerciseLinearLayout = (LinearLayout)findViewById(R.id.single_exercise_linear_layout);
        mSingleEditorImageView = new SingleExerciseView(SingleEditorActivity.this);
        mSingleEditorImageView.setOnClickImageListener(this);

        mSingleExerciseLinearLayout.addView(mSingleEditorImageView);

        mSingleExerciseListView = (ListView)findViewById(R.id.single_exercise_list_view);

        mSingleExerciseListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showAreaInput(mSingleExercise.getAreaList().get(position), AreaInputDialog.DIALOG_EDIT);
            }
        });
        mLoadButton.setOnClickListener(this);
        mSaveButton.setOnClickListener(this);
        mCancelButton.setOnClickListener(this);
    }
    private void setUpSingleExercise(Intent intent){
        if(intent.hasExtra(EditorActivity.SINGLE_EXERCISE)){
            mSingleExercise = getIntent().getParcelableExtra(EditorActivity.SINGLE_EXERCISE);

            mSingleExerciseNameInput.setText(mSingleExercise.getName());

            Bitmap x = loadBitmap();
            mSmartImage = new SmartImage(x);

            for(int i=0;i<mSingleExercise.getAreaList().size();i++){
                mAreaColors.put(mSingleExercise.getAreaList().get(i),mColorList[i%mColorList.length]);
            }
            colorsF += mSingleExercise.getAreaList().size();

            fillImageAreas(mSmartImage, mSingleExercise.getAreaList(), OperationFill.ID_COLOR, false);

            mCancelButton.setVisibility(View.GONE);
            mSaveButton.setText(getString(R.string.save_single_exercise));
            mType = EDIT;
        }
        else if(intent.hasExtra(EditorActivity.SINGLE_EXERCISE_UNIQUE_ID)) {
            int uniqueId = getIntent().getIntExtra(EditorActivity.SINGLE_EXERCISE_UNIQUE_ID, 0);

            mSingleExercise = new SingleExercise();
            mSingleExercise.setUniqueId(uniqueId);

            mType = CREATE;
            mSaveButton.setText(getString(R.string.create_single_exercise));
        }
    }

    public void fillImageAreas(SmartImage smartImage,List<Area> areaList,int id,final boolean closeDialog)
    {
        operationFill = new OperationFill(smartImage,areaList,mAreaColors,id);
        operationFill.mDelegate = new AsyncResponse() {
            @Override
            public void processFinish(String output) {
                mSingleEditorImageView.setImage(mSmartImage);
                refreshListView();
                mSingleEditorImageView.refresh();
            }
        };
        operationFill.execute();
    }

    private boolean isSingleExerciseValid() {
        boolean valid = true;

        if(mSmartImage == null){
            valid = false;
            showInfoMessage(getString(R.string.image_required));
        }
        else if(mSingleExercise.getAreaList().size() == 0) {
            valid = false;
            showInfoMessage(getString(R.string.area_required));
        }
        return valid;
    }

    private void save(){
        Bitmap thumbnail = mSmartImage.getBitmap();

        Size scaledSize =  Utilities.scaleSize(new Size(thumbnail.getWidth(), thumbnail.getHeight()), new Size(IO.MAX_THUMB_SIZE, IO.MAX_THUMB_SIZE));
        Bitmap scaledThumbBitmap = Bitmap.createScaledBitmap(thumbnail, scaledSize.getWidth(), scaledSize.getHeight(), true);

        try {
            Utilities.saveBitmapToFile(IO.getThumbnailFile(mSingleExercise.getUniqueId()), scaledThumbBitmap);
            mSingleExercise.setName(mSingleExerciseNameInput.getText().toString());

            Intent resultIntent = new Intent();
            resultIntent.putExtra(EditorActivity.SINGLE_EXERCISE, mSingleExercise);
            setResult(Activity.RESULT_OK, resultIntent);
            finish();

        } catch (IOException e) {
            showInfoMessage(getString(R.string.save_single_exercise_failed));
        }
    }


    private void showInfoMessage(String message){
        InfoDialog infoDialog = new InfoDialog();
        Bundle data = new Bundle();
        data.putString(InfoDialog.MESSAGE,message);
        infoDialog.setArguments(data);
        infoDialog.show(getFragmentManager(), "infoDialog");
    }

    @Override
    public void onClick(View v) {
        if(v == mSaveButton){
            if (isSingleExerciseValid()) {
                save();
            }
        }
        else if(v == mCancelButton){
            setResult(RESULT_CANCELED);
            finish();
        }
        else if(v == mLoadButton){
            if(mSmartImage == null)pickImage();
            else loadImage();
        }
    }

    private void pickImage(){
        Intent photoPickIntent = new Intent(Intent.ACTION_PICK);
        photoPickIntent.setType("image/*");
        startActivityForResult(photoPickIntent, SELECT_PHOTO);
    }

    private void loadImage(){
        ConfirmDialog confirmDialog = new ConfirmDialog();
        Bundle data = new Bundle();
        data.putString(ConfirmDialog.MESSAGE, getString(R.string.reload_image));
        confirmDialog.setArguments(data);
        confirmDialog.show(getFragmentManager(), "confirmDialog");
        confirmDialog.setNoticeDialogListener(new ConfirmDialog.NoticeDialogListener() {
            @Override
            public void onDialogPositive(DialogFragment dialog) {
                mAreaColors.clear();
                colorsF = 0;
                mSingleExercise.getAreaList().clear();
                pickImage();
            }

            @Override
            public void onDialogNegative(DialogFragment dialog) {

            }
        });
    }

    private void initColors(){
        mColorList= new int[20];
        mColorList[0] = Color.rgb(0xb1, 0x91, 0x6e);
        mColorList[1] = Color.rgb(0x49, 0x75, 0x9c);
        mColorList[2] = Color.rgb(0xa0, 0x45, 0x0e);
        mColorList[3] = Color.rgb(0x39, 0xad, 0x48);
        mColorList[4] = Color.rgb(0xb6, 0x6a, 0x50);
        mColorList[5] = Color.rgb(0xfe, 0x01, 0x9a);
        mColorList[6] = Color.rgb(0x8c, 0xff, 0xdb);
        mColorList[7] = Color.rgb(0xa4, 0xbe, 0x5c);
        mColorList[8] = Color.rgb(0xcb, 0x77, 0x23);
        mColorList[9] = Color.rgb(0x05, 0x69, 0x6b);
        mColorList[10] = Color.rgb(0xc8, 0x5a, 0x53);
        mColorList[11] = Color.rgb(0x1f, 0xa7, 0x74);
        mColorList[12] = Color.rgb(0x7a, 0x97, 0x03);
        mColorList[13] = Color.rgb(0xac, 0x93, 0x62);
        mColorList[14] = Color.rgb(0x01, 0xa0, 0x49);
        mColorList[15] = Color.rgb(0xd9, 0x54, 0x4d);
        mColorList[16] = Color.rgb(0x6a, 0x6e, 0x09);
        mColorList[17] = Color.rgb(0x82, 0xca, 0xfc);
        mColorList[18] = Color.rgb(0xac, 0xff, 0xfc);
        mColorList[19] = Color.rgb(0x91, 0x09, 0x51);
    }

    private Bitmap loadBitmap(){
        Bitmap bitmap = Utilities.loadBitmapFromFile(IO.getImageFile(mSingleExercise.getUniqueId()));
        Bitmap bitmapMutable = bitmap.copy(bitmap.getConfig(),true);
        return bitmapMutable;
    }

    private void refreshListView(){

        List<Area> areas = mSingleExercise.getAreaList();

        String [] names = new String[areas.size()];
        int [] colors = new int[areas.size()];

        for(int i=0;i<areas.size();i++)
        {
            names[i] = areas.get(i).getName();
            colors[i] = mAreaColors.get(areas.get(i));
        }
        AreasListAdapter adapter;

        adapter = new AreasListAdapter(SingleEditorActivity.this, names,colors);
        mSingleExerciseListView.setAdapter(adapter);
    }

    private void loadImage(String imageFilePath){
        File savedImageFile = IO.getImageFile(mSingleExercise.getUniqueId());
        File savedThumbnail = IO.getThumbnailFile(mSingleExercise.getUniqueId());

        File loadedImageFile = new File(imageFilePath);
        Size imageSize = Utilities.getImageSize(loadedImageFile);

        if(imageSize.getWidth() ==  0 || imageSize.getHeight() == 0){
            showInfoMessage(getString(R.string.not_zero_image_size));
        }
        else {
            Size scaledSize = Utilities.scaleSize(imageSize, new Size(IO.MAX_IMAGE_SIZE, IO.MAX_IMAGE_SIZE));
            Size scaledThumbSize =  Utilities.scaleSize(imageSize, new Size(IO.MAX_THUMB_SIZE, IO.MAX_THUMB_SIZE));

            Bitmap scaledBitmap = Utilities.loadBitmapFromFile(loadedImageFile, scaledSize.getWidth(), scaledSize.getHeight());
            Bitmap scaledThumbBitmap = Bitmap.createScaledBitmap(scaledBitmap, scaledThumbSize.getWidth(), scaledThumbSize.getHeight(), true);

            try {
                Utilities.saveBitmapToFile(savedImageFile,scaledBitmap);
                Utilities.saveBitmapToFile(savedThumbnail,scaledThumbBitmap);

                mSmartImage = new SmartImage(scaledBitmap.copy(scaledBitmap.getConfig(), true));
                mSingleEditorImageView.setImage(mSmartImage);

            } catch (IOException e) {
                showInfoMessage(getString(R.string.load_image_failed));
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == SELECT_PHOTO && resultCode == Activity.RESULT_OK) {
            String path = Utilities.getPath(SingleEditorActivity.this, data.getData());
            if(path != null){
                loadImage(path);
            }
            else showInfoMessage(getString(R.string.load_image_failed));
        }
    }
    @Override
    public void onClickedImage(int x, int y) {
        float scaleX = (float) mSmartImage.getWidth() / mSingleEditorImageView.getImageSize().getWidth();//0.5
        float scaleY = (float) mSmartImage.getHeight() / mSingleEditorImageView.getImageSize().getHeight();

        int clickToOrigX = (int) (x * scaleX);
        int clickToOrigY = (int) (y * scaleY);

        Area area;
        if(mSmartImage.getPixelArea(clickToOrigX,clickToOrigY) == Pixel.AREA_KEY_NOT_SET &&
                !mSmartImage.isPixelBorder(clickToOrigX,clickToOrigY)) {

            area = new Area(mSingleExercise.getMaxId());
            area.setxRepresentive(clickToOrigX);
            area.setyRepresentive(clickToOrigY);

            showAreaInput(area, AreaInputDialog.DIALOG_NEW);
        }
        else if(!mSmartImage.isPixelBorder(clickToOrigX,clickToOrigY)) {
            area = mSingleExercise.getAreaById(mSmartImage.getPixelArea(clickToOrigX, clickToOrigY));

            showAreaInput(area, AreaInputDialog.DIALOG_EDIT);
        }
    }
    public void showAreaInput(Area area,int type){
        Bundle date = new Bundle();
        date.putParcelable(AreaInputDialog.AREA, area);
        date.putInt(AreaInputDialog.TYPE, type);

        mAreaInputDialog = new AreaInputDialog();
        mAreaInputDialog.setArguments(date);
        mAreaInputDialog.setOnResultListener(this);
        mAreaInputDialog.show(getFragmentManager(), "areaInputDialog");
    }

    @Override
    public void onSaved(Area area) {
        if(!mAreaColors.containsKey(area)){
            Log.e("x","xxx");
            colorsF %= mColorList.length;
            int color = mColorList[colorsF];colorsF++;

            operationFill = new OperationFill(mSmartImage,area,color,OperationFill.ID_COLOR);
            operationFill.mDelegate  = new AsyncResponse() {
                @Override
                public void processFinish(String output) {
                    mSingleEditorImageView.refresh();
                    mAreaInputDialog.dismiss();
                }
            };
            operationFill.execute();

            mAreaColors.put(area, color);
        }
        else {
            mAreaInputDialog.dismiss();
        }

        mSingleExercise.addArea(area);
        refreshListView();
    }

    @Override
    public void onRemoved(Area area) {
        for(int i=0;i<mSmartImage.getWidth();i++){
            for(int j=0;j<mSmartImage.getHeight();j++){
                if(mSmartImage.getPixelArea(i,j) == area.getUniqueId())
                    mSmartImage.setPixelArea(i,j,Pixel.AREA_KEY_NOT_SET);
            }
        }

        mSingleExercise.removeArea(area);
        mAreaColors.remove(area);
        refreshListView();
        mSingleEditorImageView.refresh();
        mAreaInputDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        if(mType == EDIT) {
            if(isSingleExerciseValid())
                save();
        }
        else if(mType == CREATE){
            setResult(RESULT_CANCELED);
            finish();
        }
    }
}
