package cz.cvut.fit.dragon.imagepointmodul.editor;


public interface AsyncResponse {
    void processFinish(String output);
}
