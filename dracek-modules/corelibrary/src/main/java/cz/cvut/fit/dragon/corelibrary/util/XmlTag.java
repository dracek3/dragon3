package cz.cvut.fit.dragon.corelibrary.util;

/**
 * This class contains xml tags used for saving exercise to xml files
 */
public final class XmlTag {
    public static final String EXERCISE = "Exercise";
    public static final String SINGLE_EXERCISE = "SingleExercise";
    public static final String ID = "Id";
    public static final String NAME = "Name";

    public static final String TRANSFORM_TYPE = "TransformType";
    public static final String NUMBER_OF_IMAGES = "NumberOfImages";

    public static final String SPLITS_X = "SplitsX";
    public static final String SPLITS_Y = "SplitsY";

    public static final String IMAGE_OVERLAY = "ImageOverLay";
    public static final String OVERLAY_HORIZONTAL = "OverlayHorizontal";
    public static final String OVERLAY_VERTICAL  = "OverlayVertical";
    public static final String OVERLAY_MAIN_DIAGONAL  = "OverlayMainDiagonal";
    public static final String OVERLAY_SECOND_DIAGONAL = "OverlaySecondDiagonal";
    public static final String OVERLAY_DEGREE  = "OverlayDegree";
    public static final String OVERLAY_LINE_SIZE  = "OverlayLineSize";

    public static final String IMAGES_TOP = "ImagesTop";
    public static final String IMAGES_BOTTOM = "ImageBottom";

    public static final String IMAGE = "Image";
    public static final String IMAGE_ID = "ImageId";
    public static final String SPECIAL = "Special";

    public static final String IMAGES = "Images";
    public static final String TASK = "Task";

    public static final String AREAS = "Areas";
    public static final String AREA = "Area";
    public static final String AREA_ID = "AreaId";
    public static final String AREA_RANDOM_COLOR = "RandomColor";
    public static final String AREA_NAME = "AreaName";
    public static final String AREA_COLOR = "Color";
    public static final String AREA_REPRESENTATIVE_X  = "RepresentativeX";
    public static final String AREA_REPRESENTATIVE_Y  = "RepresentativeY";

    public static final String RESULTS = "Results";
    public static final String TIME = "Time";
    public static final String SCORE = "Score";
    public static final String RESULT_NAME_ATTRIBUTE = "name";
    public static final String TIME_NAME_ATTRIBUTE_VALUE = "Celkový čas";
    public static final String SCORE_NAME_ATTRIBUTE_VALUE = "Konečné skóre";
}
