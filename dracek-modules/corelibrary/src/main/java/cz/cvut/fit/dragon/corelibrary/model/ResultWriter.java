package cz.cvut.fit.dragon.corelibrary.model;

import android.util.Xml;

import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import cz.cvut.fit.dragon.corelibrary.util.IO;
import cz.cvut.fit.dragon.corelibrary.util.XmlTag;

/**
 * This class write result of exercise to xml string
 */
public class ResultWriter {

    long mTotalTime;
    int mPercentage;
    String mTotalTimeUnitText;
    String mPercentageUnitText;

    public ResultWriter(long totalTime,int percentage,String totalTimeUnitText, String percentageUnitText){
        this.mTotalTime = totalTime;
        this.mPercentage = percentage;
        this.mTotalTimeUnitText = totalTimeUnitText;
        this.mPercentageUnitText = percentageUnitText;
    }

    /**
     * This method writes results to xml string
     * @return Xml results
     * @throws IOException
     */
    public String getXmlResultString() throws IOException {
        XmlSerializer xmlSerializer = Xml.newSerializer();
        ByteArrayOutputStream output =  new ByteArrayOutputStream();
        xmlSerializer.setOutput(output, IO.XML_ENCODING);
        writeResultXml(xmlSerializer);
        return output.toString(IO.XML_ENCODING);
    }

    private void writeResultXml(XmlSerializer xmlSerializer) throws IOException{
        xmlSerializer.startDocument(IO.XML_ENCODING, false);
        xmlSerializer.startTag("", XmlTag.RESULTS);

        xmlSerializer.startTag("", XmlTag.TIME);
        xmlSerializer.attribute("", XmlTag.RESULT_NAME_ATTRIBUTE, XmlTag.TIME_NAME_ATTRIBUTE_VALUE);
        xmlSerializer.text(String.valueOf(mTotalTime) + mTotalTimeUnitText);
        xmlSerializer.endTag("", XmlTag.TIME);

        xmlSerializer.startTag("", XmlTag.SCORE);
        xmlSerializer.attribute("", XmlTag.RESULT_NAME_ATTRIBUTE, XmlTag.SCORE_NAME_ATTRIBUTE_VALUE);
        xmlSerializer.text(String.valueOf(mPercentage) + mPercentageUnitText);
        xmlSerializer.endTag("", XmlTag.SCORE);

        xmlSerializer.endTag("", XmlTag.RESULTS);
        xmlSerializer.flush();
    }
}
