package cz.cvut.fit.dragon.corelibrary.util;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import cz.cvut.fit.dragon.corelibrary.R;

/**
 * This class is used for showing simple message with one button
 */
public class InfoDialog extends DialogFragment {

    public static final String MESSAGE = "MESSAGE";

    /**
     * This method creates new info dialog
     * @param message Message to be shown in dialog
     * @return New info dialog
     */
    public static InfoDialog createNewDialog(String message) {
        InfoDialog infoDialog = new InfoDialog();
        Bundle data = new Bundle();
        data.putString(MESSAGE,message);
        infoDialog.setArguments(data);
        return infoDialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(getArguments().getString(MESSAGE,""))
                .setPositiveButton(getString(R.string.info_dialog_button), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(noticeDialogListener!=null)
                            noticeDialogListener.onDialogPositiveClick(InfoDialog.this);
                    }
                });
        return builder.create();
    }
    NoticeDialogListener noticeDialogListener;

    public interface NoticeDialogListener {
        void onDialogPositiveClick(DialogFragment dialog);
    }

    public void setNoticeDialogListener(NoticeDialogListener eventListener) {
        noticeDialogListener = eventListener;
    }
}
