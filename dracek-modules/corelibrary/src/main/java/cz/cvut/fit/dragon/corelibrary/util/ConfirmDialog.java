package cz.cvut.fit.dragon.corelibrary.util;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import cz.cvut.fit.dragon.corelibrary.R;

/**
 * This class is used for showing simple dialog with message and two buttons
 */
public class ConfirmDialog extends DialogFragment {

    public static final String MESSAGE = "MESSAGE";

    /**
     * This method creates new confirm dialog
     * @param message Message to be shown in dialog
     * @return New confirm dialog
     */
    public static ConfirmDialog createNewDialog(String message) {
        ConfirmDialog confirmDialog = new ConfirmDialog();
        Bundle data = new Bundle();
        data.putString(MESSAGE,message);
        confirmDialog.setArguments(data);
        return confirmDialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(getArguments().getString(MESSAGE,""))
                .setPositiveButton(getString(R.string.confirm_dialog_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (noticeDialogListener != null) {
                            noticeDialogListener.onDialogPositive(ConfirmDialog.this);
                        }
                        dismiss();
                    }
                })
                .setNegativeButton(getString(R.string.confirm_dialog_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (noticeDialogListener != null) {
                            noticeDialogListener.onDialogNegative(ConfirmDialog.this);
                        }
                        dismiss();
                    }
                });
        return builder.create();
    }

    NoticeDialogListener noticeDialogListener;

    public interface NoticeDialogListener {
        void onDialogPositive(DialogFragment dialog);
        void onDialogNegative(DialogFragment dialog);
    }

    public void setNoticeDialogListener(NoticeDialogListener eventListener) {
        noticeDialogListener = eventListener;
    }

}
