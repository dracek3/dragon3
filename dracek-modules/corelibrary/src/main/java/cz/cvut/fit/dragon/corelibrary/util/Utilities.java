package cz.cvut.fit.dragon.corelibrary.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.nio.channels.FileChannel;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import cz.cvut.fit.dragon.corelibrary.R;

/**
 * Class Utilities contains basic static methods used throughout the whole application.
 * This class should never be initialized.
 */
public final class Utilities {

    private Utilities(){
        throw new AssertionError();
    }

    /**
     * This method copies file to a specified directory
     * @param imageFilePath path of a file to be copied
     * @param dir final directory
     * @throws IOException
     */
    public static void copyImageToDir(String imageFilePath,File dir) throws IOException{

        File src = new File(imageFilePath);
        File dst = new File(dir, src.getName());

        if(!src.getPath().equals(dst.getPath()))
            copyFile(src, dst);
    }

    /**
     * This method copies source file to destination file
     * @param src source file
     * @param dst destination file
     * @throws IOException
     */
    private static void copyFile(File src,File dst) throws IOException {
        FileChannel inChannel;
        FileChannel outChannel;

        inChannel = new FileInputStream(src).getChannel();
        outChannel = new FileOutputStream(dst).getChannel();
        inChannel.transferTo(0, inChannel.size(), outChannel);

        inChannel.close();
        outChannel.close();
    }

    /**
     * This method gets path from Uri
     * @param context application context
     * @param uri Uri
     * @return path
     */
    public static String getPath(Context context,Uri uri)
    {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index =  cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s=cursor.getString(column_index);
        cursor.close();
        return s;
    }

    /**
     * This method saves bitmap to file
     * @param file File
     * @param bitmap Bitmap
     * @throws IOException
     */
    public static void saveBitmapToFile(File file, Bitmap bitmap) throws IOException
    {
        if(bitmap == null) Log.e("xx","null");
        else Log.e("xx","not null");
        FileOutputStream fos = new FileOutputStream(file);
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
        fos.close();
    }

    /**
     * This method decodes bitmap with requested width and height to avoid getting memory exception
     * @param image Image file
     * @param reqWidth Requested width
     * @param reqHeight Requested height
     * @return decoded Bitmap
     */
    public static Bitmap loadBitmapFromFile(File image, int reqWidth, int reqHeight) {

        String path = image.getPath();

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;

        Bitmap tmp = BitmapFactory.decodeFile(path, options);
        return Bitmap.createScaledBitmap(tmp, reqWidth, reqHeight, true);
    }

    /**
     * This method decodes bitmap with requested width and height to avoid getting memory exception
     * @param res resources (R)
     * @param resId ID of resource image
     * @param reqWidth requested width of image
     * @param reqHeight requested high of image
     * @return decoded Bitmap
     */
    public static Bitmap loadBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(res, resId, options);
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
            options.inJustDecodeBounds = false;
        Bitmap bit = BitmapFactory.decodeResource(res, resId, options);;
        bit = Bitmap.createScaledBitmap(bit, reqWidth,reqHeight, false);
            return bit;
    }

    /**
     * This method calculates the largest inSampleSize value that is a power of 2 and keeps both
     * height and width larger than the requested height and width.
     * @param options Options
     * @param reqWidth Requested width
     * @param reqHeight Requested height
     * @return
     */
    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    /**
     * This method loads bitmap from a file {@code image}
     * @param image Image file
     * @return Bitmap
     */
    public static Bitmap loadBitmapFromFile(File image) {
        return BitmapFactory.decodeFile(image.getPath());
    }

    /**
     * This method calculates size of image without loading the image
     * @param image Image file
     * @return Size of an image
     */
    public static Size getImageSize(File image){

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(image.getPath(), options);

        return new Size(options.outWidth,options.outHeight);
    }

    /**
     * This method scales size of object to fit in another object {@code maxObjectSize}, if size is less than maxSize nothing is done
     * @param objectSize size to be scaled
     * @param maxObjectSize maximum sze
     * @return Size that fits {@code maxObjectSize}
     */
    public static Size scaleSize(Size objectSize,Size maxObjectSize){

        int width = objectSize.getWidth();
        int height = objectSize.getHeight();

        if(width > maxObjectSize.getWidth() || height > maxObjectSize.getHeight()){

            float widthScale = 0, heightScale = 0;
            if (width != 0)
                widthScale = (float) maxObjectSize.getWidth() / width;
            if (height != 0)
                heightScale = (float) maxObjectSize.getHeight() / height;

            float scale = Math.min(widthScale, heightScale);

            width = (int) (width * scale);
            height = (int) (height * scale);
        }

        return new Size(width,height);
    }



    /**
     * This method shows dialog with 0-5 smiles according to the score
     * @param score score in percentage, should be 0.0-1.0 (if not behaves as the closer of the bounds)
     * @param activity activity it is called from (parent activity)
     */
    public static void showSmiley(double score, Activity activity){
        LayoutInflater in = activity.getLayoutInflater();
        View view = in.inflate(R.layout.result_message_layout, null);

        TextView text = (TextView) view.findViewById(R.id.textView);
        ImageView iw1 = (ImageView) view.findViewById(R.id.imageView);
        ImageView iw2 = (ImageView) view.findViewById(R.id.imageView2);
        ImageView iw3 = (ImageView) view.findViewById(R.id.imageView3);
        ImageView iw4 = (ImageView) view.findViewById(R.id.imageView4);
        ImageView iw5 = (ImageView) view.findViewById(R.id.imageView5);

        int percentage = (int) (score*100);
        text.setText(percentage+"%");

        if(score >= 0.2){
            iw1.setImageResource(R.drawable.smiley);
        }
        if(score >= 0.4){
            iw2.setImageResource(R.drawable.smiley);
        }
        if(score >= 0.6){
            iw3.setImageResource(R.drawable.smiley);
        }
        if(score >= 0.8){
            iw4.setImageResource(R.drawable.smiley);
        }
        if(score >= 1){
            iw5.setImageResource(R.drawable.smiley);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setView(view).setNeutralButton(R.string.ok, null);
        builder.create().show();
    }

    /**
     * This method extraxts score from xml result
     * @param xml xml document to be parsed
     * @return double 0.0-1.0, 0 on fail
     */
    public static double getResult(String xml) {
        double result = 0;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new InputSource(new StringReader(xml)));

            Node rootElement = document.getDocumentElement();
            Node child = rootElement.getLastChild();
            String s = child.getTextContent().trim().replace("%", "");
            result = Double.parseDouble(s)/100.0;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return result;
    }

    public static void showOKCheck(Context context){
        Activity activity = (Activity) context;
        LayoutInflater inflater = LayoutInflater.from(activity);
        View view = inflater.inflate(R.layout.toast_layout,
                (ViewGroup) activity.findViewById(R.id.toast_layout_root));

        View rootView = ((Activity)context).getWindow().getDecorView().findViewById(android.R.id.content);
        ImageView iw = (ImageView) view.findViewById(R.id.toast_imageView);
        iw.setImageResource(R.drawable.correctbtn);

        Toast toast = new Toast(activity);
        toast.setView(view);
        toast.show();
    }

    public static void showXCheck(Context context){
        Activity activity = (Activity) context;
        LayoutInflater inflater = LayoutInflater.from(activity);
        View view = inflater.inflate(R.layout.toast_layout,
                (ViewGroup) activity.findViewById(R.id.toast_layout_root));

        View rootView = ((Activity)context).getWindow().getDecorView().findViewById(android.R.id.content);
        ImageView iw = (ImageView) view.findViewById(R.id.toast_imageView);
        iw.setImageResource(R.drawable.wrongbtn);

        Toast toast = new Toast(activity);
        toast.setView(view);
        toast.show();
    }


}
