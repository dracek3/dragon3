package cz.cvut.fit.dragon.corelibrary.model;

import android.os.SystemClock;

/**
 * This class measures time that took user to complete the game exercise
 */
public class Time {

    long mStartTime;
    long mPauseStartTime;
    long mPauseTime;
    boolean mPaused;

    public void initTime(){
        mStartTime = SystemClock.elapsedRealtime();
        mPauseTime = 0;
        mPaused = false;
    }

    public void resume() {
        if(mPaused) mPauseTime += SystemClock.elapsedRealtime() - mPauseStartTime;
    }

    public void pause() {
        mPaused = true;
        mPauseStartTime = SystemClock.elapsedRealtime();
    }

    public long getTimeSec(){
        return (SystemClock.elapsedRealtime() - mStartTime - mPauseTime) / 1000;
    }
}
