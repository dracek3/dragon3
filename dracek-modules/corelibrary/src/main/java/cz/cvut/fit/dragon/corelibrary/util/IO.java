package cz.cvut.fit.dragon.corelibrary.util;

import android.content.Context;

import java.io.File;

import cz.cvut.fit.dragon.corelibrary.R;

public class IO {
    /**
     * File prefix used for all images
     */
    public static String IMAGE_FILE_NAME_PREFIX;
    /**
     * File prefix used for all thumbnails
     */
    public static String THUMBNAIL_FILE_NAME_PREFIX;
    /**
     * Directory where all files are stored
     */
    public static File IMAGES_DIR;
    /**
     * Xml encoding for saving exercise
     */
    public static String XML_ENCODING;
    /**
     * Maximum size of images
     */
    public static int MAX_IMAGE_SIZE;
    /**
     * Maximum size of thumbnails
     */
    public static int MAX_THUMB_SIZE;

    public static void readResources(Context context){
        IMAGE_FILE_NAME_PREFIX = context.getResources().getString(R.string.image_file_name_prefix);
        THUMBNAIL_FILE_NAME_PREFIX = context.getResources().getString(R.string.thumbnail_file_name_prefix);
        IMAGES_DIR = context.getFilesDir();
        XML_ENCODING = "UTF-8";
        MAX_IMAGE_SIZE = 500;
        MAX_THUMB_SIZE = 200;
    }

    /**
     * This method creates file associated with given single exercise id
     * @param singleExerciseId - Single exercise id
     * @return Image file associated with given single exercise id
     */
    public static File getImageFile(int singleExerciseId){
        return new File(IMAGES_DIR,IMAGE_FILE_NAME_PREFIX + singleExerciseId);
    }

    /**
     * This method creates thumbnail file associated with given single exercise id
     * @param singleExerciseId
     * @return Thumbnail file associated with given single exercise id
     */
    public static File getThumbnailFile(int singleExerciseId){
        return new File(IMAGES_DIR,THUMBNAIL_FILE_NAME_PREFIX + singleExerciseId);
    }

    /**
     * This method creates file associated with given single exercise id and image id
     * @param singleExerciseId
     * @param imageId
     * @return Image file associated with single exercise id and image id
     */
    public static File getImageFile(int singleExerciseId,int imageId){
        return new File(IMAGES_DIR,IMAGE_FILE_NAME_PREFIX + singleExerciseId + "_" + imageId);
    }
}
