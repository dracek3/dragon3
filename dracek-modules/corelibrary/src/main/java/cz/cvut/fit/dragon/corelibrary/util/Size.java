package cz.cvut.fit.dragon.corelibrary.util;


public class Size {
    int width;
    int height;

    public Size(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }


}
