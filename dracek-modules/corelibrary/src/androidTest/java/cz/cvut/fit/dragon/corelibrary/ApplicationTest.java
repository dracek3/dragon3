package cz.cvut.fit.dragon.corelibrary;

import android.app.Application;
import android.test.ApplicationTestCase;


import org.junit.Test;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import cz.cvut.fit.dragon.corelibrary.util.IO;
import cz.cvut.fit.dragon.corelibrary.util.Utilities;


public class ApplicationTest extends ApplicationTestCase<Application> {

    public ApplicationTest() {
        super(Application.class);
    }

    @Override
    protected void setUp() throws Exception{
        super.setUp();
        createApplication();
        IO.readResources(getContext());
    }

    @Test
    public void testImagePath(){
        int singleExerciseNumber = 1;
        String expectedValue = getContext().getFilesDir() + "image" + singleExerciseNumber;
        File exFile = new File(expectedValue);
        assertEquals(exFile.getPath(), IO.getImageFile(singleExerciseNumber).getPath());
    }

    @Test
    public void testMultipleImagePath(){
        int singleExerciseNumber = 1;
        int imageNumber = 5;
        String expectedValue = getContext().getFilesDir() + "image" + singleExerciseNumber + "_" + imageNumber ;
        File exFile = new File(expectedValue);
        assertEquals(exFile.getPath(), IO.getImageFile(singleExerciseNumber,imageNumber).getPath());
    }

    @Test
    public void testCopyImageToDir(){

        String name = "testFile.txt";
        try {
            File myFile = new File("/sdcard/" + name);
            myFile.createNewFile();
            FileOutputStream fOut = new FileOutputStream(myFile);
            OutputStreamWriter myOutWriter =
                    new OutputStreamWriter(fOut);
            myOutWriter.append("some random text");
            myOutWriter.close();
            fOut.close();

            Utilities.copyImageToDir(myFile.getPath(), getContext().getFilesDir());

            File ex = new File(getContext().getFilesDir(),name);
            assertTrue(ex.exists());

        } catch (Exception e) {
            fail("could not save the file");
        }
    }

}