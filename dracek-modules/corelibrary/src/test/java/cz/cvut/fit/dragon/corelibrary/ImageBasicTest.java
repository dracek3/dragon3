package cz.cvut.fit.dragon.corelibrary;

import junit.framework.Assert;

import org.junit.Test;

import cz.cvut.fit.dragon.corelibrary.util.Size;
import cz.cvut.fit.dragon.corelibrary.util.Utilities;

public class ImageBasicTest {

    @Test
    public void testGetImageScaleLoad(){

        Size expectedSize = new Size(66,80);

        Size size = new Size(100,120);
        Size maxSize = new Size(80,80);

        Size res = Utilities.scaleSize(size,maxSize);

        Assert.assertEquals(expectedSize.getWidth(),res.getWidth());
        Assert.assertEquals(expectedSize.getHeight(),res.getHeight());
    }
}
