package cz.cvut.fit.dragon.imagecolormodul.editor;

import android.graphics.Color;
import android.graphics.Point;
import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import cz.cvut.fit.dragon.imagecolormodul.model.Area;
import cz.cvut.fit.dragon.imagecolormodul.model.SmartImage;

public class OperationFill extends AsyncTask<String, Void, String> {

    public AsyncResponse mDelegate = null;
    SmartImage mSmartImage;
    List<Area> mAreaList;
    Queue<Point> mQueue;

    int mRandomColorFirst;
    int mRandomColorSecond;

    boolean [][] mPainted;

    public OperationFill(SmartImage smartImage,Point clicked,int color){
        this.mSmartImage = smartImage;
        Area a = new Area(0);
        a.setRandomColor(false);
        a.setyRepresentive(clicked.y);
        a.setxRepresentive(clicked.x);
        a.setColor(color);

        this.mAreaList = new ArrayList<>();
        mAreaList.add(a);

        mQueue = new LinkedList<>();
        mPainted = new boolean[mSmartImage.getWidth()][mSmartImage.getHeight()];
    }

    public OperationFill(SmartImage smartImage,List<Area> areaList,int randomColorFirst,int randomColorSecond){
        this.mSmartImage = smartImage;
        this.mAreaList = areaList;
        this.mRandomColorFirst = randomColorFirst;
        this.mRandomColorSecond = randomColorSecond;
        mQueue = new LinkedList<>();

        mPainted = new boolean[mSmartImage.getWidth()][mSmartImage.getHeight()];
    }

    @Override
    protected String doInBackground(String... params) {

        for(int i=0;i<mAreaList.size();i++){
            fillArea(mAreaList.get(i));
        }
        return "Executed";
    }
    private void fillArea(Area area){

        Point first = new Point(area.getxRepresentive(),area.getyRepresentive());
        mQueue.add(first);

        setPixel(first, area);

        while (!mQueue.isEmpty()) {
            Point x = mQueue.poll();
            set(new Point(x.x + 1, x.y), area);
            set(new Point(x.x-1,x.y),area);
            set(new Point(x.x,x.y+1),area);
            set(new Point(x.x,x.y-1),area);
        }
    }
    private void set(Point x,Area area){
        if(x.x >= mSmartImage.getWidth() || x.x < 0 || x.y >= mSmartImage.getHeight() || x.y < 0)return;

        if (!mSmartImage.isPixelBorder(x.x, x.y) && !mPainted[x.x][x.y]) {
            mQueue.add(new Point(x.x, x.y));
            mSmartImage.setPixelArea(x.x, x.y, area.getUniqueId());
            mPainted[x.x][x.y] = true;

            setPixel(x, area);
        }
    }
    private void setPixel(Point x,Area area){
        if(area.isRandomColor()){
            if(((x.x) % 20) < 10) mSmartImage.setPixel(x.x, x.y, mRandomColorFirst);
            else mSmartImage.setPixel(x.x, x.y, mRandomColorSecond);
        }
        else {
            mSmartImage.setPixel(x.x, x.y, area.getColor());
        }
    }
    @Override
    protected void onPostExecute(String result) {
        mDelegate.processFinish(result);
    }
}
