package cz.cvut.fit.dragon.imagecolormodul.model;

import android.graphics.Bitmap;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.cvut.fit.dragon.corelibrary.util.IO;
import cz.cvut.fit.dragon.corelibrary.util.LoadExerciseException;
import cz.cvut.fit.dragon.corelibrary.util.Utilities;
import cz.cvut.fit.dragon.corelibrary.util.XmlTag;


public class ExerciseReader {

    Exercise mExercise;
    String mXml;
    ArrayList<String> mImageFilePaths;

    public ExerciseReader(String xml, ArrayList<String> imageFilePaths){
        this.mXml = xml;
        this.mImageFilePaths = imageFilePaths;

        mExercise = new Exercise();
    }

    public Exercise load() throws XmlPullParserException, IOException, LoadExerciseException {

        XmlPullParserFactory xmlFactoryObject;
        xmlFactoryObject = XmlPullParserFactory.newInstance();
        XmlPullParser xmlPullParser = xmlFactoryObject.newPullParser();
        xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        xmlPullParser.setInput(new ByteArrayInputStream(mXml.getBytes(IO.XML_ENCODING)), IO.XML_ENCODING);
        parseXML(xmlPullParser);
        copyImages();

        return mExercise;
    }

    private void copyImages() throws IOException, LoadExerciseException{
        for(String imageFilePath : mImageFilePaths){
            File imageFile = new File(imageFilePath);
            if(!imageFile.exists())throw new LoadExerciseException();
            Utilities.copyImageToDir(imageFile.getPath(), IO.IMAGES_DIR);
        }
    }

    private void parseXML(XmlPullParser xmlPullParser) throws XmlPullParserException, IOException, LoadExerciseException {

        int eventType = xmlPullParser.getEventType();
        String text = "";

        SingleExercise singleExercise = null;
        List<Area> areaList = null;
        Area area = null;

        while (eventType != XmlPullParser.END_DOCUMENT){
            String tagName = xmlPullParser.getName();

            if(eventType == XmlPullParser.START_TAG){
                if(tagName.equals(XmlTag.SINGLE_EXERCISE))
                    singleExercise = new SingleExercise();
                else if(tagName.equals(XmlTag.AREAS))
                    areaList = new ArrayList<>();
                else if(tagName.equals(XmlTag.AREA))
                    area = new Area();
            }
            else if(eventType == XmlPullParser.TEXT){
                text = xmlPullParser.getText();
            }
            else if(eventType == XmlPullParser.END_TAG){
                if(tagName.equals(XmlTag.ID))
                    singleExercise.setUniqueId(Integer.valueOf(text));
                else if(tagName.equals(XmlTag.NAME))
                    singleExercise.setName(text);

                else if(tagName.equals(XmlTag.AREA_ID))
                    area.setUniqueId(Integer.valueOf(text));
                else if(tagName.equals(XmlTag.AREA_NAME))
                    area.setName(text);
                else if(tagName.equals(XmlTag.AREA_RANDOM_COLOR))
                    area.setRandomColor(Boolean.valueOf(text));
                else if(tagName.equals(XmlTag.AREA_COLOR))
                    area.setColor(Integer.valueOf(text));
                else if(tagName.equals(XmlTag.AREA_REPRESENTATIVE_X))
                    area.setxRepresentive(Integer.valueOf(text));
                else if(tagName.equals(XmlTag.AREA_REPRESENTATIVE_Y))
                    area.setyRepresentive(Integer.valueOf(text));
                else if(tagName.equals(XmlTag.AREA))
                    areaList.add(area);
                else if(tagName.equals(XmlTag.AREAS))
                    singleExercise.setAreaList(areaList);
                else if(tagName.equals(XmlTag.SINGLE_EXERCISE)) {
                    if(singleExercise != null) {
                        Bitmap thumb = Utilities.loadBitmapFromFile(IO.getThumbnailFile(singleExercise.getUniqueId()));
                        mExercise.addSingleExercise(singleExercise, thumb);
                    }
                    else throw new LoadExerciseException();
                }
                text="";
            }
            eventType = xmlPullParser.next();
        }
    }
}
