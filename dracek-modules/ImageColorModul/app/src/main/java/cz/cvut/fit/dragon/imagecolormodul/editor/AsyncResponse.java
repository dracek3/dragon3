package cz.cvut.fit.dragon.imagecolormodul.editor;


public interface AsyncResponse {
    void processFinish(String output);
}
