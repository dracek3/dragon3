package cz.cvut.fit.dragon.imagecolormodul.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Area implements Parcelable{

    int uniqueId;
    boolean randomColor;
    String name;
    int color;
    int xRepresentive;
    int yRepresentive;

    public Area(int uniqueId){
        this.uniqueId = uniqueId;
    }

    public Area(){

    }

    protected Area(Parcel in) {
        uniqueId = in.readInt();
        randomColor = in.readByte() != 0;
        name = in.readString();
        color = in.readInt();
        xRepresentive = in.readInt();
        yRepresentive = in.readInt();
    }

    public static final Creator<Area> CREATOR = new Creator<Area>() {
        @Override
        public Area createFromParcel(Parcel in) {
            return new Area(in);
        }

        @Override
        public Area[] newArray(int size) {
            return new Area[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isRandomColor() {
        return randomColor;
    }

    public void setRandomColor(boolean randomColor) {
        this.randomColor = randomColor;
    }

    public int getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    public int getxRepresentive() {
        return xRepresentive;
    }

    public void setxRepresentive(int xRepresentive) {
        this.xRepresentive = xRepresentive;
    }

    public int getyRepresentive() {
        return yRepresentive;
    }

    public void setyRepresentive(int yRepresentive) {
        this.yRepresentive = yRepresentive;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(uniqueId);
        dest.writeByte((byte) (randomColor ? 1 : 0));
        dest.writeString(name);
        dest.writeInt(color);
        dest.writeInt(xRepresentive);
        dest.writeInt(yRepresentive);
    }
}
