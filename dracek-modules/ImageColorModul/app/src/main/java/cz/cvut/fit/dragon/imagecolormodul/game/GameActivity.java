package cz.cvut.fit.dragon.imagecolormodul.game;

import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import cz.cvut.fit.dragon.corelibrary.model.Time;
import cz.cvut.fit.dragon.imagecolormodul.R;
import cz.cvut.fit.dragon.imagecolormodul.editor.AsyncResponse;
import cz.cvut.fit.dragon.imagecolormodul.editor.OperationFill;
import cz.cvut.fit.dragon.imagecolormodul.model.SingleExerciseView;
import cz.cvut.fit.dragon.imagecolormodul.model.Area;
import cz.cvut.fit.dragon.imagecolormodul.model.BaseActivity;
import cz.cvut.fit.dragon.imagecolormodul.model.ColorView;
import cz.cvut.fit.dragon.corelibrary.model.ResultWriter;
import cz.cvut.fit.dragon.imagecolormodul.model.SingleExercise;
import cz.cvut.fit.dragon.imagecolormodul.model.SmartImage;
import cz.cvut.fit.dragon.corelibrary.util.ConfirmDialog;
import cz.cvut.fit.dragon.corelibrary.util.IO;
import cz.cvut.fit.dragon.corelibrary.util.InfoDialog;
import cz.cvut.fit.dragon.corelibrary.util.Utilities;

public class GameActivity extends BaseActivity implements View.OnClickListener,AsyncResponse,SingleExerciseView.OnClickImageListener{

    OperationFill mOperationFill;
    LinearLayout mGameViewLinearLayout;
    RelativeLayout mColorPickerLinearLayout;
    ImageButton mCloseGameButton;
    TextView mCurrentSingleExerciseNameTextView;
    ListView mTaskListView;
    SingleExerciseView mGameView;

    ImageButton mEvaluateButton;

    ColorView[] mColorViews;
    ColorView mSelectedColorView;
    SmartImage mSmartImage;

    ExerciseManager mExerciseManager;

    Time mTime;
    SingleExercise mCurrentSingleExercise;

    String [] mColorNames;
    String [] mColorValues;

    Typeface font;

    protected void init() {
        setContentView(R.layout.activity_game);

        mEvaluateButton = (ImageButton)findViewById(R.id.evaluate_game_button);
        mCloseGameButton = (ImageButton)findViewById(R.id.close_game_button);
        mCurrentSingleExerciseNameTextView = (TextView)findViewById(R.id.exercise_names_text_view);
        mGameViewLinearLayout = (LinearLayout)findViewById(R.id.game_view_linear_layout);
        mColorPickerLinearLayout = (RelativeLayout)findViewById(R.id.color_picker_linear_layout);
        mTaskListView = (ListView)findViewById(R.id.task_list_view_game);

        mGameView = new SingleExerciseView(GameActivity.this);
        mGameViewLinearLayout.addView(mGameView);

        mEvaluateButton.setOnClickListener(this);
        mCloseGameButton.setOnClickListener(this);

        mExerciseManager = new ExerciseManager(mExercise);
        mCurrentSingleExercise = mExerciseManager.getCurrentSingleExercise();

        mColorNames = getResources().getStringArray(R.array.colors_names_game);
        mColorValues = getResources().getStringArray(R.array.colors_values);

        font = Typeface.createFromAsset(getAssets(), "roboto.ttf");

        mTime = new Time();
        mTime.initTime();
        initColorPicker();
        setSingleExercise();

        mGameView.setOnClickImageListener(this);
    }

    protected void noExerciseProvided() {
        showMessage(getString(R.string.load_exercise_failed), true);
    }


    private void setSingleExercise(){

        for(int i=0;i<mCurrentSingleExercise.getAreaList().size();i++){
            if(mCurrentSingleExercise.getAreaList().get(i).isRandomColor()){
                int randomColor = Color.parseColor(mColorValues[new Random(System.nanoTime()).nextInt(mColorValues.length)]);
                mCurrentSingleExercise.getAreaList().get(i).setColor(randomColor);
            }
        }

        mSmartImage = new SmartImage(loadBitmapMutable(mCurrentSingleExercise));
        mGameView.setImage(mSmartImage);

        mCurrentSingleExerciseNameTextView.setText(mCurrentSingleExercise.getName());
        mCurrentSingleExerciseNameTextView.setTypeface(font);
        refreshListView();
        mGameView.refresh();
    }

    private Bitmap loadBitmapMutable(SingleExercise singleExercise){
        Bitmap image = Utilities.loadBitmapFromFile(IO.getImageFile(singleExercise.getUniqueId()));
        return image.copy(image.getConfig(), true);
    }

    private void initColorPicker(){
        mColorViews = new ColorView[10];
        mColorViews[0] = (ColorView)findViewById(R.id.color_picked_image1);
        mColorViews[1] = (ColorView)findViewById(R.id.color_picked_image2);
        mColorViews[2] = (ColorView)findViewById(R.id.color_picked_image3);
        mColorViews[3] = (ColorView)findViewById(R.id.color_picked_image4);
        mColorViews[4] = (ColorView)findViewById(R.id.color_picked_image5);
        mColorViews[5] = (ColorView)findViewById(R.id.color_picked_image6);
        mColorViews[6] = (ColorView)findViewById(R.id.color_picked_image7);
        mColorViews[7] = (ColorView)findViewById(R.id.color_picked_image8);
        mColorViews[8] = (ColorView)findViewById(R.id.color_picked_image9);
        mColorViews[9] = (ColorView)findViewById(R.id.color_picked_image10);

        String [] mColorValues = getResources().getStringArray(R.array.colors_values);

        for(int i=0;i<mColorValues.length;i++){
            mColorViews[i].setColor(Color.parseColor(mColorValues[i]));
            mColorViews[i].setOnColorSelected(new ColorView.OnColorSelectedListener() {
                @Override
                public void onSelected(ColorView colorView) {
                    mSelectedColorView.unselect();
                    mSelectedColorView = colorView;
                }
            });
        }

        mSelectedColorView = mColorViews[0];
        mSelectedColorView.select();
    }

    private void refreshListView(){
        SingleExercise s = mCurrentSingleExercise;

        String [] strings = new String[s.getAreaList().size()];
        for (int i=0;i<strings.length;i++){

            strings[i] = s.getAreaList().get(i).getName() + " " + getColorName(s.getAreaList().get(i));
        }
        GameListAdapter adapter;
        adapter = new GameListAdapter(GameActivity.this,strings);
        mTaskListView.setAdapter(adapter);
    }
    private String getColorName(Area area){

        for(int i=0;i<mColorValues.length;i++){
            if(Color.parseColor(mColorValues[i]) == area.getColor()){
                return mColorNames[i];
            }
        }
        return "";
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTime.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mTime.pause();
    }


    @Override
    public void onBackPressed() {
        closeGame();
    }

    @Override
    public void onClick(View v) {
        if(v == mCloseGameButton){
            closeGame();
        }
        else if(v == mEvaluateButton){
            evaluate();
        }
    }
    private void evaluate(){

        int correctAresCount = 0;

        for(int i=0;i<mCurrentSingleExercise.getAreaList().size();i++){
            int x = mCurrentSingleExercise.getAreaList().get(i).getxRepresentive();
            int y = mCurrentSingleExercise.getAreaList().get(i).getyRepresentive();

            if(mSmartImage.getPixel(x,y) == mCurrentSingleExercise.getAreaList().get(i).getColor())
                correctAresCount++;
        }
        mExerciseManager.getCurrentResult().setCorrectAreasCount(correctAresCount);

        if (mExerciseManager.nextSingleExercise()) {
            mCurrentSingleExercise = mExerciseManager.getCurrentSingleExercise();
            setSingleExercise();
        } else
            exerciseDone();
    }

    private void closeGame(){
        ConfirmDialog confirmDialog = new ConfirmDialog();
        Bundle data = new Bundle();
        data.putString(MESSAGE, getString(R.string.game_close_confirm));
        confirmDialog.setArguments(data);
        confirmDialog.setNoticeDialogListener(new ConfirmDialog.NoticeDialogListener() {
            @Override
            public void onDialogPositive(DialogFragment dialog) {
                finish();
            }

            @Override
            public void onDialogNegative(DialogFragment dialog) {

            }
        });
        confirmDialog.show(getFragmentManager(), "confirmDialogGame");
    }

    public void exerciseDone() {

        String xmlResults = "";
        try {
            ResultWriter resultWriter = new ResultWriter(mTime.getTimeSec(),mExerciseManager.getResultsPercentage(), getString(R.string.result_time_unit),getString(R.string.result_score_unit));
            xmlResults = resultWriter.getXmlResultString();

        } catch (IOException e) {
            removeAllImages();
            setResult(RESULT_CANCELED);
            finish();
        }

        removeAllImages();
        Intent resultIntent = new Intent();
        resultIntent.putExtra(RESULTS, xmlResults);
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    @Override
    public void processFinish(String output) {
        mGameView.refresh();
    }

    @Override
    public void onClickedImage(int x, int y) {

        float scaleX = (float) mSmartImage.getBitmap().getWidth() / mGameView.getImageSize().getWidth();//0.5
        float scaleY = (float) mSmartImage.getBitmap().getHeight() / mGameView.getImageSize().getHeight();

        int clickToOrigX = (int) (x * scaleX);
        int clickToOrigY = (int) (y * scaleY);

        if(!mSmartImage.isPixelBorder(clickToOrigX,clickToOrigY)) {
            mOperationFill = new OperationFill(mSmartImage, new Point(clickToOrigX, clickToOrigY), mSelectedColorView.getSelectedColor());
            mOperationFill.mDelegate = GameActivity.this;
            mOperationFill.execute();
        }
    }
    private void removeAllImages(){
        ArrayList<String> imageFilePaths = new ArrayList<>();

        for(int i=0;i<mExercise.getSingleExerciseSize();i++){
            imageFilePaths.addAll(mExercise.getSingleExercise(i).getAllImageFilePaths());
        }

        for(int i=0;i<imageFilePaths.size();i++){
            File file = new File(imageFilePaths.get(i));
            file.delete();
        }
    }
}
