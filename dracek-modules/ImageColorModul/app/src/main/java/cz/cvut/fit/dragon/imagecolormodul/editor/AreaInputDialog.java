package cz.cvut.fit.dragon.imagecolormodul.editor;

import android.app.DialogFragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.SoftReference;

import cz.cvut.fit.dragon.imagecolormodul.R;
import cz.cvut.fit.dragon.imagecolormodul.model.Area;

public class AreaInputDialog extends DialogFragment implements View.OnClickListener ,CompoundButton.OnCheckedChangeListener,AdapterView.OnItemSelectedListener{

    public static final int DIALOG_NEW = 100;
    public static final int DIALOG_EDIT = 101;

    public static final String AREA = "AREA";
    public static final String TYPE = "TYPE";

    Spinner colorSpinner;
    Button saveAreaButton,removeAreaButton,cancelAreaButton;
    ImageView mSampleColorImageView;
    EditText areaNameInput;
    Switch randomColorSwitch;
    TextView mColorTextView;

    String [] mColorNames;
    String [] mColorValues;

    Area mArea;
    int mDialogType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mColorNames = getResources().getStringArray(R.array.colors_names);
        mColorValues = getResources().getStringArray(R.array.colors_values);

        mArea = getArguments().getParcelable(AREA);
        mDialogType = getArguments().getInt(TYPE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.area_input_dialog,container);

        getDialog().setTitle(getString(R.string.area_input_dialog));
        getDialog().setCanceledOnTouchOutside(false);
        setCancelable(false);

        mColorTextView = (TextView)view.findViewById(R.id.color_text_view);
        colorSpinner = (Spinner)view.findViewById(R.id.colors_spinner);
        saveAreaButton = (Button)view.findViewById(R.id.save_area_button);
        removeAreaButton = (Button)view.findViewById(R.id.remove_area_button);
        cancelAreaButton = (Button)view.findViewById(R.id.cancel_area_button);
        mSampleColorImageView = (ImageView)view.findViewById(R.id.sample_color_dialog);
        areaNameInput = (EditText)view.findViewById(R.id.area_name_input);
        randomColorSwitch = (Switch)view.findViewById(R.id.random_color_switch_dialog);
        randomColorSwitch.setChecked(false);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),android.R.layout.simple_spinner_item,mColorNames);
        colorSpinner.setAdapter(adapter);

        saveAreaButton.setOnClickListener(this);
        removeAreaButton.setOnClickListener(this);
        cancelAreaButton.setOnClickListener(this);
        colorSpinner.setOnItemSelectedListener(this);
        randomColorSwitch.setOnCheckedChangeListener(this);

        if(mDialogType == DIALOG_NEW)
            removeAreaButton.setEnabled(false);

        setArea();

        return view;
    }
    void setArea(){
        areaNameInput.setText(mArea.getName());
        randomColorSwitch.setChecked(mArea.isRandomColor());

        for(int i=0;i<mColorValues.length;i++){
            if(Color.parseColor(mColorValues[i]) == mArea.getColor())
                colorSpinner.setSelection(i);
        }
    }

    @Override
    public void onClick(View v) {
        if(v == saveAreaButton){
            if(areaNameInput.getText().toString().equals(""))
                Toast.makeText(getActivity(),getString(R.string.area_name_non_empty),Toast.LENGTH_LONG).show();
            else {
                mArea.setName(areaNameInput.getText().toString());
                if (onResultListener != null)
                    onResultListener.onSaved(mArea);
            }
        }
        else if(v == removeAreaButton){
            if(onResultListener != null)
                onResultListener.onRemoved(mArea);
        }
        else if(v == cancelAreaButton){
            dismiss();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(buttonView == randomColorSwitch){
            mArea.setRandomColor(isChecked);

            if(isChecked){
                colorSpinner.setVisibility(View.INVISIBLE);
                mColorTextView.setVisibility(View.INVISIBLE);
                mSampleColorImageView.setVisibility(View.INVISIBLE);
            }
            else {
                colorSpinner.setVisibility(View.VISIBLE);
                mColorTextView.setVisibility(View.VISIBLE);
                mSampleColorImageView.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        mArea.setColor(Color.parseColor(mColorValues[position]));
        mSampleColorImageView.setBackgroundColor(Color.parseColor(mColorValues[position]));
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    OnResultListener onResultListener;
    public interface OnResultListener {
        void onSaved(Area area);
        void onRemoved(Area area);
    }
    public void setOnResultListener(OnResultListener onResultListener) {
        this.onResultListener = onResultListener;
    }

}
