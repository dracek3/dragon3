package cz.cvut.fit.dragon.imagecolormodul.editor;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cz.cvut.fit.dragon.imagecolormodul.R;
import cz.cvut.fit.dragon.imagecolormodul.model.SingleExerciseView;
import cz.cvut.fit.dragon.imagecolormodul.model.SingleExerciseView.OnClickImageListener;
import cz.cvut.fit.dragon.imagecolormodul.model.Area;
import cz.cvut.fit.dragon.imagecolormodul.model.Pixel;
import cz.cvut.fit.dragon.imagecolormodul.model.SingleExercise;
import cz.cvut.fit.dragon.imagecolormodul.model.SmartImage;
import cz.cvut.fit.dragon.corelibrary.util.ConfirmDialog;
import cz.cvut.fit.dragon.corelibrary.util.IO;
import cz.cvut.fit.dragon.corelibrary.util.InfoDialog;
import cz.cvut.fit.dragon.corelibrary.util.Size;
import cz.cvut.fit.dragon.corelibrary.util.Utilities;

public class SingleEditorActivity extends Activity implements OnClickListener,OnClickImageListener,AreaInputDialog.OnResultListener {

    private static final int SELECT_PHOTO = 100;

    public final static int CREATE = 105;
    public final static int EDIT = 106;
    int mType;

    Button mLoadButton;
    private Button mSaveButton, mCancelButton;
    EditText mSingleExerciseNameInput;
    LinearLayout mSingleExerciseLinearLayout;

    ListView mSingleExerciseListView;
    AreaInputDialog mAreaInputDialog;
    OperationFill operationFill;

    private SingleExerciseView mSingleEditorImageView;
    private SingleExercise mSingleExercise;

    SmartImage mSmartImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_editor);

        init();
        setUpSingleExercise(getIntent());
    }
    private void init(){
        mLoadButton = (Button)findViewById(R.id.load_image_button);
        mSaveButton = (Button)findViewById(R.id.save_image_button);
        mCancelButton = (Button)findViewById(R.id.cancel_image_button);
        mSingleExerciseNameInput = (EditText)findViewById(R.id.single_exercise_name_input);
        mSingleExerciseLinearLayout = (LinearLayout)findViewById(R.id.single_exercise_linear_layout);
        mSingleEditorImageView = new SingleExerciseView(SingleEditorActivity.this);
        mSingleEditorImageView.setOnClickImageListener(this);

        mSingleExerciseLinearLayout.addView(mSingleEditorImageView);

        mSingleExerciseListView = (ListView)findViewById(R.id.single_exercise_list_view);

        mSingleExerciseListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showAreaInput(mSingleExercise.getAreaList().get(position), AreaInputDialog.DIALOG_EDIT);
            }
        });
        mLoadButton.setOnClickListener(this);
        mSaveButton.setOnClickListener(this);
        mCancelButton.setOnClickListener(this);
    }
    private void setUpSingleExercise(Intent intent){
        if(intent.hasExtra(EditorActivity.SINGLE_EXERCISE)){
            mSingleExercise = getIntent().getParcelableExtra(EditorActivity.SINGLE_EXERCISE);

            Bitmap x = loadBitmap();
            mSmartImage = new SmartImage(x);

            fillImageAreas(mSmartImage, mSingleExercise.getAreaList(), ContextCompat.getColor(SingleEditorActivity.this, R.color.randomFirstColor),
                    ContextCompat.getColor(SingleEditorActivity.this, R.color.randomSecondColor), false);

            mSingleExerciseNameInput.setText(mSingleExercise.getName());

            mCancelButton.setVisibility(View.GONE);
            mSaveButton.setText(getString(R.string.save_single_exercise));
            mType = EDIT;
        }
        else if(intent.hasExtra(EditorActivity.SINGLE_EXERCISE_UNIQUE_ID)) {
            int mUniqueId = getIntent().getIntExtra(EditorActivity.SINGLE_EXERCISE_UNIQUE_ID, 0);

            mSingleExercise = new SingleExercise();
            mSingleExercise.setUniqueId(mUniqueId);

            mType = CREATE;
            mSaveButton.setText(getString(R.string.create_single_exercise));
        }
    }
    public void fillImageAreas(SmartImage smartImage,List<Area> areaList,int randomColorFirst,int randomColorSecond,final boolean closeDialog)
    {
        operationFill = new OperationFill(smartImage,areaList,randomColorFirst,randomColorSecond);
        operationFill.mDelegate = new AsyncResponse() {
            @Override
            public void processFinish(String output) {
                mSingleEditorImageView.setImage(mSmartImage);
                refreshListView();
                mSingleEditorImageView.refresh();
                if(closeDialog)mAreaInputDialog.dismiss();
            }
        };
        operationFill.execute();
    }
    private boolean isSingleExerciseValid() {
        boolean valid = true;

        if(mSmartImage == null){
            valid = false;
            showInfoMessage(getString(R.string.image_required));
        }
        else if(mSingleExercise.getAreaList().size() == 0) {
            valid = false;
            showInfoMessage(getString(R.string.area_required));
        }
        return valid;
    }

    private void save(){
        Bitmap thumbnail = mSmartImage.getBitmap();

        Size scaledSize =  Utilities.scaleSize(new Size(thumbnail.getWidth(), thumbnail.getHeight()), new Size(IO.MAX_THUMB_SIZE, IO.MAX_THUMB_SIZE));
        Bitmap scaledThumbBitmap = Bitmap.createScaledBitmap(thumbnail,scaledSize.getWidth(),scaledSize.getHeight(),true);

        try {
            Utilities.saveBitmapToFile(IO.getThumbnailFile(mSingleExercise.getUniqueId()), scaledThumbBitmap);
            mSingleExercise.setName(mSingleExerciseNameInput.getText().toString());

            Intent resultIntent = new Intent();
            resultIntent.putExtra(EditorActivity.SINGLE_EXERCISE, mSingleExercise);
            setResult(Activity.RESULT_OK, resultIntent);
            finish();
        } catch (IOException e) {
            showInfoMessage(getString(R.string.save_single_exercise_failed));
        }
    }
    private void showInfoMessage(String message){
        InfoDialog infoDialog = new InfoDialog();
        Bundle data = new Bundle();
        data.putString(InfoDialog.MESSAGE,message);
        infoDialog.setArguments(data);
        infoDialog.show(getFragmentManager(),"infoDialog");
    }

    @Override
    public void onClick(View v) {
        if(v == mSaveButton) {
            if (isSingleExerciseValid()) {
                save();
            }
        }
        else if(v == mCancelButton){
            setResult(RESULT_CANCELED);
            finish();
        }
        else if(v == mLoadButton){
            if(mSmartImage == null)pickImage();
            else loadImage();
        }
    }

    private void pickImage(){
        Intent photoPickIntent = new Intent(Intent.ACTION_PICK);
        photoPickIntent.setType("image/*");
        startActivityForResult(photoPickIntent, SELECT_PHOTO);
    }

    private void loadImage(){
        ConfirmDialog confirmDialog = new ConfirmDialog();
        Bundle data = new Bundle();
        data.putString(ConfirmDialog.MESSAGE, getString(R.string.reload_image));
        confirmDialog.setArguments(data);
        confirmDialog.show(getFragmentManager(), "confirmDialog");
        confirmDialog.setNoticeDialogListener(new ConfirmDialog.NoticeDialogListener() {
            @Override
            public void onDialogPositive(DialogFragment dialog) {
                pickImage();
            }

            @Override
            public void onDialogNegative(DialogFragment dialog) {

            }
        });
    }

    private Bitmap loadBitmap(){
        Bitmap bitmap = Utilities.loadBitmapFromFile(IO.getImageFile(mSingleExercise.getUniqueId()));
        return bitmap.copy(bitmap.getConfig(),true);
    }

    private void refreshListView(){

        List<Area> areas = mSingleExercise.getAreaList();

        String [] names = new String[areas.size()];
        int [] colors = new int[areas.size()];
        boolean [] randomColors = new boolean[areas.size()];

        for(int i=0;i<areas.size();i++)
        {
            names[i] = areas.get(i).getName();
            colors[i] = areas.get(i).getColor();
            randomColors[i] = areas.get(i).isRandomColor();
        }

        AreaListAdapter adapter;
        adapter = new AreaListAdapter(SingleEditorActivity.this, names, colors,randomColors);
        mSingleExerciseListView.setAdapter(adapter);
    }

    private void loadImage(String imageFilePath){
        File savedImageFile = IO.getImageFile(mSingleExercise.getUniqueId());

        File loadedImageFile = new File(imageFilePath);
        Size imageSize = Utilities.getImageSize(loadedImageFile);

        if(imageSize.getWidth() ==  0 || imageSize.getHeight() == 0){
            showInfoMessage(getString(R.string.not_zero_image_size));
        }
        else {
            Size scaledSize = Utilities.scaleSize(imageSize, new Size(IO.MAX_IMAGE_SIZE, IO.MAX_IMAGE_SIZE));
            Bitmap scaledBitmap = Utilities.loadBitmapFromFile(loadedImageFile, scaledSize.getWidth(), scaledSize.getHeight());

            try {
                Utilities.saveBitmapToFile(savedImageFile, scaledBitmap);

                mSmartImage = new SmartImage(scaledBitmap.copy(scaledBitmap.getConfig(),true));
                mSingleEditorImageView.setImage(mSmartImage);

            } catch (IOException e) {
                showInfoMessage(getString(R.string.load_image_failed));
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == SELECT_PHOTO && resultCode == Activity.RESULT_OK) {
            String path = Utilities.getPath(SingleEditorActivity.this, data.getData());
            if(path != null) {
                loadImage(path);
            }
            else showInfoMessage(getString(R.string.load_image_failed));
        }
    }

    @Override
    public void onClickedImage(int x, int y) {
        float scaleX = (float) mSmartImage.getWidth() / mSingleEditorImageView.getImageSize().getWidth();//0.5
        float scaleY = (float) mSmartImage.getHeight() / mSingleEditorImageView.getImageSize().getHeight();

        int clickToOrigX = (int) (x * scaleX);
        int clickToOrigY = (int) (y * scaleY);

        Area area;
        if(mSmartImage.getPixelArea(clickToOrigX,clickToOrigY) == Pixel.AREA_KEY_NOT_SET &&
                !mSmartImage.isPixelBorder(clickToOrigX,clickToOrigY)) {

            area = new Area(mSingleExercise.getMaxId());
            area.setxRepresentive(clickToOrigX);
            area.setyRepresentive(clickToOrigY);

            showAreaInput(area, AreaInputDialog.DIALOG_NEW);
        }
        else if(!mSmartImage.isPixelBorder(clickToOrigX,clickToOrigY)) {
            area = mSingleExercise.getAreaById(mSmartImage.getPixelArea(clickToOrigX, clickToOrigY));

            showAreaInput(area, AreaInputDialog.DIALOG_EDIT);
        }
    }
    public void showAreaInput(Area area,int type){
        Bundle date = new Bundle();
        date.putParcelable(AreaInputDialog.AREA, area);
        date.putInt(AreaInputDialog.TYPE, type);

        mAreaInputDialog = new AreaInputDialog();
        mAreaInputDialog.setArguments(date);
        mAreaInputDialog.setOnResultListener(this);
        mAreaInputDialog.show(getFragmentManager(), "areaInputDialog");
    }

    @Override
    public void onSaved(Area area) {
        mSingleExercise.addArea(area);

        List<Area> tmpAreaList = new ArrayList<>();
        tmpAreaList.add(area);
        fillImageAreas(mSmartImage, tmpAreaList, ContextCompat.getColor(SingleEditorActivity.this, R.color.randomFirstColor),
                ContextCompat.getColor(SingleEditorActivity.this, R.color.randomSecondColor), true);
    }

    @Override
    public void onRemoved(Area area) {
        for(int i=0;i<mSmartImage.getWidth();i++){
            for(int j=0;j<mSmartImage.getHeight();j++){
                if(mSmartImage.getPixelArea(i,j) == area.getUniqueId())
                    mSmartImage.setPixelArea(i,j,Pixel.AREA_KEY_NOT_SET);
            }
        }
        mSingleExercise.removeArea(area);
        refreshListView();
        mSingleEditorImageView.refresh();
        mAreaInputDialog.dismiss();
    }
    @Override
    public void onBackPressed() {
        if(mType == EDIT) {
            if(isSingleExerciseValid())
                save();
        }
        else if(mType == CREATE){
            setResult(RESULT_CANCELED);
            finish();
        }
    }
}
