package cz.cvut.fit.dragon.imagecolormodul.model;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Xfermode;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.ImageView;

import cz.cvut.fit.dragon.imagecolormodul.R;


public class ColorView extends ImageView {

    boolean selected;
    int color;
    Bitmap colorBitmap;
    Bitmap selectedColorBitmap;

    public ColorView(Context context) {
        super(context);
        selected = false;
    }

    public ColorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        selected = false;

    }

    public ColorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        selected = false;

    }

    public void setColor(int color){

        this.color = color;
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.selected_color);
        Bitmap selectedBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.selected_color_selected);
        selectedBitmap.setHasAlpha(true);

        colorBitmap = bitmap.copy(bitmap.getConfig(),true);
        colorBitmap.setHasAlpha(true);

        for(int i=0;i<colorBitmap.getWidth();i++){
            for (int j=0;j<colorBitmap.getHeight();j++){
                if(colorBitmap.getPixel(i,j) == Color.BLACK) {
                    colorBitmap.setPixel(i, j, color);
                }
            }
        }

        selectedColorBitmap = colorBitmap.copy(colorBitmap.getConfig(),true);
        PorterDuffXfermode xx = new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER);
        Paint x = new Paint();x.setXfermode(xx);
        Canvas canvas = new Canvas(selectedColorBitmap);
        canvas.drawBitmap(selectedBitmap,0,0,x);

        this.setImageBitmap(colorBitmap);
    }

    public void select(){
        this.selected = true;
        if(selectedColorBitmap != null) this.setImageBitmap(selectedColorBitmap);

    }

    public void unselect(){
        this.selected = false;
        this.setImageBitmap(colorBitmap);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if(event.getAction() == MotionEvent.ACTION_DOWN){
            if(!selected){
                if(onColorSelectedListener != null)
                    onColorSelectedListener.onSelected(this);

                selected = true;
                this.setImageBitmap(selectedColorBitmap);
            }
        }
        return true;
    }
    public int getSelectedColor(){
        return color;
    }

    OnColorSelectedListener onColorSelectedListener;
    public interface OnColorSelectedListener {
        void onSelected(ColorView colorView);

    }
    public void setOnColorSelected(OnColorSelectedListener onResultListener) {
        this.onColorSelectedListener = onResultListener;
    }
}
