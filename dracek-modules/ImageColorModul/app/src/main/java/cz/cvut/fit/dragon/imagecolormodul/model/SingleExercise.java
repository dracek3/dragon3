package cz.cvut.fit.dragon.imagecolormodul.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cz.cvut.fit.dragon.corelibrary.util.IO;

public class SingleExercise implements Parcelable  {

    String name;
    int uniqueId;
    List<Area> areaList;

    public SingleExercise(){
        areaList = new ArrayList<>();
    }

    protected SingleExercise(Parcel in) {
        name = in.readString();
        uniqueId = in.readInt();
        areaList = in.createTypedArrayList(Area.CREATOR);
    }

    public static final Creator<SingleExercise> CREATOR = new Creator<SingleExercise>() {
        @Override
        public SingleExercise createFromParcel(Parcel in) {
            return new SingleExercise(in);
        }

        @Override
        public SingleExercise[] newArray(int size) {
            return new SingleExercise[size];
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SingleExercise that = (SingleExercise) o;

        return uniqueId == that.uniqueId;

    }
    public int getMaxId(){
        int max = 0;
        for(int i=0;i<areaList.size();i++){
            if(areaList.get(i).getUniqueId() > max)
                max = areaList.get(i).getUniqueId();
        }
        return max+1;
    }

    @Override
    public int hashCode() {
        return uniqueId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void destroyImages(){
        File thumbnail = IO.getThumbnailFile(uniqueId);
        File image = IO.getImageFile(uniqueId);
        thumbnail.delete();
        image.delete();
    }
    public Area getAreaById(int uniqueId){
        for(int i=0;i<areaList.size();i++){
            if(areaList.get(i).getUniqueId() == uniqueId)
                return areaList.get(i);
        }
        return null;
    }
    public ArrayList<String> getAllImageFilePaths(){
        ArrayList<String> imageFilePaths = new ArrayList<>();
        imageFilePaths.add(IO.getImageFile(uniqueId).getPath());
        imageFilePaths.add(IO.getThumbnailFile(uniqueId).getPath());
        return imageFilePaths;
    }

    public void addArea(Area area){

        int index = areaList.indexOf(area);

        if(index < 0)areaList.add(area);
        else areaList.set(index,area);
    }
    public void removeArea(Area area) {
        areaList.remove(area);
    }

    public int getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }

    public List<Area> getAreaList() {
        return areaList;
    }

    public void setAreaList(List<Area> areaList) {
        this.areaList = areaList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(uniqueId);
        dest.writeTypedList(areaList);
    }
}
