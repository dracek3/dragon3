package cz.cvut.fit.dragon.imagecolormodul.game;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import cz.cvut.fit.dragon.imagecolormodul.R;

public class GameListAdapter extends ArrayAdapter<String> {

    private final Activity mContext;
    private final String[] mItems;
    public GameListAdapter(Activity context,
                             String[] items) {
        super(context, R.layout.game_list_item, items);
        this.mContext = context;
        this.mItems = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.game_list_item, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);

        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.nameTextView.setText(mItems[position]);

        final Typeface tvFont = Typeface.createFromAsset(mContext.getAssets(), "roboto.ttf");
        viewHolder.nameTextView.setTypeface(tvFont);
        viewHolder.nameTextView.setTextColor(Color.BLACK);
        viewHolder.nameTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);

        return convertView;
    }

    class ViewHolder {
        public TextView nameTextView;

        public ViewHolder(View view) {
            nameTextView = (TextView) view.findViewById(R.id.game_list_view_text);
        }
    }
}