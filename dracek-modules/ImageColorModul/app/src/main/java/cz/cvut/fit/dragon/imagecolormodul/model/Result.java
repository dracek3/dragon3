package cz.cvut.fit.dragon.imagecolormodul.model;

public class Result {
    int areasCount;
    int correctAreasCount;

    public Result(){
        correctAreasCount = 0;
    }

    public void setAreasCount(int areasCount){
        this.areasCount = areasCount;
    }

    public void setCorrectAreasCount(int correctAreasCount){
        this.correctAreasCount = correctAreasCount;
    }
    public int getPercentage(){
        float percentage = (float)correctAreasCount/areasCount;

        return (int)(percentage*100);
    }
}
