package cz.cvut.fit.dragon.imagecolormodul.core;

import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import cz.cvut.fit.dragon.corelibrary.util.InfoDialog;
import cz.cvut.fit.dragon.corelibrary.util.Utilities;
import cz.cvut.fit.dragon.imagecolormodul.R;
import cz.cvut.fit.dragon.imagecolormodul.editor.EditorActivity;
import cz.cvut.fit.dragon.imagecolormodul.game.GameActivity;


public class CoreActivity extends Activity implements View.OnClickListener {

    Button runTest,run;
    Button edit;
    Button create;

    String xml;
    ArrayList<String>  paths;

    final static int EDITOR = 100;
    final static int GAME = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_core);

        runTest = (Button)findViewById(R.id.run_test);
        run = (Button)findViewById(R.id.run);
        edit = (Button)findViewById(R.id.edit);
        create = (Button)findViewById(R.id.create);

        runTest.setOnClickListener(this);
        run.setOnClickListener(this);
        edit.setOnClickListener(this);
        create.setOnClickListener(this);

        edit.setEnabled(false);
        run.setEnabled(false);
    }

    @Override
    public void onClick(View v) {

        if(v == create){
            Intent myIntent = new Intent(CoreActivity.this, EditorActivity.class);
            CoreActivity.this.startActivityForResult(myIntent, EDITOR);
        }
        else if(v == edit){
            Intent myIntent = new Intent(CoreActivity.this, EditorActivity.class);
            myIntent.putExtra("EXERCISE_FILE", xml);
            myIntent.putExtra("IMAGES", paths);
            CoreActivity.this.startActivityForResult(myIntent, EDITOR);
        }
        else if(v == run){
            Intent myIntent = new Intent(CoreActivity.this, GameActivity.class);
            myIntent.putExtra("EXERCISE_FILE", xml);
            myIntent.putExtra("IMAGES", paths);
            CoreActivity.this.startActivityForResult(myIntent, GAME);
        }
        else if(v == runTest) {

            ArrayList<String> as = new ArrayList<>();
            try {
                InputStream bitmap1 = getAssets().open("x1.png");
                InputStream bitmap2 = getAssets().open("x2.png");
                InputStream bitmap3 = getAssets().open("x3.png");
                InputStream bitmap4 = getAssets().open("x4.png");

                Utilities.saveBitmapToFile(new File(getFilesDir(), "image1"), BitmapFactory.decodeStream(bitmap1));
                Utilities.saveBitmapToFile(new File(getFilesDir(), "image2"), BitmapFactory.decodeStream(bitmap2));
                Utilities.saveBitmapToFile(new File(getFilesDir(), "image3"), BitmapFactory.decodeStream(bitmap3));
                Utilities.saveBitmapToFile(new File(getFilesDir(), "image4"), BitmapFactory.decodeStream(bitmap4));


                as.add(new File(getFilesDir(), "image1").getPath());
                as.add(new File(getFilesDir(), "image2").getPath());
                as.add(new File(getFilesDir(), "image3").getPath());
                as.add(new File(getFilesDir(), "image4").getPath());


            } catch (IOException e) {
                e.printStackTrace();
            }

            String XML = "";
            try {
                StringBuilder sb = new StringBuilder();
                InputStream is = getAssets().open("res.xml");

                BufferedReader in = new BufferedReader(new InputStreamReader(is, "UTF-8"));

                String s;
                while ((s = in.readLine()) != null) {
                    sb.append(s);
                }
                XML = sb.toString();

            } catch (IOException e) {
                e.printStackTrace();
            }

            Log.e("S:Xml:",XML);
            for(String s : as){
                Log.e("S:Image file:",s);
            }

            Intent intent = new Intent(CoreActivity.this, GameActivity.class);
            intent.putExtra("EXERCISE_FILE", XML);
            intent.putExtra("IMAGES", as);
            CoreActivity.this.startActivityForResult(intent, GAME);

            edit.setEnabled(false);
            run.setEnabled(false);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == EDITOR && resultCode == RESULT_OK){
            edit.setEnabled(true);
            run.setEnabled(true);

            xml = data.getStringExtra("EXERCISE_FILE");
            paths = data.getStringArrayListExtra("IMAGES");

            Log.e("Xml:", xml);
            Log.i("Xml:", xml);
            for(String s : paths){
                Log.e("Image file:",s);
            }
        }
        else if(requestCode == GAME && resultCode == RESULT_OK){
            Log.e("Results", data.getStringExtra("RESULTS"));
            //showInfoMessage(data.getStringExtra("RESULTS"));
            Utilities.showSmiley(Utilities.getResult(data.getStringExtra("RESULTS")), this);
        }

        if(requestCode == GAME){
            edit.setEnabled(false);
            run.setEnabled(false);
        }
    }
    /**
     * This method shows simple dialog with message
     * @param message
     */
    private void showInfoMessage(String message){
        InfoDialog infoDialog = new InfoDialog();
        Bundle data = new Bundle();
        data.putString(InfoDialog.MESSAGE,message);
        infoDialog.setArguments(data);
        infoDialog.show(getFragmentManager(), "infoDialog");
    }
}