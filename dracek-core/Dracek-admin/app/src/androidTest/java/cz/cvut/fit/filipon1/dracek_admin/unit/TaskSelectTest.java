package cz.cvut.fit.filipon1.dracek_admin.unit;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.hamcrest.Description;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.task.TaskActivity;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class TaskSelectTest {

    @Rule
    public ActivityTestRule activityRule = new ActivityTestRule<>(TaskActivity.class);

    @Test
    public void testGenerated() {
        // Used to provide time delays between actions, see details at http://droidtestlab.com/delay.html
        IdlingResource idlingResource;

        idlingResource = startTiming(1800);
        // Click at item with value 'cz.cvut.fit.filipon1.dracek_admin.model.Task@41ed5a58' in ListView
        // see details at http://droidtestlab.com/adapterView.html
        onData(allOf(is(new BoundedMatcher<Object, cz.cvut.fit.filipon1.dracek_admin.model.Task>(cz.cvut.fit.filipon1.dracek_admin.model.Task.class) {
            @Override
            public void describeTo(Description description) {
            }

            @Override
            protected boolean matchesSafely(cz.cvut.fit.filipon1.dracek_admin.model.Task obj) {
                return obj.getFromattedName().equals("úkol 1 - 10.04.2016");
            }
        }))).inAdapterView(withId(R.id.lv_tasks)).perform(click());
        stopTiming(idlingResource);

        idlingResource = startTiming(1800);
        // Click at item with value 'cz.cvut.fit.filipon1.dracek_admin.model.Task@41f63380' in ListView
        // see details at http://droidtestlab.com/adapterView.html
       onView(withText("24%"));
        stopTiming(idlingResource);

    }


    // See details at http://droidtestlab.com/delay.html
    public IdlingResource startTiming(long time) {
        IdlingResource idlingResource = new ElapsedTimeIdlingResource(time);
        Espresso.registerIdlingResources(idlingResource);
        return idlingResource;
    }

    public void stopTiming(IdlingResource idlingResource) {
        Espresso.unregisterIdlingResources(idlingResource);
    }

    public class ElapsedTimeIdlingResource implements IdlingResource {
        private long startTime;
        private final long waitingTime;
        private ResourceCallback resourceCallback;

        public ElapsedTimeIdlingResource(long waitingTime) {
            this.startTime = System.currentTimeMillis();
            this.waitingTime = waitingTime;
        }

        @Override
        public String getName() {
            return ElapsedTimeIdlingResource.class.getName() + ":" + waitingTime;
        }

        @Override
        public boolean isIdleNow() {
            long elapsed = System.currentTimeMillis() - startTime;
            boolean idle = (elapsed >= waitingTime);
            if (idle) {
                resourceCallback.onTransitionToIdle();
            }
            return idle;
        }

        @Override
        public void registerIdleTransitionCallback(ResourceCallback resourceCallback) {
            this.resourceCallback = resourceCallback;
        }
    }
}