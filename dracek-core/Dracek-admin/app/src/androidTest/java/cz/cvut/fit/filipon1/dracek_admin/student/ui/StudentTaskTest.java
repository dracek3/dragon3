package cz.cvut.fit.filipon1.dracek_admin.student.ui;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import cz.cvut.fit.filipon1.dracek_admin.R;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class StudentTaskTest {

    @Rule
    public ActivityTestRule activityRule = new ActivityTestRule<>(StudentActivity.class);

    @Test
    public void testGenerated() {
        // Used to provide time delays between actions, see details at http://droidtestlab.com/delay.html
        IdlingResource idlingResource;

        idlingResource = startTiming(1800);
        // Click at item with value 'cz.cvut.fit.filipon1.dracek_admin.model.User@41ed52a8' in ListView
        // see details at http://droidtestlab.com/adapterView.html
        onData(allOf(is(new BoundedMatcher<Object, cz.cvut.fit.filipon1.dracek_admin.model.User>(cz.cvut.fit.filipon1.dracek_admin.model.User.class) {
            @Override
            public void describeTo(Description description) {
            }

            @Override
            protected boolean matchesSafely(cz.cvut.fit.filipon1.dracek_admin.model.User obj) {
                return obj.getUsername().equals("AEinst");
            }
        }))).inAdapterView(withId(R.id.lv_user_list)).perform(click());
        stopTiming(idlingResource);

        // Select page 1 in ViewPager with id R.id.vp_pager
        onView(withId(R.id.vp_pager)).perform(selectViewPagerPage(2));

        idlingResource = startTiming(500);
        // Click at item with value 'cz.cvut.fit.filipon1.dracek_admin.model.User@41ed52a8' in ListView
        // see details at http://droidtestlab.com/adapterView.html
        onData(allOf(is(new BoundedMatcher<Object, cz.cvut.fit.filipon1.dracek_admin.model.Task>(cz.cvut.fit.filipon1.dracek_admin.model.Task.class) {
            @Override
            public void describeTo(Description description) {
            }

            @Override
            protected boolean matchesSafely(cz.cvut.fit.filipon1.dracek_admin.model.Task  obj) {
                return obj.getMinimalScore() == 97;
            }
        }))).inAdapterView(withId(R.id.lv_task)).perform(click());
        stopTiming(idlingResource);


    }


    public static ViewAction selectViewPagerPage(final int pos) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isAssignableFrom(android.support.v4.view.ViewPager.class);
            }

            @Override
            public String getDescription() {
                return "select page in ViewPager";
            }

            @Override
            public void perform(UiController uiController, View view) {
                ((android.support.v4.view.ViewPager) view).setCurrentItem(pos);
            }
        };
    }


    // See details at http://droidtestlab.com/delay.html
    public IdlingResource startTiming(long time) {
        IdlingResource idlingResource = new ElapsedTimeIdlingResource(time);
        Espresso.registerIdlingResources(idlingResource);
        return idlingResource;
    }

    public void stopTiming(IdlingResource idlingResource) {
        Espresso.unregisterIdlingResources(idlingResource);
    }

    public class ElapsedTimeIdlingResource implements IdlingResource {
        private long startTime;
        private final long waitingTime;
        private ResourceCallback resourceCallback;

        public ElapsedTimeIdlingResource(long waitingTime) {
            this.startTime = System.currentTimeMillis();
            this.waitingTime = waitingTime;
        }

        @Override
        public String getName() {
            return ElapsedTimeIdlingResource.class.getName() + ":" + waitingTime;
        }

        @Override
        public boolean isIdleNow() {
            long elapsed = System.currentTimeMillis() - startTime;
            boolean idle = (elapsed >= waitingTime);
            if (idle) {
                resourceCallback.onTransitionToIdle();
            }
            return idle;
        }

        @Override
        public void registerIdleTransitionCallback(ResourceCallback resourceCallback) {
            this.resourceCallback = resourceCallback;
        }
    }
}