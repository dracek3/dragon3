package cz.cvut.fit.filipon1.dracek_admin.module_exercise;

import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.Espresso.openContextualActionModeOverflowMenu;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.longClick;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.swipeUp;
import static android.support.test.espresso.action.ViewActions.swipeDown;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static android.support.test.InstrumentationRegistry.*;

import android.support.test.espresso.matcher.BoundedMatcher;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.runner.RunWith;
import org.junit.Rule;
import org.junit.Test;

import android.view.View;
import android.view.ViewGroup;

import cz.cvut.fit.filipon1.dracek_admin.R;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class ModuleExerciseTest {

    @Rule
    public ActivityTestRule activityRule = new ActivityTestRule<>(ModuleExerciseActivity.class);

    @Test
    public void testGenerated() {
        // Used to provide time delays between actions, see details at http://droidtestlab.com/delay.html
        IdlingResource idlingResource;

        idlingResource = startTiming(1800);
        // Click at item with value 'cz.cvut.fit.filipon1.dracek_admin.model.ModuleExercise@41e67318' in ListView
        // see details at http://droidtestlab.com/adapterView.html
        onData(allOf(is(new BoundedMatcher<Object, cz.cvut.fit.filipon1.dracek_admin.model.ModuleExercise>(cz.cvut.fit.filipon1.dracek_admin.model.ModuleExercise.class) {
            @Override
            public void describeTo(Description description) {
            }

            @Override
            protected boolean matchesSafely(cz.cvut.fit.filipon1.dracek_admin.model.ModuleExercise obj) {
                return obj.getName().equals("Přesmyčky");
            }
        }))).inAdapterView(withId(R.id.lv_module_exercise)).perform(click());
        stopTiming(idlingResource);

        // Click at AppCompatButton with id R.id.btn_add_to_class
        onView(withId(R.id.btn_add_to_class)).perform(scrollTo());
        onView(withId(R.id.btn_add_to_class)).perform(click());

        // Click at item with value 'cz.cvut.fit.filipon1.dracek_admin.model.Klass@41fa6880' in ListView
        // see details at http://droidtestlab.com/adapterView.html
        onView(withText("5.A")).perform(click());

        // Click at AppCompatButton with id R.id.btn_add_to_stuent
        onView(withId(R.id.btn_add_to_stuent)).perform(scrollTo());
        onView(withId(R.id.btn_add_to_stuent)).perform(click());


        idlingResource = startTiming(1500);
        // Click at item with value 'cz.cvut.fit.filipon1.dracek_admin.model.User@4200f1d8' in ListView
        // see details at http://droidtestlab.com/adapterView.html
        onView(withText("Albert Einstein")).perform(click());
        stopTiming(idlingResource);

    }


    // See details at http://droidtestlab.com/delay.html
    public IdlingResource startTiming(long time) {
        IdlingResource idlingResource = new ElapsedTimeIdlingResource(time);
        Espresso.registerIdlingResources(idlingResource);
        return idlingResource;
    }

    public void stopTiming(IdlingResource idlingResource) {
        Espresso.unregisterIdlingResources(idlingResource);
    }

    public class ElapsedTimeIdlingResource implements IdlingResource {
        private long startTime;
        private final long waitingTime;
        private ResourceCallback resourceCallback;

        public ElapsedTimeIdlingResource(long waitingTime) {
            this.startTime = System.currentTimeMillis();
            this.waitingTime = waitingTime;
        }

        @Override
        public String getName() {
            return ElapsedTimeIdlingResource.class.getName() + ":" + waitingTime;
        }

        @Override
        public boolean isIdleNow() {
            long elapsed = System.currentTimeMillis() - startTime;
            boolean idle = (elapsed >= waitingTime);
            if (idle) {
                resourceCallback.onTransitionToIdle();
            }
            return idle;
        }

        @Override
        public void registerIdleTransitionCallback(ResourceCallback resourceCallback) {
            this.resourceCallback = resourceCallback;
        }
    }
}