package cz.cvut.fit.filipon1.dracek_admin.unit;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.IdlingResource;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.widget.EditText;
import android.widget.ListView;

import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.User;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.StudentArrayAdapter;
import cz.cvut.fit.filipon1.dracek_admin.student.ui.StudentActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by ondra on 14.5.16.
 */
public class UserListFilterTest extends ActivityInstrumentationTestCase2<StudentActivity> {

    public UserListFilterTest() {
        super(StudentActivity.class);
    }

    @Test
    public void test() {
        getActivity();
        IdlingResource idlingResource;

        String filterText = "al";
        idlingResource = startTiming(1800);
        onView(withId(R.id.et_name_filter)).perform(replaceText(filterText));
        stopTiming(idlingResource);

        StudentArrayAdapter mAdapter = getActivity().mStudentAdapter;
        List<User> data = mAdapter.getAll();

        for (User u : data) {
            Assert.assertTrue(u.getFullNameWithUsername().toLowerCase().contains(filterText));
        }

    }

    // See details at http://droidtestlab.com/delay.html
    public IdlingResource startTiming(long time) {
        IdlingResource idlingResource = new ElapsedTimeIdlingResource(time);
        Espresso.registerIdlingResources(idlingResource);
        return idlingResource;
    }

    public void stopTiming(IdlingResource idlingResource) {
        Espresso.unregisterIdlingResources(idlingResource);
    }

    public class ElapsedTimeIdlingResource implements IdlingResource {
        private long startTime;
        private final long waitingTime;
        private ResourceCallback resourceCallback;

        public ElapsedTimeIdlingResource(long waitingTime) {
            this.startTime = System.currentTimeMillis();
            this.waitingTime = waitingTime;
        }

        @Override
        public String getName() {
            return ElapsedTimeIdlingResource.class.getName() + ":" + waitingTime;
        }

        @Override
        public boolean isIdleNow() {
            long elapsed = System.currentTimeMillis() - startTime;
            boolean idle = (elapsed >= waitingTime);
            if (idle) {
                resourceCallback.onTransitionToIdle();
            }
            return idle;
        }

        @Override
        public void registerIdleTransitionCallback(ResourceCallback resourceCallback) {
            this.resourceCallback = resourceCallback;
        }
    }
}
