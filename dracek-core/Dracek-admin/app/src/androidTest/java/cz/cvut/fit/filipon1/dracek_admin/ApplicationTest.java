package cz.cvut.fit.filipon1.dracek_admin;

import android.app.Activity;
import android.app.Application;
import android.app.Instrumentation;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.test.ActivityInstrumentationTestCase2;
import android.test.ApplicationTestCase;
import android.util.Log;
import android.widget.TextView;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import cz.cvut.fit.filipon1.dracek_admin.exercise.ExerciseActivity;
import cz.cvut.fit.filipon1.dracek_admin.exercise.ExerciseDetailFragment;
import cz.cvut.fit.filipon1.dracek_admin.model.Exercise;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ActivityInstrumentationTestCase2<ExerciseActivity> {

    private Instrumentation.ActivityMonitor mBrowserActivityMonitor;

    public ApplicationTest() {
        super(ExerciseActivity.class);
    }

    @Before
    public void setUp() {
        mBrowserActivityMonitor = new Instrumentation.ActivityMonitor(
                    "cz.cvut.fit.dragon.imagerotationmodul.editor.EditorActivity", null, false);
        getInstrumentation().addMonitor(mBrowserActivityMonitor);
    }



    @Test
    public void test() {

//        Intent i = new Intent();
//        i.setComponent(new ComponentName("cz.cvut.fit.dragon.imagerotationmodul",
//                "cz.cvut.fit.dragon.imagerotationmodul.editor.EditorActivity"));

        Intent i = new Intent(getActivity(), DummyCore.class);
//        addDataToIntent(i);
        getActivity().startActivity(i);

        Activity activity = mBrowserActivityMonitor.waitForActivityWithTimeout(5*1000);
        assertNotNull("Module wasn't started", activity);

    }

    private void addDataToIntent(Intent i) {
        String image1 = prepareImage("f1_image");
        String image2 = prepareImage("f1_thumb");
        List<String> paths = new ArrayList<>();
        paths.add(image1);
        paths.add(image2);

        i.putExtra("IMAGES", i);

        String file = "<?xml version='1.0' encoding='UTF-8' standalone='yes' ?><Exercise><SingleExercise><Id>1</Id><Name>jj</Name><ImageId>f1_image</ImageId><Rotation>90</Rotation><NumberOfImages>5</NumberOfImages></SingleExercise><SingleExercise><Id>2</Id><Name>hggy</Name><ImageId>f2_image</ImageId><Rotation>90</Rotation><NumberOfImages>5</NumberOfImages></SingleExercise></Exercise>";

        i.putExtra("EXERCISE_FILE", file);
    }

    private String prepareImage(String name) {
        File imageFile = new File(getActivity().getFilesDir(), name);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(imageFile);
            Bitmap bmp = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.square);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        }

        imageFile.setReadable(true,false);
        return imageFile.getPath();
    }
}