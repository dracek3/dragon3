package cz.cvut.fit.filipon1.dracek_admin.klass;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.Klass;

/**
 * Created by ondra on 25.3.16.
 */
public class ClassOptionsArrayAdapter extends ArrayAdapter<Klass> {

    private static final String actions[] = {"", "Přidat studenta", "Přejmenovat", "Odstranit"};


    /**
     * Instantiates a new Class options array adapter.
     *
     * @param context the context
     * @param objects the objects
     */
    public ClassOptionsArrayAdapter(Context context, List<Klass> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.list_item_class_options, parent, false);
        }

        final Klass klass = getItem(position);

        ((TextView) convertView.findViewById(R.id.tv_text)).setText(klass.getName());

        final Spinner optionsSpinner = (Spinner) convertView.findViewById(R.id.spn_options);
        ArrayAdapter<String> optionsAdapter = new ArrayAdapter<>(
                            getContext(), android.R.layout.simple_spinner_item, actions);
        optionsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        optionsSpinner.setAdapter(optionsAdapter);

        optionsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    return;
                }

                switch (position) {
                    case 1:
                        AddStudentToClassDialog dialog =
                                new AddStudentToClassDialog(getContext(), klass);
                        dialog.setListener((AddStudentToClassDialog.OnUserSelectedListener) getContext());
                        dialog.show();
                        break;
                    case 2:
                        EditClassNameDialog ialog =
                                new EditClassNameDialog(getContext(), klass);
                        ialog.setListener((EditClassNameDialog.OnNameEditedListener) getContext());
                        ialog.show();
                        break;
                    case 3:
                        showDeleteDialog(klass);
                        break;
                }

                optionsSpinner.setSelection(0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return convertView;
    }

    private void showDeleteDialog(final Klass klass) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Opravdu chcete tuto třídu smazat?");
        builder.setPositiveButton("ANO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((ClassActivity) getContext()).deleteClass(klass.getId());
            }
        });
        builder.setNegativeButton("NE", null);
        builder.create().show();
    }
}
