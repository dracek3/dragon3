package cz.cvut.fit.filipon1.dracek_admin.student.dialog;

import android.app.AlertDialog;
import android.app.Dialog;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import java.util.HashMap;

import cz.cvut.fit.filipon1.dracek_admin.LoginManager;
import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.User;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ApiConstants;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ErrorHandler;
import cz.cvut.fit.filipon1.dracek_admin.webservice.RestClient;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ServiceGenerator;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ondra on 23.2.16.
 */
public class ChangePasswordDialog extends DialogFragment {

    private static final String USER_ID = "userId";

    private EditText etPassword;
    private EditText etPasswordAgain;

    /**
     * New instance change password dialog.
     *
     * @param userId the user id
     * @return the change password dialog
     */
    public static ChangePasswordDialog newInstance(long userId) {

        Bundle args = new Bundle();
        args.putLong(USER_ID, userId);
        ChangePasswordDialog fragment = new ChangePasswordDialog();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View dialogView = LayoutInflater.from(getContext())
                .inflate(R.layout.dialog_change_password, null);
        etPassword = (EditText) dialogView.findViewById(R.id.et_password);
        etPasswordAgain = (EditText) dialogView.findViewById(R.id.et_password_again);
        View btnCancel = dialogView.findViewById(R.id.btn_cancel);
        View btnSave = dialogView.findViewById(R.id.btn_save);

        builder.setView(dialogView);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String password = etPassword.getText().toString();
                String passwordAgain = etPasswordAgain.getText().toString();

                if (!validData(password, passwordAgain)) {
                    return;
                }


                uploadNewPassword(password);
            }
        });

        return builder.create();
    }

    private boolean validData(String password, String passwordAgain) {
        boolean valid = true;

        if (TextUtils.isEmpty(password)) {
            etPassword.setError("Vyplňte heslo.");
            valid = false;
        }

        if (TextUtils.isEmpty(passwordAgain)) {
            etPasswordAgain.setError("Vyplňte heslo.");
            valid = false;
        } else if (!password.equals(passwordAgain)) {
            etPasswordAgain.setError("Hesla se neschodují");
            valid = false;
        }

        return valid;
    }

    private void uploadNewPassword(String password) {
        RestClient client = ServiceGenerator.createServiceUserToken(RestClient.class, LoginManager.getToken());
        HashMap<String, Object> body = new HashMap<>();
        body.put(ApiConstants.USER_PASSWORD, password);
        client.editStudent(getArguments().getLong(USER_ID, -1), body, new Callback<User>() {
                    @Override
                    public void success(User user, Response response) {
                        dismiss();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        ErrorHandler.handle(error, getContext());
                    }
                });
    }
}
