package cz.cvut.fit.filipon1.dracek_admin.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ondra on 23.2.16.
 */
public class Score {

    private long id;
    private int percentage;
    private long totalTime;
    private String file;
    private User user;

    @SerializedName("idExcercise")
    private long exerciseId;

    @SerializedName("idUser")
    private long userId;

    @SerializedName("idModuleExcercise")
    private long moduleExerciseId;

    private boolean finished;

    private long createdAt;


    /**
     * Gets user.
     *
     * @return the user
     */
    public User getUser() {
        return user;
    }

    /**
     * Sets user.
     *
     * @param user the user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Sets finished.
     *
     * @param finished the finished
     */
    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    /**
     * Sets created at.
     *
     * @param createdAt the created at
     */
    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Is finished boolean.
     *
     * @return the boolean
     */
    public boolean isFinished() {
        return finished;
    }

    /**
     * Gets created at.
     *
     * @return the created at
     */
    public long getCreatedAt() {
        return createdAt;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Sets percentage.
     *
     * @param percentage the percentage
     */
    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    /**
     * Sets total time.
     *
     * @param totalTime the total time
     */
    public void setTotalTime(long totalTime) {
        this.totalTime = totalTime;
    }

    /**
     * Sets file.
     *
     * @param file the file
     */
    public void setFile(String file) {
        this.file = file;
    }

    /**
     * Sets exercise id.
     *
     * @param exerciseId the exercise id
     */
    public void setExerciseId(long exerciseId) {
        this.exerciseId = exerciseId;
    }

    /**
     * Sets user id.
     *
     * @param userId the user id
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    /**
     * Sets module exercise id.
     *
     * @param moduleExerciseId the module exercise id
     */
    public void setModuleExerciseId(long moduleExerciseId) {
        this.moduleExerciseId = moduleExerciseId;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Gets percentage.
     *
     * @return the percentage
     */
    public int getPercentage() {
        return percentage;
    }

    /**
     * Gets total time.
     *
     * @return the total time
     */
    public long getTotalTime() {
        return totalTime;
    }

    /**
     * Gets file.
     *
     * @return the file
     */
    public String getFile() {
        return file;
    }

    /**
     * Gets exercise id.
     *
     * @return the exercise id
     */
    public long getExerciseId() {
        return exerciseId;
    }

    /**
     * Gets user id.
     *
     * @return the user id
     */
    public long getUserId() {
        return userId;
    }

    /**
     * Gets module exercise id.
     *
     * @return the module exercise id
     */
    public long getModuleExerciseId() {
        return moduleExerciseId;
    }
}
