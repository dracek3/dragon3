package cz.cvut.fit.filipon1.dracek_admin.student.ui;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.DataCache;
import cz.cvut.fit.filipon1.dracek_admin.LoginManager;
import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.Exercise;
import cz.cvut.fit.filipon1.dracek_admin.model.ModuleExercise;
import cz.cvut.fit.filipon1.dracek_admin.model.Task;
import cz.cvut.fit.filipon1.dracek_admin.model.User;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.ExerciseAdapter;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.ModuleExerciseAdapter;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.TasksOfUserAdapter;
import cz.cvut.fit.filipon1.dracek_admin.student.dialog.NumberPickerDialog;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ErrorHandler;
import cz.cvut.fit.filipon1.dracek_admin.webservice.RestClient;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ServiceGenerator;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ondra on 23.2.16.
 */
public class StudentTaskFragment extends Fragment {

    private static final String USER = "user";

    private ListView mTasksListView;
    private Spinner spnModuleExercise;
    private Spinner spnExercise;
    private TextView tvCreateDateFrom;
    private TextView tvCreateDateTo;
    private TextView tvFinishDateFrom;
    private TextView tvFinishDateTo;
    private TextView tvScoreFrom;
    private TextView tvScoreTo;
    private View btnCreateDateFromReset;
    private View btnCreateDateToReset;
    private View btnFinishDateFromReset;
    private View btnFinishDateToReset;
    private View btnFilter;
    private View tbFilterShow;
    private View mFilterContainer;

    private boolean mIsFilterShown;

    private TasksOfUserAdapter mTasksAdapter;
    private ModuleExerciseAdapter mModuleExerciseAdapter;
    private ExerciseAdapter mExerciseAdapter;

    private Calendar mCreateDateFrom;
    private Calendar mCreateDateTo;
    private Calendar mFinishDateFrom;
    private Calendar mFinishDateTo;
    private int mScoreFrom = 0;
    private int mScoreTo = 100;

    private User mUser;

    /**
     * New instance student task fragment.
     *
     * @param user the user
     * @return the student task fragment
     */
    public static StudentTaskFragment newInstance(User user) {

        Bundle args = new Bundle();
        args.putSerializable(USER, user);
        StudentTaskFragment fragment = new StudentTaskFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_student_task, container, false);

        mUser = (User) getArguments().getSerializable(USER);

        mTasksListView = (ListView) v.findViewById(R.id.lv_task);
        spnModuleExercise = (Spinner) v.findViewById(R.id.spn_module_exercise);
        spnExercise = (Spinner) v.findViewById(R.id.spn_exercise);
        tvCreateDateFrom = (TextView) v.findViewById(R.id.tv_create_date_from);
        tvCreateDateTo = (TextView) v.findViewById(R.id.tv_create_date_to);
        tvFinishDateFrom = (TextView) v.findViewById(R.id.tv_finish_date_from);
        tvFinishDateTo = (TextView) v.findViewById(R.id.tv_finish_date_to);
        tvScoreFrom = (TextView) v.findViewById(R.id.tv_score_from);
        tvScoreTo = (TextView) v.findViewById(R.id.tv_score_to);
        btnCreateDateFromReset = v.findViewById(R.id.iv_create_date_from_reset);
        btnCreateDateToReset = v.findViewById(R.id.iv_create_date_to_reset);
        btnFinishDateFromReset = v.findViewById(R.id.iv_finish_date_from_reset);
        btnFinishDateToReset = v.findViewById(R.id.iv_finish_date_to_reset);
        btnFilter = v.findViewById(R.id.btn_filter);
        tbFilterShow = v.findViewById(R.id.tb_show_filter);
        mFilterContainer = v.findViewById(R.id.ll_filter_container);

        mIsFilterShown = true;

        loadModuleExercises();

        setListeners();

        return v;
    }

    private void loadModuleExercises() {
        DataCache.getInstance().getModuleExercise(new DataCache.ModuleExerciseCallback() {
            @Override
            public void onSuccess(List<ModuleExercise> moduleExercises) {
                mModuleExerciseAdapter = new ModuleExerciseAdapter(getContext(), moduleExercises);
                spnModuleExercise.setAdapter(mModuleExerciseAdapter);
            }

            @Override
            public void onFail(RetrofitError error) {
                ErrorHandler.handle(error, getContext());
            }
        });
    }

    private void loadExercises(long moduleExerciseId) {
        RestClient client = ServiceGenerator
                .createServiceUserToken(RestClient.class, LoginManager.getToken());
        client.getExercsisesOfModule(moduleExerciseId, new Callback<List<Exercise>>() {
            @Override
            public void success(List<Exercise> exercises, Response response) {
                mExerciseAdapter = new ExerciseAdapter(getContext(), exercises);
                spnExercise.setAdapter(mExerciseAdapter);

                /* module exercise and exercises where loaded so scores can be refreshed */
                displayTasks();
            }

            @Override
            public void failure(RetrofitError error) {
                ErrorHandler.handle(error, getContext());
            }
        });
    }

    private void setListeners() {

        tbFilterShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsFilterShown) {
                    mFilterContainer.setVisibility(View.GONE);
                    mIsFilterShown = false;
                } else {
                    mFilterContainer.setVisibility(View.VISIBLE);
                    mIsFilterShown = true;
                }
            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayTasks();
            }
        });

        tvCreateDateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int day = 0, month = 0, year = 0;
                if (mCreateDateFrom != null) {
                    day = mCreateDateFrom.get(Calendar.DAY_OF_MONTH);
                    month = mCreateDateFrom.get(Calendar.MONTH);
                    year = mCreateDateFrom.get(Calendar.YEAR);
                }
                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mCreateDateFrom = Calendar.getInstance();
                        mCreateDateFrom.set(Calendar.YEAR, year);
                        mCreateDateFrom.set(Calendar.MONTH, monthOfYear);
                        mCreateDateFrom.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        displayDate(tvCreateDateFrom, mCreateDateFrom);
                    }
                }, year, month, day);
                dialog.getDatePicker().setMinDate(946688461000l);
                dialog.show();
            }
        });

        tvCreateDateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int day = 0, month = 0, year = 0;
                if (mCreateDateTo != null) {
                    day = mCreateDateTo.get(Calendar.DAY_OF_MONTH);
                    month = mCreateDateTo.get(Calendar.MONTH);
                    year = mCreateDateTo.get(Calendar.YEAR);
                }
                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mCreateDateTo = Calendar.getInstance();
                        mCreateDateTo.set(Calendar.YEAR, year);
                        mCreateDateTo.set(Calendar.MONTH, monthOfYear);
                        mCreateDateTo.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        displayDate(tvCreateDateTo, mCreateDateTo);
                    }
                }, year, month, day);
                dialog.getDatePicker().setMinDate(946688461000l);
                dialog.show();
            }
        });

        tvFinishDateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int day = 0, month = 0, year = 0;
                if (mFinishDateFrom != null) {
                    day = mFinishDateFrom.get(Calendar.DAY_OF_MONTH);
                    month = mFinishDateFrom.get(Calendar.MONTH);
                    year = mFinishDateFrom.get(Calendar.YEAR);
                }
                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mFinishDateFrom = Calendar.getInstance();
                        mFinishDateFrom.set(Calendar.YEAR, year);
                        mFinishDateFrom.set(Calendar.MONTH, monthOfYear);
                        mFinishDateFrom.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        displayDate(tvFinishDateFrom, mFinishDateFrom);
                    }
                }, year, month, day);
                dialog.getDatePicker().setMinDate(946688461000l);
                dialog.show();
            }
        });

        tvFinishDateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int day = 0, month = 0, year = 0;
                if (mFinishDateTo != null) {
                    day = mFinishDateTo.get(Calendar.DAY_OF_MONTH);
                    month = mFinishDateTo.get(Calendar.MONTH);
                    year = mFinishDateTo.get(Calendar.YEAR);
                }
                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mFinishDateTo = Calendar.getInstance();
                        mFinishDateTo.set(Calendar.YEAR, year);
                        mFinishDateTo.set(Calendar.MONTH, monthOfYear);
                        mFinishDateTo.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        displayDate(tvFinishDateTo, mFinishDateTo);
                    }
                }, year, month, day);
                dialog.getDatePicker().setMinDate(946688461000l);
                dialog.show();
            }
        });

        tvScoreFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPickerDialog dialog = NumberPickerDialog.newInstance(mScoreFrom);
                dialog.setListener(new NumberPickerDialog.OnNumberSelectedListener() {
                    @Override
                    public void onSelected(int number) {
                        mScoreFrom = number;
                        tvScoreFrom.setText(String.valueOf(mScoreFrom));
                    }
                });
                dialog.show(getChildFragmentManager(), "E");
            }
        });

        tvScoreTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPickerDialog dialog = NumberPickerDialog.newInstance(mScoreTo);
                dialog.setListener(new NumberPickerDialog.OnNumberSelectedListener() {
                    @Override
                    public void onSelected(int number) {
                        mScoreTo = number;
                        tvScoreTo.setText(String.valueOf(mScoreTo));
                    }
                });
                dialog.show(getChildFragmentManager(), "F");
            }
        });

        btnCreateDateFromReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCreateDateFrom = null;
                displayDate(tvCreateDateFrom, null);
            }
        });

        btnCreateDateToReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCreateDateTo = null;
                displayDate(tvCreateDateTo, null);
            }
        });

        btnFinishDateFromReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFinishDateFrom = null;
                displayDate(tvFinishDateFrom, null);
            }
        });

        btnFinishDateToReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFinishDateTo = null;
                displayDate(tvFinishDateTo, null);
            }
        });

        spnModuleExercise.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mModuleExerciseAdapter != null) {
                    ModuleExercise moduleExercise = mModuleExerciseAdapter.getItem(position);
                    loadExercises(moduleExercise.getId());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }



    private void displayDate(TextView tv, Calendar date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

        if (date == null) {
            tv.setText("--.--.----");
        } else {
            tv.setText(dateFormat.format(date.getTime()));
        }
    }

    private void displayTasks() {

        long moduleExerciseId = mModuleExerciseAdapter
                                .getItem(spnModuleExercise.getSelectedItemPosition()).getId();
        long exercieId = mExerciseAdapter.getItem(spnExercise.getSelectedItemPosition()).getId();

        RestClient client = ServiceGenerator
                .createServiceUserToken(RestClient.class, LoginManager.getToken());
        client.getTasksOfUser(mUser.getId(), moduleExerciseId, exercieId, new Callback<List<Task>>() {
            @Override
            public void success(List<Task> tasks, Response response) {
                mTasksAdapter = new TasksOfUserAdapter(getContext(), filterTasks(tasks));
                mTasksListView.setAdapter(mTasksAdapter);
            }

            @Override
            public void failure(RetrofitError error) {
                mTasksAdapter = new TasksOfUserAdapter(getContext(), new ArrayList<Task>());
                mTasksListView.setAdapter(mTasksAdapter);
            }
        });
    }

    private List<Task> filterTasks(List<Task> tasks) {
        List<Task> filteredTasks = new ArrayList<>();

        for (Task t : tasks) {

            if (mCreateDateFrom != null) {
                if (t.getCreatedAt() < mCreateDateFrom.getTime().getTime()) {
                    continue;
                }
            }

            if (mCreateDateTo != null) {
                if (t.getCreatedAt() > mCreateDateTo.getTime().getTime()) {
                    continue;
                }
            }

            if (t.getScore() != null) {
                if (mFinishDateFrom != null) {
                    if (t.getScore().getCreatedAt() < mFinishDateFrom.getTime().getTime()) {
                        continue;
                    }
                }

                if (mFinishDateTo != null) {
                    if (t.getScore().getCreatedAt() > mFinishDateTo.getTime().getTime()) {
                        continue;
                    }
                }
            }

            if (t.getMinimalScore() > mScoreTo) {
                continue;
            }

            if (t.getMinimalScore() < mScoreFrom) {
                continue;
            }



            filteredTasks.add(t);
        }

        return filteredTasks;
    }
}


