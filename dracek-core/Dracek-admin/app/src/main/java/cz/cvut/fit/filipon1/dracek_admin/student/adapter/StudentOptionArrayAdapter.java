package cz.cvut.fit.filipon1.dracek_admin.student.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.User;

/**
 * Created by ondra on 21.2.16.
 */
public class StudentOptionArrayAdapter extends ArrayAdapter<User> {

    private long mClassId;
    private OnRemoveFromClassListener mListener;

    /**
     * The interface On remove from class listener.
     */
    public interface OnRemoveFromClassListener {
        /**
         * On remove from class.
         *
         * @param userId  the user id
         * @param classId the class id
         */
        void onRemoveFromClass(long userId, long classId);
    }

    /**
     * Instantiates a new Student option array adapter.
     *
     * @param context the context
     * @param objects the objects
     * @param classId the class id
     */
    public StudentOptionArrayAdapter(Context context, List<User> objects, long classId) {
        super(context, 0, objects);
        mClassId = classId;
    }

    /**
     * Sets listener.
     *
     * @param listener the listener
     */
    public void setListener(OnRemoveFromClassListener listener) {
        this.mListener = listener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.list_item_user_option, parent, false);
        }

        final User user = getItem(position);

        ((TextView) convertView.findViewById(R.id.tv_text)).setText(user.getFullName());
        ((TextView) convertView.findViewById(R.id.tv_username)).setText(user.getUsernameFormatted());

        convertView.findViewById(R.id.iv_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onRemoveFromClass(user.getId(), mClassId);
                }
            }
        });

        return convertView;
    }
}
