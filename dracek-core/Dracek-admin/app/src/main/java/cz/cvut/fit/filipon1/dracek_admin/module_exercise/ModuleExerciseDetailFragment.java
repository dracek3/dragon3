package cz.cvut.fit.filipon1.dracek_admin.module_exercise;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.LoginManager;
import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.ModuleExercise;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ApiConstants;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ErrorHandler;
import cz.cvut.fit.filipon1.dracek_admin.webservice.RestClient;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ServiceGenerator;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ondra on 26.3.16.
 */
public class ModuleExerciseDetailFragment extends Fragment {

    private static String MODULE_EXERCISE = "moduleExercise";

    private ModuleExercise mModuleExercise;

    private Button btnAddToStudent;
    private Button btnAddToClass;
    private Button btnRemoveFromStudent;
    private Button btnRemoveFromClass;

    /**
     * New instance module exercise detail fragment.
     *
     * @param moduleExercise the module exercise
     * @return the module exercise detail fragment
     */
    public static ModuleExerciseDetailFragment newInstance(ModuleExercise moduleExercise) {

        Bundle args = new Bundle();
        args.putSerializable(MODULE_EXERCISE, moduleExercise);
        ModuleExerciseDetailFragment fragment = new ModuleExerciseDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_module_exercise_detail, container, false);

        mModuleExercise = (ModuleExercise) getArguments().getSerializable(MODULE_EXERCISE);

        TextView name = (TextView) v.findViewById(R.id.tv_title);
        TextView description = (TextView) v.findViewById(R.id.tv_description);
        ImageView image = (ImageView) v.findViewById(R.id.iv_image);
        btnAddToStudent = (Button) v.findViewById(R.id.btn_add_to_stuent);
        btnAddToClass = (Button) v.findViewById(R.id.btn_add_to_class);
        btnRemoveFromStudent = (Button) v.findViewById(R.id.btn_remove_from_student);
        btnRemoveFromClass = (Button) v.findViewById(R.id.btn_remove_from_class);


        name.setText(mModuleExercise.getName());
        description.setText(mModuleExercise.getDescription());
        Picasso.with(getContext()).load(ApiConstants.BASE_URL + mModuleExercise.getImage()).into(image);

        setClickListeners();

        return v;
    }

    private void setClickListeners() {
        btnAddToStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectStudentDialog dialog = new SelectStudentDialog(getContext());
                dialog.setListener(new SelectStudentDialog.OnUserSelectedListener() {
                    @Override
                    public void onUserSelect(long userId) {
                        addModuleToStudent(userId);
                    }
                });
                dialog.show();
            }
        });

        btnRemoveFromStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectStudentDialog dialog = new SelectStudentDialog(getContext());
                dialog.setListener(new SelectStudentDialog.OnUserSelectedListener() {
                    @Override
                    public void onUserSelect(long userId) {
                        removeModuleFromStudent(userId);
                    }
                });
                dialog.show();
            }
        });

        btnAddToClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectClassDialog dialog = new SelectClassDialog(getContext());
                dialog.setListener(new SelectClassDialog.OnClassSelectedListener() {
                    @Override
                    public void onClassSelect(long classId) {
                        addModuleToClass(classId);
                    }
                });
                dialog.show();
            }
        });

        btnRemoveFromClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectClassDialog dialog = new SelectClassDialog(getContext());
                dialog.setListener(new SelectClassDialog.OnClassSelectedListener() {
                    @Override
                    public void onClassSelect(long classId) {
                        removeModuleFromClass(classId);
                    }
                });
                dialog.show();
            }
        });
    }

    private void addModuleToStudent(long studentId) {
        RestClient client = ServiceGenerator
                .createServiceUserToken(RestClient.class, LoginManager.getToken());
        client.addModuleExerciseToUser(studentId, mModuleExercise.getId(),
                new HashMap<String, Object>(), new Callback<List<Object>>() {
                    @Override
                    public void success(List<Object> objects, Response response) {
                        Toast.makeText(getContext(), "Modul přidán", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        ErrorHandler.handle(error, getContext());
                    }
                });
    }


    private void removeModuleFromStudent(long studentId) {
        RestClient client = ServiceGenerator
                .createServiceUserToken(RestClient.class, LoginManager.getToken());
        client.deleteModuleExcercideFromUser(studentId, mModuleExercise.getId(),
                new Callback<List<Object>>() {
                    @Override
                    public void success(List<Object> objects, Response response) {
                        Toast.makeText(getContext(), "Modul odebrán", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        ErrorHandler.handle(error, getContext());
                    }
                });
    }

    private void addModuleToClass(long classId) {
        RestClient client = ServiceGenerator
                .createServiceUserToken(RestClient.class, LoginManager.getToken());
        client.addModuleExerciseToClass(classId, mModuleExercise.getId(),
                new HashMap<String, Object>(), new Callback<List<Object>>() {
                    @Override
                    public void success(List<Object> objects, Response response) {
                        Toast.makeText(getContext(), "Modul přidán", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        ErrorHandler.handle(error, getContext());
                    }
                });
    }

    private void removeModuleFromClass(long classId) {
        RestClient client = ServiceGenerator
                .createServiceUserToken(RestClient.class, LoginManager.getToken());
        client.removeModuleExerciseFromClass(classId, mModuleExercise.getId(),
                                                        new Callback<List<Object>>() {
                    @Override
                    public void success(List<Object> objects, Response response) {
                        Toast.makeText(getContext(), "Modul odebrán", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        ErrorHandler.handle(error, getContext());
                    }
                });
    }
}
