package cz.cvut.fit.filipon1.dracek_admin.webservice;

import android.telecom.Call;

import java.util.HashMap;
import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.model.Exercise;
import cz.cvut.fit.filipon1.dracek_admin.model.Klass;
import cz.cvut.fit.filipon1.dracek_admin.model.ModuleExercise;
import cz.cvut.fit.filipon1.dracek_admin.model.Score;
import cz.cvut.fit.filipon1.dracek_admin.model.Task;
import cz.cvut.fit.filipon1.dracek_admin.model.User;
import cz.cvut.fit.filipon1.dracek_admin.webservice.response.LoginResponse;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by ondra on 25.7.2015.
 */
public interface RestClient {

    ///////////////////////////////////////////////////////////////////////////
    // CLASS
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Gets class list.
     *
     * @param cb the cb
     */
    @GET("/admin/class")
    void getClassList(
            Callback<List<Klass>> cb
    );

    /**
     * Add student to class.
     *
     * @param studentId the student id
     * @param classId   the class id
     * @param body      the body
     * @param cb        the cb
     */
    @POST("/admin/student/{student_id}/class/{class_id}")
    void addStudentToClass(
            @Path("student_id") long studentId,
            @Path("class_id") long classId,
            @Body HashMap<String, Object> body,
            Callback<List<Object>> cb
    );

    /**
     * Edit class name.
     *
     * @param classId the class id
     * @param body    the body
     * @param cb      the cb
     */
    @PUT("/admin/class/{class_id}")
    void editClassName(
            @Path("class_id") long classId,
            @Body HashMap<String, Object> body,
            Callback<Klass> cb
    );

    /**
     * Delete class.
     *
     * @param classId the class id
     * @param cb      the cb
     */
    @DELETE("/admin/class/{class_id}")
    void deleteClass(
            @Path("class_id") long classId,
            Callback<List<Object>> cb
    );

    /**
     * Create class.
     *
     * @param body the body
     * @param cb   the cb
     */
    @POST("/admin/class")
    void createClass(
            @Body HashMap<String, Object> body,
            Callback<Klass> cb
    );

    /**
     * Delete student from class.
     *
     * @param studentId the student id
     * @param classId   the class id
     * @param cb        the cb
     */
    @DELETE("/admin/student/{student_id}/class/{class_id}")
    void deleteStudentFromClass(
            @Path("student_id") long studentId,
            @Path("class_id") long classId,
            Callback<List<Object>> cb
    );

    ///////////////////////////////////////////////////////////////////////////
    // USER / STUDENT
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Gets all student list.
     *
     * @param cb the cb
     */
    @GET("/admin/student")
    void getAllStudentList(
            Callback<List<User>> cb
    );

    /**
     * Gets students from class list.
     *
     * @param classId the class id
     * @param cb      the cb
     */
    @GET("/admin/class/{class_id}/student")
    void getStudentsFromClassList(
            @Path("class_id") long classId,
            Callback<List<User>> cb
    );

    /**
     * Edit student.
     *
     * @param studentId the student id
     * @param body      the body
     * @param cb        the cb
     */
    @PUT("/admin/student/{student_id}")
    void editStudent(
            @Path("student_id") long studentId,
            @Body HashMap<String, Object> body,
            Callback<User> cb
    );

    /**
     * Create student.
     *
     * @param body the body
     * @param cb   the cb
     */
    @POST("/admin/student")
    void createStudent(
            @Body HashMap<String, Object> body,
            Callback<User> cb
    );

    @DELETE("/admin/student/{student_id}")
    void deleteStudent(
            @Path("student_id") long studentId,
            Callback<User> cb
    );

    ///////////////////////////////////////////////////////////////////////////
    // EXERCISE
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Gets exercsises of module.
     *
     * @param moduleExerciseId the module exercise id
     * @param cb               the cb
     */
    @GET("/admin/module_excercise/{module_exercise_id}/excercise")
    void getExercsisesOfModule(
            @Path("module_exercise_id") long moduleExerciseId,
            Callback<List<Exercise>> cb
    );

    /**
     * Delete exercise.
     *
     * @param moduleExerciseId the module exercise id
     * @param exerciseId       the exercise id
     * @param cb               the cb
     */
    @DELETE("/admin/module_excercise/{module_exercise_id}/excercise/{exercise_id}")
    void deleteExercise(
            @Path("module_exercise_id") long moduleExerciseId,
            @Path("exercise_id") long exerciseId,
            Callback<List<Exercise>> cb
    );

    /**
     * Create exercise.
     *
     * @param moduleExerciseId the module exercise id
     * @param body             the body
     * @param cb               the cb
     */
    @POST("/admin/module_excercise/{module_exercise_id}/excercise")
    void createExercise(
            @Path("module_exercise_id") long moduleExerciseId,
            @Body HashMap<String, Object> body,
            Callback<List<Exercise>> cb
    );

    @PUT("/admin/module_excercise/{module_exercise_id}/excercise/{exercise_id}")
    void editExercise(
            @Path("module_exercise_id") long moduleExerciseId,
            @Path("exercise_id") long exerciseId,
            @Body HashMap<String, Object> body,
            Callback<List<Exercise>> cb
    );


    ///////////////////////////////////////////////////////////////////////////
    // MODULE EXERCISE
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Gets module exercises of student.
     *
     * @param studentId the student id
     * @param cb        the cb
     */
    @GET("/admin/student/{student_id}/module_excercise")
    void getModuleExercisesOfStudent(
            @Path("student_id") long studentId,
            Callback<List<ModuleExercise>> cb
    );

    /**
     * Gets module exercise list.
     *
     * @param cb the cb
     */
    @GET("/admin/module_excercise")
    void getModuleExerciseList(
            Callback<List<ModuleExercise>> cb
    );

    /**
     * Add module exercise to user.
     *
     * @param studentId        the student id
     * @param moduleExerciseId the module exercise id
     * @param body             the body
     * @param cb               the cb
     */
    @POST("/admin/student/{student_id}/module_excercise/{module_exercise_id}")
    void addModuleExerciseToUser(
            @Path("student_id") long studentId,
            @Path("module_exercise_id") long moduleExerciseId,
            @Body HashMap<String, Object> body,
            Callback<List<Object>> cb
    );

    /**
     * Add module exercise to class.
     *
     * @param classId          the class id
     * @param moduleExerciseId the module exercise id
     * @param body             the body
     * @param cb               the cb
     */
    @POST("/admin/class/{class_id}/module_excercise/{module_exercise_id}")
    void addModuleExerciseToClass(
            @Path("class_id") long classId,
            @Path("module_exercise_id") long moduleExerciseId,
            @Body HashMap<String, Object> body,
            Callback<List<Object>> cb
    );

    /**
     * Delete module excercide from user.
     *
     * @param studentId        the student id
     * @param moduleExerciseId the module exercise id
     * @param cb               the cb
     */
    @DELETE("/admin/student/{student_id}/module_excercise/{module_exercise_id}")
    void deleteModuleExcercideFromUser(
            @Path("student_id") long studentId,
            @Path("module_exercise_id") long moduleExerciseId,
            Callback<List<Object>> cb
    );

    /**
     * Remove module exercise from class.
     *
     * @param classId          the class id
     * @param moduleExerciseId the module exercise id
     * @param cb               the cb
     */
    @DELETE("/admin/class/{class_id}/module_excercise/{module_exercise_id}")
    void removeModuleExerciseFromClass(
            @Path("class_id") long classId,
            @Path("module_exercise_id") long moduleExerciseId,
            Callback<List<Object>> cb
    );


    ///////////////////////////////////////////////////////////////////////////
    // TASK
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Gets tasks of user.
     *
     * @param studentId        the student id
     * @param moduleExerciseId the module exercise id
     * @param exerciseId       the exercise id
     * @param cb               the cb
     */
    @GET("/admin/student/{student_id}/module_excercise/{module_exercise_id}/excercise/{exercise_id}/task")
    void getTasksOfUser(
            @Path("student_id") long studentId,
            @Path("module_exercise_id") long moduleExerciseId,
            @Path("exercise_id") long exerciseId,
            Callback<List<Task>> cb
    );

    /**
     * Gets all tasks.
     *
     * @param moduleExerciseId the module exercise id
     * @param exerciseId       the exercise id
     * @param cb               the cb
     */
    @GET("/admin/module_excercise/{module_exercise_id}/excercise/{exercise_id}/task")
    void getAllTasks(
            @Path("module_exercise_id") long moduleExerciseId,
            @Path("exercise_id") long exerciseId,
            Callback<List<Task>> cb
    );

    /**
     * Gets tasks of users.
     *
     * @param taskId the task id
     * @param cb     the cb
     */
    @GET("/admin/task/{task_id}")
    void getTasksOfUsers(
            @Path("task_id") long taskId,
            Callback<List<Task>> cb
    );

    /**
     * Gets tasks of class.
     *
     * @param classId the class id
     * @param taskId  the task id
     * @param cb      the cb
     */
    @GET("/admin/class/{class_id}/task/{task_id}")
    void getTasksOfClass(
            @Path("class_id") long classId,
            @Path("task_id") long taskId,
            Callback<List<Task>> cb
    );

    /**
     * Add task to student.
     *
     * @param studentId the student id
     * @param taskId    the task id
     * @param body      the body
     * @param cb        the cb
     */
    @POST("/admin/student/{student_id}/task/{task_id}")
    void addTaskToStudent(
            @Path("student_id") long studentId,
            @Path("task_id") long taskId,
            @Body Object body,
            Callback<List<Object>> cb
    );

    /**
     * Add task to class.
     *
     * @param classId the class id
     * @param taskId  the task id
     * @param body    the body
     * @param cb      the cb
     */
    @POST("/admin/class/{class_id}/task/{task_id}")
    void addTaskToClass(
            @Path("class_id") long classId,
            @Path("task_id") long taskId,
            @Body Object body,
            Callback<List<Object>> cb
    );

    /**
     * Create task.
     *
     * @param moduleExerciseId the module exercise id
     * @param exerciseId       the exercise id
     * @param body             the body
     * @param cb               the cb
     */
    @POST("/admin/module_excercise/{module_exercise_id}/excercise/{exercise_id}/task")
    void createTask(
            @Path("module_exercise_id") long moduleExerciseId,
            @Path("exercise_id") long exerciseId,
            @Body HashMap<String, Object> body,
            Callback<List<Object>> cb
    );


    ///////////////////////////////////////////////////////////////////////////
    // SCORE
    ///////////////////////////////////////////////////////////////////////////


    /**
     * Gets score of user.
     *
     * @param studentId        the student id
     * @param moduleExerciseId the module exercise id
     * @param exerciseId       the exercise id
     * @param cb               the cb
     */
    @GET("/admin/student/{student_id}/module_excercise/{module_exercise_id}/excercise/{exercise_id}/score")
    void getScoreOfUser(
            @Path("student_id") long studentId,
            @Path("module_exercise_id") long moduleExerciseId,
            @Path("exercise_id") long exerciseId,
            Callback<List<Score>> cb
    );

    /**
     * Gets score.
     *
     * @param moduleExerciseId the module exercise id
     * @param exerciseId       the exercise id
     * @param cb               the cb
     */
    @GET("/admin/module_excercise/{module_exercise_id}/excercise/{exercise_id}/score")
    void getScore(
            @Path("module_exercise_id") long moduleExerciseId,
            @Path("exercise_id") long exerciseId,
            Callback<List<Score>> cb
    );

    /**
     * Gets score of class.
     *
     * @param classId          the class id
     * @param moduleExerciseId the module exercise id
     * @param exerciseId       the exercise id
     * @param cb               the cb
     */
    @GET("/admin/class/{class_id}/module_excercise/{module_exercise_id}/excercise/{exercise_id}/score")
    void getScoreOfClass(
            @Path("class_id") long classId,
            @Path("module_exercise_id") long moduleExerciseId,
            @Path("exercise_id") long exerciseId,
            Callback<List<Score>> cb
    );

    ///////////////////////////////////////////////////////////////////////////
    // TOKEN
    ///////////////////////////////////////////////////////////////////////////

    /**
     * Login.
     *
     * @param body the body
     * @param cb   the cb
     */
    @POST("/token")
    void login(
            @Body HashMap<String, Object> body,
            Callback<LoginResponse> cb
    );





}
