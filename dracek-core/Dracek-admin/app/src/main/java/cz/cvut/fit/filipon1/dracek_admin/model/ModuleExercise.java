package cz.cvut.fit.filipon1.dracek_admin.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ondra on 21.2.16.
 */
public class ModuleExercise implements Serializable {

    private long id;

    @SerializedName("google_play_url")
    private String googlePlayUrl;

    private String description;
    private String image;
    private String name;

    @SerializedName("package_name_editor")
    private String packageNameEditor;

    @SerializedName("package_name_excercise")
    private String packageNameExercise;

    @SerializedName("package")
    private String packageName;


    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Gets package name.
     *
     * @return the package name
     */
    public String getPackageName() {
        return packageName;
    }

    /**
     * Sets package name.
     *
     * @param packageName the package name
     */
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    /**
     * Sets google play url.
     *
     * @param googlePlayUrl the google play url
     */
    public void setGooglePlayUrl(String googlePlayUrl) {
        this.googlePlayUrl = googlePlayUrl;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Sets image.
     *
     * @param image the image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets package name editor.
     *
     * @param packageNameEditor the package name editor
     */
    public void setPackageNameEditor(String packageNameEditor) {
        this.packageNameEditor = packageNameEditor;
    }

    /**
     * Sets package name exercise.
     *
     * @param packageNameExercise the package name exercise
     */
    public void setPackageNameExercise(String packageNameExercise) {
        this.packageNameExercise = packageNameExercise;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Gets google play url.
     *
     * @return the google play url
     */
    public String getGooglePlayUrl() {
        return googlePlayUrl;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets image.
     *
     * @return the image
     */
    public String getImage() {
        return image;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets package name editor.
     *
     * @return the package name editor
     */
    public String getPackageNameEditor() {
        return packageNameEditor;
    }

    /**
     * Gets package name exercise.
     *
     * @return the package name exercise
     */
    public String getPackageNameExercise() {
        return packageNameExercise;
    }

}
