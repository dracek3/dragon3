package cz.cvut.fit.filipon1.dracek_admin.utils;

import android.util.Base64;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by ondra on 15.5.16.
 */
public class HashSHA256 {

    public static String hash (String text) throws NoSuchAlgorithmException {

        MessageDigest md = MessageDigest.getInstance("SHA-256");

        md.update(text.getBytes());
        byte[] digest = md.digest();

        String res = Base64.encodeToString(digest, Base64.DEFAULT);
        return res.substring(0, res.length() - 1);
    }
}
