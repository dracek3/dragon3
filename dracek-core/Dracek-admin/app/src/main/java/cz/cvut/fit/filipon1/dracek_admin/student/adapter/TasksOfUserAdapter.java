package cz.cvut.fit.filipon1.dracek_admin.student.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.Score;
import cz.cvut.fit.filipon1.dracek_admin.model.Task;
import cz.cvut.fit.filipon1.dracek_admin.utils.TextFormatter;

/**
 * Created by ondra on 23.2.16.
 */
public class TasksOfUserAdapter extends ArrayAdapter<Task> {


    /**
     * Instantiates a new Tasks of user adapter.
     *
     * @param context the context
     * @param objects the objects
     */
    public TasksOfUserAdapter(Context context, List<Task> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.list_item_task_of_user, parent, false);
        }

        Task task = getItem(position);



        ((TextView) convertView.findViewById(R.id.tv_created_date_time))
                .setText(TextFormatter.formatDateTime(task.getCreatedAt()));

        TextView turnInDateView = (TextView) convertView.findViewById(R.id.tv_turn_in_date_time);
        if (task.getScore() == null) {
            turnInDateView.setText("-");
        } else {
            turnInDateView.setText(TextFormatter.formatDateTime(task.getScore().getCreatedAt()));
        }

        ((TextView) convertView.findViewById(R.id.tv_minimal_score))
                .setText(String.valueOf(task.getMinimalScore()));

        TextView achievedScoreView = (TextView) convertView.findViewById(R.id.tv_achieved_score);
        if (task.getScore() == null) {
            achievedScoreView.setText("-");
        } else {
            achievedScoreView.setText(String.valueOf(task.getScore().getPercentage()));
        }

        return convertView;
    }
}
