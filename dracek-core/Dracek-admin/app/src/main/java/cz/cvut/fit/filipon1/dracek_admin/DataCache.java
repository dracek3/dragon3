package cz.cvut.fit.filipon1.dracek_admin;

import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.model.Klass;
import cz.cvut.fit.filipon1.dracek_admin.model.ModuleExercise;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.ModuleExerciseAdapter;
import cz.cvut.fit.filipon1.dracek_admin.webservice.RestClient;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ServiceGenerator;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ondra on 23.2.16.
 */
public class DataCache {

    private static DataCache mInstance;

    private List<Klass> mClasses;
    private List<ModuleExercise> mModuleExcercises;

    /**
     * The interface Klass callback.
     */
    public interface KlassCallback {
        /**
         * On success.
         *
         * @param classes the classes
         */
        void onSuccess(List<Klass> classes);

        /**
         * On fail.
         *
         * @param error the error
         */
        void onFail(RetrofitError error);
    }

    /**
     * The interface Module exercise callback.
     */
    public interface ModuleExerciseCallback {
        /**
         * On success.
         *
         * @param moduleExercises the module exercises
         */
        void onSuccess(List<ModuleExercise> moduleExercises);

        /**
         * On fail.
         *
         * @param error the error
         */
        void onFail(RetrofitError error);
    }

    private DataCache() {
        /* Prevent from initialization from outside */
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static DataCache getInstance() {
        if (mInstance == null) {
            mInstance = new DataCache();
        }
        return mInstance;
    }

    /**
     * Gets class list.
     *
     * @param cb the cb
     */
    public void getClassList(final KlassCallback cb) {
        if (mClasses != null) {
            cb.onSuccess(mClasses);
        } else {
            RestClient client = ServiceGenerator.createServiceUserToken(RestClient.class, LoginManager.getToken());
            client.getClassList(new Callback<List<Klass>>() {
                @Override
                public void success(List<Klass> klasses, Response response) {
                    mClasses = klasses;
                    cb.onSuccess(klasses);
                }

                @Override
                public void failure(RetrofitError error) {
                    cb.onFail(error);
                }
            });
        }
    }

    /**
     * Invalidate classes.
     */
    public void invalidateClasses() {
        mClasses = null;
    }

    /**
     * Gets module exercise.
     *
     * @param cb the cb
     */
    public void getModuleExercise(final ModuleExerciseCallback cb) {
        if (mModuleExcercises != null) {
            cb.onSuccess(mModuleExcercises);
        } else {
            RestClient client = ServiceGenerator
                    .createServiceUserToken(RestClient.class, LoginManager.getToken());
            client.getModuleExerciseList(new Callback<List<ModuleExercise>>() {
                @Override
                public void success(List<ModuleExercise> moduleExercises, Response response) {
                    mModuleExcercises = moduleExercises;
                    cb.onSuccess(moduleExercises);
                }

                @Override
                public void failure(RetrofitError error) {
                    cb.onFail(error);
                }
            });
        }
    }

    /**
     * Invalidate module exercise.
     */
    public void invalidateModuleExercise() {
        mModuleExcercises = null;
    }


}
