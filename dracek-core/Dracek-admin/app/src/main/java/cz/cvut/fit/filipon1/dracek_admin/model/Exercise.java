package cz.cvut.fit.filipon1.dracek_admin.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ondra on 2. 3. 2016.
 */
public class Exercise implements Serializable {

    private long id;
    private String name;
    private String description;
    private String file;
    private boolean deleted;
    /// TODO version
    @SerializedName("moduleExcerciseId")
    private long moduleExerciseId;

    private List<Image> images;

    /**
     * Gets images.
     *
     * @return the images
     */
    public List<Image> getImages() {
        return images;
    }


    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Sets file.
     *
     * @param file the file
     */
    public void setFile(String file) {
        this.file = file;
    }

    /**
     * Sets deleted.
     *
     * @param deleted the deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    /**
     * Sets module exercise id.
     *
     * @param moduleExerciseId the module exercise id
     */
    public void setModuleExerciseId(long moduleExerciseId) {
        this.moduleExerciseId = moduleExerciseId;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets file.
     *
     * @return the file
     */
    public String getFile() {
        return file;
    }

    /**
     * Is deleted boolean.
     *
     * @return the boolean
     */
    public boolean isDeleted() {
        return deleted;
    }

    /**
     * Gets module exercise id.
     *
     * @return the module exercise id
     */
    public long getModuleExerciseId() {
        return moduleExerciseId;
    }


    /**
     * The type Image.
     */
    public static class Image implements Serializable {
        /**
         * The Image.
         */
        public String image;
        /**
         * The Original name.
         */
        public String originalName;

        /**
         * Instantiates a new Image.
         *
         * @param image        the image
         * @param originalName the original name
         */
        public Image(String image, String originalName) {
            this.image = image;
            this.originalName = originalName;
        }
    }
}
