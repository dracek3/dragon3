package cz.cvut.fit.filipon1.dracek_admin.exercise;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v4.graphics.BitmapCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cz.cvut.fit.filipon1.dracek_admin.LoginManager;
import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.Exercise;
import cz.cvut.fit.filipon1.dracek_admin.model.ModuleExercise;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ApiConstants;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ErrorHandler;
import cz.cvut.fit.filipon1.dracek_admin.webservice.RestClient;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ServiceGenerator;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ondra on 26.3.16.
 */
public class ExerciseDetailFragment extends Fragment {

    private static final String EXERCISE = "exercise";
    private static final String MODULE_EXERCISE = "moduleEcercise";

    private static final int EXERCISE_START_REQUEST = 445;
    private static final int EXERCISE_EDIT_REQUEST = 4477;

    private TextView tvTitle;
    private TextView tvDescription;
    private Button btnEdit;
    private Button btnStart;
    private Button btnDelete;

    private Exercise mExercise;
    private ModuleExercise mModuleExercise;

    private OnExerciseChangeListener mListener;

    private ProgressDialog progressDialog;

    /**
     * The interface On exercise change listener.
     */
    public interface OnExerciseChangeListener {
        /**
         * On exercise delete.
         *
         * @param exerciseId       the exercise id
         * @param moduleExerciseId the module exercise id
         */
        void onExerciseDelete(long exerciseId, long moduleExerciseId);

        /**
         * On edit.
         */
        void onEdit();
    }

    /**
     * New instance exercise detail fragment.
     *
     * @param exercise       the exercise
     * @param moduleExercise the module exercise
     * @return the exercise detail fragment
     */
    public static ExerciseDetailFragment newInstance(Exercise exercise, ModuleExercise moduleExercise) {

        Bundle args = new Bundle();
        args.putSerializable(EXERCISE, exercise);
        args.putSerializable(MODULE_EXERCISE, moduleExercise);
        ExerciseDetailFragment fragment = new ExerciseDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_exercise_detail, container, false);

        mExercise = (Exercise) getArguments().getSerializable(EXERCISE);
        mModuleExercise = (ModuleExercise) getArguments().getSerializable(MODULE_EXERCISE);

        tvTitle = (TextView) v.findViewById(R.id.tv_title);
        tvDescription = (TextView) v.findViewById(R.id.tv_description);
        btnEdit = (Button) v.findViewById(R.id.btn_edit);
        btnStart = (Button) v.findViewById(R.id.btn_start);
        btnDelete = (Button) v.findViewById(R.id.btn_delete);

        progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Prosím čekejte...");
        progressDialog.setCancelable(false);


        tvTitle.setText(mExercise.getName());
        tvDescription.setText(mExercise.getDescription());


        setClickListeners();

        return v;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (OnExerciseChangeListener) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void setClickListeners() {
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent exerciseIntent = new Intent();
                exerciseIntent.setComponent(new ComponentName(mModuleExercise.getPackageName(),
                        mModuleExercise.getPackageNameExercise()));
                progressDialog.show();
                getImagesPaths(new ImagesLoadedCallback() {
                    @Override
                    public void onLoaded(ArrayList<String> paths) {
                        if (paths != null) {
                            for (String i : paths) {
                                Log.d("XXXX", "Path " + i);
                            }
                            exerciseIntent.putExtra("IMAGES", paths);
                            // HACK: grant read permissions to each uri
                            for (String path : paths) {
                                getActivity().grantUriPermission(mModuleExercise.getPackageName(), Uri.parse(path), Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            }
                            exerciseIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        }

                        Task task = new Task(new OnLoadedListener() {
                            @Override
                            public void onLoaded(String file) {
                                exerciseIntent.putExtra("EXERCISE_FILE", file);
                                Log.d("XXXX", "File " + file);
                                progressDialog.dismiss();
                                try {
                                    startActivityForResult(exerciseIntent, 4554);
                                } catch (ActivityNotFoundException ex) {
                                    Toast.makeText(getContext(), "Cviceni nenalezeno", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                        task.execute(mExercise.getFile());
                    }
                });

            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteExercise();
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent exerciseIntent = new Intent();
                exerciseIntent.setComponent(new ComponentName(mModuleExercise.getPackageName(),
                        mModuleExercise.getPackageNameEditor()));

                progressDialog.show();
                getImagesPaths(new ImagesLoadedCallback() {
                    @Override
                    public void onLoaded(ArrayList<String> paths) {

                        if (paths != null) {
                            exerciseIntent.putExtra("IMAGES", paths);
                            // HACK: grant read permissions to each uri
                            for (String path : paths) {
                                getActivity().grantUriPermission(mModuleExercise.getPackageName(), Uri.parse(path), Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            }
                            exerciseIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        }

                        Task task = new Task(new OnLoadedListener() {
                            @Override
                            public void onLoaded(String file) {
                                exerciseIntent.putExtra("EXERCISE_FILE", file);
                                Log.d("XXXX", "File " + file);
                                progressDialog.dismiss();
                                try {
                                    startActivityForResult(exerciseIntent, EXERCISE_EDIT_REQUEST);
                                } catch (ActivityNotFoundException ex) {
                                    Toast.makeText(getContext(), "Cviceni nenalezeno", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                        task.execute(mExercise.getFile());
                    }
                });

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("XXXXX", "Received " + requestCode + " " + resultCode);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == EXERCISE_EDIT_REQUEST) {
                String file = data.getStringExtra("EXERCISE_FILE");
                ArrayList<String> paths = (ArrayList<String>)data.getSerializableExtra("IMAGES");
                showCreateExerciseDialog(file, paths, mExercise.getName(), mExercise.getDescription());
            }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void deleteExercise() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Opravdu chcete toto cvičení smazat?");
        builder.setPositiveButton("ANO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (mListener != null) {
                    mListener.onExerciseDelete(mExercise.getId(), mExercise.getModuleExerciseId());
                }
            }
        });
        builder.setNegativeButton("NE", null);
        builder.create().show();
    }


    /**
     * The interface Images loaded callback.
     */
    interface ImagesLoadedCallback {
        /**
         * On loaded.
         *
         * @param paths the paths
         */
        void onLoaded(ArrayList<String> paths);
    }

    int size;

    Set<Target> targets;

    private void getImagesPaths(final ImagesLoadedCallback cb) {
        if (mExercise.getImages() == null || mExercise.getImages().isEmpty()) {
            cb.onLoaded(null);
        }



        final ArrayList<String> paths = new ArrayList<>();
        size = mExercise.getImages().size();

        Log.d("XXXX", "images loading " + size);

        targets = new HashSet<>();



        for (final Exercise.Image image : mExercise.getImages()) {

            Target t = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    Log.d("XXXXX", "loaded " + mExercise.getImages());
                    File imageFile = new File(getContext().getFilesDir(), "exercise_images/" + image.originalName);

                    // HACK: ensure existing directory
                    new File(getContext().getFilesDir(), "exercise_images").mkdirs();

                    FileOutputStream fos = null;
                    try {
                        fos = new FileOutputStream(imageFile);

                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    imageFile.setReadable(true,false);
                    Uri imageUri = FileProvider.getUriForFile(getContext(), "cz.cvut.fit.filipon1.dracek_admin.FileProvider", imageFile);
                    paths.add(imageUri.toString());

                    synchronized (getContext()) {
                        size--;
                        if (size <= 0) {
                            cb.onLoaded(paths);
                        }
                        Log.d("XXXX", "loaded " + size);
                    }
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                    Log.d("XXXXX", "fail ");
                    synchronized (getContext()) {
                        size--;
                        if (size <= 0) {
                            cb.onLoaded(paths);
                        }
                    }
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                    Log.d("XXXXX", "preparing");
                }
            };
            targets.add(t);
            Log.d("XXXXX", "start " + mExercise.getImages());
            Picasso.with(getContext()).load(ApiConstants.BASE_URL + image.image).into(t);
        }
    }

    private interface OnLoadedListener {
        /**
         * On loaded.
         *
         * @param file the file
         */
        void onLoaded(String file);
    }

    private class Task extends AsyncTask<String, Void, String> {

        /**
         * The M listener.
         */
        OnLoadedListener mListener;

        /**
         * Instantiates a new Task.
         *
         * @param mListener the m listener
         */
        public Task(OnLoadedListener mListener) {
            this.mListener = mListener;
        }

        @Override
        protected String doInBackground(String... params) {
            String file = "";
            try {
                // Create a URL for the desired page
                URL url = new URL(ApiConstants.BASE_URL + params[0]);

                // Read all the text returned by the server
                BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
                String str;

                while ((str = in.readLine()) != null) {
                    file += str;
                }
                in.close();
            } catch (MalformedURLException e) {
            } catch (IOException e) {
            }
            return file;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            mListener.onLoaded(s);
        }
    }

    private void showCreateExerciseDialog(final String file, final List<String> paths, String nameStr,
                                          String descriptionStr) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View v = LayoutInflater.from(getContext()).inflate(R.layout.dialog_create_exercise, null);
        final EditText name = (EditText) v.findViewById(R.id.et_name);
        final EditText description = (EditText) v.findViewById(R.id.et_description);
        View btnSave = v.findViewById(R.id.btn_save);
        View btnCancel = v.findViewById(R.id.btn_cancel);

        name.setText(nameStr);
        description.setText(descriptionStr);

        builder.setView(v);

        final AlertDialog d = builder.create();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(name.getText().toString())) {
                    name.setError("Vyplňte jméno");
                    return;
                }

                if (TextUtils.isEmpty(description.getText().toString())) {
                    description.setError("Vyplňte popis");
                    return;
                }

                uploadCreatedExercise(name.getText().toString(),
                        description.getText().toString(), file, paths);
                d.dismiss();
            }
        });



        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });

        d.show();
    }

    private void uploadCreatedExercise(final String name, final String description, final String file, final List<String> paths) {
        HashMap<String, Object> body = new HashMap<>();
        if (paths != null) {
            List<Exercise.Image> images = new ArrayList<>();

            for (String path : paths) {

                File f = new File(path);
                try {
                    FileInputStream fis = new FileInputStream(f);
                    Bitmap bmp = BitmapFactory.decodeStream(fis);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
                    String originalName = path.substring(path.lastIndexOf("/") + 1);
                    images.add(new Exercise.Image(encoded, originalName));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
            body.put("images", images);
        }

        body.put("name", name);
        body.put("file", file);
        body.put("description", description);

        RestClient client = ServiceGenerator.createServiceUserToken(RestClient.class, LoginManager.getToken());

        client.editExercise(mModuleExercise.getId(), mExercise.getId(), body, new Callback<List<Exercise>>() {
            @Override
            public void success(List<Exercise> exercises, Response response) {
                if (mListener != null) {
                    mListener.onEdit();
                }
                Toast.makeText(getContext(), "Cvičení upraveno", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(RetrofitError error) {
                if (error.getResponse().getStatus() == 403) {
                    Toast.makeText(getContext(), "Cvičení s tímto jménem již existuje", Toast.LENGTH_SHORT).show();
                    showCreateExerciseDialog(file, paths, name, description);
                }

                if (error.getResponse().getStatus() == 201) {
                    if (mListener != null) {
                        mListener.onEdit();
                    }
                    Toast.makeText(getContext(), "Cvičení upraveno", Toast.LENGTH_SHORT).show();
                }
                ErrorHandler.handle(error, getActivity());
            }
        });
    }
}
