package cz.cvut.fit.filipon1.dracek_admin.module_exercise;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.DataCache;
import cz.cvut.fit.filipon1.dracek_admin.LoginManager;
import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.klass.ClassSimpleArrayAdapter;
import cz.cvut.fit.filipon1.dracek_admin.model.Klass;
import cz.cvut.fit.filipon1.dracek_admin.model.User;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.StudentArrayAdapter;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ErrorHandler;
import cz.cvut.fit.filipon1.dracek_admin.webservice.RestClient;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ServiceGenerator;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ondra on 26.3.16.
 */
public class SelectClassDialog extends AlertDialog {

    /**
     * Instantiates a new Select class dialog.
     *
     * @param context the context
     */
    public SelectClassDialog(Context context) {
        super(context);
    }

    private ListView mListView;
    private ClassSimpleArrayAdapter mAdapter;

    private OnClassSelectedListener mListener;

    /**
     * The interface On class selected listener.
     */
    public interface OnClassSelectedListener {
        /**
         * On class select.
         *
         * @param userId the user id
         */
        void onClassSelect(long userId);
    }

    /**
     * Sets listener.
     *
     * @param listener the listener
     */
    public void setListener(OnClassSelectedListener listener) {
        mListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_select_class);

        mListView = (ListView) findViewById(R.id.lv_classes);


        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Klass klass = mAdapter.getItem(position);
                if (mListener != null) {
                    mListener.onClassSelect(klass.getId());
                }
                dismiss();
            }
        });

        findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        loadStudents();


    }

    private void loadStudents() {

        DataCache.getInstance().getClassList(new DataCache.KlassCallback() {
            @Override
            public void onSuccess(List<Klass> classes) {
                mAdapter = new ClassSimpleArrayAdapter(getContext(), classes);
                mListView.setAdapter(mAdapter);
            }

            @Override
            public void onFail(RetrofitError error) {
                ErrorHandler.handle(error, getContext());
            }
        });
    }

}
