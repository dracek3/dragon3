package cz.cvut.fit.filipon1.dracek_admin.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ondra on 2. 3. 2016.
 */
public class Task implements Serializable {

    private long id;

    @SerializedName("idExcercise")
    private long exerciseId;

    @SerializedName("idModuleExcercise")
    private long moduleExerciseId;

    @SerializedName("idUser")
    private long userId;

    private long endingDate;

    private int minimalScore;

    private boolean active;

    private boolean loaded;

    private Score score;

    private User user;

    private long createdAt;

    private String title;


    /**
     * Gets fromatted name.
     *
     * @return the fromatted name
     */
    public String getFromattedName() {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        String dateStr = format.format(new Date(createdAt));
        return title + " - " + dateStr;
    }

    /**
     * Sets title.
     *
     * @param title the title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets title.
     *
     * @return the title
     */
    public String getTitle() {

        return title;
    }

    /**
     * Sets user.
     *
     * @param user the user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Gets user.
     *
     * @return the user
     */
    public User getUser() {

        return user;
    }

    /**
     * Sets created at.
     *
     * @param createdAt the created at
     */
    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Gets created at.
     *
     * @return the created at
     */
    public long getCreatedAt() {

        return createdAt;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Sets exercise id.
     *
     * @param exerciseId the exercise id
     */
    public void setExerciseId(long exerciseId) {
        this.exerciseId = exerciseId;
    }

    /**
     * Sets module exercise id.
     *
     * @param moduleExerciseId the module exercise id
     */
    public void setModuleExerciseId(long moduleExerciseId) {
        this.moduleExerciseId = moduleExerciseId;
    }

    /**
     * Sets user id.
     *
     * @param userId the user id
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    /**
     * Sets ending date.
     *
     * @param endingDate the ending date
     */
    public void setEndingDate(long endingDate) {
        this.endingDate = endingDate;
    }

    /**
     * Sets minimal score.
     *
     * @param minimalScore the minimal score
     */
    public void setMinimalScore(int minimalScore) {
        this.minimalScore = minimalScore;
    }

    /**
     * Sets active.
     *
     * @param active the active
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Sets loaded.
     *
     * @param loaded the loaded
     */
    public void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }

    /**
     * Sets score.
     *
     * @param score the score
     */
    public void setScore(Score score) {
        this.score = score;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Gets exercise id.
     *
     * @return the exercise id
     */
    public long getExerciseId() {
        return exerciseId;
    }

    /**
     * Gets module exercise id.
     *
     * @return the module exercise id
     */
    public long getModuleExerciseId() {
        return moduleExerciseId;
    }

    /**
     * Gets user id.
     *
     * @return the user id
     */
    public long getUserId() {
        return userId;
    }

    /**
     * Gets ending date.
     *
     * @return the ending date
     */
    public long getEndingDate() {
        return endingDate;
    }

    /**
     * Gets minimal score.
     *
     * @return the minimal score
     */
    public int getMinimalScore() {
        return minimalScore;
    }

    /**
     * Is active boolean.
     *
     * @return the boolean
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Is loaded boolean.
     *
     * @return the boolean
     */
    public boolean isLoaded() {
        return loaded;
    }

    /**
     * Gets score.
     *
     * @return the score
     */
    public Score getScore() {
        return score;
    }
}
