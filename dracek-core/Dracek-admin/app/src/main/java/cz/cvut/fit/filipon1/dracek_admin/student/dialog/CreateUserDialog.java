package cz.cvut.fit.filipon1.dracek_admin.student.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import cz.cvut.fit.filipon1.dracek_admin.LoginManager;
import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.User;
import cz.cvut.fit.filipon1.dracek_admin.utils.HashSHA256;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ApiConstants;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ErrorHandler;
import cz.cvut.fit.filipon1.dracek_admin.webservice.RestClient;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ServiceGenerator;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ondra on 23.2.16.
 */
public class CreateUserDialog extends DialogFragment {


    private OnUserCreateListener mListener;

    /**
     * The interface On user create listener.
     */
    public interface OnUserCreateListener {
        /**
         * On create.
         *
         * @param newUser the new user
         */
        void onCreate(User newUser);
    }

    /**
     * Sets listener.
     *
     * @param listener the listener
     */
    public void setListener(OnUserCreateListener listener) {
        mListener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View dialogView = LayoutInflater.from(getContext())
                .inflate(R.layout.dialog_create_user, null);
        final EditText etName = (EditText) dialogView.findViewById(R.id.et_name);
        final EditText etSurname = (EditText) dialogView.findViewById(R.id.et_surname);
        final EditText etPassword = (EditText) dialogView.findViewById(R.id.et_password);


        builder.setView(dialogView);
        builder.setPositiveButton("Uložit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String name = etName.getText().toString();
                String surname = etSurname.getText().toString();
                String password = etPassword.getText().toString();

                if (TextUtils.isEmpty(name) || TextUtils.isEmpty(surname)
                        || TextUtils.isEmpty(password)) {
                    Toast.makeText(getContext(), "Vyplňte všechny údaje", Toast.LENGTH_SHORT).show();
                } else {
                    uploadUser(name, surname, password);
                }

            }
        });
        builder.setNegativeButton("Zrušit", null);
        return builder.create();
    }

    private void uploadUser(String name , String surname, String password) {
        RestClient client = ServiceGenerator.createServiceUserToken(RestClient.class, LoginManager.getToken());
        HashMap<String, Object> body = new HashMap<>();
        body.put(ApiConstants.USER_NAME, name);
        body.put(ApiConstants.USER_SURNAME, surname);
        try {
            body.put(ApiConstants.USER_PASSWORD, HashSHA256.hash(password));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        client.createStudent(body, new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                if (mListener != null) {
                    mListener.onCreate(user);
                }

                if (getActivity() != null) {
                    Toast.makeText(getActivity(), "Student vytvořen", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                ErrorHandler.handle(error, getContext());
            }
        });
    }

}
