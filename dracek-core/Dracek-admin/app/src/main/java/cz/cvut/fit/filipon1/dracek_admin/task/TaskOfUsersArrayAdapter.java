package cz.cvut.fit.filipon1.dracek_admin.task;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.Task;
import cz.cvut.fit.filipon1.dracek_admin.utils.TextFormatter;

/**
 * Created by ondra on 27. 3. 2016.
 */
public class TaskOfUsersArrayAdapter extends ArrayAdapter<Task> {

    /**
     * Instantiates a new Task of users array adapter.
     *
     * @param context the context
     * @param objects the objects
     */
    public TaskOfUsersArrayAdapter(Context context, List<Task> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.list_item_task_of_class, parent, false);
        }

        Task task = getItem(position);



        ((TextView) convertView.findViewById(R.id.tv_user_name)).setText(task.getUser().getFullNameWithUsername());

        TextView createDate = (TextView) convertView.findViewById(R.id.tv_created_date);
        TextView createTime = (TextView) convertView.findViewById(R.id.tv_created_time);
        TextView score = (TextView) convertView.findViewById(R.id.tv_score);
        TextView totalTime = (TextView) convertView.findViewById(R.id.tv_total_time);

        if (task.getScore() == null) {
            createDate.setText("-");
            createTime.setText("-");
            score.setText("-");
            totalTime.setText("-");
        } else {
            createDate.setText(TextFormatter.formatDate(task.getScore().getCreatedAt()));
            createTime.setText(TextFormatter.formatTime(task.getScore().getCreatedAt()));
            score.setText(String.valueOf(task.getScore().getPercentage() + "%"));
            totalTime.setText(TextFormatter.formatDuration(task.getScore().getTotalTime()));
        }

        return convertView;
    }
}
