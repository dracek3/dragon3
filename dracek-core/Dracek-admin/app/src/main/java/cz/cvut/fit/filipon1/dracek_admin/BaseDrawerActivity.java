package cz.cvut.fit.filipon1.dracek_admin;

import android.app.Activity;


import android.content.Intent;
import android.content.res.Configuration;

import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


import java.util.Map;

import cz.cvut.fit.filipon1.dracek_admin.exercise.ExerciseActivity;
import cz.cvut.fit.filipon1.dracek_admin.klass.ClassActivity;
import cz.cvut.fit.filipon1.dracek_admin.model.Score;
import cz.cvut.fit.filipon1.dracek_admin.module_exercise.ModuleExerciseActivity;
import cz.cvut.fit.filipon1.dracek_admin.score.ScoreActivity;
import cz.cvut.fit.filipon1.dracek_admin.student.ui.StudentActivity;
import cz.cvut.fit.filipon1.dracek_admin.task.TaskActivity;

/**
 * The type Base drawer activity.
 */
public class BaseDrawerActivity extends AppCompatActivity {

    private static final String[] sDrawerContent =
                {"Studenti", "Třídy", "Cvičení", "Zadání", "Hodnocení", "Úkoly"};

    /**
     * The M drawer layout.
     */
    public DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private Toolbar mToolbar;
    private ListView mListView;

    /**
     * Create drawer.
     */
    protected void createDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mListView = (ListView) findViewById(R.id.lv_drawer);

        setSupportActionBar(mToolbar);

        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                mToolbar,
                0,
                0) {

            public void onDrawerClosed(View view)
            {
            }

            public void onDrawerOpened(View drawerView)
            {
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,
                                android.R.layout.simple_list_item_1, sDrawerContent);
        mListView.setAdapter(arrayAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i;
                switch (position) {
                    case 0:
                        i = new Intent(BaseDrawerActivity.this, StudentActivity.class);
                        break;
                    case 1:
                        i = new Intent(BaseDrawerActivity.this, ClassActivity.class);
                        break;
                    case 2:
                        i = new Intent(BaseDrawerActivity.this, ModuleExerciseActivity.class);
                        break;
                    case 3:
                        i = new Intent(BaseDrawerActivity.this, ExerciseActivity.class);
                        break;
                    case 4:
                        i = new Intent(BaseDrawerActivity.this, ScoreActivity.class);
                        break;
                    case 5:
                        i = new Intent(BaseDrawerActivity.this, TaskActivity.class);
                        break;
                    default:
                        return;
                }
                startActivity(i);
                overridePendingTransition(0, 0);
                finish();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
}
