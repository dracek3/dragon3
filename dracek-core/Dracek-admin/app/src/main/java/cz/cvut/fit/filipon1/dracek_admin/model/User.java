package cz.cvut.fit.filipon1.dracek_admin.model;

import android.text.TextUtils;

import java.io.Serializable;

/**
 * Created by ondra on 21.2.16.
 */
public class User  implements Serializable, Comparable<User> {

    /**
     * The constant ROLE_TEACHER.
     */
    public static final String ROLE_TEACHER = "teacher";
    /**
     * The constant ROLE_STUDENT.
     */
    public static final String ROLE_STUDENT = "student";


    private long id;
    private String name;
    private String surname;
    private String username;
    private String role;


    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets surname.
     *
     * @param surname the surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * Sets username.
     *
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Sets role.
     *
     * @param role the role
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets surname.
     *
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Gets role.
     *
     * @return the role
     */
    public String getRole() {
        return role;
    }

    /**
     * Gets full name.
     *
     * @return the full name
     */
    public String getFullName() {
        String fullName = name;

        if (!TextUtils.isEmpty(fullName)) {
            fullName += " ";
        }

        fullName += surname;

        return fullName;
    }

    @Override
    public int compareTo(User another) {
        String name1 = surname + " " + name;
        String name2 = another.surname + " " + another.name;
        return name1.compareTo(name2);
    }

    /**
     * Gets username formatted.
     *
     * @return the username formatted
     */
    public String getUsernameFormatted() {
        return "(" + username + ")";
    }

    /**
     * Gets full name with username.
     *
     * @return the full name with username
     */
    public String getFullNameWithUsername() {
        return getFullName() + " (" + username + ")";
    }
}
