package cz.cvut.fit.filipon1.dracek_admin.student.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.Score;
import cz.cvut.fit.filipon1.dracek_admin.utils.TextFormatter;

/**
 * Created by ondra on 23.2.16.
 */
public class ScoreOfUserAdapter extends ArrayAdapter<Score> {


    /**
     * Instantiates a new Score of user adapter.
     *
     * @param context the context
     * @param objects the objects
     */
    public ScoreOfUserAdapter(Context context, List<Score> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.list_item_score_of_user, parent, false);
        }

        Score score = getItem(position);



        ((TextView) convertView.findViewById(R.id.tv_created_date))
                .setText(TextFormatter.formatDate(score.getCreatedAt()));
        ((TextView) convertView.findViewById(R.id.tv_created_time))
                .setText(TextFormatter.formatTime(score.getCreatedAt()));
        ((TextView) convertView.findViewById(R.id.tv_score))
                .setText(String.valueOf(score.getPercentage()));
        ((TextView) convertView.findViewById(R.id.tv_total_time))
                .setText(TextFormatter.formatDuration(score.getTotalTime()));
        ((TextView) convertView.findViewById(R.id.tv_finished))
                .setText(score.isFinished() ? "Ano" : "Ne");


        return convertView;
    }
}
