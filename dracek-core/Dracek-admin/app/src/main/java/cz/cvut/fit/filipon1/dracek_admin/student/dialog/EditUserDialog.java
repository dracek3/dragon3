package cz.cvut.fit.filipon1.dracek_admin.student.dialog;

import android.app.AlertDialog;
import android.app.Dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;

import cz.cvut.fit.filipon1.dracek_admin.LoginManager;
import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.User;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ApiConstants;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ErrorHandler;
import cz.cvut.fit.filipon1.dracek_admin.webservice.RestClient;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ServiceGenerator;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ondra on 23.2.16.
 */
public class EditUserDialog  extends DialogFragment {

    private static final String USER = "user";

    private User mUser;

    private OnUserEditListener mListener;

    /**
     * The interface On user edit listener.
     */
    public interface OnUserEditListener {
        /**
         * On edit.
         *
         * @param oldUser the old user
         * @param newUser the new user
         */
        void onEdit(User oldUser, User newUser);
    }

    /**
     * Sets listener.
     *
     * @param listener the listener
     */
    public void setListener(OnUserEditListener listener) {
        mListener = listener;
    }

    /**
     * New instance edit user dialog.
     *
     * @param user the user
     * @return the edit user dialog
     */
    public static EditUserDialog newInstance(User user) {

        Bundle args = new Bundle();
        args.putSerializable(USER, user);
        EditUserDialog fragment = new EditUserDialog();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mUser = (User) getArguments().getSerializable(USER);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View dialogView = LayoutInflater.from(getContext())
                .inflate(R.layout.dialog_edit_user, null);
        final EditText etName = (EditText) dialogView.findViewById(R.id.et_name);
        final EditText etSurname = (EditText) dialogView.findViewById(R.id.et_surname);

        etName.setText(mUser.getName());
        etSurname.setText(mUser.getSurname());

        builder.setView(dialogView);
        builder.setPositiveButton("Uložit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String name = etName.getText().toString();
                String surname = etSurname.getText().toString();

                if (name.equals(mUser.getName()) && surname.equals(mUser.getSurname())) {
                    Toast.makeText(getContext(), "Nedošlo k žádné úpravě", Toast.LENGTH_SHORT).show();
                    return;
                }
                uploadEditedUser(name, surname);
            }
        });
        builder.setNegativeButton("Zrušit", null);
        return builder.create();
    }


    /**
     * Uploads changed data to server
     * @param name new name of user
     * @param surname new surname of user
     */
    private void uploadEditedUser(String name, String surname) {
        RestClient client = ServiceGenerator.createServiceUserToken(RestClient.class, LoginManager.getToken());
        HashMap<String, Object> body = new HashMap<>();
        body.put(ApiConstants.USER_NAME, name);
        body.put(ApiConstants.USER_SURNAME, surname);
        client.editStudent(mUser.getId(), body, new Callback<User>() {
            @Override
            public void success(User user, Response response) {
               if (mListener != null) {
                   mListener.onEdit(mUser, user);
               }
            }

            @Override
            public void failure(RetrofitError error) {
                ErrorHandler.handle(error, getContext());
            }
        });
    }

}
