package cz.cvut.fit.filipon1.dracek_admin.student.dialog;

import android.app.AlertDialog;
import android.app.Dialog;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.DataCache;
import cz.cvut.fit.filipon1.dracek_admin.LoginManager;
import cz.cvut.fit.filipon1.dracek_admin.model.ModuleExercise;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.ModuleExerciseAdapter;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ErrorHandler;
import cz.cvut.fit.filipon1.dracek_admin.webservice.RestClient;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ServiceGenerator;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ondra on 23.2.16.
 */
public class ModuleExerciseListDialog extends DialogFragment {

    private ListView mListView;
    private ModuleExerciseAdapter mAdapter;

    private OnModuleExerciseSelectedListener mListener;

    /**
     * The interface On module exercise selected listener.
     */
    public interface OnModuleExerciseSelectedListener {
        /**
         * On selected.
         *
         * @param moduleExercise the module exercise
         */
        void onSelected(ModuleExercise moduleExercise);
    }

    /**
     * Sets listener.
     *
     * @param listener the listener
     */
    public void setListener(OnModuleExerciseSelectedListener listener) {
        mListener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        mListView = new ListView(getContext());
        builder.setView(mListView);
        builder.setNegativeButton("Zrušit", null);

        loadModuleExercise();

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mListener != null) {
                    ModuleExercise moduleExercise = mAdapter.getItem(position);
                    mListener.onSelected(moduleExercise);
                }
                dismiss();
            }
        });

        return builder.create();
    }

    private void loadModuleExercise() {
        DataCache.getInstance().getModuleExercise(new DataCache.ModuleExerciseCallback() {
            @Override
            public void onSuccess(List<ModuleExercise> moduleExercises) {
                mAdapter = new ModuleExerciseAdapter(getContext(), moduleExercises);
                mListView.setAdapter(mAdapter);
            }

            @Override
            public void onFail(RetrofitError error) {
                ErrorHandler.handle(error, getContext());
            }
        });
    }
}
