package cz.cvut.fit.filipon1.dracek_admin.klass;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.LoginManager;
import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.Klass;
import cz.cvut.fit.filipon1.dracek_admin.model.User;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.StudentArrayAdapter;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ErrorHandler;
import cz.cvut.fit.filipon1.dracek_admin.webservice.RestClient;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ServiceGenerator;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ondra on 25.3.16.
 */
public class AddStudentToClassDialog extends AlertDialog {

    private TextView tvClassName;
    private Klass mClass;
    private ListView mListView;
    private StudentArrayAdapter mAdapter;

    private OnUserSelectedListener mListener;

    /**
     * The interface On user selected listener.
     */
    public interface OnUserSelectedListener {
        /**
         * On user select.
         *
         * @param userId  the user id
         * @param classId the class id
         */
        void onUserSelect(long userId, long classId);
    }

    /**
     * Instantiates a new Add student to class dialog.
     *
     * @param context the context
     * @param klass   the klass
     */
    protected AddStudentToClassDialog(Context context, Klass klass) {
        super(context);
        mClass = klass;
    }

    /**
     * Sets listener.
     *
     * @param listener the listener
     */
    public void setListener(OnUserSelectedListener listener) {
        mListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_add_student_to_class);

        tvClassName = (TextView) findViewById(R.id.tv_class_name);
        mListView = (ListView) findViewById(R.id.lv_students);


        tvClassName.setText(mClass.getName());


        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User user = mAdapter.getItem(position);
                if (mListener != null) {
                    mListener.onUserSelect(user.getId(), mClass.getId());
                }
                dismiss();
            }
        });

        findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        loadStudents();


    }

    private void loadStudents() {

        RestClient client = ServiceGenerator.createServiceUserToken(RestClient.class, LoginManager.getToken());
        client.getAllStudentList(new Callback<List<User>>() {
            @Override
            public void success(List<User> users, Response response) {
                mAdapter = new StudentArrayAdapter(getContext(), users);
                mListView.setAdapter(mAdapter);
            }

            @Override
            public void failure(RetrofitError error) {
                ErrorHandler.handle(error, getContext());
            }
        });

    }


}
