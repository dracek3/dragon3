package cz.cvut.fit.filipon1.dracek_admin.score;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.DataCache;
import cz.cvut.fit.filipon1.dracek_admin.LoginManager;
import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.klass.ClassSimpleArrayAdapter;
import cz.cvut.fit.filipon1.dracek_admin.model.Exercise;
import cz.cvut.fit.filipon1.dracek_admin.model.Klass;
import cz.cvut.fit.filipon1.dracek_admin.model.ModuleExercise;
import cz.cvut.fit.filipon1.dracek_admin.model.Score;
import cz.cvut.fit.filipon1.dracek_admin.model.User;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.ExerciseAdapter;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.ModuleExerciseAdapter;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.ScoreOfUserAdapter;
import cz.cvut.fit.filipon1.dracek_admin.student.dialog.NumberPickerDialog;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ErrorHandler;
import cz.cvut.fit.filipon1.dracek_admin.webservice.RestClient;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ServiceGenerator;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ondra on 26.3.16.
 */
public class ScoreDetailFragment extends Fragment {

    private static final String EXERCISE = "exercise";

    private ListView mScoreListView;
    private TextView tvDateFrom;
    private TextView tvDateTo;
    private TextView tvScoreFrom;
    private TextView tvScoreTo;
    private View btnDateFromReset;
    private View btnDateToReset;
    private View btnFilter;
    private View tbFilterShow;
    private LinearLayout mFilterContainer;

    private Spinner spnClasses;
    private ClassSimpleArrayAdapter mClassesAdapter;
    private ScoreArrayAdapter mScoreAdapter;

    private boolean mIsFilterShown;

    private Calendar mDateFrom;
    private Calendar mDateTo;
    private int mScoreFrom = 0;
    private int mScoreTo = 100;

    private Exercise mExercise;

    /**
     * New instance score detail fragment.
     *
     * @param exercise the exercise
     * @return the score detail fragment
     */
    public static ScoreDetailFragment newInstance(Exercise exercise) {

        Bundle args = new Bundle();
        args.putSerializable(EXERCISE, exercise);
        ScoreDetailFragment fragment = new ScoreDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_score_detail, container, false);

        mExercise = (Exercise) getArguments().getSerializable(EXERCISE);

        mScoreListView = (ListView) v.findViewById(R.id.lv_score);
        tvDateFrom = (TextView) v.findViewById(R.id.tv_date_from);
        tvDateTo = (TextView) v.findViewById(R.id.tv_date_to);
        tvScoreFrom = (TextView) v.findViewById(R.id.tv_score_from);
        tvScoreTo = (TextView) v.findViewById(R.id.tv_score_to);
        btnDateFromReset = v.findViewById(R.id.iv_date_from_reset);
        btnDateToReset = v.findViewById(R.id.iv_date_to_reset);
        btnFilter = v.findViewById(R.id.btn_filter);
        tbFilterShow = v.findViewById(R.id.tb_show_filter);
        mFilterContainer = (LinearLayout) v.findViewById(R.id.ll_filter_container);
        spnClasses = (Spinner) v.findViewById(R.id.spn_class);

        mIsFilterShown = false;

        setListeners();

        loadClasses();

        return v;
    }

    private void loadClasses() {
        DataCache.getInstance().getClassList(new DataCache.KlassCallback() {
            @Override
            public void onSuccess(List<Klass> c) {
                List<Klass> classes = new ArrayList<Klass>(c);
                Klass klass = new Klass();
                klass.setId(0);
                klass.setName("Všechny třídy");
                classes.add(0, klass);

                mClassesAdapter = new ClassSimpleArrayAdapter(getContext(), classes);
                spnClasses.setAdapter(mClassesAdapter);
                loadScore();
            }

            @Override
            public void onFail(RetrofitError error) {

            }
        });
    }

    private void loadScore() {
        Klass klass = mClassesAdapter.getItem(spnClasses.getSelectedItemPosition());
        if (klass.getId() == 0) {
            RestClient client = ServiceGenerator
                    .createServiceUserToken(RestClient.class, LoginManager.getToken());
            client.getScore(mExercise.getModuleExerciseId(), mExercise.getId(),
                                                        new Callback<List<Score>>() {
                @Override
                public void success(List<Score> scores, Response response) {
                    if (getContext() != null) {
                        mScoreAdapter = new ScoreArrayAdapter(getContext(), filterScore(scores));
                        mScoreListView.setAdapter(mScoreAdapter);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if (getContext() != null) {
                        mScoreAdapter = new ScoreArrayAdapter(getContext(), new ArrayList<Score>());
                        mScoreListView.setAdapter(mScoreAdapter);
                    }
                    ErrorHandler.handle(error, getContext());
                }
            });
        } else {
            RestClient client = ServiceGenerator
                    .createServiceUserToken(RestClient.class, LoginManager.getToken());
            client.getScoreOfClass(klass.getId(), mExercise.getModuleExerciseId(), mExercise.getId(),
                    new Callback<List<Score>>() {
                        @Override
                        public void success(List<Score> scores, Response response) {
                            if (getContext() != null) {
                                mScoreAdapter = new ScoreArrayAdapter(getContext(), filterScore(scores));
                                mScoreListView.setAdapter(mScoreAdapter);
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            if (getContext() != null) {
                                mScoreAdapter = new ScoreArrayAdapter(getContext(), new ArrayList<Score>());
                                mScoreListView.setAdapter(mScoreAdapter);
                            }
                            ErrorHandler.handle(error, getContext());
                        }
                    });
        }
    }



    private void setListeners() {

        tbFilterShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsFilterShown) {
                    mFilterContainer.setVisibility(View.GONE);
                    mIsFilterShown = false;
                } else {
                    mFilterContainer.setVisibility(View.VISIBLE);
                    mIsFilterShown = true;
                }
            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadScore();
            }
        });

        tvDateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int day = 0, month = 0, year = 0;
                if (mDateFrom != null) {
                    day = mDateFrom.get(Calendar.DAY_OF_MONTH);
                    month = mDateFrom.get(Calendar.MONTH);
                    year = mDateFrom.get(Calendar.YEAR);
                }
                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mDateFrom = Calendar.getInstance();
                        mDateFrom.set(Calendar.YEAR, year);
                        mDateFrom.set(Calendar.MONTH, monthOfYear);
                        mDateFrom.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        displayDate(tvDateFrom, mDateFrom);
                    }
                }, year, month, day);
                dialog.getDatePicker().setMinDate(946688461000l);
                dialog.show();
            }
        });

        tvDateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int day = 0, month = 0, year = 0;
                if (mDateTo != null) {
                    day = mDateTo.get(Calendar.DAY_OF_MONTH);
                    month = mDateTo.get(Calendar.MONTH);
                    year = mDateTo.get(Calendar.YEAR);
                }
                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mDateTo = Calendar.getInstance();
                        mDateTo.set(Calendar.YEAR, year);
                        mDateTo.set(Calendar.MONTH, monthOfYear);
                        mDateTo.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        displayDate(tvDateTo, mDateTo);
                    }
                }, year, month, day);
                dialog.getDatePicker().setMinDate(946688461000l);
                dialog.show();
            }
        });

        tvScoreFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPickerDialog dialog = NumberPickerDialog.newInstance(mScoreFrom);
                dialog.setListener(new NumberPickerDialog.OnNumberSelectedListener() {
                    @Override
                    public void onSelected(int number) {
                        mScoreFrom = number;
                        tvScoreFrom.setText(String.valueOf(mScoreFrom));
                    }
                });
                dialog.show(getChildFragmentManager(), "E");
            }
        });

        tvScoreTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPickerDialog dialog = NumberPickerDialog.newInstance(mScoreTo);
                dialog.setListener(new NumberPickerDialog.OnNumberSelectedListener() {
                    @Override
                    public void onSelected(int number) {
                        mScoreTo = number;
                        tvScoreTo.setText(String.valueOf(mScoreTo));
                    }
                });
                dialog.show(getChildFragmentManager(), "F");
            }
        });

        btnDateFromReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDateFrom = null;
                displayDate(tvDateFrom, null);
            }
        });

        btnDateToReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDateTo = null;
                displayDate(tvDateTo, null);
            }
        });

    }


    private void displayDate(TextView tv, Calendar date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

        if (date == null) {
            tv.setText("--.--.----");
        } else {
            tv.setText(dateFormat.format(date.getTime()));
        }
    }


    private List<Score> filterScore(List<Score> scores) {
        List<Score> filteredScore = new ArrayList<>();

        for (Score s : scores) {

            if (mDateFrom != null) {
                if (s.getCreatedAt() < mDateFrom.getTime().getTime()) {
                    continue;
                }
            }

            if (mDateTo != null) {
                if (s.getCreatedAt() > mDateTo.getTime().getTime()) {
                    continue;
                }
            }


            if (s.getPercentage() > mScoreTo) {
                continue;
            }

            if (s.getPercentage() < mScoreFrom) {
                continue;
            }

            filteredScore.add(s);
        }

        return filteredScore;
    }


}
