package cz.cvut.fit.filipon1.dracek_admin.webservice;

import com.squareup.okhttp.OkHttpClient;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by ondra on 25.7.2015.
 */
public class ServiceGenerator {

    /**
     * Instantiates a new Service generator.
     */
    public ServiceGenerator() {
    }

    /**
     * Create service s.
     *
     * @param <S>          the type parameter
     * @param serviceClass the service class
     * @return the s
     */
    public static <S> S createService(Class<S> serviceClass) {

        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(ApiConstants.API_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setClient(new OkClient(new OkHttpClient()));

        RestAdapter adapter = builder.build();
        return adapter.create(serviceClass);
    }

    /**
     * Create service app token s.
     *
     * @param <S>          the type parameter
     * @param serviceClass the service class
     * @param appToken     the app token
     * @return the s
     */
    public static <S> S createServiceAppToken(Class<S> serviceClass, final String appToken) {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                if (appToken != null) {
                    request.addHeader("API-API-KEY", appToken);
                }
            }
        };

        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(ApiConstants.API_URL)
                .setRequestInterceptor(requestInterceptor)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setClient(new OkClient(new OkHttpClient()));

        RestAdapter adapter = builder.build();
        return adapter.create(serviceClass);
    }


    /**
     * Create service user token s.
     *
     * @param <S>          the type parameter
     * @param serviceClass the service class
     * @param userToken    the user token
     * @return the s
     */
    public static <S> S createServiceUserToken(Class<S> serviceClass, final String userToken) {
        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                if (userToken != null) {
                    request.addHeader("API-USER-TOKEN", userToken);
                }
            }
        };

        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(ApiConstants.API_URL)
                .setRequestInterceptor(requestInterceptor)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setClient(new OkClient(new OkHttpClient()));

        RestAdapter adapter = builder.build();
        return adapter.create(serviceClass);
    }



}
