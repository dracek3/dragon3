package cz.cvut.fit.filipon1.dracek_admin.webservice;

/**
 * Created by ondra on 21.2.16.
 */
public class ApiConstants {

    /**
     * The constant BASE_URL.
     */
    public static final String BASE_URL = "http://185.88.73.97/dracek-nette";
    /**
     * The constant API_URL.
     */
    public static final String API_URL = "http://185.88.73.97/dracek-nette/www/api/v1/";
    /**
     * The constant USER_NAME.
     */
    public static final String USER_NAME = "name";
    /**
     * The constant USER_SURNAME.
     */
    public static final String USER_SURNAME = "surname";
    /**
     * The constant USER_USERNAME.
     */
    public static final String USER_USERNAME = "username";
    /**
     * The constant USER_PASSWORD.
     */
    public static final String USER_PASSWORD = "password";

}
