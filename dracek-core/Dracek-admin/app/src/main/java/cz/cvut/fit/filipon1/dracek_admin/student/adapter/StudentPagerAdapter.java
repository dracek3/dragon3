package cz.cvut.fit.filipon1.dracek_admin.student.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import cz.cvut.fit.filipon1.dracek_admin.model.User;
import cz.cvut.fit.filipon1.dracek_admin.student.ui.DummyFragment;
import cz.cvut.fit.filipon1.dracek_admin.student.ui.StudentDetailFragment;
import cz.cvut.fit.filipon1.dracek_admin.student.ui.StudentScoreFragment;
import cz.cvut.fit.filipon1.dracek_admin.student.ui.StudentTaskFragment;

/**
 * Created by ondra on 21.2.16.
 */
public class StudentPagerAdapter extends FragmentStatePagerAdapter {

    private static final int TAB_COUNT = 3;

    private User mUser;

    /**
     * Instantiates a new Student pager adapter.
     *
     * @param fm   the fm
     * @param user the user
     */
    public StudentPagerAdapter(FragmentManager fm, User user) {
        super(fm);
        mUser = user;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return StudentDetailFragment.newInstance(mUser);
            case 1:
                return StudentScoreFragment.newInstance(mUser);
            case 2:
                return StudentTaskFragment.newInstance(mUser);
        }

        return new DummyFragment();
    }

    @Override
    public int getCount() {
        return TAB_COUNT;
    }
}
