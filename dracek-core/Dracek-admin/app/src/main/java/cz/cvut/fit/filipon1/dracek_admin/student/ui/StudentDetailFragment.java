package cz.cvut.fit.filipon1.dracek_admin.student.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.LoginManager;
import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.ModuleExercise;
import cz.cvut.fit.filipon1.dracek_admin.model.User;
import cz.cvut.fit.filipon1.dracek_admin.student.dialog.ModuleExerciseListDialog;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.ModuleExerciseOfUserAdapter;
import cz.cvut.fit.filipon1.dracek_admin.student.dialog.ChangePasswordDialog;
import cz.cvut.fit.filipon1.dracek_admin.student.dialog.DeleteUserDialog;
import cz.cvut.fit.filipon1.dracek_admin.student.dialog.EditUserDialog;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ErrorHandler;
import cz.cvut.fit.filipon1.dracek_admin.webservice.RestClient;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ServiceGenerator;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ondra on 21.2.16.
 */
public class StudentDetailFragment extends Fragment {

    private static final String USER = "user";

    private TextView tvFullName;
    private TextView tvName;
    private TextView tvSurname;
    private TextView tvUsername;
    private ListView mListView;
    private ModuleExerciseOfUserAdapter mModuleExerciseAdapter;

    private User mUser;

    private OnUserChange mUserChangeListener;

    /**
     * The interface On user change.
     */
    public interface OnUserChange {

        /**
         * Called when user was edited and needs to be updated in user list view
         *
         * @param user new instance of updated user
         */
        void onUserEdited(User user);

        /**
         * Called when user was deleted and needs to be removed from user list view
         *
         * @param user user that has been deleted
         */
        void onUserDeleted(User user);
    }

    /**
     * New instance student detail fragment.
     *
     * @param user the user
     * @return the student detail fragment
     */
    public static StudentDetailFragment newInstance(User user) {

        Bundle args = new Bundle();
        args.putSerializable(USER, user);
        StudentDetailFragment fragment = new StudentDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_student_detail, container, false);
        mUser = (User) getArguments().getSerializable(USER);

        mListView = (ListView) v.findViewById(R.id.lv_module_exercises);

        tvFullName = (TextView) v.findViewById(R.id.tv_full_name);
        tvName = (TextView) v.findViewById(R.id.tv_name);
        tvSurname = (TextView) v.findViewById(R.id.tv_surname);
        tvUsername = (TextView) v.findViewById(R.id.tv_username);

        updateUserContentWithData();

        v.findViewById(R.id.btn_edit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditUserDialog dialog = EditUserDialog.newInstance(mUser);
                dialog.setListener(new EditUserDialog.OnUserEditListener() {
                    @Override
                    public void onEdit(User oldUser, User newUser) {
                        mUser = newUser;
                        updateUserContentWithData();
                        if (mUserChangeListener != null) {
                            mUserChangeListener.onUserEdited(mUser);
                        }
                    }
                });
                dialog.show(getChildFragmentManager(), "D");
            }
        });

        v.findViewById(R.id.btn_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeleteUserDialog dialog = new DeleteUserDialog();
                dialog.setListener(new DeleteUserDialog.OnDeleteUser() {
                    @Override
                    public void onDelete() {
                        if (mUserChangeListener != null) {
                            mUserChangeListener.onUserDeleted(mUser);
                        }
                    }
                });
                dialog.show(getChildFragmentManager(), "A");
            }
        });

        v.findViewById(R.id.btn_change_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangePasswordDialog dialog = ChangePasswordDialog.newInstance(mUser.getId());
                dialog.show(getChildFragmentManager(), "B");
            }
        });

        v.findViewById(R.id.btn_add_module_exercise).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModuleExerciseListDialog dialog = new ModuleExerciseListDialog();
                dialog.setListener(new ModuleExerciseListDialog.OnModuleExerciseSelectedListener() {
                    @Override
                    public void onSelected(final ModuleExercise moduleExercise) {
                        RestClient client = ServiceGenerator
                                .createServiceUserToken(RestClient.class, LoginManager.getToken());
                        client.addModuleExerciseToUser(mUser.getId(), moduleExercise.getId(),
                                new HashMap<String, Object>(), new Callback<List<Object>>() {
                                    @Override
                                    public void success(List<Object> objects, Response response) {
                                        mModuleExerciseAdapter.add(moduleExercise);
                                        mModuleExerciseAdapter.notifyDataSetChanged();
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {

                                    }
                                });
                    }
                });
                dialog.show(getChildFragmentManager(), "C");
            }
        });

        loadModuleExercise();

        return v;
    }

    /**
     * Gets data from actual saved user and display them
     */
    private void updateUserContentWithData() {
        tvFullName.setText(mUser.getFullName());
        tvName.setText(mUser.getName());
        tvSurname.setText(mUser.getSurname());
        tvUsername.setText(mUser.getUsername());
    }


    /**
     * Loads module exercises of user and shows them in list view
     */
    private void loadModuleExercise() {
        RestClient client = ServiceGenerator
                        .createServiceUserToken(RestClient.class, LoginManager.getToken());
        client.getModuleExercisesOfStudent(mUser.getId(), new Callback<List<ModuleExercise>>() {
            @Override
            public void success(List<ModuleExercise> moduleExercises, Response response) {
                if (getContext() != null) {
                    mModuleExerciseAdapter = new ModuleExerciseOfUserAdapter(
                            getContext(), moduleExercises, mUser.getId());
                    mListView.setAdapter(mModuleExerciseAdapter);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                ErrorHandler.handle(error, getContext());
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mUserChangeListener = (OnUserChange) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mUserChangeListener = null;
    }
}
