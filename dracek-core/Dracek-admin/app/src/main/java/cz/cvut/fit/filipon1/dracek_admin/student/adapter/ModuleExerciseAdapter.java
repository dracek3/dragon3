package cz.cvut.fit.filipon1.dracek_admin.student.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.ModuleExercise;

/**
 * Created by ondra on 23.2.16.
 */
public class ModuleExerciseAdapter extends ArrayAdapter<ModuleExercise> {

    /**
     * Instantiates a new Module exercise adapter.
     *
     * @param context the context
     * @param objects the objects
     */
    public ModuleExerciseAdapter(Context context, List<ModuleExercise> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.list_item_module_exercise, parent, false);
        }

        ModuleExercise moduleExercise = getItem(position);

        ((TextView) convertView.findViewById(R.id.tv_text)).setText(moduleExercise.getName());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.list_item_module_exercise, parent, false);
        }

        ModuleExercise moduleExercise = getItem(position);

        ((TextView) convertView.findViewById(R.id.tv_text)).setText(moduleExercise.getName());

        return convertView;
    }
}
