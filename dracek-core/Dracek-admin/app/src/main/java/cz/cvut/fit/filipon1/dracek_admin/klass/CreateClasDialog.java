package cz.cvut.fit.filipon1.dracek_admin.klass;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.Klass;

/**
 * Created by ondra on 25.3.16.
 */
public class CreateClasDialog extends AlertDialog {
    private EditText etName;

    private OnClassCreatedListener mListener;

    /**
     * The interface On class created listener.
     */
    public interface OnClassCreatedListener {
        /**
         * On class created.
         *
         * @param name the name
         */
        void onClassCreated(String name);
    }

    /**
     * Instantiates a new Create clas dialog.
     *
     * @param context the context
     */
    protected CreateClasDialog(Context context) {
        super(context);
    }

    /**
     * Sets listener.
     *
     * @param listener the listener
     */
    public void setListener(OnClassCreatedListener listener) {
        mListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_edit_class_name);

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);

        etName = (EditText) findViewById(R.id.et_name);

        findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        findViewById(R.id.btn_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = etName.getText().toString();

                if (TextUtils.isEmpty(name)) {
                    etName.setError("Vyplňte jméno");
                    return;
                }

                if (mListener != null) {
                    mListener.onClassCreated(name);
                }
                dismiss();
            }
        });
    }
}
