package cz.cvut.fit.filipon1.dracek_admin.webservice.response;

import cz.cvut.fit.filipon1.dracek_admin.model.Token;
import cz.cvut.fit.filipon1.dracek_admin.model.User;

/**
 * Created by ondra on 21.2.16.
 */
public class LoginResponse {

    private Token token;

    {token = new Token();
    token.setToken("0b9c8dc8259bb75567daf6f07df6825f");
    }

    private User user;

    /**
     * Sets token.
     *
     * @param token the token
     */
    public void setToken(Token token) {
        this.token = token;
    }

    /**
     * Sets user.
     *
     * @param user the user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Gets token.
     *
     * @return the token
     */
    public Token getToken() {
        return token;
    }

    /**
     * Gets user.
     *
     * @return the user
     */
    public User getUser() {
        return user;
    }
}
