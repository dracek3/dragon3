package cz.cvut.fit.filipon1.dracek_admin.task;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.DataCache;
import cz.cvut.fit.filipon1.dracek_admin.LoginManager;
import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.Exercise;
import cz.cvut.fit.filipon1.dracek_admin.model.ModuleExercise;
import cz.cvut.fit.filipon1.dracek_admin.model.Task;
import cz.cvut.fit.filipon1.dracek_admin.model.User;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.ExerciseAdapter;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.ModuleExerciseAdapter;
import cz.cvut.fit.filipon1.dracek_admin.student.dialog.NumberPickerDialog;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ApiConstants;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ErrorHandler;
import cz.cvut.fit.filipon1.dracek_admin.webservice.RestClient;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ServiceGenerator;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ondra on 23.2.16.
 */
public class CreateTaskDialog extends DialogFragment {


    private OnTaskCreateListener mListener;
    private ModuleExerciseAdapter mModuleExerciseAdapter;
    private ExerciseAdapter mExerciseAdapter;
    private Spinner spnModuleExercise;
    private Spinner spnExercise;
    private TextView tvScore;
    private TextView tvDate;

    /**
     * The Date format.
     */
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

    private Calendar mDate;
    private int mScore;

    /**
     * The interface On task create listener.
     */
    public interface OnTaskCreateListener {
        /**
         * On create.
         */
        void onCreate();
    }

    /**
     * Sets listener.
     *
     * @param listener the listener
     */
    public void setListener(OnTaskCreateListener listener) {
        mListener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View dialogView = LayoutInflater.from(getContext())
                .inflate(R.layout.dialog_create_task, null);
        final EditText etTitle = (EditText) dialogView.findViewById(R.id.et_title);
        tvDate = (TextView) dialogView.findViewById(R.id.tv_date);
        tvScore = (TextView) dialogView.findViewById(R.id.tv_minimal_score);
        spnModuleExercise = (Spinner) dialogView.findViewById(R.id.spn_module_exercise);
        spnExercise = (Spinner) dialogView.findViewById(R.id.spn_exercise);

        loadModuleExercises();

        mDate = Calendar.getInstance();
        mScore = 50;

        tvDate.setText(dateFormat.format(mDate.getTime()));
        tvScore.setText(String.valueOf(mScore));

        setClickListeners();

        builder.setView(dialogView);
        builder.setPositiveButton("Uložit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String title = etTitle.getText().toString();

                if (TextUtils.isEmpty(title)) {
                    Toast.makeText(getContext(), "Vyplňte všechny údaje", Toast.LENGTH_SHORT).show();
                } else {
                    uploadTask(title);
                }

            }
        });
        builder.setNegativeButton("Zrušit", null);
        return builder.create();
    }

    private void uploadTask(String title) {
        RestClient client = ServiceGenerator
                .createServiceUserToken(RestClient.class, LoginManager.getToken());
        long moduleExerciseId = mModuleExerciseAdapter.getItem(spnModuleExercise.getSelectedItemPosition()).getId();
        long exerciseId = mExerciseAdapter.getItem(spnModuleExercise.getSelectedItemPosition()).getId();
        HashMap<String, Object> body = new HashMap<>();
        body.put("title", title);
        body.put("ending_date", mDate.getTime().getTime());
        body.put("minimal_score", mScore);
        client.createTask(moduleExerciseId, exerciseId, body, new Callback<List<Object>>() {
            @Override
            public void success(List<Object> objects, Response response) {
                if (mListener != null) {
                    mListener.onCreate();
                }
                dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
//                if (getContext() != null) {
//                    Toast.makeText(getContext(), "Úkol s tímto jénem již existuje.", Toast.LENGTH_SHORT).show();
//                }
                ErrorHandler.handle(error, getContext());
            }
        });
    }

    private void setClickListeners() {
        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int day = 0, month = 0, year = 0;
                if (mDate != null) {
                    day = mDate.get(Calendar.DAY_OF_MONTH);
                    month = mDate.get(Calendar.MONTH);
                    year = mDate.get(Calendar.YEAR);
                }
                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mDate = Calendar.getInstance();
                        mDate.set(Calendar.YEAR, year);
                        mDate.set(Calendar.MONTH, monthOfYear);
                        mDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        tvDate.setText(dateFormat.format(mDate.getTime()));
                    }
                }, year, month, day);
                dialog.getDatePicker().setMinDate(946688461000l);
                dialog.show();
            }
        });

        tvScore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPickerDialog dialog = NumberPickerDialog.newInstance(mScore);
                dialog.setListener(new NumberPickerDialog.OnNumberSelectedListener() {
                    @Override
                    public void onSelected(int number) {
                        mScore = number;
                        tvScore.setText(String.valueOf(mScore));
                    }
                });
                dialog.show(getChildFragmentManager(), "E");
            }
        });


        spnModuleExercise.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mModuleExerciseAdapter != null) {
                    ModuleExercise moduleExercise = mModuleExerciseAdapter.getItem(position);
                    loadExercises(moduleExercise.getId());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void loadModuleExercises() {
        DataCache.getInstance().getModuleExercise(new DataCache.ModuleExerciseCallback() {
            @Override
            public void onSuccess(List<ModuleExercise> moduleExercises) {
                mModuleExerciseAdapter = new ModuleExerciseAdapter(getContext(), moduleExercises);
                spnModuleExercise.setAdapter(mModuleExerciseAdapter);
            }

            @Override
            public void onFail(RetrofitError error) {
                ErrorHandler.handle(error, getContext());
            }
        });
    }

    private void loadExercises(long moduleExerciseId) {
        RestClient client = ServiceGenerator
                .createServiceUserToken(RestClient.class, LoginManager.getToken());
        client.getExercsisesOfModule(moduleExerciseId, new Callback<List<Exercise>>() {
            @Override
            public void success(List<Exercise> exercises, Response response) {
                mExerciseAdapter = new ExerciseAdapter(getContext(), exercises);
                spnExercise.setAdapter(mExerciseAdapter);
            }

            @Override
            public void failure(RetrofitError error) {
                ErrorHandler.handle(error, getContext());
            }
        });
    }


}
