package cz.cvut.fit.filipon1.dracek_admin.student.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.LoginManager;
import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.ModuleExercise;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ErrorHandler;
import cz.cvut.fit.filipon1.dracek_admin.webservice.RestClient;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ServiceGenerator;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ondra on 21.2.16.
 */
public class ModuleExerciseOfUserAdapter extends ArrayAdapter<ModuleExercise>{

    private long mUserId;

    /**
     * Instantiates a new Module exercise of user adapter.
     *
     * @param context the context
     * @param objects the objects
     * @param userId  the user id
     */
    public ModuleExerciseOfUserAdapter(Context context, List<ModuleExercise> objects, long userId) {
        super(context, 0, objects);
        mUserId = userId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.list_item_module_execise_of_user, parent, false);
        }

        final ModuleExercise moduleExercise = getItem(position);

        ((TextView) convertView.findViewById(R.id.tv_name)).setText(moduleExercise.getName());

        convertView.findViewById(R.id.iv_delete_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RestClient client = ServiceGenerator
                        .createServiceUserToken(RestClient.class, LoginManager.getToken());
                client.deleteModuleExcercideFromUser(mUserId, moduleExercise.getId(),
                        new Callback<List<Object>>() {
                            @Override
                            public void success(List<Object> aVoid, Response response) {
                                remove(moduleExercise);
                                notifyDataSetChanged();
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                ErrorHandler.handle(error, getContext());
                            }
                        });
            }
        });

        return convertView;
    }
}
