package cz.cvut.fit.filipon1.dracek_admin.webservice;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import retrofit.RetrofitError;

/**
 * Created by ondra on 14.5.16.
 */
public class ErrorHandler {

    public static void handle(RetrofitError error, Activity activity) {
        if (activity == null) {
            return;
        }
        if (error.getKind().equals(RetrofitError.Kind.NETWORK)) {
            Toast.makeText(activity, "Zkontrolujte internetové připojení",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        if (error.getResponse() != null && error.getResponse().getStatus() == 401) {
            activity.finish();
        }
    }

    public static void handle(RetrofitError error, Context activity) {
        if (activity == null) {
            return;
        }
        if (error.getKind().equals(RetrofitError.Kind.NETWORK)) {
            Toast.makeText(activity, "Zkontrolujte internetové připojení",
                    Toast.LENGTH_SHORT).show();
        }
    }
}
