package cz.cvut.fit.filipon1.dracek_admin;

/**
 * Created by ondra on 21.2.16.
 */
public class LoginManager {

    private static String sToken = "dcc72012f8d6a15366e257cf2a30e56b";


    /**
     * Gets token.
     *
     * @return the token
     */
    public static String getToken() {
        return sToken;
    }

    /**
     * Sets token.
     *
     * @param token the token
     */
    public static void setToken(String token) {
        sToken = token;
    }
}
