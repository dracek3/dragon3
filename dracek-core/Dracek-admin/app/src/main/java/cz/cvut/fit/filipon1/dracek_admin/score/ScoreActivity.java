package cz.cvut.fit.filipon1.dracek_admin.score;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.BaseDrawerActivity;
import cz.cvut.fit.filipon1.dracek_admin.DataCache;
import cz.cvut.fit.filipon1.dracek_admin.LoginManager;
import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.exercise.ExerciseDetailFragment;
import cz.cvut.fit.filipon1.dracek_admin.model.Exercise;
import cz.cvut.fit.filipon1.dracek_admin.model.ModuleExercise;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.ExerciseAdapter;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.ModuleExerciseAdapter;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ErrorHandler;
import cz.cvut.fit.filipon1.dracek_admin.webservice.RestClient;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ServiceGenerator;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ondra on 26.3.16.
 */
public class ScoreActivity extends BaseDrawerActivity {

    private Spinner spnModuleExercise;
    private ListView mListView;

    private ModuleExerciseAdapter mModuleExerciseAdapter;
    private ExerciseAdapter mExerciseAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        spnModuleExercise = (Spinner) findViewById(R.id.spn_module_exercise);

        mListView = (ListView) findViewById(R.id.lv_exercise);

        createDrawer();

        loadModuleExercises();

        setListeners();

        mListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    }

    private void loadModuleExercises() {
        DataCache.getInstance().getModuleExercise(new DataCache.ModuleExerciseCallback() {
            @Override
            public void onSuccess(List<ModuleExercise> moduleExercises) {
                mModuleExerciseAdapter = new ModuleExerciseAdapter(
                        ScoreActivity.this, moduleExercises);
                spnModuleExercise.setAdapter(mModuleExerciseAdapter);
            }

            @Override
            public void onFail(RetrofitError error) {

            }
        });
    }

    private void loadExercises(long moduleExerciseId) {
        RestClient client = ServiceGenerator
                .createServiceUserToken(RestClient.class, LoginManager.getToken());
        client.getExercsisesOfModule(moduleExerciseId, new Callback<List<Exercise>>() {
            @Override
            public void success(List<Exercise> exercises, Response response) {
                mExerciseAdapter = new ExerciseAdapter(ScoreActivity.this, exercises);
                mListView.setAdapter(mExerciseAdapter);
            }

            @Override
            public void failure(RetrofitError error) {
                ErrorHandler.handle(error, ScoreActivity.this);
            }
        });
    }

    private void setListeners() {

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Exercise exercise = mExerciseAdapter.getItem(position);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_fragment_container,
                                ScoreDetailFragment.newInstance(exercise))
                        .commit();
            }
        });


        spnModuleExercise.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ModuleExercise moduleExercise = mModuleExerciseAdapter.getItem(position);
                loadExercises(moduleExercise.getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                /* Nothing */
            }
        });


    }
}
