package cz.cvut.fit.filipon1.dracek_admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ButtonBarLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import cz.cvut.fit.filipon1.dracek_admin.student.ui.StudentActivity;
import cz.cvut.fit.filipon1.dracek_admin.utils.HashSHA256;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ApiConstants;
import cz.cvut.fit.filipon1.dracek_admin.webservice.RestClient;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ServiceGenerator;
import cz.cvut.fit.filipon1.dracek_admin.webservice.response.LoginResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ondra on 1.5.16.
 */
public class DummyCore extends AppCompatActivity {

    private EditText username;
    private EditText password;
    private Button button;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dummy_core);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Dummy core");
        }

        username = (EditText) findViewById(R.id.et_username);
        password = (EditText) findViewById(R.id.et_password);
        button = (Button) findViewById(R.id.btn_login);



        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RestClient client = ServiceGenerator.createServiceAppToken(RestClient.class, "abc");
                final HashMap<String, Object> body = new HashMap<>();
                body.put(ApiConstants.USER_USERNAME, username.getText().toString());

                /*
                try {
                    body.put(ApiConstants.USER_PASSWORD, HashSHA256.hash(password.getText().toString()));
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }*/
                body.put(ApiConstants.USER_PASSWORD, password.getText().toString());

                client.login(body, new Callback<LoginResponse>() {
                    @Override
                    public void success(LoginResponse loginResponse, Response response) {
                        Intent i = new Intent(DummyCore.this, StudentActivity.class);
                        i.putExtra(StudentActivity.TOKEN, loginResponse.getToken().getToken());
                        startActivity(i);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Toast.makeText(DummyCore.this, "Špatné údaje", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

    }
}
