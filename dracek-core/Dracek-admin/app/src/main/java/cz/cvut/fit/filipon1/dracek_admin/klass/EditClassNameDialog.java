package cz.cvut.fit.filipon1.dracek_admin.klass;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;

import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.Klass;

/**
 * Created by ondra on 25.3.16.
 */
public class EditClassNameDialog extends AlertDialog {

    private EditText etName;
    private Klass mClass;

    private OnNameEditedListener mListener;

    /**
     * The interface On name edited listener.
     */
    public interface OnNameEditedListener {
        /**
         * On name edited.
         *
         * @param name    the name
         * @param classId the class id
         */
        void onNameEdited(String name, long classId);
    }

    /**
     * Instantiates a new Edit class name dialog.
     *
     * @param context the context
     * @param klass   the klass
     */
    protected EditClassNameDialog(Context context, Klass klass) {
        super(context);
        mClass = klass;
    }

    /**
     * Sets listener.
     *
     * @param listener the listener
     */
    public void setListener(OnNameEditedListener listener) {
        mListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_edit_class_name);

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                                | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);

        etName = (EditText) findViewById(R.id.et_name);

        etName.setText(mClass.getName());

        findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        findViewById(R.id.btn_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = etName.getText().toString();

                if (TextUtils.isEmpty(name)) {
                    etName.setError("Vyplňte jméno");
                    return;
                }

                if (mListener != null) {
                    mListener.onNameEdited(name, mClass.getId());
                }
                dismiss();
            }
        });
    }
}
