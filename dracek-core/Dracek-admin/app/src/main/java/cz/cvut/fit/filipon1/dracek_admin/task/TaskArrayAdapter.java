package cz.cvut.fit.filipon1.dracek_admin.task;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.Task;

/**
 * Created by ondra on 27. 3. 2016.
 */
public class TaskArrayAdapter extends ArrayAdapter<Task> {

    /**
     * Instantiates a new Task array adapter.
     *
     * @param context the context
     * @param objects the objects
     */
    public TaskArrayAdapter(Context context, List<Task> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.list_item_task, parent, false);
        }

        Task task = getItem(position);

        ((TextView) convertView.findViewById(R.id.tv_title)).setText(task.getFromattedName());

        return convertView;
    }
}
