package cz.cvut.fit.filipon1.dracek_admin.task;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.DataCache;
import cz.cvut.fit.filipon1.dracek_admin.LoginManager;
import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.klass.ClassSimpleArrayAdapter;
import cz.cvut.fit.filipon1.dracek_admin.model.Klass;
import cz.cvut.fit.filipon1.dracek_admin.model.Score;
import cz.cvut.fit.filipon1.dracek_admin.model.Task;
import cz.cvut.fit.filipon1.dracek_admin.module_exercise.SelectClassDialog;
import cz.cvut.fit.filipon1.dracek_admin.module_exercise.SelectStudentDialog;
import cz.cvut.fit.filipon1.dracek_admin.score.ScoreArrayAdapter;
import cz.cvut.fit.filipon1.dracek_admin.student.dialog.NumberPickerDialog;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ErrorHandler;
import cz.cvut.fit.filipon1.dracek_admin.webservice.RestClient;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ServiceGenerator;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ondra on 26.3.16.
 */
public class TaskDetailFragment extends Fragment {

    private static final String TASK = "task";

    private ListView mTaskListView;
    private TextView tvDateFrom;
    private TextView tvDateTo;
    private TextView tvScoreFrom;
    private TextView tvScoreTo;
    private View btnDateFromReset;
    private View btnDateToReset;
    private View btnFilter;
    private View tbFilterShow;
    private LinearLayout mFilterContainer;
    private Switch swShowUndone;

    private View btnAddToStudent;
    private View btnAddToClass;

    private Spinner spnClasses;
    private ClassSimpleArrayAdapter mClassesAdapter;
    private TaskOfUsersArrayAdapter mTaskAdapter;

    private boolean mIsFilterShown;

    private Calendar mDateFrom;
    private Calendar mDateTo;
    private int mScoreFrom = 0;
    private int mScoreTo = 100;

    private Task mTask;

    /**
     * New instance task detail fragment.
     *
     * @param task the task
     * @return the task detail fragment
     */
    public static TaskDetailFragment newInstance(Task task) {

        Bundle args = new Bundle();
        args.putSerializable(TASK, task);
        TaskDetailFragment fragment = new TaskDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_task_detail, container, false);

        mTask = (Task) getArguments().getSerializable(TASK);

        mTaskListView = (ListView) v.findViewById(R.id.lv_tasks);
        tvDateFrom = (TextView) v.findViewById(R.id.tv_date_from);
        tvDateTo = (TextView) v.findViewById(R.id.tv_date_to);
        tvScoreFrom = (TextView) v.findViewById(R.id.tv_score_from);
        tvScoreTo = (TextView) v.findViewById(R.id.tv_score_to);
        btnDateFromReset = v.findViewById(R.id.iv_date_from_reset);
        btnDateToReset = v.findViewById(R.id.iv_date_to_reset);
        btnFilter = v.findViewById(R.id.btn_filter);
        tbFilterShow = v.findViewById(R.id.tb_show_filter);
        mFilterContainer = (LinearLayout) v.findViewById(R.id.ll_filter_container);
        spnClasses = (Spinner) v.findViewById(R.id.spn_class);
        swShowUndone = (Switch) v.findViewById(R.id.sw_show_undone);
        btnAddToClass = v.findViewById(R.id.btn_add_to_class);
        btnAddToStudent = v.findViewById(R.id.btn_add_to_student);

        swShowUndone.setChecked(true);
        mIsFilterShown = false;

        setListeners();

        loadClasses();

        return v;
    }

    private void loadClasses() {
        DataCache.getInstance().getClassList(new DataCache.KlassCallback() {
            @Override
            public void onSuccess(List<Klass> c) {
                List<Klass> classes = new ArrayList<Klass>(c);
                Klass klass = new Klass();
                klass.setId(0);
                klass.setName("Všechny třídy");
                classes.add(0, klass);

                mClassesAdapter = new ClassSimpleArrayAdapter(getContext(), classes);
                spnClasses.setAdapter(mClassesAdapter);
                loadTask();
            }

            @Override
            public void onFail(RetrofitError error) {
                ErrorHandler.handle(error, getContext());
            }
        });
    }

    private void loadTask() {
        Klass klass = mClassesAdapter.getItem(spnClasses.getSelectedItemPosition());
        if (klass.getId() == 0) {
            RestClient client = ServiceGenerator
                    .createServiceUserToken(RestClient.class, LoginManager.getToken());
            client.getTasksOfUsers(mTask.getId(), new Callback<List<Task>>() {
                @Override
                public void success(List<Task> tasks, Response response) {
                    if (getContext() != null) {
                        mTaskAdapter = new TaskOfUsersArrayAdapter(getContext(), filterTask(tasks));
                        mTaskListView.setAdapter(mTaskAdapter);
                    }

                }

                @Override
                public void failure(RetrofitError error) {
                    if (getContext() != null) {
                        mTaskAdapter = new TaskOfUsersArrayAdapter(getContext(), new ArrayList<Task>());
                        mTaskListView.setAdapter(mTaskAdapter);
                    }
                    ErrorHandler.handle(error, getContext());
                }
            });
        } else {
            RestClient client = ServiceGenerator
                    .createServiceUserToken(RestClient.class, LoginManager.getToken());

            client.getTasksOfClass(klass.getId(), mTask.getId(), new Callback<List<Task>>() {
                @Override
                public void success(List<Task> tasks, Response response) {
                    if (getContext() != null) {
                        mTaskAdapter = new TaskOfUsersArrayAdapter(getContext(), filterTask(tasks));
                        mTaskListView.setAdapter(mTaskAdapter);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if (getContext() != null) {
                        mTaskAdapter = new TaskOfUsersArrayAdapter(getContext(), new ArrayList<Task>());
                        mTaskListView.setAdapter(mTaskAdapter);
                    }
                    ErrorHandler.handle(error, getContext());
                }
            });
        }
    }



    private void setListeners() {

        btnAddToClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectClassDialog dialog = new SelectClassDialog(getContext());
                dialog.setListener(new SelectClassDialog.OnClassSelectedListener() {
                    @Override
                    public void onClassSelect(long classId) {
                        addTaskToClass(classId);
                    }
                });
                dialog.show();
            }
        });

        btnAddToStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectStudentDialog dialog = new SelectStudentDialog(getContext());
                dialog.setListener(new SelectStudentDialog.OnUserSelectedListener() {
                    @Override
                    public void onUserSelect(long userId) {
                        addTaskToStudent(userId);
                    }
                });
                dialog.show();
            }
        });

        tbFilterShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsFilterShown) {
                    mFilterContainer.setVisibility(View.GONE);
                    mIsFilterShown = false;
                } else {
                    mFilterContainer.setVisibility(View.VISIBLE);
                    mIsFilterShown = true;
                }
            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadTask();
            }
        });

        tvDateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int day = 0, month = 0, year = 0;
                if (mDateFrom != null) {
                    day = mDateFrom.get(Calendar.DAY_OF_MONTH);
                    month = mDateFrom.get(Calendar.MONTH);
                    year = mDateFrom.get(Calendar.YEAR);
                }
                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mDateFrom = Calendar.getInstance();
                        mDateFrom.set(Calendar.YEAR, year);
                        mDateFrom.set(Calendar.MONTH, monthOfYear);
                        mDateFrom.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        displayDate(tvDateFrom, mDateFrom);
                    }
                }, year, month, day);
                dialog.getDatePicker().setMinDate(946688461000l);
                dialog.show();
            }
        });

        tvDateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int day = 0, month = 0, year = 0;
                if (mDateTo != null) {
                    day = mDateTo.get(Calendar.DAY_OF_MONTH);
                    month = mDateTo.get(Calendar.MONTH);
                    year = mDateTo.get(Calendar.YEAR);
                }
                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mDateTo = Calendar.getInstance();
                        mDateTo.set(Calendar.YEAR, year);
                        mDateTo.set(Calendar.MONTH, monthOfYear);
                        mDateTo.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        displayDate(tvDateTo, mDateTo);
                    }
                }, year, month, day);
                dialog.getDatePicker().setMinDate(946688461000l);
                dialog.show();
            }
        });

        tvScoreFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPickerDialog dialog = NumberPickerDialog.newInstance(mScoreFrom);
                dialog.setListener(new NumberPickerDialog.OnNumberSelectedListener() {
                    @Override
                    public void onSelected(int number) {
                        mScoreFrom = number;
                        tvScoreFrom.setText(String.valueOf(mScoreFrom));
                    }
                });
                dialog.show(getChildFragmentManager(), "E");
            }
        });

        tvScoreTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPickerDialog dialog = NumberPickerDialog.newInstance(mScoreTo);
                dialog.setListener(new NumberPickerDialog.OnNumberSelectedListener() {
                    @Override
                    public void onSelected(int number) {
                        mScoreTo = number;
                        tvScoreTo.setText(String.valueOf(mScoreTo));
                    }
                });
                dialog.show(getChildFragmentManager(), "F");
            }
        });

        btnDateFromReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDateFrom = null;
                displayDate(tvDateFrom, null);
            }
        });

        btnDateToReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDateTo = null;
                displayDate(tvDateTo, null);
            }
        });

    }

    private void addTaskToStudent(long userId) {
        RestClient client = ServiceGenerator
                .createServiceUserToken(RestClient.class, LoginManager.getToken());
        client.addTaskToStudent(userId, mTask.getId(), new Object(), new Callback<List<Object>>() {
            @Override
            public void success(List<Object> objects, Response response) {
                loadTask();
                Toast.makeText(getContext(), "Úkol přidán", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(RetrofitError error) {
                ErrorHandler.handle(error, getContext());
            }
        });
    }

    private void addTaskToClass(long classId) {
        RestClient client = ServiceGenerator
                .createServiceUserToken(RestClient.class, LoginManager.getToken());
        client.addTaskToClass(classId, mTask.getId(), new Object(), new Callback<List<Object>>() {
            @Override
            public void success(List<Object> objects, Response response) {
                loadTask();
                Toast.makeText(getContext(), "Úkol přidán", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(RetrofitError error) {
                ErrorHandler.handle(error, getContext());
            }
        });
    }


    private void displayDate(TextView tv, Calendar date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

        if (date == null) {
            tv.setText("--.--.----");
        } else {
            tv.setText(dateFormat.format(date.getTime()));
        }
    }


    private List<Task> filterTask(List<Task> tasks) {
        List<Task> filteredTask = new ArrayList<>();

        for (Task t : tasks) {

            if (mDateFrom != null) {
                if (t.getCreatedAt() < mDateFrom.getTime().getTime()) {
                    continue;
                }
            }

            if (mDateTo != null) {
                if (t.getCreatedAt() > mDateTo.getTime().getTime()) {
                    continue;
                }
            }

            if (t.getScore() == null) {
                if (!swShowUndone.isChecked()) {
                    continue;
                }
            } else {

                if (t.getScore().getPercentage() > mScoreTo) {
                    continue;
                }

                if (t.getScore().getPercentage() < mScoreFrom) {
                    continue;
                }
            }

            filteredTask.add(t);
        }

        return filteredTask;
    }


}
