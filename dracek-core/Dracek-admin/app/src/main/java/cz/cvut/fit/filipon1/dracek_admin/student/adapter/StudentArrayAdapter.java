package cz.cvut.fit.filipon1.dracek_admin.student.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.User;

/**
 * Created by ondra on 21.2.16.
 */
public class StudentArrayAdapter extends ArrayAdapter<User> {

    private List<User> data;
    /**
     * Instantiates a new Student array adapter.
     *
     * @param context the context
     * @param objects the objects
     */
    public StudentArrayAdapter(Context context, List<User> objects) {
        super(context, 0, objects);
        data = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.list_item_user, parent, false);
        }

        User user = getItem(position);

        ((TextView) convertView.findViewById(R.id.tv_text)).setText(user.getFullName());
        ((TextView) convertView.findViewById(R.id.tv_username)).setText(user.getUsernameFormatted());

        return convertView;
    }

    public List<User>  getAll() {
        return data;
    }
}
