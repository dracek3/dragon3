package cz.cvut.fit.filipon1.dracek_admin.task;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.BaseDrawerActivity;
import cz.cvut.fit.filipon1.dracek_admin.DataCache;
import cz.cvut.fit.filipon1.dracek_admin.LoginManager;
import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.Exercise;
import cz.cvut.fit.filipon1.dracek_admin.model.ModuleExercise;
import cz.cvut.fit.filipon1.dracek_admin.model.Task;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.ExerciseAdapter;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.ModuleExerciseAdapter;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ErrorHandler;
import cz.cvut.fit.filipon1.dracek_admin.webservice.RestClient;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ServiceGenerator;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ondra on 26.3.16.
 */
public class TaskActivity extends BaseDrawerActivity {

    private Spinner spnModuleExercise;
    private Spinner spnExercise;
    private ListView mListView;

    private ModuleExerciseAdapter mModuleExerciseAdapter;
    private ExerciseAdapter mExerciseAdapter;
    private TaskArrayAdapter mTaskAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);

        spnModuleExercise = (Spinner) findViewById(R.id.spn_module_exercise);
        spnExercise = (Spinner) findViewById(R.id.spn_exercise);

        mListView = (ListView) findViewById(R.id.lv_tasks);

        createDrawer();

        loadModuleExercises();

        setListeners();

        mListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    }

    private void loadModuleExercises() {
        DataCache.getInstance().getModuleExercise(new DataCache.ModuleExerciseCallback() {
            @Override
            public void onSuccess(List<ModuleExercise> moduleExercises) {
                mModuleExerciseAdapter = new ModuleExerciseAdapter(
                        TaskActivity.this, moduleExercises);
                spnModuleExercise.setAdapter(mModuleExerciseAdapter);
            }

            @Override
            public void onFail(RetrofitError error) {
                ErrorHandler.handle(error, TaskActivity.this);
            }
        });
    }

    private void loadExercises(final long moduleExerciseId) {
        RestClient client = ServiceGenerator
                .createServiceUserToken(RestClient.class, LoginManager.getToken());
        client.getExercsisesOfModule(moduleExerciseId, new Callback<List<Exercise>>() {
            @Override
            public void success(List<Exercise> exercises, Response response) {
                mExerciseAdapter = new ExerciseAdapter(TaskActivity.this, exercises);
                spnExercise.setAdapter(mExerciseAdapter);
            }

            @Override
            public void failure(RetrofitError error) {
                ErrorHandler.handle(error, TaskActivity.this);
            }
        });
    }

    private void loadTasks(long moduleExerciseId, long exerciseId) {
        RestClient client = ServiceGenerator
                .createServiceUserToken(RestClient.class, LoginManager.getToken());
        client.getAllTasks(moduleExerciseId, exerciseId, new Callback<List<Task>>() {
            @Override
            public void success(List<Task> tasks, Response response) {
                mTaskAdapter = new TaskArrayAdapter(TaskActivity.this, tasks);
                mListView.setAdapter(mTaskAdapter);
            }

            @Override
            public void failure(RetrofitError error) {
                mTaskAdapter = new TaskArrayAdapter(TaskActivity.this, new ArrayList<Task>());
                mListView.setAdapter(mTaskAdapter);
                ErrorHandler.handle(error, TaskActivity.this);
            }
        });
    }

    private void setListeners() {

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Task task = mTaskAdapter.getItem(position);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_fragment_container,
                                TaskDetailFragment.newInstance(task))
                        .commit();
            }
        });


        spnModuleExercise.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ModuleExercise moduleExercise = mModuleExerciseAdapter.getItem(position);
                loadExercises(moduleExercise.getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                /* Nothing */
            }
        });

        spnExercise.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Exercise exercise = mExerciseAdapter.getItem(position);
                long moduleExerciseId = mModuleExerciseAdapter
                        .getItem(spnModuleExercise.getSelectedItemPosition()).getId();
                loadTasks(moduleExerciseId, exercise.getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_task, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_create_task) {
            CreateTaskDialog dialog = new CreateTaskDialog();
            dialog.setListener(new CreateTaskDialog.OnTaskCreateListener() {
                @Override
                public void onCreate() {
                    long moduleExerciseId = mModuleExerciseAdapter
                            .getItem(spnModuleExercise.getSelectedItemPosition()).getId();
                    long exerciseId = mExerciseAdapter
                            .getItem(spnExercise.getSelectedItemPosition()).getId();
                    loadTasks(moduleExerciseId, exerciseId);
                }
            });
            dialog.show(getSupportFragmentManager(), "D");
            return true;
        }
        return false;
    }
}
