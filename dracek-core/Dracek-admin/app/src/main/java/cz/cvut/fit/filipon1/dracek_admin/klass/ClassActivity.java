package cz.cvut.fit.filipon1.dracek_admin.klass;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.BaseDrawerActivity;
import cz.cvut.fit.filipon1.dracek_admin.DataCache;
import cz.cvut.fit.filipon1.dracek_admin.LoginManager;
import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.Klass;
import cz.cvut.fit.filipon1.dracek_admin.model.User;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.StudentArrayAdapter;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.StudentOptionArrayAdapter;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ApiConstants;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ErrorHandler;
import cz.cvut.fit.filipon1.dracek_admin.webservice.RestClient;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ServiceGenerator;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ondra on 9. 3. 2016.
 */
public class ClassActivity extends BaseDrawerActivity implements
            AddStudentToClassDialog.OnUserSelectedListener, EditClassNameDialog.OnNameEditedListener {

    private ListView mClassesListView;
    private ListView mStudentsListView;

    private ClassOptionsArrayAdapter mClassesAdapter;
    private StudentOptionArrayAdapter mStudentsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class);
        createDrawer();

        mClassesListView = (ListView) findViewById(R.id.lv_classes);
        mStudentsListView = (ListView) findViewById(R.id.lv_students);

        mClassesListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);


        mClassesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Klass klass = mClassesAdapter.getItem(position);
                loadStudentsOfClass(klass.getId());
            }
        });


        loadClasses();
    }

    private void loadClasses() {
        DataCache.getInstance().getClassList(new DataCache.KlassCallback() {
            @Override
            public void onSuccess(List<Klass> classes) {
                mClassesAdapter = new ClassOptionsArrayAdapter(ClassActivity.this, classes);
                mClassesListView.setAdapter(mClassesAdapter);
            }

            @Override
            public void onFail(RetrofitError error) {
                ErrorHandler.handle(error, ClassActivity.this);
            }
        });
    }

    private void loadStudentsOfClass(final long classID) {
        RestClient client = ServiceGenerator.createServiceUserToken(RestClient.class, LoginManager.getToken());
        client.getStudentsFromClassList(classID, new Callback<List<User>>() {
            @Override
            public void success(List<User> users, Response response) {
                mStudentsAdapter = new StudentOptionArrayAdapter(ClassActivity.this, users, classID);
                mStudentsAdapter.setListener(new StudentOptionArrayAdapter.OnRemoveFromClassListener() {
                    @Override
                    public void onRemoveFromClass(long userId, long classId) {
                        RestClient client = ServiceGenerator
                                .createServiceUserToken(RestClient.class, LoginManager.getToken());
                        client.deleteStudentFromClass(userId, classId, new Callback<List<Object>>() {
                            @Override
                            public void success(List<Object> objects, Response response) {
                                loadStudentsOfClass(classID);
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                ErrorHandler.handle(error, ClassActivity.this);
                            }
                        });
                    }
                });
                mStudentsListView.setAdapter(mStudentsAdapter);
            }

            @Override
            public void failure(RetrofitError error) {
                mStudentsAdapter = new StudentOptionArrayAdapter(ClassActivity.this, new ArrayList<User>(), classID);
                mStudentsListView.setAdapter(mStudentsAdapter);
            }
        });
    }

    @Override
    public void onUserSelect(long userId, long classId) {
        RestClient client = ServiceGenerator
                .createServiceUserToken(RestClient.class, LoginManager.getToken());
        client.addStudentToClass(userId, classId,
                new HashMap<String, Object>(), new Callback<List<Object>>() {
                    @Override
                    public void success(List<Object> objects, Response response) {
                        if (mStudentsAdapter != null) {
                            mStudentsAdapter.clear();
                            mStudentsAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        ErrorHandler.handle(error, ClassActivity.this);
                    }
                });
    }

    @Override
    public void onNameEdited(String name, long classId) {
        RestClient client = ServiceGenerator
                .createServiceUserToken(RestClient.class, LoginManager.getToken());
        HashMap<String, Object> body = new HashMap<>();
        body.put("name", name);
        client.editClassName(classId, body, new Callback<Klass>() {
            @Override
            public void success(Klass objects, Response response) {
                DataCache.getInstance().invalidateClasses();
                loadClasses();
            }

            @Override
            public void failure(RetrofitError error) {
                ErrorHandler.handle(error, ClassActivity.this);
            }
        });
    }

    /**
     * Delete class.
     *
     * @param classId the class id
     */
    public void deleteClass(long classId) {
        RestClient client = ServiceGenerator
                .createServiceUserToken(RestClient.class, LoginManager.getToken());
        client.deleteClass(classId, new Callback<List<Object>>() {
            @Override
            public void success(List<Object> objects, Response response) {
                DataCache.getInstance().invalidateClasses();
                loadClasses();
            }

            @Override
            public void failure(RetrofitError error) {
                ErrorHandler.handle(error, ClassActivity.this);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_class, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_class:
                createNewClass();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void createNewClass() {
        CreateClasDialog dialog = new CreateClasDialog(this);
        dialog.setListener(new CreateClasDialog.OnClassCreatedListener() {
            @Override
            public void onClassCreated(String name) {
                RestClient client = ServiceGenerator
                        .createServiceUserToken(RestClient.class, LoginManager.getToken());
                HashMap<String, Object> body = new HashMap<>();
                body.put("name", name);
                client.createClass(body, new Callback<Klass>() {
                    @Override
                    public void success(Klass klass, Response response) {
                        DataCache.getInstance().invalidateClasses();
                        loadClasses();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        ErrorHandler.handle(error, ClassActivity.this);
                    }
                });
            }
        });
        dialog.show();
    }

}
