package cz.cvut.fit.filipon1.dracek_admin.klass;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.Klass;

/**
 * Created by ondra on 21.2.16.
 */
public class ClassSimpleArrayAdapter extends ArrayAdapter<Klass> {

    /**
     * Instantiates a new Class simple array adapter.
     *
     * @param context the context
     * @param objects the objects
     */
    public ClassSimpleArrayAdapter(Context context, List<Klass> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.list_item_class, parent, false);
        }

        Klass klass = getItem(position);

        ((TextView) convertView.findViewById(R.id.tv_text)).setText(klass.getName());

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.list_item_class, parent, false);
        }

        Klass klass = getItem(position);

        ((TextView) convertView.findViewById(R.id.tv_text)).setText(klass.getName());

        return convertView;
    }
}
