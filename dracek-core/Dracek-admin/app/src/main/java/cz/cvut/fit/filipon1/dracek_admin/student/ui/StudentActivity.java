package cz.cvut.fit.filipon1.dracek_admin.student.ui;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.BaseDrawerActivity;
import cz.cvut.fit.filipon1.dracek_admin.DataCache;
import cz.cvut.fit.filipon1.dracek_admin.LoginManager;
import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.Klass;
import cz.cvut.fit.filipon1.dracek_admin.model.User;
import cz.cvut.fit.filipon1.dracek_admin.klass.ClassSimpleArrayAdapter;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.StudentArrayAdapter;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.StudentPagerAdapter;
import cz.cvut.fit.filipon1.dracek_admin.student.dialog.CreateUserDialog;
import cz.cvut.fit.filipon1.dracek_admin.student.dialog.EditUserDialog;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ErrorHandler;
import cz.cvut.fit.filipon1.dracek_admin.webservice.RestClient;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ServiceGenerator;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ondra on 21.2.16.
 */
public class StudentActivity extends BaseDrawerActivity
        implements TabLayout.OnTabSelectedListener, StudentDetailFragment.OnUserChange {

    /**
     * The constant TOKEN.
     */
    public static final String TOKEN = "token";

    private Spinner spnClassSpinner;
    private EditText etNameFiler;
    private ListView mListView;
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private ClassSimpleArrayAdapter mClassAdapter;
    public StudentArrayAdapter mStudentAdapter;
    private StudentPagerAdapter mPagerAdapter;

    /* List of all loaded users */
    private List<User> mLoadedUsers;

    /* Id of class that is currently loaded */
    private long mLoadedClassId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);

        if (LoginManager.getToken() == null) {
            String token = getIntent().getStringExtra(TOKEN);
            if (token == null) {
                Toast.makeText(StudentActivity.this, "Missing token.", Toast.LENGTH_SHORT).show();
                finish();
            }
            LoginManager.setToken(token);
        }

        spnClassSpinner = (Spinner) findViewById(R.id.spn_class_spinner);
        etNameFiler = (EditText) findViewById(R.id.et_name_filter);
        mListView = (ListView) findViewById(R.id.lv_user_list);
        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mViewPager = (ViewPager) findViewById(R.id.vp_pager);

        createDrawer();

        loadClasses();

        /* Implicitly load users of all classes -> class id 0 */
        loadUsers(0);

        setListeners();

        mListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

    }

    private void setListeners() {

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User user = mStudentAdapter.getItem(position);
                mPagerAdapter = new StudentPagerAdapter(getSupportFragmentManager(), user);
                mViewPager.setAdapter(mPagerAdapter);
                mTabLayout.setupWithViewPager(mViewPager);
                mTabLayout.getTabAt(0).setText("Přehled");
                mTabLayout.getTabAt(1).setText("Hodnocení");
                mTabLayout.getTabAt(2).setText("Úkoly");
            }
        });


        etNameFiler.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                /* Nothing */
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateViewWithStudentList();
            }

            @Override
            public void afterTextChanged(Editable s) {
                /* Nothing */
            }
        });

        spnClassSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Klass klass = mClassAdapter.getItem(position);
                loadUsers(klass.getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                /* Nothing */
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_student, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_add_student) {
            CreateUserDialog dialog = new CreateUserDialog();
            dialog.setListener(new CreateUserDialog.OnUserCreateListener() {
                @Override
                public void onCreate(User newUser) {
                    mLoadedClassId = -1;
                    loadUsers(mClassAdapter.getItem(spnClassSpinner.getSelectedItemPosition()).getId());
                }
            });
            dialog.show(getSupportFragmentManager(), "D");
            return true;
        }
        return false;
    }

    /**
     * Loads list of all classes and display them in spinner
     */
    private void loadClasses() {

        DataCache.getInstance().getClassList(new DataCache.KlassCallback() {
            @Override
            public void onSuccess(List<Klass> c) {
                /* Add implicit class witch id 0, if this class is selected users of
                 * all classes are displayed */
                List<Klass> classes = new ArrayList<Klass>(c);
                Klass klass = new Klass();
                klass.setId(0);
                klass.setName("Všechny třídy");
                classes.add(0, klass);

                mClassAdapter = new ClassSimpleArrayAdapter(StudentActivity.this, classes);
                spnClassSpinner.setAdapter(mClassAdapter);
            }

            @Override
            public void onFail(RetrofitError error) {
                ErrorHandler.handle(error, StudentActivity.this);
            }
        });
    }

    /**
     * Loads users from server and display them in list view
     * @param classId id of class witch should be used as filter,
     *                if it's 0 all users will be loaded
     */
    private void loadUsers(long classId) {

        if (mLoadedClassId == classId) {
            /* This class is already loaded, no need for loading it again */
            return;
        }
        mLoadedClassId = classId;

        RestClient client = ServiceGenerator.createServiceUserToken(RestClient.class, LoginManager.getToken());
        if (classId <= 0) {
            client.getAllStudentList(new Callback<List<User>>() {
                @Override
                public void success(List<User> users, Response response) {
                    mLoadedUsers = users;
                    Collections.sort(mLoadedUsers);
                    updateViewWithStudentList();
                }

                @Override
                public void failure(RetrofitError error) {
                    ErrorHandler.handle(error, StudentActivity.this);
                }
            });
        } else {
            client.getStudentsFromClassList(classId, new Callback<List<User>>() {
                @Override
                public void success(List<User> users, Response response) {
                    mLoadedUsers = users;
                    Collections.sort(mLoadedUsers);
                    updateViewWithStudentList();
                }

                @Override
                public void failure(RetrofitError error) {
                    ErrorHandler.handle(error, StudentActivity.this);
                }
            });
        }
    }

    /**
     * Gets currently loaded users and filter them according to current state
     * of filter name edit text. After that shows filtered users in list view.
     */
    private void updateViewWithStudentList() {
        List<User> filteredUsers = new ArrayList<>();
        String filterText = etNameFiler.getText().toString().toLowerCase();

        if (TextUtils.isEmpty(filterText)) {
            mStudentAdapter = new StudentArrayAdapter(this, mLoadedUsers);
        } else if (mLoadedUsers != null){
            for (User u : mLoadedUsers) {
                if (u.getFullName().toLowerCase().contains(filterText)
                        || u.getUsername().toLowerCase().contains(filterText)) {
                    filteredUsers.add(u);
                }
            }
            mStudentAdapter = new StudentArrayAdapter(this, filteredUsers);
        }

        mListView.setAdapter(mStudentAdapter);
    }

    /* -------- BEGIN TabLayout.OnTabSelectedListener Callbacks ------------ */

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        /* Nothing */
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        /* Nothing */
    }

    /* -------- END TabLayout.OnTabSelectedListener Callbacks -------------- */


    /* -------- BEGIN TabLayout.OnTabSelectedListener Callbacks ------------ */


    /* ---------- BEGIN StudentDetailFragment.OnUserChange Callbacks ------- */

    @Override
    public void onUserEdited(User user) {
        for (int i = 0; i < mLoadedUsers.size(); i++) {
            if (mLoadedUsers.get(i).getId() == user.getId()) {
                mLoadedUsers.set(i, user);
                break;
            }
        }
        updateViewWithStudentList();
    }

    @Override
    public void onUserDeleted(User user) {
        mLoadedUsers.remove(user);
        updateViewWithStudentList();
    }

    /* ---------- END StudentDetailFragment.OnUserChange Callbacks ------- */
}
