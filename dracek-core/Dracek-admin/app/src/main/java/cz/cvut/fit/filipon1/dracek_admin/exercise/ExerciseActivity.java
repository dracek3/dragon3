package cz.cvut.fit.filipon1.dracek_admin.exercise;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.BaseDrawerActivity;
import cz.cvut.fit.filipon1.dracek_admin.DataCache;
import cz.cvut.fit.filipon1.dracek_admin.LoginManager;
import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.Exercise;
import cz.cvut.fit.filipon1.dracek_admin.model.ModuleExercise;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.ExerciseAdapter;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.ModuleExerciseAdapter;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ErrorHandler;
import cz.cvut.fit.filipon1.dracek_admin.webservice.RestClient;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ServiceGenerator;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ondra on 26.3.16.
 */
public class ExerciseActivity extends BaseDrawerActivity
                            implements ExerciseDetailFragment.OnExerciseChangeListener {

    private Spinner spnModuleExercise;
    private ListView mListView;

    private ModuleExerciseAdapter mModuleExerciseAdapter;
    private ExerciseAdapter mExerciseAdapter;

    private final static int CREATE_EXERCISE_REQUEST = 4444;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise);

        spnModuleExercise = (Spinner) findViewById(R.id.spn_module_exercise);

        mListView = (ListView) findViewById(R.id.lv_exercise);

        createDrawer();

        loadModuleExercises();

        setListeners();

        mListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
    }

    private void loadModuleExercises() {
        DataCache.getInstance().getModuleExercise(new DataCache.ModuleExerciseCallback() {
            @Override
            public void onSuccess(List<ModuleExercise> moduleExercises) {
                mModuleExerciseAdapter = new ModuleExerciseAdapter(
                        ExerciseActivity.this, moduleExercises);
                spnModuleExercise.setAdapter(mModuleExerciseAdapter);
            }

            @Override
            public void onFail(RetrofitError error) {
                ErrorHandler.handle(error, ExerciseActivity.this);
            }
        });
    }

    private void loadExercises(long moduleExerciseId) {
        RestClient client = ServiceGenerator
                .createServiceUserToken(RestClient.class, LoginManager.getToken());
        client.getExercsisesOfModule(moduleExerciseId, new Callback<List<Exercise>>() {
            @Override
            public void success(List<Exercise> exercises, Response response) {
                mExerciseAdapter = new ExerciseAdapter(ExerciseActivity.this, exercises);
                mListView.setAdapter(mExerciseAdapter);
            }

            @Override
            public void failure(RetrofitError error) {
                mExerciseAdapter = new ExerciseAdapter(ExerciseActivity.this, new ArrayList<Exercise>());
                mListView.setAdapter(mExerciseAdapter);
                ErrorHandler.handle(error, ExerciseActivity.this);
            }
        });
    }

    private void setListeners() {

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Exercise exercise = mExerciseAdapter.getItem(position);
                ModuleExercise moduleExercise = mModuleExerciseAdapter
                        .getItem(spnModuleExercise.getSelectedItemPosition());
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_fragment_container,
                                    ExerciseDetailFragment.newInstance(exercise, moduleExercise))
                        .commit();
            }
        });


        spnModuleExercise.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ModuleExercise moduleExercise = mModuleExerciseAdapter.getItem(position);
                loadExercises(moduleExercise.getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                /* Nothing */
            }
        });


    }


    @Override
    public void onExerciseDelete(long exerciseId, final long moduleExerciseId) {
        RestClient client = ServiceGenerator
                .createServiceUserToken(RestClient.class, LoginManager.getToken());
        client.deleteExercise(moduleExerciseId, exerciseId, new Callback<List<Exercise>>() {
            @Override
            public void success(List<Exercise> objects, Response response) {
                loadExercises(moduleExerciseId);
            }

            @Override
            public void failure(RetrofitError error) {
                ErrorHandler.handle(error, ExerciseActivity.this);
            }
        });
    }

    @Override
    public void onEdit() {
        loadModuleExercises();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_exercise, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_create_exercise) {
            startEditorOfCurrentModule();
            return true;
        }
        return false;
    }

    private void startEditorOfCurrentModule() {
        if (mExerciseAdapter == null) {
            return;
        }
        ModuleExercise exercise = mModuleExerciseAdapter.getItem(spnModuleExercise.getSelectedItemPosition());

        if (exercise == null) {
            return;
        }

        if (TextUtils.isEmpty(exercise.getPackageNameEditor())) {
            Toast.makeText(ExerciseActivity.this, "Toto cvičení neumžňuje vytvářet zadání.",
                    Toast.LENGTH_SHORT).show();
            return;
        }

        Intent exerciseIntent = new Intent();
        exerciseIntent.setComponent(new ComponentName(exercise.getPackageName(),
                exercise.getPackageNameEditor()));
        try {
            startActivityForResult(exerciseIntent, CREATE_EXERCISE_REQUEST);
        } catch (ActivityNotFoundException ex) {
            Toast.makeText(ExerciseActivity.this, "Cviceni nenalezeno", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CREATE_EXERCISE_REQUEST && resultCode == RESULT_OK) {
            final String file = data.getStringExtra("EXERCISE_FILE");
            final List<String> paths = (ArrayList<String>) data.getSerializableExtra("IMAGES");
            showCreateExerciseDialog(file, paths, "", "");
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showCreateExerciseDialog(final String file, final List<String> paths, String nameStr,
                                          String descriptionStr) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(ExerciseActivity.this);
        View v = getLayoutInflater().inflate(R.layout.dialog_create_exercise, null);
        final EditText name = (EditText) v.findViewById(R.id.et_name);
        final EditText description = (EditText) v.findViewById(R.id.et_description);
        View btnSave = v.findViewById(R.id.btn_save);
        View btnCancel = v.findViewById(R.id.btn_cancel);

        name.setText(nameStr);
        description.setText(descriptionStr);

        builder.setView(v);

        final AlertDialog d = builder.create();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(name.getText().toString())) {
                    name.setError("Vyplňte jméno");
                    return;
                }

                if (TextUtils.isEmpty(description.getText().toString())) {
                    description.setError("Vyplňte popis");
                    return;
                }

                uploadCreatedExercise(name.getText().toString(),
                        description.getText().toString(), file, paths);
                d.dismiss();
            }
        });



        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });

        d.show();
    }

    private void uploadCreatedExercise(final String name, final String description, final String file, final List<String> paths) {
        HashMap<String, Object> body = new HashMap<>();
        if (paths != null) {
            List<Exercise.Image> images = new ArrayList<>();

            for (String path : paths) {

                File f = new File(path);
                try {
                    FileInputStream fis = new FileInputStream(f);
                    Bitmap bmp = BitmapFactory.decodeStream(fis);
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                    bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                    byte[] byteArray = byteArrayOutputStream .toByteArray();
                    String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
                    String originalName = path.substring(path.lastIndexOf("/") + 1);
                    images.add(new Exercise.Image(encoded, originalName));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
            body.put("images", images);
        }

        body.put("name", name);
        body.put("file", file);
        body.put("description", description);

        long moduleId = mModuleExerciseAdapter.getItem(spnModuleExercise.getSelectedItemPosition()).getId();

        RestClient client = ServiceGenerator.createServiceUserToken(RestClient.class, LoginManager.getToken());

        client.createExercise(moduleId, body, new Callback<List<Exercise>>() {
            @Override
            public void success(List<Exercise> exercises, Response response) {
                loadModuleExercises();
                Toast.makeText(ExerciseActivity.this, "Cvičení vytvořeno", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(RetrofitError error) {
                if (error.getResponse().getStatus() == 403) {
                    Toast.makeText(ExerciseActivity.this, "Cvičení s tímto jménem již existuje", Toast.LENGTH_SHORT).show();
                    showCreateExerciseDialog(file, paths, name, description);
                }

                if (error.getResponse().getStatus() == 201) {
                    loadModuleExercises();
                    Toast.makeText(ExerciseActivity.this, "Cvičení vytvořeno", Toast.LENGTH_SHORT).show();
                }
                ErrorHandler.handle(error, ExerciseActivity.this);
            }
        });


    }


}
