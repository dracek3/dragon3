package cz.cvut.fit.filipon1.dracek_admin.utils;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

import cz.cvut.fit.filipon1.dracek_admin.model.User;

/**
 * Created by ondra on 2. 3. 2016.
 */
public class TextFormatter {

    /**
     * Format date string.
     *
     * @param mills the mills
     * @return the string
     */
    public static String formatDate(long mills) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        Date date = new Date(mills);
        return format.format(date);
    }

    /**
     * Format time string.
     *
     * @param mills the mills
     * @return the string
     */
    public static final String formatTime(long mills) {
        SimpleDateFormat format = new SimpleDateFormat("hh:mm");
        Date date = new Date(mills);
        return format.format(date);
    }

    /**
     * Format date time string.
     *
     * @param mills the mills
     * @return the string
     */
    public static final String formatDateTime(long mills) {
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy - hh:mm");
        Date date = new Date(mills);
        return format.format(date);
    }


    /**
     * Format duration string.
     *
     * @param secs the secs
     * @return the string
     */
    public static String formatDuration(long secs) {
        String result = "";
        if (secs / 3600 > 0) {
            result += (secs / 3600) + ":";
            secs %= 3600;
        }

        result += (secs / 60) + ":";
        secs %= 60;

        result += secs;

        return result;
    }
}
