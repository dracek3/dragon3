package cz.cvut.fit.filipon1.dracek_admin.model;

/**
 * Created by ondra on 21.2.16.
 */
public class Token {

    private String token;
    private long expiration;


    /**
     * Sets token.
     *
     * @param token the token
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * Sets expiration.
     *
     * @param expiration the expiration
     */
    public void setExpiration(long expiration) {
        this.expiration = expiration;
    }

    /**
     * Gets token.
     *
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * Gets expiration.
     *
     * @return the expiration
     */
    public long getExpiration() {
        return expiration;
    }
}
