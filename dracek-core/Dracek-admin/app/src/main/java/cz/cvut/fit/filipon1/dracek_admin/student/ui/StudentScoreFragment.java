package cz.cvut.fit.filipon1.dracek_admin.student.ui;

import android.animation.LayoutTransition;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.DataCache;
import cz.cvut.fit.filipon1.dracek_admin.LoginManager;
import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.model.Exercise;
import cz.cvut.fit.filipon1.dracek_admin.model.ModuleExercise;
import cz.cvut.fit.filipon1.dracek_admin.model.Score;
import cz.cvut.fit.filipon1.dracek_admin.model.User;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.ExerciseAdapter;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.ModuleExerciseAdapter;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.ScoreOfUserAdapter;
import cz.cvut.fit.filipon1.dracek_admin.student.dialog.NumberPickerDialog;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ErrorHandler;
import cz.cvut.fit.filipon1.dracek_admin.webservice.RestClient;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ServiceGenerator;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ondra on 23.2.16.
 */
public class StudentScoreFragment extends Fragment {

    private static final String USER = "user";

    private ListView mScoreListView;
    private Spinner spnModuleExercise;
    private Spinner spnExercise;
    private TextView tvDateFrom;
    private TextView tvDateTo;
    private TextView tvScoreFrom;
    private TextView tvScoreTo;
    private View btnDateFromReset;
    private View btnDateToReset;
    private View btnFilter;
    private View tbFilterShow;
    private LinearLayout mFilterContainer;

    private boolean mIsFilterShown;

    private ScoreOfUserAdapter mScoreAdapter;
    private ModuleExerciseAdapter mModuleExerciseAdapter;
    private ExerciseAdapter mExerciseAdapter;

    private Calendar mDateFrom;
    private Calendar mDateTo;
    private int mScoreFrom = 0;
    private int mScoreTo = 100;

    private User mUser;

    /**
     * New instance student score fragment.
     *
     * @param user the user
     * @return the student score fragment
     */
    public static StudentScoreFragment newInstance(User user) {

        Bundle args = new Bundle();
        args.putSerializable(USER, user);
        StudentScoreFragment fragment = new StudentScoreFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_student_score, container, false);

        mUser = (User) getArguments().getSerializable(USER);

        mScoreListView = (ListView) v.findViewById(R.id.lv_score);
        spnModuleExercise = (Spinner) v.findViewById(R.id.spn_module_exercise);
        spnExercise = (Spinner) v.findViewById(R.id.spn_exercise);
        tvDateFrom = (TextView) v.findViewById(R.id.tv_date_from);
        tvDateTo = (TextView) v.findViewById(R.id.tv_date_to);
        tvScoreFrom = (TextView) v.findViewById(R.id.tv_score_from);
        tvScoreTo = (TextView) v.findViewById(R.id.tv_score_to);
        btnDateFromReset = v.findViewById(R.id.iv_date_from_reset);
        btnDateToReset = v.findViewById(R.id.iv_date_to_reset);
        btnFilter = v.findViewById(R.id.btn_filter);
        tbFilterShow = v.findViewById(R.id.tb_show_filter);
        mFilterContainer = (LinearLayout) v.findViewById(R.id.ll_filter_container);


        mIsFilterShown = true;

        loadModuleExercises();

        setListeners();

        return v;
    }

    private void loadModuleExercises() {
        DataCache.getInstance().getModuleExercise(new DataCache.ModuleExerciseCallback() {
            @Override
            public void onSuccess(List<ModuleExercise> moduleExercises) {
                if (getContext() != null) {
                    mModuleExerciseAdapter = new ModuleExerciseAdapter(getContext(), moduleExercises);
                    spnModuleExercise.setAdapter(mModuleExerciseAdapter);
                }
            }

            @Override
            public void onFail(RetrofitError error) {
                ErrorHandler.handle(error, getContext());
            }
        });
    }

    private void setListeners() {

        tbFilterShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsFilterShown) {
                    mFilterContainer.setVisibility(View.GONE);
                    mIsFilterShown = false;
                } else {
                    mFilterContainer.setVisibility(View.VISIBLE);
                    mIsFilterShown = true;
                }
            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayScores();
            }
        });

        tvDateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int day = 0, month = 0, year = 0;
                if (mDateFrom != null) {
                    day = mDateFrom.get(Calendar.DAY_OF_MONTH);
                    month = mDateFrom.get(Calendar.MONTH);
                    year = mDateFrom.get(Calendar.YEAR);
                }
                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mDateFrom = Calendar.getInstance();
                        mDateFrom.set(Calendar.YEAR, year);
                        mDateFrom.set(Calendar.MONTH, monthOfYear);
                        mDateFrom.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        displayDate(tvDateFrom, mDateFrom);
                    }
                }, year, month, day);
                dialog.getDatePicker().setMinDate(946688461000l);
                dialog.show();
            }
        });

        tvDateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int day = 0, month = 0, year = 0;
                if (mDateTo != null) {
                    day = mDateTo.get(Calendar.DAY_OF_MONTH);
                    month = mDateTo.get(Calendar.MONTH);
                    year = mDateTo.get(Calendar.YEAR);
                }
                DatePickerDialog dialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        mDateTo = Calendar.getInstance();
                        mDateTo.set(Calendar.YEAR, year);
                        mDateTo.set(Calendar.MONTH, monthOfYear);
                        mDateTo.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        displayDate(tvDateTo, mDateTo);
                    }
                }, year, month, day);
                dialog.getDatePicker().setMinDate(946688461000l);
                dialog.show();
            }
        });

        tvScoreFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPickerDialog dialog = NumberPickerDialog.newInstance(mScoreFrom);
                dialog.setListener(new NumberPickerDialog.OnNumberSelectedListener() {
                    @Override
                    public void onSelected(int number) {
                        mScoreFrom = number;
                        tvScoreFrom.setText(String.valueOf(mScoreFrom));
                    }
                });
                dialog.show(getChildFragmentManager(), "E");
            }
        });

        tvScoreTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NumberPickerDialog dialog = NumberPickerDialog.newInstance(mScoreTo);
                dialog.setListener(new NumberPickerDialog.OnNumberSelectedListener() {
                    @Override
                    public void onSelected(int number) {
                        mScoreTo = number;
                        tvScoreTo.setText(String.valueOf(mScoreTo));
                    }
                });
                dialog.show(getChildFragmentManager(), "F");
            }
        });

        btnDateFromReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDateFrom = null;
                displayDate(tvDateFrom, null);
            }
        });

        btnDateToReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDateTo = null;
                displayDate(tvDateTo, null);
            }
        });

        spnModuleExercise.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (mModuleExerciseAdapter != null) {
                    ModuleExercise moduleExercise = mModuleExerciseAdapter.getItem(position);
                    loadExercises(moduleExercise.getId());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void loadExercises(long moduleExerciseId) {
        RestClient client = ServiceGenerator
                .createServiceUserToken(RestClient.class, LoginManager.getToken());
        client.getExercsisesOfModule(moduleExerciseId, new Callback<List<Exercise>>() {
            @Override
            public void success(List<Exercise> exercises, Response response) {
                if (getContext() != null) {
                    mExerciseAdapter = new ExerciseAdapter(getContext(), exercises);
                    spnExercise.setAdapter(mExerciseAdapter);
                /* module exercise and exercises where loaded so scores can be refreshed */
                    displayScores();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                ErrorHandler.handle(error, getContext());
            }
        });
    }

    private void displayDate(TextView tv, Calendar date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

        if (date == null) {
            tv.setText("--.--.----");
        } else {
            tv.setText(dateFormat.format(date.getTime()));
        }
    }

    private void displayScores() {

        long moduleExerciseId = mModuleExerciseAdapter
                                .getItem(spnModuleExercise.getSelectedItemPosition()).getId();
        long exercieId = mExerciseAdapter.getItem(spnExercise.getSelectedItemPosition()).getId();

        RestClient client = ServiceGenerator
                .createServiceUserToken(RestClient.class, LoginManager.getToken());
        client.getScoreOfUser(mUser.getId(), moduleExerciseId, exercieId, new Callback<List<Score>>() {
            @Override
            public void success(List<Score> scores, Response response) {
                if (getContext() != null) {
                    mScoreAdapter = new ScoreOfUserAdapter(getContext(), filterScore(scores));
                    mScoreListView.setAdapter(mScoreAdapter);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (getContext() != null) {
                    mScoreAdapter = new ScoreOfUserAdapter(getContext(), new ArrayList<Score>());
                    mScoreListView.setAdapter(mScoreAdapter);
                }
                ErrorHandler.handle(error, getContext());
            }
        });
    }

    private List<Score> filterScore(List<Score> scores) {
        List<Score> filteredScore = new ArrayList<>();

        for (Score s : scores) {

            if (mDateFrom != null) {
                if (s.getCreatedAt() < mDateFrom.getTime().getTime()) {
                    continue;
                }
            }

            if (mDateTo != null) {
                if (s.getCreatedAt() > mDateTo.getTime().getTime()) {
                    continue;
                }
            }


            if (s.getPercentage() > mScoreTo) {
                continue;
            }

            if (s.getPercentage() < mScoreFrom) {
                continue;
            }

            filteredScore.add(s);
        }

        return filteredScore;
    }
}


