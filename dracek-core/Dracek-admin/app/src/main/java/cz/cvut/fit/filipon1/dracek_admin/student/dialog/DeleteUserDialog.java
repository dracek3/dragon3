package cz.cvut.fit.filipon1.dracek_admin.student.dialog;

import android.app.AlertDialog;
import android.app.Dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

import cz.cvut.fit.filipon1.dracek_admin.LoginManager;
import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.utils.HashSHA256;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ApiConstants;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ErrorHandler;
import cz.cvut.fit.filipon1.dracek_admin.webservice.RestClient;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ServiceGenerator;
import cz.cvut.fit.filipon1.dracek_admin.webservice.response.LoginResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ondra on 21.2.16.
 */
public class DeleteUserDialog extends DialogFragment {

    private OnDeleteUser mListener;

    /**
     * The interface On delete user.
     */
    public interface OnDeleteUser {
        /**
         * On delete.
         */
        void onDelete();
    }

    /**
     * Sets listener.
     *
     * @param listener the listener
     */
    public void setListener(OnDeleteUser listener) {
        mListener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View dialogView = LayoutInflater.from(getContext())
                .inflate(R.layout.dialog_delete_student, null);
        final EditText etUsername = (EditText) dialogView.findViewById(R.id.et_username);
        final EditText etPassword = (EditText) dialogView.findViewById(R.id.et_password);
        View btnCancel = dialogView.findViewById(R.id.btn_cancel);
        View btnDelete = dialogView.findViewById(R.id.btn_delete);

        builder.setView(dialogView);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = etUsername.getText().toString();
                String password = etPassword.getText().toString();


                if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
                    return;
                }

                checkAuthorizationAndDeleteUSer(username, password);
            }
        });

        return builder.create();
    }

    private void checkAuthorizationAndDeleteUSer(String username, String password) {
        final RestClient client = ServiceGenerator.createServiceAppToken(RestClient.class, "abc");
        HashMap<String, Object> body = new HashMap<>();
        body.put(ApiConstants.USER_USERNAME, username);

        try {
            body.put(ApiConstants.USER_PASSWORD, HashSHA256.hash(password));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        client.login(body, new Callback<LoginResponse>() {
            @Override
            public void success(LoginResponse loginResponse, Response response) {

                if (mListener != null) {
                    mListener.onDelete();
                }
                dismiss();
            }

            @Override
            public void failure(RetrofitError error) {
                ErrorHandler.handle(error, getContext());
            }
        });
    }


}
