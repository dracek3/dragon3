package cz.cvut.fit.filipon1.dracek_admin.student.dialog;


import android.app.AlertDialog;
import android.app.Dialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.NumberPicker;

/**
 * Created by ondra on 23.2.16.
 */
public class NumberPickerDialog extends DialogFragment {

    private static final String VALUE = "value";

    private OnNumberSelectedListener mListener;

    /**
     * The interface On number selected listener.
     */
    public interface OnNumberSelectedListener {
        /**
         * On selected.
         *
         * @param number the number
         */
        void onSelected(int number);
    }

    /**
     * Sets listener.
     *
     * @param listener the listener
     */
    public void setListener(OnNumberSelectedListener listener) {
        mListener = listener;
    }

    /**
     * New instance number picker dialog.
     *
     * @param value the value
     * @return the number picker dialog
     */
    public static NumberPickerDialog newInstance(int value) {

        Bundle args = new Bundle();
        args.putInt(VALUE, value);
        NumberPickerDialog fragment = new NumberPickerDialog();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final NumberPicker np = new NumberPicker(getContext());
        np.setMaxValue(100);
        np.setMinValue(0);
        np.setValue(getArguments().getInt(VALUE, 50));
        builder.setView(np);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (mListener != null) {
                    mListener.onSelected(np.getValue());
                }
            }
        });

        return builder.create();
    }
}
