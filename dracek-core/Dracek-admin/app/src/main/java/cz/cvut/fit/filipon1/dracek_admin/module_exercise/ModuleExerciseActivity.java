package cz.cvut.fit.filipon1.dracek_admin.module_exercise;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import cz.cvut.fit.filipon1.dracek_admin.BaseDrawerActivity;
import cz.cvut.fit.filipon1.dracek_admin.DataCache;
import cz.cvut.fit.filipon1.dracek_admin.LoginManager;
import cz.cvut.fit.filipon1.dracek_admin.R;
import cz.cvut.fit.filipon1.dracek_admin.klass.ClassSimpleArrayAdapter;
import cz.cvut.fit.filipon1.dracek_admin.model.Klass;
import cz.cvut.fit.filipon1.dracek_admin.model.ModuleExercise;
import cz.cvut.fit.filipon1.dracek_admin.model.User;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.ModuleExerciseAdapter;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.StudentArrayAdapter;
import cz.cvut.fit.filipon1.dracek_admin.student.adapter.StudentPagerAdapter;
import cz.cvut.fit.filipon1.dracek_admin.student.ui.StudentDetailFragment;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ErrorHandler;
import cz.cvut.fit.filipon1.dracek_admin.webservice.RestClient;
import cz.cvut.fit.filipon1.dracek_admin.webservice.ServiceGenerator;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ondra on 21.2.16.
 */
public class ModuleExerciseActivity extends BaseDrawerActivity {

    private ListView mListView;
    private ModuleExerciseAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_module_exercise);

        mListView = (ListView) findViewById(R.id.lv_module_exercise);


        createDrawer();

        loadModuleExercises();

        setListeners();

        mListView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

    }

    private void setListeners() {

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ModuleExercise moduleExercise = mAdapter.getItem(position);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_fragment_container,
                                    ModuleExerciseDetailFragment.newInstance(moduleExercise))
                        .commit();
            }
        });

    }

    private void loadModuleExercises() {
        RestClient client = ServiceGenerator
                .createServiceUserToken(RestClient.class, LoginManager.getToken());
        client.getModuleExerciseList(new Callback<List<ModuleExercise>>() {
            @Override
            public void success(List<ModuleExercise> moduleExercises, Response response) {
                mAdapter = new ModuleExerciseAdapter(ModuleExerciseActivity.this, moduleExercises);
                mListView.setAdapter(mAdapter);
            }

            @Override
            public void failure(RetrofitError error) {
                ErrorHandler.handle(error, ModuleExerciseActivity.this);
            }
        });
    }

}
