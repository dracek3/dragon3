package cz.cvut.fit.filipon1.dracek_admin.test;

import com.robotium.solo.*;
import android.test.ActivityInstrumentationTestCase2;


@SuppressWarnings("rawtypes")
public class asd extends ActivityInstrumentationTestCase2 {
  	private Solo solo;
  	
  	private static final String LAUNCHER_ACTIVITY_FULL_CLASSNAME = "cz.cvut.fit.filipon1.dracek_admin.DummyCore";

    private static Class<?> launcherActivityClass;
    static{
        try {
            launcherActivityClass = Class.forName(LAUNCHER_ACTIVITY_FULL_CLASSNAME);
        } catch (ClassNotFoundException e) {
           throw new RuntimeException(e);
        }
    }
  	
  	@SuppressWarnings("unchecked")
    public asd() throws ClassNotFoundException {
        super(launcherActivityClass);
    }

  	public void setUp() throws Exception {
        super.setUp();
		solo = new Solo(getInstrumentation());
		getActivity();
  	}
  
   	@Override
   	public void tearDown() throws Exception {
        solo.finishOpenedActivities();
        super.tearDown();
  	}
  
	public void testRun() {
        //Wait for activity: 'cz.cvut.fit.filipon1.dracek_admin.DummyCore'
		solo.waitForActivity("DummyCore", 2000);
        //Click on login
		solo.clickOnView(solo.getView("btn_login"));
        //Wait for activity: 'cz.cvut.fit.filipon1.dracek_admin.student.ui.StudentActivity'
		assertTrue("StudentActivity is not found!", solo.waitForActivity("StudentActivity"));
        //Click on ImageView
		solo.clickOnView(solo.getView(android.widget.ImageButton.class, 0));
        //Click on Třídy
		solo.clickOnView(solo.getView(android.R.id.text1, 1));
        //Wait for activity: 'cz.cvut.fit.filipon1.dracek_admin.klass.ClassActivity'
		assertTrue("ClassActivity is not found!", solo.waitForActivity("ClassActivity"));
        //Click on 5.A AppCompatSpinner
		solo.clickInList(1, 0);
        //Click on ImageView
		solo.clickOnView(solo.getView("iv_delete", 1));
        //Click on 5.B AppCompatSpinner
		solo.clickInList(2, 0);
        //Click on 5.A AppCompatSpinner
		solo.clickInList(1, 0);
        //Click on AppCompatSpinner
		solo.clickOnView(solo.getView("spn_options"));
        //Click on Přidat studenta
		solo.clickOnView(solo.getView(android.R.id.text1, 1));
        //Wait for dialog
		solo.waitForDialogToOpen(5000);
        //Click on Karel IV. (KIV.)
		solo.clickInList(3, 0);
        //Click on 5.A AppCompatSpinner
		solo.clickInList(1, 0);
        //Click on Karel IV. (KIV.)
		solo.clickInList(2, 1);
	}
}
