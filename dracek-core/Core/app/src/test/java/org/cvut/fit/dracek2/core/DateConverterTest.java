package org.cvut.fit.dracek2.core;

import junit.framework.Assert;

import org.cvut.fit.dracek2.core.util.DateConverter;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class DateConverterTest {

    @Test
    public void converting_from_unix() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd. MM. yyyy");

        Date date = DateConverter.fromUnixToDate("1460231666000");
        Assert.assertEquals(sdf.format(date), "09. 04. 2016");
        String dateStr = DateConverter.fromUnixToDateString("1460231666000");
        Assert.assertEquals(dateStr, "09. 04. 2016");
    }
}