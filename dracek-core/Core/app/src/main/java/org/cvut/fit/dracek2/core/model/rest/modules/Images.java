package org.cvut.fit.dracek2.core.model.rest.modules;

/**
 * Created by Administrator on 14.11.2016.
 */
public class Images {

    private String image;
    private String originalName;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }
}
