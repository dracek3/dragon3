package org.cvut.fit.dracek2.core.model.rest.results;

/**
 * Created by Administrator on 10.2.2016.
 */
public class AllResults {

    private String id;
    private String idModuleExcercise;
    private String idExcercise;
    private String percentage;
    private String totalTime;
    private String file;
    private String finished;
    private String finishedAsTask;
    private String createdAt;
    private boolean isOfflineSaved = false;

    public AllResults(String id, String moduleExcerciseId, String excerciseId, String percentage, String totalTime, String file, String finished, String finishedAsTask, String createdAt) {
        this.id = id;
        this.idModuleExcercise = moduleExcerciseId;
        this.idExcercise = excerciseId;
        this.percentage = percentage;
        this.totalTime = totalTime;
        this.file = file;
        this.finished = finished;
        this.finishedAsTask = finishedAsTask;
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModuleExcerciseId() {
        return idModuleExcercise;
    }

    public void setModuleExcerciseId(String moduleExcerciseId) {
        this.idModuleExcercise = moduleExcerciseId;
    }

    public String getIdExcercise() {
        return idExcercise;
    }

    public void setIdExcercise(String idExcercise) {
        this.idExcercise = idExcercise;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(String totalTime) {
        this.totalTime = totalTime;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getFinished() {
        return finished;
    }

    public void setFinished(String finished) {
        this.finished = finished;
    }

    public String getFinishedAsTask() {
        return finishedAsTask;
    }

    public void setFinishedAsTask(String finishedAsTask) {
        this.finishedAsTask = finishedAsTask;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public boolean isOfflineSaved() {
        return isOfflineSaved;
    }

    public void setOfflineSaved(boolean offlineSaved) {
        isOfflineSaved = offlineSaved;
    }


}
