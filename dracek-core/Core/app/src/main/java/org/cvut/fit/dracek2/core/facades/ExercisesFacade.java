package org.cvut.fit.dracek2.core.facades;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.FileProvider;
import android.util.Log;

import org.cvut.fit.dracek2.core.activity.DragonModuleStartingActivity;
import org.cvut.fit.dracek2.core.activity.ExercisesActivity;
import org.cvut.fit.dracek2.core.model.rest.modules.AllExercisesResult;
import org.cvut.fit.dracek2.core.model.rest.modules.AllowedModulesResult;
import org.cvut.fit.dracek2.core.model.rest.results.NewResult;
import org.cvut.fit.dracek2.core.offline.OfflineResourceManager;
import org.cvut.fit.dracek2.core.rest.RestApiGenerator;
import org.cvut.fit.dracek2.core.rest.tasks.GetExercisesTaskSynchronous;
import org.cvut.fit.dracek2.core.rest.tasks.SendResultTask;
import org.cvut.fit.dracek2.core.util.ResourceDownloader;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import static org.cvut.fit.dracek2.core.R.id.exercise;

/**
 * Created by Administrator on 4.1.2016.
 */
public class ExercisesFacade {

    private static ExercisesActivity activity;
    private static int lastExerciseId;


    private static String testZadani = "<?xml version='1.0' encoding='UTF-8' standalone='no' ?>\n" +
            "<Exercise>\n" +
            "    <SingleExercise>\n" +
            "        <Id>1</Id>\n" +
            "        <Name>Cvičení 1</Name>\n" +
            "        <Task>Přesuň do koše obrázky, které se do řady nehodí</Task>\n" +
            "        <Images>\n" +
            "            <Image>\n" +
            "                <ImageId>1</ImageId>\n" +
            "                <Special>false</Special>\n" +
            "            </Image>\n" +
            "            <Image>\n" +
            "                <ImageId>1</ImageId>\n" +
            "                <Special>false</Special>\n" +
            "            </Image>\n" +
            "            <Image>\n" +
            "                <ImageId>1</ImageId>\n" +
            "                <Special>false</Special>\n" +
            "            </Image>\n" +
            "            <Image>\n" +
            "                <ImageId>2</ImageId>\n" +
            "                <Special>true</Special>\n" +
            "            </Image>\n" +
            "            <Image>\n" +
            "                <ImageId>1</ImageId>\n" +
            "                <Special>false</Special>\n" +
            "            </Image>\n" +
            "            <Image>\n" +
            "                <ImageId>1</ImageId>\n" +
            "                <Special>false</Special>\n" +
            "            </Image>\n" +
            "        </Images>\n" +
            "    </SingleExercise>\n" +
            "</Exercise>";

    private ExercisesFacade() {
    }

    public static void init(ExercisesActivity exActivity) {
        activity = exActivity;
    }

//    public static void resultAllExercises(List<AllExercisesResult> results) {
//        Map<Integer, AllExercisesResult> tmpExercises = new ConcurrentHashMap<>();
//        for (AllExercisesResult result : results) {
//            tmpExercises.put(Integer.parseInt(result.getId()), result);
//        }
//        cachedExercises = tmpExercises;
//    }

    public static AllExercisesResult getExerciseInfo(Integer id) {
        return OfflineResourceManager.getInstance().getExercise(id);
    }

    public static List<AllExercisesResult> getAllExerciseInfoByModuleId(Integer id) {
        List<AllExercisesResult> result = new ArrayList<>();
        for (AllExercisesResult r : OfflineResourceManager.getInstance().getExercises()) {
            if (r.getModuleExcerciseId().equals(Integer.toString(id))) {
                result.add(r);
            }
        }
        return result;
    }

    public static void requestAllExercisesSynchronous() {
        new GetExercisesTaskSynchronous().execute();
    }

    public static int getLastExerciseId() {
        return lastExerciseId;
    }

    public static void setLastExerciseId(int lastExerciseId) {
        ExercisesFacade.lastExerciseId = lastExerciseId;
    }

    public static Intent getIntentForExercise(Context context, int moduleId, int exerciseId) {
        Intent launchIntent = new Intent();
        AllowedModulesResult moduleInfo = ModuleExercisesFacade.getModuleInfo(moduleId);


        launchIntent.setComponent(new ComponentName(moduleInfo.getPackage(), moduleInfo.getPackage_name_excercise()));

        // TODO až bude zadání nahrané na serveru, je třeba odkomentovat tento a zakomentovat další řádek, zatím jsem nedostal
        // na server přístup. Nezapomenout odstranit testZadani promennou.
        launchIntent.putExtra("EXERCISE_FILE", downloadExerciseXml(exerciseId));
        //launchIntent.putExtra("EXERCISE_FILE", testZadani);
        setLastExerciseId(exerciseId);

        ArrayList<String> fileUris = new ArrayList<>();
        File imageFolder = new ResourceDownloader().getExerciseImageFolder(context, exerciseId);
        File[] exerciseImages = new File[0];
        if (imageFolder != null) {
            exerciseImages = imageFolder.listFiles();
        }
        if(exerciseImages == null) exerciseImages = new File[0];
        for (File exerciseImage : exerciseImages) {
            Uri uriForFile = FileProvider.getUriForFile(
                    activity,
                    "org.cvut.fit.dracek2.core.CoreFileProvider",
                    exerciseImage);
            fileUris.add(uriForFile.toString());
            context.grantUriPermission(moduleInfo.getPackage(), uriForFile, Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        launchIntent.putStringArrayListExtra("IMAGES", fileUris);
        launchIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        return launchIntent;
    }

    public static void sendOrSaveResult(int moduleId, String result, boolean asTask, DragonModuleStartingActivity caller) throws ParserConfigurationException, IOException, SAXException {
        Document resultXml = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(result.getBytes()));
        NewResult newResult = new NewResult();

        NodeList scoreList = resultXml.getElementsByTagName("Score");
        String score = scoreList.item(0).getTextContent();
        score = score.substring(0, score.indexOf("%"));
        newResult.setPercentage(score);

        NodeList timeList = resultXml.getElementsByTagName("Time");
        String time = timeList.item(0).getTextContent();
        time = time.substring(0, time.indexOf("s"));
        newResult.setTotalTime(time);

        newResult.setCreatedAt(Long.toString(System.currentTimeMillis()));
        newResult.setDoneAsTask(asTask);
        newResult.setExcerciseId(Integer.toString(ExercisesFacade.getLastExerciseId()));
        newResult.setModuleExcerciseId(Integer.toString(moduleId));
        newResult.setFinished("true");
        newResult.setFile(result);
        new SendResultTask().execute(newResult, caller);
    }

    private static String downloadExerciseXml(int exerciseId) {
        String xml="";
        try {
            // Create a URL for the desired page
            URL url = new URL(RestApiGenerator.BASE_URL + getExerciseInfo(exerciseId).getFile());

            // Read all the text returned by the server
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            String str;

            while ((str = in.readLine()) != null) {
                xml += str;
            }
            in.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return xml;
    }
}
