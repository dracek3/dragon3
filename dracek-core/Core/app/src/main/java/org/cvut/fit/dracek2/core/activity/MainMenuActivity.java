package org.cvut.fit.dracek2.core.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import org.cvut.fit.dracek2.core.R;
import org.cvut.fit.dracek2.core.facades.LoggingFacade;
import org.cvut.fit.dracek2.core.facades.MainMenuFacade;
import org.cvut.fit.dracek2.core.model.LoggedUser;
import org.cvut.fit.dracek2.core.offline.InternetConnectionChecker;
import org.cvut.fit.dracek2.core.offline.OfflineResultsChecker;

public class MainMenuActivity extends DragonActivity {

    private static MainMenuActivity instance;

    private ImageButton exercisesBtn;
    private ImageButton resultsBtn;
    private ImageButton tasksBtn;
    private ImageButton achievementsBtn;

    private ImageButton infoBtn;
    private ImageButton logoutBtn;
    private ImageButton refreshBtn;

    private TextView nameLbl;
    private TextView modeLbl;

    private boolean firstStart = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        initComponents();
        setListeners();
        instance = this;
        MainMenuFacade.init(this);
        MainMenuFacade.changeStudentName(LoggedUser.getFullName());
        OfflineResultsChecker.start();
    }

    @Override
    protected void onStart() {
        super.onStart();
        String logginError = getIntent().getStringExtra("error");
        if (logginError != null) {
            showMessageWindow("Nastal problém", logginError);
        }
        if (firstStart) {
            refreshBtn.callOnClick();
            firstStart = false;
        }
        if (!InternetConnectionChecker.isOnline()) {
            modeLbl.setVisibility(View.VISIBLE);
        } else {
            modeLbl.setVisibility(View.INVISIBLE);
        }
    }

    private void initComponents() {
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.hide();

        exercisesBtn = (ImageButton) findViewById(R.id.cviceni_button);
        resultsBtn = (ImageButton) findViewById(R.id.vysledky_button);
        tasksBtn = (ImageButton) findViewById(R.id.ukoly_button);
        achievementsBtn = (ImageButton) findViewById(R.id.uspechy_button);
        nameLbl = (TextView) findViewById(R.id.student_name_label);
        modeLbl = (TextView) findViewById(R.id.mode_label);
        infoBtn = (ImageButton) findViewById(R.id.info_button);
        logoutBtn = (ImageButton) findViewById(R.id.off_button);
        refreshBtn = (ImageButton) findViewById(R.id.refresh_button);
    }

    private void setListeners() {
        getExercisesBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainMenuFacade.openExercises();
            }
        });

        getResultsBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainMenuFacade.openResults();
            }
        });

        getTasksBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainMenuFacade.openTasks();
            }
        });

        getAchievementsBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainMenuFacade.openAchievements();
            }
        });

        getLogoutBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(MainMenuActivity.getInstance())
                        .setTitle("Odhlášení")
                        .setMessage("Opravdu se chcete odhlásit z Dráčka? Pro přihlášení bude třeba opětovné zadání jména a hesla a připojení k internetu!")
                        .setPositiveButton("Odhlásit", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                LoggingFacade.deleteToken();
                                MainMenuFacade.logout();
                            }
                        })
                        .setNegativeButton("Zpět do Dráčka", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Do nothing.
                            }
                        })
                        .show();
            }
        });

        getInfoBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMessageWindow("Hlavní menu", "Výtejte v aplikací Dráček! Nacházíte se v hlavním menu, kde si můžete vybrat mezi navigací do výukových modulů, zobrazením výsledků, " +
                        "zkontrolováním a spuštěním zadaných úkolú a prohlídnutím dosažených úspěchů.");
            }
        });

        getRefreshBtn().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (InternetConnectionChecker.isOnline()) {
                    modeLbl.setVisibility(View.INVISIBLE);
                } else {
                    showMessageWindow("Offline", "Aplikace běží v offline módu, dostupné informace mohou být neaktuální");
                    modeLbl.setVisibility(View.VISIBLE);
                }
                MainMenuFacade.getResourcesAfterStart();
            }
        });
    }

    public ImageButton getRefreshBtn() {
        return refreshBtn;
    }

    public static MainMenuActivity getInstance() {
        return instance;
    }

    public ImageButton getExercisesBtn() {
        return exercisesBtn;
    }

    public ImageButton getResultsBtn() {
        return resultsBtn;
    }

    public ImageButton getTasksBtn() {
        return tasksBtn;
    }

    public ImageButton getAchievementsBtn() {
        return achievementsBtn;
    }

    public TextView getNameLbl() {
        return nameLbl;
    }

    public ImageButton getInfoBtn() {
        return infoBtn;
    }

    public ImageButton getLogoutBtn() {
        return logoutBtn;
    }

    @Override
    public void showMessageWindow(String title, String message) {
        new AlertDialog.Builder(MainMenuActivity.getInstance())
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", null)
                .setCancelable(false)
                .show();
    }

}
