package org.cvut.fit.dracek2.core.model.rest.tasks;

import org.cvut.fit.dracek2.core.model.rest.modules.AllowedModulesResult;

/**
 * Created by Administrator on 18.10.2016.
 */

public class AllTasks {

    private String id;
    private String minimalScore;
    private String endingDate;
    private boolean loaded;
    private TaskScore score;
    private UserCreator userCreator;
    private AllowedModulesResult modlueExcercise;
    private TaskExercise excercise;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMinimalScore() {
        return minimalScore;
    }

    public void setMinimalScore(String minimalScore) {
        this.minimalScore = minimalScore;
    }

    public String getEndingDate() {
        return endingDate;
    }

    public void setEndingDate(String endingDate) {
        this.endingDate = endingDate;
    }

    public boolean isLoaded() {
        return loaded;
    }

    public void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }

    public UserCreator getUserCreator() {
        return userCreator;
    }

    public void setUserCreator(UserCreator userCreator) {
        this.userCreator = userCreator;
    }

    public TaskExercise getExcercise() {
        return excercise;
    }

    public void setExcercise(TaskExercise excercise) {
        this.excercise = excercise;
    }

    public AllowedModulesResult getModlueExcercise() {
        return modlueExcercise;
    }

    public void setModlueExcercise(AllowedModulesResult modlueExcercise) {
        this.modlueExcercise = modlueExcercise;
    }

    public TaskScore getScore() {
        return score;
    }

    public void setScore(TaskScore score) {
        this.score = score;
    }
}
