package org.cvut.fit.dracek2.core.facades;

import android.content.Intent;

import org.cvut.fit.dracek2.core.activity.MainMenuActivity;
import org.cvut.fit.dracek2.core.activity.ModuleExercisesActivity;
import org.cvut.fit.dracek2.core.activity.ResultsActivity;
import org.cvut.fit.dracek2.core.activity.TasksActivity;
import org.cvut.fit.dracek2.core.offline.OfflineResourceManager;

/**
 * Created by Administrator on 30.12.2015.
 */
public class MainMenuFacade {

    private static MainMenuActivity activity;

    private MainMenuFacade() {
    }

    public static void init(MainMenuActivity mainActivity) {
        activity = mainActivity;
    }

    public static void getResourcesAfterStart() {
        OfflineResourceManager.getInstance().getOrSendAllInfo(activity);
    }

    public static void changeStudentName(String name) {
        activity.getNameLbl().setText(name);
    }

    public static void openTasks() {
        Intent intent = new Intent(activity, TasksActivity.class);
        activity.startActivity(intent);
    }

    public static void openResults() {
        Intent intent = new Intent(activity, ResultsActivity.class);
        activity.startActivity(intent);
    }

    public static void openAchievements() {

    }

    public static void openExercises() {
        Intent intent = new Intent(activity, ModuleExercisesActivity.class);
        activity.startActivity(intent);
    }

    public static void logout() {

        activity.finish();
        System.exit(0);
    }

    public static MainMenuActivity getActivity() {
        return activity;
    }
}
