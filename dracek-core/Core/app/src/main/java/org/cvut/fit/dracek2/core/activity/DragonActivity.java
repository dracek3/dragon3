package org.cvut.fit.dracek2.core.activity;

import android.app.Activity;
import android.app.ProgressDialog;

/**
 * Created by Administrator on 1.11.2016.
 */

public abstract class DragonActivity extends Activity {

    public abstract void showMessageWindow(String title, String message);

    private ProgressDialog progresDialog;


    public void showWaitWindow(String message) {
        progresDialog = ProgressDialog.show(this, "Vyčkejte prosím", message);
    }

    public void dismissWaitWindow() {
        if (progresDialog != null && progresDialog.isShowing()) {
            progresDialog.dismiss();
        }
    }
}
