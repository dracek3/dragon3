package org.cvut.fit.dracek2.core.model.rest.logging;

/**
 * Created by Administrator on 9.2.2016.
 */
public class Token {

    private String token;
    private String expiration;

    public Token(String token, String expiration) {
        this.token = token;
        this.expiration = expiration;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

}
