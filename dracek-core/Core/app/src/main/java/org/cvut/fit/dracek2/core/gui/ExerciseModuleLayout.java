package org.cvut.fit.dracek2.core.gui;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.cvut.fit.dracek2.core.R;
import org.cvut.fit.dracek2.core.activity.ModuleExercisesActivity;

/**
 * Created by Administrator on 31.12.2015.
 */
public class ExerciseModuleLayout {

    private ViewGroup layout;
    private TextView moduleName;

    public ExerciseModuleLayout(ViewGroup layout) {
        this.layout = layout;
        moduleName = (TextView) layout.findViewById(R.id.module_name);
    }

    public void setImageBitmap(Bitmap bitmap) {
        ((ImageButton) layout.findViewById(R.id.module_image)).setImageBitmap(bitmap);
    }

    public void setImageListener(View.OnClickListener listener) {
        layout.findViewById(R.id.module_image).setOnClickListener(listener);
    }

    public void setModuleName(String name) {
        moduleName.setText(name);
    }

    public ViewGroup getLayout() {
        return layout;
    }

    public static ExerciseModuleLayout create(ViewGroup parent) {
        LinearLayout inflatedView = (LinearLayout) LayoutInflater.from(ModuleExercisesActivity.getInstance()).inflate(R.layout.exercise_module_layout, parent);
        ExerciseModuleLayout duplicate = new ExerciseModuleLayout((ViewGroup) inflatedView.getChildAt(inflatedView.getChildCount() - 1));
        return duplicate;
    }
}
