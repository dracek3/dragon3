package org.cvut.fit.dracek2.core.rest.tasks;

import org.cvut.fit.dracek2.core.model.rest.results.NewResult;
import org.cvut.fit.dracek2.core.rest.RestApiGenerator;
import org.cvut.fit.dracek2.core.rest.api.RestResultsApi;

import java.io.File;

import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Administrator on 10.2.2016.
 */
public class SendResultForCheckerTask {


    public void execute(final NewResult result, String token, final File onSuccessDelete) {
        RestApiGenerator.createApi(RestResultsApi.class, token).sendResult(result).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Response<Void> response) {
                onSuccessDelete.delete();
            }

            @Override
            public void onFailure(Throwable t) {
            }
        });
    }
}
