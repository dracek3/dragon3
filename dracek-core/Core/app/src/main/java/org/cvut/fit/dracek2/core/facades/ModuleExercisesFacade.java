package org.cvut.fit.dracek2.core.facades;

import org.cvut.fit.dracek2.core.activity.ModuleExercisesActivity;
import org.cvut.fit.dracek2.core.gui.ExerciseModuleLayout;
import org.cvut.fit.dracek2.core.model.rest.modules.AllowedModulesResult;
import org.cvut.fit.dracek2.core.offline.OfflineResourceManager;
import org.cvut.fit.dracek2.core.rest.tasks.GetModulesTaskSynchronous;
import org.cvut.fit.dracek2.core.util.ResourceDownloader;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Administrator on 30.12.2015.
 */
public class ModuleExercisesFacade {

    private static ModuleExercisesActivity activity;


    private ModuleExercisesFacade() {
    }

    public static void init(ModuleExercisesActivity moduleExeriseActivity) {
        activity = moduleExeriseActivity;
    }

    public static void requestAllowedModules() {
        new GetModulesTaskSynchronous().execute();
        resultAllowedModules(OfflineResourceManager.getInstance().getModules());
    }

    public static void resultAllowedModules(List<AllowedModulesResult> results) {
        Map<Integer, AllowedModulesResult> tmpModules = new ConcurrentHashMap<>();
        for (AllowedModulesResult result : results) {
            tmpModules.put(Integer.parseInt(result.getId()), result);
        }
        if (activity != null) {
            activity.showModules(tmpModules);
        }
    }


    public static AllowedModulesResult getModuleInfo(Integer id) {
        return OfflineResourceManager.getInstance().getModule(id);
    }

    public static void requestFailed() {
    }

    public static void downloadModuleImage(AllowedModulesResult allowedModulesResult, ExerciseModuleLayout exerciseModuleLayout) {
        new ResourceDownloader().downloadAndSetModuleImage(activity, allowedModulesResult, exerciseModuleLayout);
    }

}
