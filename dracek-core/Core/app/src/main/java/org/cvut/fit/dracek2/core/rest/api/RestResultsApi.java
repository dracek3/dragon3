package org.cvut.fit.dracek2.core.rest.api;

import org.cvut.fit.dracek2.core.model.rest.results.AllResults;
import org.cvut.fit.dracek2.core.model.rest.results.NewResult;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Administrator on 10.2.2016.
 */
public interface RestResultsApi {

    @GET("score")
    public Call<List<AllResults>> getAllResults();

    @POST("score")
    public Call<Void> sendResult(@Body NewResult result);

}
