package org.cvut.fit.dracek2.core.model.rest.modules;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 10.2.2016.
 */
public class AllExercisesResult {

    private String id;
    private String name;
    private String description;
    private String file;
    private String deleted;
    private String version;
    private String depends;
    private String moduleExcerciseId;
    private List<Images> images;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDepends() {
        return depends;
    }

    public void setDepends(String depends) {
        this.depends = depends;
    }

    public String getModuleExcerciseId() {
        return moduleExcerciseId;
    }

    public void setModuleExcerciseId(String moduleExcerciseId) {
        this.moduleExcerciseId = moduleExcerciseId;
    }

    public List<Images> getImages() {
        if(images != null)
        return images;
        return new ArrayList<>();
    }

    public void setImages(List<Images> images) {
        this.images = images;
    }
}
