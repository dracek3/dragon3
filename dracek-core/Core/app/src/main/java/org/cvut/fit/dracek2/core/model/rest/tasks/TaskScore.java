package org.cvut.fit.dracek2.core.model.rest.tasks;

/**
 * Created by Administrator on 14.11.2016.
 */

public class TaskScore {

    private String idScore;
    private String percentage;
    private String totalTime;
    private String file;

    public String getIdScore() {
        return idScore;
    }

    public void setIdScore(String idScore) {
        this.idScore = idScore;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(String totalTime) {
        this.totalTime = totalTime;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
