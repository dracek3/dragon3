package org.cvut.fit.dracek2.core.facades;

import org.cvut.fit.dracek2.core.activity.ResultsActivity;
import org.cvut.fit.dracek2.core.model.rest.modules.AllowedModulesResult;
import org.cvut.fit.dracek2.core.model.rest.results.AllResults;
import org.cvut.fit.dracek2.core.offline.OfflineResourceManager;
import org.cvut.fit.dracek2.core.rest.tasks.GetResultsTaskSynchronous;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Administrator on 4.1.2016.
 */
public class ResultsFacade {

    private static ResultsActivity activity;

    public static void init(ResultsActivity resultsActivity) {
        activity = resultsActivity;
    }

    public static void requestResults() {
        new GetResultsTaskSynchronous().execute();
        showResults(OfflineResourceManager.getInstance().getResults());
    }

    public static void showResults(List<AllResults> results) {
        Map<String, List<AllResults>> tmpResults = new ConcurrentHashMap<>();
        for (AllResults result : results) {
            AllowedModulesResult moduleInfo = ModuleExercisesFacade.getModuleInfo(Integer.parseInt(result.getModuleExcerciseId()));
            if (moduleInfo == null) {
                continue;
            }
            List<AllResults> specificResults = tmpResults.get(moduleInfo.getName());
            if (specificResults == null) {
                specificResults = new ArrayList<>();
                specificResults.add(result);
                tmpResults.put(moduleInfo.getName(), specificResults);

            } else {
                specificResults.add(result);
            }
        }

        activity.showResults(tmpResults);
    }

    public static void requestFailed() {

    }

}
