package org.cvut.fit.dracek2.core.model.rest.logging;

/**
 * Created by Administrator on 9.2.2016.
 */
public class LogInRequest {

    private String username;
    private String password;

    public LogInRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
