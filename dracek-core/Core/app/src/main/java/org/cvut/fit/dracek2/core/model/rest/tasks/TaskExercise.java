package org.cvut.fit.dracek2.core.model.rest.tasks;

/**
 * Created by Administrator on 14.11.2016.
 */

public class TaskExercise {

    private String id;
    private String name;
    private String description;
    private String file;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
