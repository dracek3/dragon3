package org.cvut.fit.dracek2.core.exception;

/**
 * Created by Administrator on 30.12.2015.
 */
public class ImageDownloadException extends Exception {
    public ImageDownloadException(String s) {
        super(s);
    }
}
