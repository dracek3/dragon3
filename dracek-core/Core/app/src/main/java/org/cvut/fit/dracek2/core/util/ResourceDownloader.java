package org.cvut.fit.dracek2.core.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;

import org.cvut.fit.dracek2.core.activity.DragonActivity;
import org.cvut.fit.dracek2.core.gui.ExerciseModuleLayout;
import org.cvut.fit.dracek2.core.model.LoggedUser;
import org.cvut.fit.dracek2.core.model.rest.modules.AllExercisesResult;
import org.cvut.fit.dracek2.core.model.rest.modules.AllowedModulesResult;
import org.cvut.fit.dracek2.core.model.rest.modules.Images;
import org.cvut.fit.dracek2.core.rest.RestApiGenerator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

/**
 * Created by Administrator on 4.10.2016.
 */

public class ResourceDownloader {

    private static final String MODULE_IMAGES = "module_images/";
    private static final String EXERCISE_IMAGES = "exercise_images/";

    /**
     * Checks whether an image of Module with a given id is already downloaded
     */
    private boolean isModuleImageAlreadyDownloaded(Context context, int id) {
        return getModuleImageOffline(context, id).exists();
    }

    /**
     * Gets the path to the module image with a given id
     */
    private File getModuleImageOffline(Context context, int id) {
        return new File(context.getFilesDir(), MODULE_IMAGES + LoggedUser.getUserId() + "/modules/images/" + id);
    }

    public File getExerciseImageFolder(Context context, int id) {
        return new File(context.getFilesDir(), EXERCISE_IMAGES + "images/" + id);
    }

    /**
     * Checks whether the image already exists, if yes it is used in Module Activity. Afterwards always downloads an image by
     * the given URL and sets it into a correct module in a the Module Activity, if the download fails, image is not updated.
     * (In the offline mode is therefore used image, that was downloaded previously.)
     *
     * @param context
     * @param moduleInfo
     * @param relatedImageLayout
     */
    public void downloadAndSetModuleImage(final Context context, AllowedModulesResult moduleInfo, final ExerciseModuleLayout relatedImageLayout) {
        final int moduleId = Integer.parseInt(moduleInfo.getId());

        if (isModuleImageAlreadyDownloaded(context, moduleId)) {
            File image = getModuleImageOffline(context, moduleId);
            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream(new FileInputStream(image));
            } catch (FileNotFoundException e) {
                // This should never happen, file existance is checked in previous step.
                e.printStackTrace();
                return;
            }
            relatedImageLayout.setImageBitmap(bitmap);
        }

        downloadModuleImage(context, moduleInfo);
    }

    public void downloadModuleImage(final Context context, AllowedModulesResult moduleInfo) {
        final int moduleId = Integer.parseInt(moduleInfo.getId());
        if (moduleInfo.getImage() == null) {
            return;
        }
        String url = RestApiGenerator.BASE_URL + moduleInfo.getImage();

        ImageRequest imgRequest = new ImageRequest(url, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                File imageToBeSaved = getModuleImageOffline(context, moduleId);
                if (!imageToBeSaved.getParentFile().exists()) {
                    imageToBeSaved.getParentFile().mkdirs();
                }
                try {
                    response.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(imageToBeSaved));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }, 0, 0, ImageView.ScaleType.CENTER_CROP, Bitmap.Config.ARGB_8888,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        System.out.println(error);
                        // do nothing, default image will stay in place
                    }
                });
        MyRequestSingleton.getInstance(context).addToRequestQueue(imgRequest);
    }

    public void downloadImageForExercise(final DragonActivity context, AllExercisesResult exercise) {
        final int exerciseId = Integer.parseInt(exercise.getId());

        final List<Images> images = exercise.getImages();


        for (final Images image : images) {
            final String url = image.getImage();
            String restUrl = RestApiGenerator.BASE_URL + url;
            restUrl = restUrl.replace("//www", "/www");
            final File exerciseDir = getExerciseImageFolder(context, exerciseId);
            if (!exerciseDir.exists()) {
                exerciseDir.mkdirs();
            }

            ImageRequest imgRequest = new ImageRequest(restUrl, new Response.Listener<Bitmap>() {
                @Override
                public void onResponse(Bitmap response) {
                    try {
                        String imageName = image.getOriginalName();

                        String imageExtension = url.substring(url.lastIndexOf("."));
                        Bitmap.CompressFormat compressFormat;
                        if (imageExtension.equalsIgnoreCase("jpg") || imageExtension.equalsIgnoreCase("jpeg")) {
                            compressFormat = Bitmap.CompressFormat.JPEG;
                        } else {
                            compressFormat = Bitmap.CompressFormat.PNG;
                        }
                        response.compress(compressFormat, 100, new FileOutputStream(new File(exerciseDir, imageName)));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }, 0, 0, ImageView.ScaleType.CENTER_CROP, Bitmap.Config.ARGB_8888,
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if (getExerciseImageFolder(context, exerciseId).listFiles().length != images.size()) {
                                context.showMessageWindow("Chyba", "Chyba při stahovnání potřebných obrázků ze serveru");
                            }
                        }
                    });
            MyRequestSingleton.getInstance(context).addToRequestQueue(imgRequest);
        }
    }

}
