package org.cvut.fit.dracek2.core.model.rest.modules;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Administrator on 9.2.2016.
 */
public class AllowedModulesResult {

    private String id;
    private String name;
    private String package_name_excercise;
    private String package_name_editor;
    @SerializedName("package")
    private String package_;
    private String description;
    private String image;
    @SerializedName("google_play_url")
    private String googlePlayUrl;

    public AllowedModulesResult(String id, String name, String packageNameExercise, String packageNameEditor, String description, String image, String googlePlayUrl) {
        this.id = id;
        this.name = name;
        this.package_name_excercise = packageNameExercise;
        this.package_name_editor = packageNameEditor;
        this.description = description;
        this.image = image;
        this.googlePlayUrl = googlePlayUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPackage_name_excercise() {
        return package_name_excercise;
    }

    public void setPackage_name_excercise(String package_name_excercise) {
        this.package_name_excercise = package_name_excercise;
    }

    public String getPackage_name_editor() {
        return package_name_editor;
    }

    public void setPackage_name_editor(String package_name_editor) {
        this.package_name_editor = package_name_editor;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getGooglePlayUrl() {
        return googlePlayUrl;
    }

    public void setGooglePlayUrl(String googlePlayUrl) {
        this.googlePlayUrl = googlePlayUrl;
    }

    public String getPackage() {
        return package_;
    }

    public void setPackage(String package_) {
        this.package_ = package_;
    }
}
