package org.cvut.fit.dracek2.core.offline;

import android.os.StrictMode;

import org.cvut.fit.dracek2.core.rest.RestApiGenerator;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Administrator on 9.2.2016.
 */
public class InternetConnectionChecker {

    public static boolean isOnline() {
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
            HttpURLConnection urlc = (HttpURLConnection) new URL(RestApiGenerator.REST_URL).openConnection();
            urlc.setRequestProperty("User-Agent", "Test");
            urlc.setRequestProperty("Connection", "close");
            urlc.setConnectTimeout(2000);
            urlc.connect();
        } catch (MalformedURLException e) {
            throw new RuntimeException("REST_URL of Dragon II server is not valid!", e);
        } catch (IOException e) {
            return false;
        }
        return true;
    }

}
