package org.cvut.fit.dracek2.core.rest.api;

import org.cvut.fit.dracek2.core.model.rest.modules.AllExercisesResult;
import org.cvut.fit.dracek2.core.model.rest.modules.AllowedModulesResult;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Administrator on 9.2.2016.
 */
public interface RestModuleApi {

    @GET("module_excercise")
    public Call<List<AllowedModulesResult>> getAllowedModules();

    @GET("excercise/all")
    public Call<List<AllExercisesResult>> getAllExercises();

}
