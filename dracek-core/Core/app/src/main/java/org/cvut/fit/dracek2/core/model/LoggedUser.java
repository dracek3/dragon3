package org.cvut.fit.dracek2.core.model;

import org.cvut.fit.dracek2.core.model.rest.logging.LogInResult;
import org.cvut.fit.dracek2.core.model.rest.logging.Token;
import org.cvut.fit.dracek2.core.model.rest.logging.User;

import java.util.Date;

/**
 * Created by Administrator on 4.1.2016.
 */
public class LoggedUser {

    private static User user;
    private static Token token;
    private static Date loggedTime;

    public static void init(LogInResult jsonResult) {
        LoggedUser.user = jsonResult.getUser();
        LoggedUser.token = jsonResult.getToken();
        LoggedUser.loggedTime = new Date();
    }

    public static String getFullName() {
        if (user == null) {
            return "User has not been set.";
        }
        return user.getName() + " " + user.getSurname();
    }

    public static String getToken() {
        if (token == null) {
            return null;
        }
        return token.getToken();
    }

    public static String getUserId() {
        return user.getId();
    }

    public static Date getLoggedTime() {
        return loggedTime;
    }

}
