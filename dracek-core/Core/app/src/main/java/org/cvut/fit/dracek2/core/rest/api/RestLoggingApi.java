package org.cvut.fit.dracek2.core.rest.api;

import org.cvut.fit.dracek2.core.model.rest.logging.LogInRequest;
import org.cvut.fit.dracek2.core.model.rest.logging.LogInResult;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;


/**
 * Created by Administrator on 9.2.2016.
 */
public interface RestLoggingApi {

    @POST("token")
    public Call<LogInResult> logIn(@Body LogInRequest body);

}
