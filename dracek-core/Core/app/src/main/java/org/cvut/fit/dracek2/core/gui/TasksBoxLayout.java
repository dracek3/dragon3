package org.cvut.fit.dracek2.core.gui;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import org.cvut.fit.dracek2.core.R;
import org.cvut.fit.dracek2.core.activity.TasksActivity;

/**
 * Created by Administrator on 4.1.2016.
 */
public class TasksBoxLayout {

    private ViewGroup layout;
    private TextView teachersName;

    public TasksBoxLayout(ViewGroup layout) {
        this.layout = layout;
        teachersName = (TextView) layout.findViewById(R.id.teachersNameText);
    }

    public void setTeachersName(String name) {
        teachersName.setText(name);
    }

    public TasksRowLayout addTaskRow() {
        return TasksRowLayout.create(getRowsLayout());
    }

    public ViewGroup getLayout() {
        return layout;
    }

    public ViewGroup getRowsLayout() {
        return (ViewGroup) layout.findViewById(R.id.tasks_rows);
    }


    public static TasksBoxLayout create(ViewGroup parent) {
        ScrollView inflatedViewRoot = (ScrollView) LayoutInflater.from(TasksActivity.getInstance()).inflate(R.layout.tasks_box_layout, parent);
        TasksBoxLayout duplicate = new TasksBoxLayout((ViewGroup) inflatedViewRoot.getChildAt(inflatedViewRoot.getChildCount() - 1));
        return duplicate;
    }
}
