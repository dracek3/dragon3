package org.cvut.fit.dracek2.core.exception;

/**
 * Created by Administrator on 30.12.2015.
 */
public class LoggingException extends Exception {
    public LoggingException(String s) {
        super(s);
    }
}
