package org.cvut.fit.dracek2.core.gui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.cvut.fit.dracek2.core.R;
import org.cvut.fit.dracek2.core.activity.TasksActivity;

/**
 * Created by Administrator on 3.1.2016.
 */
public class TasksRowLayout {

    private ViewGroup layout;
    private TextView taskName;
    private TextView percentageNeeded;
    private TextView dateDeadline;
    private Button startButton;

    public TasksRowLayout(ViewGroup layout) {
        this.layout = layout;
        this.taskName = (TextView) layout.findViewById(R.id.exercise);
        this.percentageNeeded = (TextView) layout.findViewById(R.id.percentageNeeded);
        this.dateDeadline = (TextView) layout.findViewById(R.id.dateDeadline);
        this.startButton = (Button) layout.findViewById(R.id.startButton);
    }

    public ViewGroup getLayout() {
        return layout;
    }

    public static TasksRowLayout create(ViewGroup parent) {
        LinearLayout inflatedViewRoot = (LinearLayout) LayoutInflater.from(TasksActivity.getInstance()).inflate(R.layout.tasks_row_layout, parent);
        TasksRowLayout duplicate = new TasksRowLayout((ViewGroup) inflatedViewRoot.getChildAt(inflatedViewRoot.getChildCount() - 1));
        return duplicate;
    }

    public void setTaskName(String taskName) {
        this.taskName.setText(taskName);
    }

    public void setDateDeadline(String dateDeadline) {
        this.dateDeadline.setText(dateDeadline);
    }

    public void setPercentageNeeded(String percentageNeeded) {
        this.percentageNeeded.setText(percentageNeeded);
    }

    public void setButtonListener(View.OnClickListener listener) {
        this.startButton.setOnClickListener(listener);
    }

    public void hideButton() {
        startButton.setVisibility(View.INVISIBLE);
    }
}
