package org.cvut.fit.dracek2.core.model.rest.tasks;

/**
 * Created by Administrator on 18.10.2016.
 */
public class UserCreator {

    private String name;
    private String surname;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}

