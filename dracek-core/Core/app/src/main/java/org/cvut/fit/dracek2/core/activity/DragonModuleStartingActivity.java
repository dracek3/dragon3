package org.cvut.fit.dracek2.core.activity;

import org.cvut.fit.dracek2.core.model.rest.results.NewResult;
import org.cvut.fit.dracek2.core.offline.OfflineResourceManager;

/**
 * Created by Administrator on 1.11.2016.
 */

public abstract class DragonModuleStartingActivity extends DragonActivity {

    public abstract void onResultPassed(NewResult result);


    public void onComunicationFailure(NewResult result) {
        OfflineResourceManager.getInstance().saveResultOffline(result, this);
        showMessageWindow("Výsledek uložen offline", "Dosáhli jste celkového score:" + result.getPercentage() + "\nVýsledek byl uložen lokalně a bude poslán na server při online synchronizaci.");
    }

    ;

}
