package org.cvut.fit.dracek2.core.activity;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.cvut.fit.dracek2.core.R;
import org.cvut.fit.dracek2.core.facades.ExercisesFacade;
import org.cvut.fit.dracek2.core.facades.ResultsFacade;
import org.cvut.fit.dracek2.core.gui.ResultsBoxLayout;
import org.cvut.fit.dracek2.core.gui.ResultsRowLayout;
import org.cvut.fit.dracek2.core.model.LoggedUser;
import org.cvut.fit.dracek2.core.model.rest.modules.AllExercisesResult;
import org.cvut.fit.dracek2.core.model.rest.results.AllResults;
import org.cvut.fit.dracek2.core.util.DateConverter;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ResultsActivity extends DragonActivity {

    private static ResultsActivity instance;

    private ImageButton infoBtn;
    private ImageButton backBtn;

    private LinearLayout boxes;

    private TextView nameLbl;

    public static ResultsActivity getInstance() {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        initCompoments();
        setListeners();
        ResultsFacade.init(this);
        instance = this;
        nameLbl.setText(LoggedUser.getFullName());
    }

    @Override
    protected void onStart() {
        super.onStart();
        boxes.removeAllViews();
        ResultsFacade.requestResults();
    }

    public void showResults(Map<String, List<AllResults>> results) {
        Set<String> modules = results.keySet();
        int currentResultCount = boxes.getChildCount();
        for (int i = 0; i < currentResultCount; i++) {
            boxes.removeViewAt(0);
        }

        // Sort the results by date
        for (String moduleName : modules) {
            List<AllResults> resultsList = results.get(moduleName);
            Collections.sort(resultsList, new Comparator<AllResults>() {
                @Override
                public int compare(AllResults first, AllResults second) {
                    return second.getCreatedAt().compareTo(first.getCreatedAt());
                }
            });
        }

        for (String moduleName : modules) {
            ResultsBoxLayout newResultBox = ResultsBoxLayout.create(boxes);
            newResultBox.setModuleName(moduleName);
            for (AllResults result : results.get(moduleName)) {
                AllExercisesResult exerciseInfo = ExercisesFacade.getExerciseInfo(Integer.parseInt(result.getIdExcercise()));
                ResultsRowLayout newRow = newResultBox.addResultRow();
                newRow.setExerciseName(exerciseInfo.getName());
                String offlineSaved = result.isOfflineSaved() ? "- OFFLINE" : "";
                newRow.setExerciseDate(DateConverter.fromUnixToDateString(result.getCreatedAt()) + offlineSaved);
                newRow.setExerciseResult(result.getPercentage() + "%");
            }
        }

    }

    private void initCompoments() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.hide();

        infoBtn = (ImageButton) findViewById(R.id.info_button_res);
        backBtn = (ImageButton) findViewById(R.id.back_button_res);
        boxes = (LinearLayout) findViewById(R.id.result_boxes);
        nameLbl = (TextView) findViewById(R.id.student_name_label_res);
    }

    private void setListeners() {
        infoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMessageWindow("Vaše výsledky", "Na této obrazovce vidíte své dosažené výsledky rozdělené podle modulů. Pro zobrazení dalších můžete obrazovku posunout směrem do prava.");
            }

        });
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResultsActivity.getInstance().finish();
            }
        });
    }


    @Override
    public void showMessageWindow(String title, String message) {
        new AlertDialog.Builder(ResultsActivity.getInstance())
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", null)
                .setCancelable(false)
                .show();
    }
}
