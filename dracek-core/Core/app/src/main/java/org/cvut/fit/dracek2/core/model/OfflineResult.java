package org.cvut.fit.dracek2.core.model;

import org.cvut.fit.dracek2.core.model.rest.results.NewResult;

/**
 * Created by Administrator on 4.12.2016.
 */

public class OfflineResult {

    private NewResult result;
    private String token;

    public OfflineResult(NewResult result, String token) {
        this.result = result;
        this.token = token;
    }

    public NewResult getResult() {
        return result;
    }

    public void setResult(NewResult result) {
        this.result = result;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
