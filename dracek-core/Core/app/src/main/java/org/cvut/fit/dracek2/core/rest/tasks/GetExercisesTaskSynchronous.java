package org.cvut.fit.dracek2.core.rest.tasks;

import org.cvut.fit.dracek2.core.exception.CoreFatalException;
import org.cvut.fit.dracek2.core.model.rest.modules.AllExercisesResult;
import org.cvut.fit.dracek2.core.offline.OfflineResourceManager;
import org.cvut.fit.dracek2.core.rest.RestApiGenerator;
import org.cvut.fit.dracek2.core.rest.api.RestModuleApi;

import java.util.List;

/**
 * Created by Administrator on 9.2.2016.
 */
public class GetExercisesTaskSynchronous {

    public void execute() {

        Runnable requestRunnable = new Runnable() {
            @Override
            public void run() {
                List<AllExercisesResult> exercises;
                try {
                    exercises = RestApiGenerator.createApi(RestModuleApi.class).getAllExercises().execute().body();
                } catch (Exception | Error e) {
                    OfflineResourceManager.getInstance().setExercises(null);
                    return;
                }
                OfflineResourceManager.getInstance().setExercises(exercises);
            }
        };

        Thread requestThread = new Thread(requestRunnable);
        requestThread.start();
        try {
            requestThread.join(20000);
        } catch (InterruptedException e) {
            throw new CoreFatalException("Unexpected interruption of waiting for synchronous GetModules request");
        }
    }

}
