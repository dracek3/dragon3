package org.cvut.fit.dracek2.core.exception;

/**
 * Created by Administrator on 30.12.2015.
 */
public class CoreFatalException extends RuntimeException {

    public CoreFatalException(String msg) {
        super(msg);
    }

    public CoreFatalException(String msg, Exception cause) {
        super(msg, cause);
    }
}
