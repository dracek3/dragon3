package org.cvut.fit.dracek2.core.rest.tasks;

import org.cvut.fit.dracek2.core.facades.LoggingFacade;
import org.cvut.fit.dracek2.core.model.rest.logging.LogInRequest;
import org.cvut.fit.dracek2.core.model.rest.logging.LogInResult;
import org.cvut.fit.dracek2.core.rest.RestApiGenerator;
import org.cvut.fit.dracek2.core.rest.api.RestLoggingApi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Administrator on 9.2.2016.
 */
public class LogInRestTask {

    private String name;
    private String password;

    public LogInRestTask(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public void execute() {
        LogInRequest request = new LogInRequest(name, password);
        Call<LogInResult> logInResultCall = RestApiGenerator.createApi(RestLoggingApi.class).logIn(request);
        logInResultCall.enqueue(new Callback<LogInResult>() {

            @Override
            public void onResponse(Response<LogInResult> response) {
                if (response.isSuccess()) {
                    LoggingFacade.loginSuccess(response.body());
                } else {
                    LoggingFacade.loginFailed();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                LoggingFacade.loginFailed();
            }

        });
    }

}
