package org.cvut.fit.dracek2.core.activity;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.cvut.fit.dracek2.core.R;
import org.cvut.fit.dracek2.core.facades.ExercisesFacade;
import org.cvut.fit.dracek2.core.facades.ModuleExercisesFacade;
import org.cvut.fit.dracek2.core.gui.ExerciseRowLayout;
import org.cvut.fit.dracek2.core.model.LoggedUser;
import org.cvut.fit.dracek2.core.model.rest.modules.AllExercisesResult;
import org.cvut.fit.dracek2.core.model.rest.results.NewResult;

import java.util.List;

public class ExercisesActivity extends DragonModuleStartingActivity {

    private static ExercisesActivity instance;

    private ImageButton infoBtn;
    private ImageButton backBtn;

    private LinearLayout table;

    private TextView nameLbl;
    private TextView moduleNameLbl;
    private int moduleId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercises);
        ExercisesFacade.init(this);
        initComponents();
        setListeners();
        instance = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        int currentExerciseNumber = table.getChildCount();
        for (int i = 0; i < currentExerciseNumber; i++) {
            table.removeViewAt(0);
        }
        List<AllExercisesResult> allExercises = ExercisesFacade.getAllExerciseInfoByModuleId(moduleId);
        for (final AllExercisesResult exercise : allExercises) {
            ExerciseRowLayout row = ExerciseRowLayout.create(table);
            row.setName(exercise.getName());
            String description = exercise.getDescription();
            row.setDescription(description);
            row.setListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent launchIntent = ExercisesFacade.getIntentForExercise(ExercisesActivity.getInstance(), moduleId, Integer.parseInt(exercise.getId()));
                    try {
                        startActivityForResult(launchIntent, 10);
                    } catch (ActivityNotFoundException e) {
                        showMessageWindow("Modul nenalezen", "Vámi spouštěný modul není nainstalován na zařízení. Vraťte se prosím do hlavního menu a kliknětě na tlačítko aktualizace.");
                    }
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            try {
                ExercisesFacade.sendOrSaveResult(moduleId, data.getStringExtra("RESULTS"), false, this);
            } catch (Exception e) {
                e.printStackTrace();
                showMessageWindow("Výsledek neuložen", "Výsledek je v nesprávném formátu a nebude uložen. Kontaktujte prosím učitele nebo podporu.");
            }
        } else {
            showMessageWindow("Výsledek neuložen", "Neočekávaný kód výsledku.");
        }
    }

    public static ExercisesActivity getInstance() {
        return instance;
    }

    private void initComponents() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.hide();

        infoBtn = (ImageButton) findViewById(R.id.info_button_ex);
        backBtn = (ImageButton) findViewById(R.id.back_button_ex);
        table = (LinearLayout) findViewById(R.id.exercises_table);
        nameLbl = (TextView) findViewById(R.id.student_name_label_ex);
        moduleId = getIntent().getIntExtra("module_id", 0);
        nameLbl.setText(LoggedUser.getFullName());
        moduleNameLbl = (TextView) findViewById(R.id.exercise_name_label);
        moduleNameLbl.setText(ModuleExercisesFacade.getModuleInfo(moduleId).getName());
    }


    private void setListeners() {
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExercisesActivity.getInstance().finish();
            }
        });
        infoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMessageWindow("Výběr cvičení", "Na této obrazovce si vyberte cvičení zvoleného modulu. Pro spuštění ťukněte na příslušný řádek.");
            }
        });
    }

    @Override
    public void showMessageWindow(String title, String message) {
        new AlertDialog.Builder(ExercisesActivity.getInstance())
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", null)
                .setCancelable(false)
                .show();
    }

    @Override
    public void onResultPassed(NewResult result) {
        showMessageWindow("Výsledek uložen", "Dosáhli jste celkového score: " + result.getPercentage() + "%");
    }
}
