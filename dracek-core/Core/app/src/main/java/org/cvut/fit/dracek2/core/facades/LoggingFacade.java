package org.cvut.fit.dracek2.core.facades;

import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.apache.commons.io.IOUtils;
import org.cvut.fit.dracek2.core.activity.LoggingActivity;
import org.cvut.fit.dracek2.core.activity.MainMenuActivity;
import org.cvut.fit.dracek2.core.exception.LoggingException;
import org.cvut.fit.dracek2.core.model.LoggedUser;
import org.cvut.fit.dracek2.core.model.rest.logging.LogInResult;
import org.cvut.fit.dracek2.core.offline.InternetConnectionChecker;
import org.cvut.fit.dracek2.core.rest.tasks.LogInRestTask;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;


/**
 * Created by Administrator on 30.12.2015.
 */
public class LoggingFacade {

    private static final String DIR_NAME = "credits";
    private static final String FILE_NAME = "login_info.json";
    private static LoggingActivity activity;

    private LoggingFacade() {
    }

    public static void init(LoggingActivity loggACtivity) {
        activity = loggACtivity;
    }


    public static void requestLogin(String nameStr, String passwordStr) throws LoggingException {
        if (nameStr == null || nameStr.equals("") || passwordStr == null || passwordStr.equals("")) {
            throw new LoggingException("Políčko se jménem ani s heslem nesmí být prázdné!");
        }
        if (!InternetConnectionChecker.isOnline()) {
            activity.showErrorWindow("Připojení k internetu není v tuto chvíli k dispozici.");
            return;
        }
        new LogInRestTask(nameStr, passwordStr).execute();
    }

    public static void loginSuccess(LogInResult result) {
        File prevTokenFile = new File(activity.getDir(DIR_NAME, Context.MODE_PRIVATE), FILE_NAME);
        prevTokenFile.getParentFile().mkdirs();
        if (prevTokenFile.exists()) {
            prevTokenFile.delete();
        }
        PrintWriter writer = null;
        String error = null;
        try (FileOutputStream fos = new FileOutputStream(prevTokenFile)) {
            IOUtils.write(new Gson().toJson(result), fos);
        } catch (Exception e) {
            // If this fails, something is wrong with the application storage space permissions. Nevertheless, logging in can proceed.
            error = "Dráčkovi se nepodařilo uložit údaje o vašem přihlášení, přo příští příhlášení bude opět " +
                    "třeba připojení k internetu a jméno s heslem. Důvodem může být například málo místa či nedostatečné" +
                    " oprávnění aplikace pro přístup k paměti tabletu.";
        }


        LoggedUser.init(result);
        Intent intent = new Intent(activity, MainMenuActivity.class);
        if (error != null) {
            intent.putExtra("error", error);
        }
        activity.startActivity(intent);
        activity.finish();
    }

    public static void loginFailed() {
        showError("Zadané jméno a heslo nesouhlasí.");
    }

    public static void showError(String error) {
        activity.showErrorWindow(error);
    }


    /*
     * This method will try to read previously saved login token and use it to login again.
     */
    public static void tryPreviousLogin() {
        File prevTokenFile = new File(activity.getDir(DIR_NAME, Context.MODE_PRIVATE), FILE_NAME);
        if (!prevTokenFile.exists()) {
            return;
        }
        try (FileInputStream fis = new FileInputStream(prevTokenFile)) {
            String content = IOUtils.toString(fis);
            if (content.equals("")) {
                return;
            }
            LogInResult logInResult = new Gson().fromJson(content, LogInResult.class);
            Date expirationUnix = new Date(Long.parseLong(logInResult.getToken().getExpiration()) * 1000);

            if (new Date().after(expirationUnix)) {
                return;
            }
            loginSuccess(logInResult);
        } catch (IOException | JsonSyntaxException e) {
            showError("Předchozí přihlašovací token se nepodařilo načíst, přihlašte se prosím znovu.");
        }
    }

    public static void deleteToken() {
        File tokenFile = new File(activity.getDir(DIR_NAME, Context.MODE_PRIVATE), FILE_NAME);
        if (tokenFile.exists()) {
            tokenFile.delete();
        }
    }
}
