package org.cvut.fit.dracek2.core.rest.tasks;

import org.cvut.fit.dracek2.core.activity.DragonModuleStartingActivity;
import org.cvut.fit.dracek2.core.model.rest.results.NewResult;
import org.cvut.fit.dracek2.core.rest.RestApiGenerator;
import org.cvut.fit.dracek2.core.rest.api.RestResultsApi;

import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by Administrator on 10.2.2016.
 */
public class SendResultTask {

    public void execute(final NewResult result, final DragonModuleStartingActivity caller) {
        RestApiGenerator.createApi(RestResultsApi.class).sendResult(result).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Response<Void> response) {
                caller.onResultPassed(result);
            }

            @Override
            public void onFailure(Throwable t) {
                caller.onComunicationFailure(result);
            }
        });
    }
}
