package org.cvut.fit.dracek2.core.rest.tasks;

import org.cvut.fit.dracek2.core.exception.CoreFatalException;
import org.cvut.fit.dracek2.core.model.rest.modules.AllowedModulesResult;
import org.cvut.fit.dracek2.core.offline.OfflineResourceManager;
import org.cvut.fit.dracek2.core.rest.RestApiGenerator;
import org.cvut.fit.dracek2.core.rest.api.RestModuleApi;

import java.util.List;

/**
 * Created by Administrator on 9.2.2016.
 */
public class GetModulesTaskSynchronous {

    public void execute() {

        Runnable requestRunnable = new Runnable() {
            @Override
            public void run() {
                List<AllowedModulesResult> modules;
                try {
                    modules = RestApiGenerator.createApi(RestModuleApi.class).getAllowedModules().execute().body();
                } catch (Exception | Error e) {
                    OfflineResourceManager.getInstance().setModules(null);
                    return;
                }
                OfflineResourceManager.getInstance().setModules(modules);

            }
        };

        Thread requestThread = new Thread(requestRunnable);
        requestThread.start();
        try {
            requestThread.join(20000);
        } catch (InterruptedException e) {
            throw new CoreFatalException("Unexpected interruption of waiting for synchronous GetModules request");
        }
    }
}
