package org.cvut.fit.dracek2.core.model;

/**
 * Created by Administrator on 31.12.2015.
 */
public class ExerciseModule {

    private String name;
    private int drawableImageId;

    ExerciseModule(String name, int drawableImageId) {
        this.name = name;
        this.drawableImageId = drawableImageId;
    }

}
