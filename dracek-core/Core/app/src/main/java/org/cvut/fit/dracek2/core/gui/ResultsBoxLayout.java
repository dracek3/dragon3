package org.cvut.fit.dracek2.core.gui;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.cvut.fit.dracek2.core.R;
import org.cvut.fit.dracek2.core.activity.ResultsActivity;

/**
 * Created by Administrator on 4.1.2016.
 */
public class ResultsBoxLayout {

    private ViewGroup layout;
    private TextView moduleName;

    private ResultsBoxLayout(ViewGroup layout) {
        this.layout = layout;
        moduleName = (TextView) layout.findViewById(R.id.res_module_name);
    }

    public void setModuleName(String name) {
        moduleName.setText(name);
    }

    public ResultsRowLayout addResultRow() {
        ResultsRowLayout row = ResultsRowLayout.create((ViewGroup) layout.findViewById(R.id.result_rows));
        return row;
    }

    public ViewGroup getLayout() {
        return layout;
    }

    public static ResultsBoxLayout create(ViewGroup parent) {
        LinearLayout inflatedViewRoot = (LinearLayout) LayoutInflater.from(ResultsActivity.getInstance()).inflate(R.layout.result_box_layout, parent);
        ResultsBoxLayout duplicate = new ResultsBoxLayout((ViewGroup) inflatedViewRoot.getChildAt(inflatedViewRoot.getChildCount() - 1));
        return duplicate;
    }
}
