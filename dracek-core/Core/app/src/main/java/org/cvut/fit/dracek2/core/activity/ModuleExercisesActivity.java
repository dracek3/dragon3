package org.cvut.fit.dracek2.core.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.cvut.fit.dracek2.core.R;
import org.cvut.fit.dracek2.core.facades.ModuleExercisesFacade;
import org.cvut.fit.dracek2.core.gui.ExerciseModuleLayout;
import org.cvut.fit.dracek2.core.model.LoggedUser;
import org.cvut.fit.dracek2.core.model.rest.modules.AllowedModulesResult;

import java.util.Map;
import java.util.Set;

public class ModuleExercisesActivity extends DragonActivity {

    private static ModuleExercisesActivity instance;

    private LinearLayout topRow;
    private LinearLayout bottRow;

    private ImageButton infoBtn;
    private ImageButton backBtn;

    private TextView nameLbl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_module_exercises);
        ModuleExercisesFacade.init(this);
        initComponents();
        setListeners();
        instance = this;
        nameLbl.setText(LoggedUser.getFullName());
    }

    @Override
    protected void onStart() {
        super.onStart();
        topRow.removeAllViews();
        bottRow.removeAllViews();
        ModuleExercisesFacade.requestAllowedModules();
    }

    private void initComponents() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.hide();

        nameLbl = (TextView) findViewById(R.id.student_name_label_modex);
        infoBtn = (ImageButton) findViewById(R.id.info_button_modex);
        backBtn = (ImageButton) findViewById(R.id.back_button_modex);
        topRow = (LinearLayout) findViewById(R.id.top_row_modex);
        bottRow = (LinearLayout) findViewById(R.id.bott_row_modex);
    }

    public static ModuleExercisesActivity getInstance() {
        return instance;
    }

    private void setListeners() {
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ModuleExercisesActivity.getInstance().finish();
            }
        });
        infoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMessageWindow("Výběr výukového modulu", "Na této obrazovce si můžete vybrat výukový modul, který byste rádi zkusili.");
            }
        });
    }

    public void showModules(Map<Integer, AllowedModulesResult> results) {
        Set<Integer> resultIds = results.keySet();
        for (final Integer id : resultIds) {
            ExerciseModuleLayout exerciseModuleLayout = ExerciseModuleLayout.create(getRowToBeFilled());
            exerciseModuleLayout.setModuleName(results.get(id).getName());
            ModuleExercisesFacade.downloadModuleImage(results.get(id), exerciseModuleLayout);
            exerciseModuleLayout.setImageListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ModuleExercisesActivity.getInstance(), ExercisesActivity.class);
                    intent.putExtra("module_id", id);
                    startActivity(intent);
                }
            });
        }
    }

    public LinearLayout getRowToBeFilled() {
        if (topRow.getChildCount() <= bottRow.getChildCount()) {
            return topRow;
        }
        return bottRow;
    }

    @Override
    public void showMessageWindow(String title, String message) {
        new AlertDialog.Builder(ModuleExercisesActivity.getInstance())
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", null)
                .setCancelable(false)
                .show();
    }
}
