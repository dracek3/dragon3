package org.cvut.fit.dracek2.core.rest.api;

import org.cvut.fit.dracek2.core.model.rest.tasks.AllTasks;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;


/**
 * Created by Administrator on 9.2.2016.
 */
public interface RestTasksApi {

    @GET("task")
    public Call<List<AllTasks>> getAllTasks();

}
