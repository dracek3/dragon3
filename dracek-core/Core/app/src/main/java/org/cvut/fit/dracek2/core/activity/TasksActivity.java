package org.cvut.fit.dracek2.core.activity;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;

import org.cvut.fit.dracek2.core.R;
import org.cvut.fit.dracek2.core.facades.ExercisesFacade;
import org.cvut.fit.dracek2.core.facades.ModuleExercisesFacade;
import org.cvut.fit.dracek2.core.facades.TasksFacade;
import org.cvut.fit.dracek2.core.gui.TasksBoxLayout;
import org.cvut.fit.dracek2.core.gui.TasksRowLayout;
import org.cvut.fit.dracek2.core.model.LoggedUser;
import org.cvut.fit.dracek2.core.model.rest.modules.AllowedModulesResult;
import org.cvut.fit.dracek2.core.model.rest.results.NewResult;
import org.cvut.fit.dracek2.core.model.rest.tasks.AllTasks;
import org.cvut.fit.dracek2.core.util.DateConverter;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TasksActivity extends DragonModuleStartingActivity {

    private static TasksActivity instance;

    private ImageButton infoBtn;
    private ImageButton backBtn;

    private ScrollView boxes;

    private TextView nameLbl;

    private int moduleId;
    private int percentageNeeded;

    public static TasksActivity getInstance() {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks);
        initCompoments();
        setListeners();
        TasksFacade.init(this);
        instance = this;
        nameLbl.setText(LoggedUser.getFullName());
    }

    @Override
    protected void onStart() {
        super.onStart();
        boxes.removeAllViews();
        TasksFacade.requestTasks();
    }

    private void initCompoments() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.hide();

        infoBtn = (ImageButton) findViewById(R.id.info_button_task);
        backBtn = (ImageButton) findViewById(R.id.back_button_task);
        boxes = (ScrollView) findViewById(R.id.taskScrollView);
        nameLbl = (TextView) findViewById(R.id.student_name_label_task);
    }

    private void setListeners() {
        infoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMessageWindow("Zadané úkoly", "Na této obrazovce vidíte své zadané úkoly, které Vám byly zadány seřazené podle zadavatele. ");
            }
        });
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TasksActivity.getInstance().finish();
            }

        });
    }

    public void showResults(Map<String, List<AllTasks>> results) {
        Set<String> teachers = results.keySet();
        int currentResultCount = boxes.getChildCount();
        for (int i = 0; i < currentResultCount; i++) {
            boxes.removeViewAt(0);
        }

        for (String teacher : teachers) {
            TasksBoxLayout newTaskBox = TasksBoxLayout.create(boxes);
            newTaskBox.setTeachersName(teacher);

            for (final AllTasks result : results.get(teacher)) {
                final int moduleId = Integer.parseInt(result.getModlueExcercise().getId());
                final int exerciseId = Integer.parseInt(result.getExcercise().getId());
                AllowedModulesResult moduleInfo = ModuleExercisesFacade.getModuleInfo(moduleId);
                if (moduleInfo == null) {
                    continue;
                }
                TasksRowLayout newRow = newTaskBox.addTaskRow();
                newRow.setTaskName(moduleInfo.getName());
                newRow.setDateDeadline(DateConverter.fromUnixToDateString(result.getEndingDate()));
                newRow.setPercentageNeeded(result.getMinimalScore() + "%");
                if (DateConverter.fromUnixToDate(result.getEndingDate()).before(new Date())) {
                    newRow.hideButton();
                } else {
                    newRow.setButtonListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            TasksActivity.getInstance().setModuleId(moduleId);
                            TasksActivity.getInstance().setPercentageNeeded(Integer.parseInt(result.getMinimalScore()));
                            Intent launchIntent = ExercisesFacade.getIntentForExercise(TasksActivity.getInstance(), moduleId, exerciseId);
                            try {
                                startActivityForResult(launchIntent, 10);
                            } catch (ActivityNotFoundException e) {
                                showMessageWindow("Modul nenalezen", "Vámi spouštěný modul není nainstalován na zařízení. Vraťte se prosím do hlavního menu a kliknětě na tlačítko aktualizace.");
                            }
                        }
                    });
                }
            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            try {
                ExercisesFacade.sendOrSaveResult(moduleId, data.getStringExtra("RESULTS"), true, this);
                TasksFacade.requestTasks();
            } catch (Exception e) {
                e.printStackTrace();
                showMessageWindow("Výsledek neuložen", "Výsledek je v nesprávném formátu a nebude uložen. Kontaktujte prosím učitele nebo podporu.");
            }
        } else {
            showMessageWindow("Výsledek neuložen", "Neočekávaný kód výsledku.");
        }
    }

    public void setModuleId(int moduleId) {
        this.moduleId = moduleId;
    }

    public void setPercentageNeeded(int percentageNeeded) {
        this.percentageNeeded = percentageNeeded;
    }

    @Override
    public void showMessageWindow(String title, String message) {
        new AlertDialog.Builder(TasksActivity.getInstance())
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", null)
                .setCancelable(false)
                .show();
    }

    @Override
    public void onResultPassed(NewResult result) {
        showMessageWindow("Výsledek uložen", "Dosáhli jste celkového score: " + result.getPercentage() + "% z potřebných " + percentageNeeded + "%");
    }
}
