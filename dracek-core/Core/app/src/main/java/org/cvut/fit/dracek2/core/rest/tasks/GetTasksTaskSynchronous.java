package org.cvut.fit.dracek2.core.rest.tasks;

import org.cvut.fit.dracek2.core.exception.CoreFatalException;
import org.cvut.fit.dracek2.core.model.rest.tasks.AllTasks;
import org.cvut.fit.dracek2.core.offline.OfflineResourceManager;
import org.cvut.fit.dracek2.core.rest.RestApiGenerator;
import org.cvut.fit.dracek2.core.rest.api.RestTasksApi;

import java.util.List;


/**
 * Created by Administrator on 10.2.2016.
 */
public class GetTasksTaskSynchronous {

    public void execute() {

        Runnable requestRunnable = new Runnable() {
            @Override
            public void run() {
                List<AllTasks> tasks;
                try {
                    tasks = RestApiGenerator.createApi(RestTasksApi.class).getAllTasks().execute().body();
                } catch (Exception | Error e) {
                    OfflineResourceManager.getInstance().setTasks(null);
                    return;
                }
                OfflineResourceManager.getInstance().setTasks(tasks);
            }
        };

        Thread requestThread = new Thread(requestRunnable);
        requestThread.start();
        try {
            requestThread.join(20000);
        } catch (InterruptedException e) {
            throw new CoreFatalException("Unexpected interruption of waiting for synchronous getAllResults request");
        }
    }

}
