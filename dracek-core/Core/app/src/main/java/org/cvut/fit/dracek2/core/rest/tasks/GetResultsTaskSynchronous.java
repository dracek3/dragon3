package org.cvut.fit.dracek2.core.rest.tasks;

import org.cvut.fit.dracek2.core.exception.CoreFatalException;
import org.cvut.fit.dracek2.core.model.rest.results.AllResults;
import org.cvut.fit.dracek2.core.offline.OfflineResourceManager;
import org.cvut.fit.dracek2.core.rest.RestApiGenerator;
import org.cvut.fit.dracek2.core.rest.api.RestResultsApi;

import java.util.List;


/**
 * Created by Administrator on 10.2.2016.
 */
public class GetResultsTaskSynchronous {


    public void execute() {

        Runnable requestRunnable = new Runnable() {
            @Override
            public void run() {
                List<AllResults> results;
                try {
                    results = RestApiGenerator.createApi(RestResultsApi.class).getAllResults().execute().body();
                } catch (Exception | Error e) {
                    OfflineResourceManager.getInstance().setResults(null);
                    return;
                }
                OfflineResourceManager.getInstance().setResults(results);
            }
        };

        Thread requestThread = new Thread(requestRunnable);
        requestThread.start();
        try {
            requestThread.join(20000);
        } catch (InterruptedException e) {
            throw new CoreFatalException("Unexpected interruption of waiting for synchronous getAllResults request");
        }
    }

}
