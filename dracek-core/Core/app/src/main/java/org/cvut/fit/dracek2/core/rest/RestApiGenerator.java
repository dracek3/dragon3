package org.cvut.fit.dracek2.core.rest;

import org.cvut.fit.dracek2.core.model.LoggedUser;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;

/**
 * Created by Administrator on 9.2.2016.
 */
public abstract class RestApiGenerator {

    public static String BASE_URL = "http://185.88.73.97/dracek-nette/";
    public static String REST_URL = BASE_URL + "www/api/v1/";

    private RestApiGenerator() {
    }

    public static <S> S createApi(Class<S> serviceInterface, final String token) {
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(REST_URL)
                .addConverterFactory(GsonConverterFactory.create());

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient httpClient = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request.Builder reqBuilder = chain.request().newBuilder();
                reqBuilder.addHeader("API-API-KEY", "abc");
                if (LoggedUser.getToken() != null && !LoggedUser.getToken().equals("")) {
                    reqBuilder.addHeader("API-USER-TOKEN", token);
                }
                return chain.proceed(reqBuilder.build());
            }
        }).addInterceptor(loggingInterceptor).build();

        builder.client(httpClient);
        return builder.build().create(serviceInterface);
    }


    public static <S> S createApi(Class<S> serviceInterface) {
        return createApi(serviceInterface, LoggedUser.getToken());
    }

}
