package org.cvut.fit.dracek2.core.model.rest.logging;

/**
 * Created by Administrator on 9.2.2016.
 */
public class LogInResult {

    private Token token;
    private User user;

    public LogInResult(Token token, User user) {
        this.token = token;
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }
}
