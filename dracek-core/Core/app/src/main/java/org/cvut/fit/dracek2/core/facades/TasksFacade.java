package org.cvut.fit.dracek2.core.facades;

import org.cvut.fit.dracek2.core.activity.TasksActivity;
import org.cvut.fit.dracek2.core.model.rest.tasks.AllTasks;
import org.cvut.fit.dracek2.core.offline.OfflineResourceManager;
import org.cvut.fit.dracek2.core.rest.tasks.GetTasksTaskSynchronous;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Administrator on 9.2.2016.
 */
public class TasksFacade {

    private static TasksActivity activity;

    public static void init(TasksActivity resultsActivity) {
        activity = resultsActivity;
    }


    public static void requestTasks() {
        new GetTasksTaskSynchronous().execute();
        showResults(OfflineResourceManager.getInstance().getTasks());
    }

    public static void showResults(List<AllTasks> tasks) {
        Map<String, List<AllTasks>> tmpResults = new ConcurrentHashMap<>();
        for (AllTasks task : tasks) {
            if (task.getScore() != null && task.getScore().getIdScore() != null) {
                // Task has already been finished
                continue;
            }
            String teacherName = task.getUserCreator().getSurname() + " " + task.getUserCreator().getName();
            List<AllTasks> teacherList = tmpResults.get(teacherName);
            if (teacherList == null) {
                teacherList = new ArrayList<>();
            }

            teacherList.add(task);
            tmpResults.put(teacherName, teacherList);
        }

        activity.showResults(tmpResults);
    }


    public static void requestFailed() {
    }
}
