package org.cvut.fit.dracek2.core.gui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.cvut.fit.dracek2.core.R;
import org.cvut.fit.dracek2.core.activity.ExercisesActivity;

/**
 * Created by Administrator on 3.1.2016.
 */
public class ExerciseRowLayout {

    private ViewGroup layout;
    private TextView name;
    private TextView description;

    public ExerciseRowLayout(ViewGroup layout) {
        this.layout = layout;
        this.name = (TextView) layout.findViewById(R.id.ex_row_name);
        this.description = (TextView) layout.findViewById(R.id.ex_row_description);
    }

    public void setListener(View.OnClickListener listener) {
        layout.setOnClickListener(listener);
    }

    public ViewGroup getLayout() {
        return layout;
    }

    public void setName(String name) {
        if (name == null) {
            name = "";
        }
        this.name.setText(name);
    }

    public void setDescription(String description) {
        if (description == null) {
            description = "";
        }
        this.description.setText(description);
    }

    public static ExerciseRowLayout create(ViewGroup parent) {
        LinearLayout inflatedView = (LinearLayout) LayoutInflater.from(ExercisesActivity.getInstance()).inflate(R.layout.exercise_row_layout, parent);
        ExerciseRowLayout duplicate = new ExerciseRowLayout((ViewGroup) inflatedView.getChildAt(inflatedView.getChildCount() - 1));
        return duplicate;
    }
}
