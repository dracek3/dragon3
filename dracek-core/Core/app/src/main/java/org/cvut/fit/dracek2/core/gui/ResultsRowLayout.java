package org.cvut.fit.dracek2.core.gui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.cvut.fit.dracek2.core.R;
import org.cvut.fit.dracek2.core.activity.ResultsActivity;

/**
 * Created by Administrator on 3.1.2016.
 */
public class ResultsRowLayout {

    private ViewGroup layout;
    private TextView exercise;
    private TextView date;
    private TextView result;

    private ResultsRowLayout(ViewGroup layout) {
        this.layout = layout;
        exercise = (TextView) layout.findViewById(R.id.res_row_exercise);
        date = (TextView) layout.findViewById(R.id.res_row_date);
        result = (TextView) layout.findViewById(R.id.res_row_result);
    }

    public View getLayout() {
        return layout;
    }

    public void setExerciseName(String name) {
        exercise.setText(name);
    }

    public void setExerciseDate(String date) {
        this.date.setText(date);
    }

    public void setExerciseResult(String result) {
        this.result.setText(result);
    }

    public static ResultsRowLayout create(ViewGroup parent) {
        LinearLayout inflatedViewRoot = (LinearLayout) LayoutInflater.from(ResultsActivity.getInstance()).inflate(R.layout.result_row_layout, parent);
        ResultsRowLayout duplicate = new ResultsRowLayout((ViewGroup) inflatedViewRoot.getChildAt(inflatedViewRoot.getChildCount() - 1));
        return duplicate;
    }
}
