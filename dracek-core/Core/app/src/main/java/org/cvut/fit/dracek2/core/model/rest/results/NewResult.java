package org.cvut.fit.dracek2.core.model.rest.results;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Administrator on 25.4.2016.
 */
public class NewResult {

    private String moduleExcerciseId;
    private String excerciseId;
    private String createdAt;
    private String percentage;
    private String totalTime;
    private String file;
    private String finished;
    @SerializedName("finishedAsTask")
    private boolean doneAsTask;

    public AllResults asAllResults(int id) {
        AllResults allResults = new AllResults(new Integer(id).toString(), moduleExcerciseId, excerciseId, percentage.replace("%", ""), totalTime, file, finished, new Boolean(doneAsTask).toString(), createdAt);
        allResults.setOfflineSaved(true);
        return allResults;
    }

    public String getModuleExcerciseId() {
        return moduleExcerciseId;
    }

    public void setModuleExcerciseId(String moduleExcerciseId) {
        this.moduleExcerciseId = moduleExcerciseId;
    }

    public String getExcerciseId() {
        return excerciseId;
    }

    public void setExcerciseId(String excerciseId) {
        this.excerciseId = excerciseId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public String getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(String totalTime) {
        this.totalTime = totalTime;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getFinished() {
        return finished;
    }

    public void setFinished(String finished) {
        this.finished = finished;
    }

    public boolean getDoneAsTask() {
        return doneAsTask;
    }

    public void setDoneAsTask(boolean doneAsTask) {
        this.doneAsTask = doneAsTask;
    }
}
