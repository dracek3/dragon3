package org.cvut.fit.dracek2.core.util;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Environment;

import org.cvut.fit.dracek2.core.activity.DragonActivity;
import org.cvut.fit.dracek2.core.model.rest.modules.AllowedModulesResult;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Administrator on 1.11.2016.
 */

public class ModuleInstaller {

    public static void installModules(DragonActivity context, List<AllowedModulesResult> modules) {
        Set<String> installedPackageNames = new HashSet<>();
        List<AllowedModulesResult> notInstalledModules = new ArrayList<>();
        List<AllowedModulesResult> installedModules = new ArrayList<>();

        for (PackageInfo info : context.getPackageManager().getInstalledPackages(0)) {
            installedPackageNames.add(info.packageName);
        }
        for (AllowedModulesResult module : modules) {
            if (!installedPackageNames.contains(module.getPackage())) {
                notInstalledModules.add(module);
            } else {
                installedModules.add(module);
            }
        }

        for (AllowedModulesResult installedModule : installedModules) {
            File download = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            download = new File(download, "dragon/" + installedModule.getId() + ".apk");
            if (download.exists()) {
//                download.delete();
            }
        }

        for (AllowedModulesResult moduleToInstall : notInstalledModules) {
            File download = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            download = new File(download, "dragon/" + moduleToInstall.getId() + ".apk");
            if (!download.exists()) {
                context.showMessageWindow("Modul nenalezen", "Instalační balíček " + moduleToInstall.getId() + ".apk " + " nebyl nalezen na vyžadovaném umístění.");
                return;
            }
            Intent promptInstall = new Intent(Intent.ACTION_VIEW)
                    .setDataAndType(Uri.fromFile(download), "application/vnd.android.package-archive");
            context.startActivity(promptInstall);
        }
    }

}
