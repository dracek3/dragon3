package org.cvut.fit.dracek2.core.offline;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.apache.commons.io.IOUtils;
import org.cvut.fit.dracek2.core.activity.MainMenuActivity;
import org.cvut.fit.dracek2.core.model.OfflineResult;
import org.cvut.fit.dracek2.core.rest.tasks.SendResultForCheckerTask;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Administrator on 4.12.2016.
 * This class manages an offine result checker, which periodically checks the offline result folder and tries to save content to the server,
 * if internet connection is available.
 */

public class OfflineResultsChecker {

    private static Thread checkedThread;
    private static int waitTimeInSec = 30;

    public static final ReentrantLock offlineResultsLock = new ReentrantLock();

    public static void start() {
        if (checkedThread != null) {
            return;
        }

        checkedThread = new Thread(new CheckOfflineResultsRunnable(waitTimeInSec, true));
        checkedThread.setDaemon(true);
        checkedThread.start();
    }

    public static void checkNow() {
        checkedThread = new Thread(new CheckOfflineResultsRunnable(0, false));
        checkedThread.start();
    }

    private static class CheckOfflineResultsRunnable implements Runnable {

        private int waitTimeInSec;
        private boolean repeated;

        public CheckOfflineResultsRunnable(int waitTimeInSec, boolean repeated) {
            this.waitTimeInSec = waitTimeInSec;
            this.repeated = repeated;
        }

        @Override
        public void run() {
            while (true) {
                if (InternetConnectionChecker.isOnline()) {
                    offlineResultsLock.lock();
                    File baseDirectory = MainMenuActivity.getInstance().getDir(OfflineResourceManager.BASE_DIR, Context.MODE_PRIVATE);
                    for (File userDir : baseDirectory.listFiles()) {
                        if (userDir.isDirectory()) {
                            File offlineResults = new File(userDir, OfflineResourceManager.RESULTS_OFFLINE_DIR);
                            if (!offlineResults.exists()) {
                                continue;
                            }
                            for (File offlineResultFile : offlineResults.listFiles()) {
                                try (FileInputStream fis = new FileInputStream(offlineResultFile)) {
                                    String content = IOUtils.toString(fis);
                                    OfflineResult loadedOfflineResult = new Gson().fromJson(content, OfflineResult.class);
                                    new SendResultForCheckerTask().execute(loadedOfflineResult.getResult(), loadedOfflineResult.getToken(), offlineResultFile);
                                } catch (IOException | JsonSyntaxException e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                    }
                    offlineResultsLock.unlock();
                }
                if (!repeated) {
                    break;
                }
                try {
                    Thread.sleep(waitTimeInSec * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }

    }

}
