package org.cvut.fit.dracek2.core.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 4.10.2016.
 */

public class DateConverter {

    public static String fromUnixToDateString(String unix) {
        if (unix.length() == 10) {
            unix += "000";
        }
        Date time = new Date(Long.parseLong(unix));
        return getDateFormat().format(time);
    }

    public static Date fromUnixToDate(String unix) {
        if (unix.length() == 10) {
            unix += "000";
        }
        Date time = new Date(Long.parseLong(unix));
        return time;
    }

    public static SimpleDateFormat getDateFormat() {
        return new SimpleDateFormat("dd. MM. yyyy");
    }
}
