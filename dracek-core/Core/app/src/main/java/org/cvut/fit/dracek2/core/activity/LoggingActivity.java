package org.cvut.fit.dracek2.core.activity;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.cvut.fit.dracek2.core.R;
import org.cvut.fit.dracek2.core.exception.LoggingException;
import org.cvut.fit.dracek2.core.facades.LoggingFacade;

public class LoggingActivity extends AppCompatActivity {

    private static LoggingActivity instance;

    private Button loginButton;
    private EditText name = null;
    private EditText password = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logging);
        initComponents();
        LoggingFacade.init(this);
        instance = this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        LoggingFacade.tryPreviousLogin();
    }

    private void initComponents() {
        loginButton = (Button) findViewById(R.id.login_button);
        name = (EditText) findViewById(R.id.name_textfield);
        password = (EditText) findViewById(R.id.password_textfield);

        loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String nameStr = name.getText().toString();
                String passwordStr = password.getText().toString();
                try {
                    LoggingFacade.requestLogin(nameStr, passwordStr);
                } catch (LoggingException e) {
                    showErrorWindow(e.getMessage());
                }
            }
        });

    }

    public void showErrorWindow(String message) {
        new AlertDialog.Builder(LoggingActivity.getInstance())
                .setTitle("Chybné přihášení!")
                .setMessage(message)
                .setPositiveButton("Ok", null)
                .show();
    }

    public static LoggingActivity getInstance() {
        return instance;
    }
}
