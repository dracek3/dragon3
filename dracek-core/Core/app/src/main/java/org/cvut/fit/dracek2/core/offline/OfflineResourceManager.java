package org.cvut.fit.dracek2.core.offline;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.apache.commons.io.IOUtils;
import org.cvut.fit.dracek2.core.activity.DragonActivity;
import org.cvut.fit.dracek2.core.activity.DragonModuleStartingActivity;
import org.cvut.fit.dracek2.core.facades.MainMenuFacade;
import org.cvut.fit.dracek2.core.model.LoggedUser;
import org.cvut.fit.dracek2.core.model.OfflineResult;
import org.cvut.fit.dracek2.core.model.rest.modules.AllExercisesResult;
import org.cvut.fit.dracek2.core.model.rest.modules.AllowedModulesResult;
import org.cvut.fit.dracek2.core.model.rest.results.AllResults;
import org.cvut.fit.dracek2.core.model.rest.results.NewResult;
import org.cvut.fit.dracek2.core.model.rest.tasks.AllTasks;
import org.cvut.fit.dracek2.core.rest.tasks.GetExercisesTaskSynchronous;
import org.cvut.fit.dracek2.core.rest.tasks.GetModulesTaskSynchronous;
import org.cvut.fit.dracek2.core.rest.tasks.GetResultsTaskSynchronous;
import org.cvut.fit.dracek2.core.rest.tasks.GetTasksTaskSynchronous;
import org.cvut.fit.dracek2.core.util.ModuleInstaller;
import org.cvut.fit.dracek2.core.util.ResourceDownloader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 1.11.2016.
 * This class serves as a cache of every server-sided information. It refreshes and saves it to an application storage space
 * after every online request.
 */

public class OfflineResourceManager {

    public static final String BASE_DIR = "users_data";
    public static final String EXERCISES_DIR = "exercises";
    public static final String MODULES_DIR = "modules";
    public static final String RESULTS_DIR = "results";
    public static final String RESULTS_OFFLINE_DIR = "results/offline";
    public static final String TASKS_DIR = "tasks";

    private static OfflineResourceManager instance;
    private String userId;

    private Map<Integer, AllExercisesResult> exercises = new HashMap<>();
    private Map<Integer, AllResults> results = new HashMap<>();
    private Map<Integer, AllTasks> tasks = new HashMap<>();
    private Map<Integer, AllowedModulesResult> modules = new HashMap<>();

    private OfflineResourceManager() {
        userId = LoggedUser.getUserId();
    }

    public static OfflineResourceManager getInstance() {
        if (instance == null) {
            instance = new OfflineResourceManager();
        }
        return instance;
    }

    public void getOrSendAllInfo(final DragonActivity caller) {
        caller.runOnUiThread(new Thread() {

            @Override
            public void run() {
                caller.showWaitWindow("Čekejte prosím na stažení potřebných materiálů a instalci modulů.");
                new GetExercisesTaskSynchronous().execute();
                new GetModulesTaskSynchronous().execute();
                new GetResultsTaskSynchronous().execute();
                new GetTasksTaskSynchronous().execute();
                for (AllowedModulesResult module : getModules()) {
                    new ResourceDownloader().downloadModuleImage(caller, module);
                }
                OfflineResultsChecker.checkNow();
                ModuleInstaller.installModules(caller, getModules());
                caller.dismissWaitWindow();
                if (InternetConnectionChecker.isOnline()) {
                    caller.showMessageWindow("Aktualizováno", "Dráček II byl úspěšně aktualizován se serverem.");
                }
            }
        });
    }


    public File getUserDirectory(Context context) {
        File userDirectory = new File(context.getDir(BASE_DIR, Context.MODE_PRIVATE), userId);
        userDirectory.getParentFile().mkdirs();
        return userDirectory;
    }

    public void setExercises(List<AllExercisesResult> exercises) {
        if (exercises == null) {
            this.exercises = null;
            return;
        }
        this.exercises = new HashMap<>();

        for (AllExercisesResult result : exercises) {
            this.exercises.put(Integer.parseInt(result.getId()), result);
        }

        File exercisesDir = new File(getUserDirectory(MainMenuFacade.getActivity()), EXERCISES_DIR);
        if (!exercisesDir.exists()) {
            exercisesDir.mkdirs();
        }
        try (FileOutputStream fos = new FileOutputStream(new File(exercisesDir, "file.json"))) {
            IOUtils.write(new Gson().toJson(this.exercises.values()), fos);
        } catch (Exception e) {
            e.printStackTrace();
            // write not successful, data not saved for offline usage
            // not fatal
        }

        for (AllExercisesResult exercise : exercises) {
            getExerciseImages(exercise);
        }
    }

    public void setResults(List<AllResults> results) {
        if (results == null) {
            this.results = null;
            return;
        }
        results.addAll(getOfflineResults());
        this.results = new HashMap<>();
        for (AllResults result : results) {
            this.results.put(Integer.parseInt(result.getId()), result);
        }

        File resultsDir = new File(getUserDirectory(MainMenuFacade.getActivity()), RESULTS_DIR);
        if (!resultsDir.exists()) {
            resultsDir.mkdirs();
        }
        try (FileOutputStream fos = new FileOutputStream(new File(resultsDir, "file.json"))) {
            IOUtils.write(new Gson().toJson(this.results.values()), fos);
        } catch (Exception e) {
            e.printStackTrace();
            // write not successful, data not saved for offline usage
            // not fatal
        }
    }

    private List<AllResults> getOfflineResults() {
        List<AllResults> offlineResults = new ArrayList<>();
        OfflineResultsChecker.offlineResultsLock.lock();

        File offlineResultsDir = new File(getUserDirectory(MainMenuFacade.getActivity()), RESULTS_OFFLINE_DIR);
        if (offlineResultsDir.exists()) {
            int offlineResultId = -1;
            for (File offlineResult : offlineResultsDir.listFiles()) {
                try (FileInputStream fis = new FileInputStream(offlineResult)) {
                    String content = IOUtils.toString(fis);
                    OfflineResult loadedOfflineResult = new Gson().fromJson(content, OfflineResult.class);
                    offlineResults.add(loadedOfflineResult.getResult().asAllResults(offlineResultId));
                    offlineResultId--;
                } catch (IOException | JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }
        }

        OfflineResultsChecker.offlineResultsLock.unlock();
        return offlineResults;
    }

    public void setModules(List<AllowedModulesResult> modules) {
        if (modules == null) {
            this.modules = null;
            return;
        }
        this.modules = new HashMap<>();

        for (AllowedModulesResult result : modules) {
            this.modules.put(Integer.parseInt(result.getId()), result);
        }

        File modulesDir = new File(getUserDirectory(MainMenuFacade.getActivity()), MODULES_DIR);
        if (!modulesDir.exists()) {
            modulesDir.mkdirs();
        }
        try (FileOutputStream fos = new FileOutputStream(new File(modulesDir, "file.json"))) {
            IOUtils.write(new Gson().toJson(this.modules.values()), fos);
        } catch (Exception e) {
            e.printStackTrace();
            // write not successful, data not saved for offline usage
            // not fatal
        }
    }

    public void setTasks(List<AllTasks> tasks) {
        if (tasks == null) {
            this.tasks = null;
            return;
        }
        this.tasks = new HashMap<>();

        for (AllTasks result : tasks) {
            this.tasks.put(Integer.parseInt(result.getId()), result);
        }

        File tasksDir = new File(getUserDirectory(MainMenuFacade.getActivity()), TASKS_DIR);
        if (!tasksDir.exists()) {
            tasksDir.mkdirs();
        }
        try (FileOutputStream fos = new FileOutputStream(new File(tasksDir, "file.json"))) {
            IOUtils.write(new Gson().toJson(this.tasks.values()), fos);
        } catch (Exception e) {
            e.printStackTrace();
            // write not successful, data not saved for offline usage
            // not fatal
        }
    }

    public List<AllExercisesResult> getExercises() {
        if (exercises == null) {
            File resultsDir = new File(getUserDirectory(MainMenuFacade.getActivity()), EXERCISES_DIR + "/file.json");
            try (FileInputStream fis = new FileInputStream(resultsDir)) {
                String content = IOUtils.toString(fis);
                if (content.equals("")) {
                    return new ArrayList<>();
                }
                AllExercisesResult[] offlineResults = new Gson().fromJson(content, AllExercisesResult[].class);
                setExercises(Arrays.asList(offlineResults));
            } catch (IOException | JsonSyntaxException e) {
                return new ArrayList<>();
            }
        }
        return new ArrayList<>(exercises.values());
    }

    private void getExerciseImages(AllExercisesResult exercise) {
        new ResourceDownloader().downloadImageForExercise(MainMenuFacade.getActivity(), exercise);
    }

    public AllExercisesResult getExercise(Integer id) {
        if (exercises == null) {
            getExercises();
        }
        if (exercises == null) {
            return null;
        } else {
            return exercises.get(id);
        }
    }

    public List<AllowedModulesResult> getModules() {
        if (modules == null) {
            File resultsDir = new File(getUserDirectory(MainMenuFacade.getActivity()), MODULES_DIR + "/file.json");
            try (FileInputStream fis = new FileInputStream(resultsDir)) {
                String content = IOUtils.toString(fis);
                if (content.equals("")) {
                    return new ArrayList<>();
                }
                AllowedModulesResult[] offlineResults = new Gson().fromJson(content, AllowedModulesResult[].class);
                setModules(Arrays.asList(offlineResults));
            } catch (IOException | JsonSyntaxException e) {
                return new ArrayList<>();
            }
        }
        return new ArrayList<>(modules.values());
    }

    public AllowedModulesResult getModule(Integer id) {
        if (modules == null) {
            getModules();
        }
        if (modules == null) {
            return null;
        } else {
            return modules.get(id);
        }
    }

    public List<AllResults> getResults() {
        if (results == null) {
            File resultsDir = new File(getUserDirectory(MainMenuFacade.getActivity()), RESULTS_DIR + "/file.json");
            try (FileInputStream fis = new FileInputStream(resultsDir)) {
                String content = IOUtils.toString(fis);
                if (content.equals("")) {
                    return new ArrayList<>();
                }
                AllResults[] offlineResults = new Gson().fromJson(content, AllResults[].class);
                setResults(new ArrayList<>(Arrays.asList(offlineResults)));
            } catch (IOException | JsonSyntaxException e) {
                return new ArrayList<>();
            }
        }
        return new ArrayList<>(results.values());

    }

    public List<AllTasks> getTasks() {
        if (tasks == null) {
            File resultsDir = new File(getUserDirectory(MainMenuFacade.getActivity()), TASKS_DIR + "/file.json");
            try (FileInputStream fis = new FileInputStream(resultsDir)) {
                String content = IOUtils.toString(fis);
                if (content.equals("")) {
                    return new ArrayList<>();
                }
                AllTasks[] offlineResults = new Gson().fromJson(content, AllTasks[].class);
                setTasks(Arrays.asList(offlineResults));
            } catch (IOException | JsonSyntaxException e) {
                return new ArrayList<>();
            }
        }
        return new ArrayList<>(tasks.values());
    }

    public AllTasks getTask(Integer id) {
        if (tasks == null) {
            getModules();
        }
        if (tasks == null) {
            return null;
        } else {
            return tasks.get(id);
        }
    }

    public void saveResultOffline(NewResult result, DragonModuleStartingActivity caller) {
        OfflineResultsChecker.offlineResultsLock.lock();
        SimpleDateFormat sdf = new SimpleDateFormat("HHmmssSS");
        File resultFile = new File(getUserDirectory(MainMenuFacade.getActivity()), RESULTS_OFFLINE_DIR + "/" + sdf.format(new Date()));
        if (!resultFile.getParentFile().exists()) {
            resultFile.getParentFile().mkdirs();
        }
        try (FileOutputStream fos = new FileOutputStream(resultFile)) {
            IOUtils.write(new Gson().toJson(new OfflineResult(result, LoggedUser.getToken())), fos);
        } catch (Exception e) {
            e.printStackTrace();
            // write not successful, data not saved for offline usage
            caller.showMessageWindow("Chyba při ukládání offline", "Výsledek se nepodařilo offline uložit. Kontaktujte prosím učitele či správce.");
        } finally {
            OfflineResultsChecker.offlineResultsLock.unlock();
        }
    }

}
