package com.example.rybajaro.pozadi1.DataBusiness;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by maxik on 6/17/16.
 */
public class Exercise implements Serializable {
    private ArrayList<Task> tasks;

    // TODO: implementation
    public Exercise() {
    }

    public ArrayList<Task> getTasks() { return tasks; }

    public Task getTask(int index) {return tasks.get(index);}

    public void setTasks(ArrayList<Task> tasks) {
        this.tasks = tasks;
    }
}
