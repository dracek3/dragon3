package com.example.rybajaro.pozadi1.Presentation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.rybajaro.pozadi1.R;

public class EditorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);
    }

    public void editExcerciseButtonOnClick(View v){
        startActivity(new Intent(EditorActivity.this, EditorExcerciseActivity.class));

    }

    public void addImageOnClick(View v){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Vybrat obrázek"), 5);
        //TODO

    }
}
