package com.example.rybajaro.pozadi1.DataBusiness;

/**
 * Created by maxik on 6/17/16.
 */
public class TaskEvaluator {
    private static TaskEvaluator ourInstance = new TaskEvaluator();

    private int currentScore;
    private Exercise exercise;

    public static TaskEvaluator getInstance() {
        return ourInstance;
    }

    // TODO: implementation
    private TaskEvaluator() {
    }

    // TODO: implementation
    public int evaluateAnswer(int answer, Task task)
    {
        return 0;
    }


    public int getCurrentScore() {
        return currentScore;
    }

    public Exercise getExercise() {
        return exercise;
    }

    public void setExercise(Exercise exercise) {
        this.exercise = exercise;
    }

    // TODO: implementation
    public double getSuccessRate() {
        return 0;
    }

    // TODO: implementation
    public void reportSuccessRate() {

    }

    // TODO: implementation
    public void resetScore() {

    }
}
