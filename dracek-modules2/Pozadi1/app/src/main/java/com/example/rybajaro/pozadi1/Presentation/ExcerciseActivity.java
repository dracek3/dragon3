package com.example.rybajaro.pozadi1.Presentation;

import android.graphics.Bitmap;
import android.graphics.Picture;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.example.rybajaro.pozadi1.DataBusiness.*;

import com.example.rybajaro.pozadi1.R;

import java.util.ArrayList;

public class ExcerciseActivity extends AppCompatActivity {
    Exercise e;
    int currentTaskIndex = 0;
    Task currentTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        e = getIntent().getParcelableExtra("excercise");
        currentTask = e.getTask(currentTaskIndex);
        setContentView(R.layout.activity_excercise);
        showCurrentTask();
    }

    public void answerButtonOnClick(View v){
        //find out answer number
        int answer = 0;
        switch (v.getId()){
            case R.id.answerImageButton1:
                answer = 1;
                break;
            case R.id.answerImageButton2:
                answer = 2;
                break;
            case R.id.answerImageButton3:
                answer = 3;
                break;
            case R.id.answerImageButton4:
                answer = 4;
                break;
            case R.id.answerImageButton5:
                answer = 5;
                break;
        }

        //do something with it


    }

    private void showCurrentTask(){
        Image topImage = currentTask.getTopImage();
        ArrayList<Image> bottomImages = currentTask.getBottomImages();

        ((ImageButton) findViewById(R.id.mainImageButton)).setImageDrawable(topImage.getImageData());

        ((ImageButton) findViewById(R.id.answerImageButton1)).setImageDrawable(bottomImages.get(0).getImageData());
        ((ImageButton) findViewById(R.id.answerImageButton3)).setImageDrawable(bottomImages.get(2).getImageData());
        ((ImageButton) findViewById(R.id.answerImageButton4)).setImageDrawable(bottomImages.get(3).getImageData());
        ((ImageButton) findViewById(R.id.answerImageButton2)).setImageDrawable(bottomImages.get(1).getImageData());
        ((ImageButton) findViewById(R.id.answerImageButton5)).setImageDrawable(bottomImages.get(4).getImageData());
    }

}
