package com.example.rybajaro.pozadi1.Presentation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import com.example.rybajaro.pozadi1.DataBusiness.*;

import com.example.rybajaro.pozadi1.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void editorButtonOnClick(View v){
       // Button b = (Button) v;
       // b.setText("hi");
        startActivity(new Intent(MainActivity.this, EditorActivity.class));
    }

    public void excerciseButtonOnClick(View v){
       // Button b = (Button) v;
       // b.setText("hi");
        ExerciseDAO ed = ExerciseDAO.getInstance();
        Exercise e = ed.getRandomExcercise();
        Intent i = new Intent(MainActivity.this, ExcerciseActivity.class);
        i.putExtra("excercise", e);
        startActivity(i);
    }
}
