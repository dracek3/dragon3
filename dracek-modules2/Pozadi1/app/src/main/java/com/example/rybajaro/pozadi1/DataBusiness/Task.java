package com.example.rybajaro.pozadi1.DataBusiness;

import java.util.ArrayList;

/**
 * Created by maxik on 6/17/16.
 */
public class Task {
    private ArrayList<Image> bottomImages;
    private int correctChoice;
    private Image topImage;

    // TODO: implementation
    public Task() {
    }

    public ArrayList<Image> getBottomImages() {
        return bottomImages;
    }

    public void setBottomImages(ArrayList<Image> bottomImages) {
        this.bottomImages = bottomImages;
    }

    public int getCorrectChoice() {
        return correctChoice;
    }

    public void setCorrectChoice(int correctChoice) {
        this.correctChoice = correctChoice;
    }

    public Image getTopImage() {
        return topImage;
    }

    public void setTopImage(Image topImage) {
        this.topImage = topImage;
    }
}
