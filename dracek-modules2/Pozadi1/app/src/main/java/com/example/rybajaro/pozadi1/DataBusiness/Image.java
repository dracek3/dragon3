package com.example.rybajaro.pozadi1.DataBusiness;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.PictureDrawable;

/**
 * Created by maxik on 6/17/16.
 */
public class Image {

    private PictureDrawable imageData;

    public Image() {
    }

    public Drawable getImageData() {
        return imageData;
    }

    // TODO: implementation
    public void setImageData(String imageFile) {
        imageData = (PictureDrawable) PictureDrawable.createFromPath(imageFile);
    }

    public void setImageData(PictureDrawable imageData) {
        this.imageData = imageData;
    }
}
